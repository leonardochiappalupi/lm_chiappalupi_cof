import game.board.Council;
import game.cards.PoliticsColor;
import game.pawns.Councilor;
import org.junit.Assert;
import org.junit.Test;

public class TestInsertCouncilor {
    private Council council = new Council();
    @Test
    public void testInsertCouncilor(){
        setCouncil();
        testFullCouncil();
        testEmptyCouncil();
        testPartiallyEmptyCouncil();
    }

    private void testFullCouncil(){
        council.insertCouncilor(new Councilor(PoliticsColor.JOLLY));
        council.insertCouncilor(new Councilor(PoliticsColor.BLUE));
        Assert.assertEquals("Number of councilors should be 4",4,council.getCouncilors().size());
        Assert.assertEquals("First element should be a BLUE councilor",PoliticsColor.BLUE,council.getCouncilors().get(0).getColor());
        Assert.assertEquals("Second element should be a JOLLY councilor",PoliticsColor.JOLLY,council.getCouncilors().get(1).getColor());
        Assert.assertEquals("Third element should be a BLACK councilor",PoliticsColor.BLACK,council.getCouncilors().get(2).getColor());
        Assert.assertEquals("Fourth element should be a BLACK councilor",PoliticsColor.BLACK,council.getCouncilors().get(3).getColor());
    }

    private void testPartiallyEmptyCouncil(){
        council = new Council();
        council.addCouncilor(new Councilor(PoliticsColor.BLACK));
        council.addCouncilor(new Councilor(PoliticsColor.JOLLY));
        Assert.assertEquals("First element should be a JOLLY councilor",PoliticsColor.JOLLY,council.getCouncilors().get(0).getColor());
        Assert.assertEquals("Second element should be a BLACK councilor",PoliticsColor.BLACK,council.getCouncilors().get(1).getColor());
        Assert.assertEquals("Array dimension should be 2",2,council.getCouncilors().size());
    }

    private void testEmptyCouncil(){
        council = new Council();
        Assert.assertTrue("Council should be empty",council.getCouncilors().isEmpty());
    }

    private void setCouncil(){
        for (int i = 0; i < 4; i++)
            council.addCouncilor(new Councilor(PoliticsColor.BLACK));
    }
}
