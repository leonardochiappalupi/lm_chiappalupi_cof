import game.GameState;
import game.cards.Card;
import game.cards.PoliticsCard;
import org.junit.Assert;
import org.junit.Test;


public class TestGameStatePoliticsCardsCreation {
    private GameState gameState = new GameState();
    @Test
    public void testGameState(){
        gameState.setDefaultPoliticsCardsDeck(3);
        Assert.assertEquals("Number of politics cards should be 483",483,gameState.getPoliticsCardsDeck().getCards().size());
        for (Card card : gameState.getPoliticsCardsDeck().getCards()){
            Assert.assertEquals("Every card in the PoliticsCardDeck should be a PoliticsCard",PoliticsCard.class,card.getClass());
        }
    }
}
