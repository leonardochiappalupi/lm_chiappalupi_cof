package configurator;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import util.JSONElementNotFoundException;
import util.JSONHelper;

public class TestConfigurationParserExceptions {
    @Test
    public void testException(){
        String test = "test";
        try{
            JSONHelper.ConfigurationParser.configurationFromJSonConfig(test);
        }catch (JSONException e){
            Assert.assertEquals("Caught exception should be a JSONException",JSONException.class,e.getClass());
        }catch (JSONElementNotFoundException e){
            Assert.assertEquals("Caught exception should be a JSONElementNotFoundException",JSONElementNotFoundException.class,e.getClass());
        }
    }
}
