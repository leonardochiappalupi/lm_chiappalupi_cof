package configurator;

import game.Configuration;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.bonus.IndirectBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import util.JSONHelper;

import static constants.ConfigurationConstants.NOBILITY_ROUTE_SIZE;
import static constants.JsonConstants.ConfigurationConst.*;

public class TestToJsonConfigurationParser {
    private JSONObject jsonConfiguration;
    private Configuration configuration = new Configuration();

    @Test
    public void testToJsonConfigurationParser() {
        setupTestConfiguration();

        testToJsonMap();
        testToJsonMapField();
        testToJsonNobilityRoute();
        testToJsonNobilityRouteCell();
    }

    private void testToJsonMap() {
        Assert.assertTrue("There must be a valid map", jsonConfiguration.has(NOBILITY_ROUTE_JSON_TAG));
    }

    private void testToJsonMapField() {
        JSONArray mapFields = jsonConfiguration
                .getJSONObject(MAP_JSON_TAG)
                .getJSONArray(MAPFIELD_JSON_TAG);

        for (int i = 0; i < mapFields.length(); i++) {
            String correctZone = Field.values()[i].getDescription().toUpperCase();
            Assert.assertEquals(correctZone + " field should have " + correctZone + " as zone", correctZone, mapFields
                    .getJSONObject(i)
                    .getString(FIELD_TYPE_JSON_TAG));

            Assert.assertEquals("Number of towns should be 1", 1, mapFields
                    .getJSONObject(i)
                    .getInt(NUMBER_OF_TOWNS_PER_FIELD_TAG));

            Assert.assertTrue("There should be a valid town list", mapFields
                    .getJSONObject(i)
                    .has(TOWNS_JSON_TAG));

            testToJsonDeck(mapFields.getJSONObject(i));

            JSONArray towns = mapFields
                    .getJSONObject(i)
                    .getJSONArray(TOWNS_JSON_TAG);
            JSONObject deck = mapFields
                    .getJSONObject(i)
                    .getJSONObject(DECK_JSON_TAG);

            /**
             * I test the towns configured in the test configuration created; there is only one town per field: in sector 0 for Seaside field, in sector 3 for Mainland field and in sector 1 for
             * Mountains field.
             **/
            testToJsonTown(towns.getJSONObject(0), i);

            Assert.assertTrue("There should be a valid list of building permission cards", deck.has(PERMISSION_CARDS_JSON_TAG));

            testToJsonDeckCard(deck
                    .getJSONArray(PERMISSION_CARDS_JSON_TAG)
                    .getJSONObject(0), i);
        }
    }

    private void testToJsonTown(JSONObject town, int index) {
        switch (index) {
            case 0:
                Assert.assertEquals("Town name should be seasideTown0", "seasideTown0", town.getString(TOWN_NAME_JSON_TAG));
                Assert.assertEquals("Town sector should be 0", 0, town.getInt(TOWN_SECTOR_JSON_TAG));
                Assert.assertEquals("Town color should be BLUE", TownColor.BLUE.getDescription().toUpperCase(), town.getString(TOWN_COLOR_JSON_TAG));
                Assert.assertFalse("hasKing should be false for this town", town.getBoolean(HAS_KING_JSON_TAG));
                Assert.assertEquals("This bonus should be of type CARDS", BonusType.CARDS.getDescription().toUpperCase(), town.getJSONArray(BONUSES_JSON_TAG).getJSONObject(0).getString(BONUS_TYPE_JSON_TAG));
                Assert.assertEquals("This bonus should have a quantity of 7", 7, town.getJSONArray(BONUSES_JSON_TAG).getJSONObject(0).getInt(QUANTITY_OF_BONUSES_JSON_TAG));
                Assert.assertEquals("Town shoul be connected to Anchor 0", 0, town.getJSONArray(ANCHOR_CONNECTIONS_JSON_TAG).getInt(0));
                break;
            case 1:
                Assert.assertEquals("Town name should be mainlandTown3", "mainlandTown3", town.getString(TOWN_NAME_JSON_TAG));
                Assert.assertEquals("Town sector should be 3", 3, town.getInt(TOWN_SECTOR_JSON_TAG));
                Assert.assertEquals("Town color should be KING", TownColor.KING.getDescription().toUpperCase(), town.getString(TOWN_COLOR_JSON_TAG));
                Assert.assertTrue("hasKing should be true for this town", town.getBoolean(HAS_KING_JSON_TAG));
                Assert.assertEquals("This bonus should be of type NOBSTEPS", BonusType.NOBSTEPS.getDescription().toUpperCase(), town.getJSONArray(BONUSES_JSON_TAG).getJSONObject(0).getString(BONUS_TYPE_JSON_TAG));
                Assert.assertEquals("This bonus should have a quantity of 11", 11, town.getJSONArray(BONUSES_JSON_TAG).getJSONObject(0).getInt(QUANTITY_OF_BONUSES_JSON_TAG));
                Assert.assertEquals("Town shoul be connected to Anchor 3", 3, town.getJSONArray(ANCHOR_CONNECTIONS_JSON_TAG).getInt(0));
                break;
            case 2:
                Assert.assertEquals("Town name should be mountainTown1", "mountainTown1", town.getString(TOWN_NAME_JSON_TAG));
                Assert.assertEquals("Town sector should be 1", 1, town.getInt(TOWN_SECTOR_JSON_TAG));
                Assert.assertEquals("Town color should be ORANGE", TownColor.ORANGE.getDescription().toUpperCase(), town.getString(TOWN_COLOR_JSON_TAG));
                Assert.assertFalse("hasKing should be false for this town", town.getBoolean(HAS_KING_JSON_TAG));
                Assert.assertEquals("This bonus should be of type COINS", BonusType.COINS.getDescription().toUpperCase(), town.getJSONArray(BONUSES_JSON_TAG).getJSONObject(0).getString(BONUS_TYPE_JSON_TAG));
                Assert.assertEquals("This bonus should have a quantity of 3", 3, town.getJSONArray(BONUSES_JSON_TAG).getJSONObject(0).getInt(QUANTITY_OF_BONUSES_JSON_TAG));
                Assert.assertEquals("Town shoul be connected to Anchor 5", 5, town.getJSONArray(ANCHOR_CONNECTIONS_JSON_TAG).getInt(0));
                break;
        }
    }

    private void testToJsonDeck(JSONObject mapField) {
        Assert.assertTrue("There should be a valid deck", mapField.has(DECK_JSON_TAG));
    }

    /**
     * I test the only permission card per field (always in position 0)
     */
    private void testToJsonDeckCard(JSONObject card, int index) {
        switch (index) {
            case 0:
                Assert.assertEquals("This card should have 1 bonus", 1, card.getInt(NUMBER_OF_BONUSES_JSON_TAG));

                Assert.assertEquals("This card must allow to build on town: SeasideBPCTest", "SeasideBPCTest", card
                        .getJSONArray(PERMITTED_TOWNS_JSON_TAG)
                        .getString(0));
                break;
            case 1:
                Assert.assertEquals("This card should have 1 bonus", 1, card.getInt(NUMBER_OF_BONUSES_JSON_TAG));

                Assert.assertEquals("This card must allow to build on town: MainlandBPCTest", "MainlandBPCTest", card
                        .getJSONArray(PERMITTED_TOWNS_JSON_TAG)
                        .getString(0));
                break;
            case 2:
                Assert.assertEquals("This card should have 1 bonus", 1, card.getInt(NUMBER_OF_BONUSES_JSON_TAG));

                Assert.assertEquals("This card must allow to build on town: MountainBPCTest", "MountainBPCTest", card
                        .getJSONArray(PERMITTED_TOWNS_JSON_TAG)
                        .getString(0));
                break;
        }

        testToJsonBonus(card
                .getJSONArray(BONUSES_JSON_TAG)
                .getJSONObject(0), index);
    }

    private void testToJsonBonus(JSONObject bonus, int index) {
        Assert.assertTrue(Field.values()[index].getDescription().toUpperCase() + " card should have a direct bonus", bonus.getBoolean(IS_DIRECT_JSON_TAG));

        switch (index) {
            case 0:
                Assert.assertEquals(Field.values()[index].getDescription().toUpperCase() + " card should have a bonus of type HELPERS",
                        BonusType.HELPERS.getDescription().toUpperCase(),
                        bonus.getString(BONUS_TYPE_JSON_TAG));

                Assert.assertEquals(Field.values()[index].getDescription().toUpperCase() + " card should have a bonus with a quantity of 4", 4, bonus.getInt(QUANTITY_OF_BONUSES_JSON_TAG));
                break;
            case 1:
                Assert.assertEquals(Field.values()[index].getDescription().toUpperCase() + " card should have a bonus of type CARDS",
                        BonusType.CARDS.getDescription().toUpperCase(),
                        bonus.getString(BONUS_TYPE_JSON_TAG));

                Assert.assertEquals(Field.values()[index].getDescription().toUpperCase() + " card should have a bonus with a quantity of 2", 2, bonus.getInt(QUANTITY_OF_BONUSES_JSON_TAG));
                break;
            case 2:
                Assert.assertEquals(Field.values()[index].getDescription().toUpperCase() + " card should have a bonus of type NOBSTEPS",
                        BonusType.NOBSTEPS.getDescription().toUpperCase(),
                        bonus.getString(BONUS_TYPE_JSON_TAG));

                Assert.assertEquals(Field.values()[index].getDescription().toUpperCase() + " card should have a bonus with a quantity of 1", 1, bonus.getInt(QUANTITY_OF_BONUSES_JSON_TAG));
                break;
        }
    }

    private void testToJsonNobilityRoute() {
        Assert.assertTrue("There must be a valid nobility route", jsonConfiguration.has(NOBILITY_ROUTE_JSON_TAG));
    }

    private void testToJsonNobilityRouteCell() {
        Assert.assertEquals("In cell 0 there should be a direct bonus of type HELPERS",
                BonusType.HELPERS.getDescription().toUpperCase(),
                jsonConfiguration
                        .getJSONObject(NOBILITY_ROUTE_JSON_TAG)
                        .getJSONArray(NOBILITY_ROUTE_CELLS_JSON_TAG)
                        .getJSONObject(0)
                        .getJSONArray(BONUSES_JSON_TAG)
                        .getJSONObject(0)
                        .getString(BONUS_TYPE_JSON_TAG));
    }

    private void setupTestConfiguration() {
        setupMap();
        setupSeasideField();
        setupMainlandField();
        setuopMountainsField();
        setupNobilityRoute();

        jsonConfiguration = new JSONObject(JSONHelper.ConfigurationParser.toJsonConfiguration(configuration));
    }

    /**
     * Setting up map
     */
    private void setupMap() {
        configuration.setMap(new Map(new MapField(6), new MapField(6), new MapField(6)));
    }

    /**
     * Setting up seaside field ({@link Town}s and {@link BuildingPermissionCard}s deck)
     */
    private void setupSeasideField() {
        MapField seaField = configuration.getMap().getSeaField();
        seaField.setFieldType(Field.SEASIDE);
        seaField.replaceTownInSector(0, new Town("seasideTown0"));
        seaField.getTownInSector(0).setSector(0);
        seaField.getTownInSector(0).setColor(TownColor.BLUE);
        seaField.getTownInSector(0).setHasKing(false);
        seaField.getTownInSector(0).addBonus(new DirectBonus(BonusType.CARDS, 7));
        seaField.getTownInSector(0).addConnectedAnchor(new Anchor(0));

        seaField.setBuildingPermissionCardsDeck(new Deck());
        BuildingPermissionCard seasideBuildingPermissionCard = new BuildingPermissionCard();
        seasideBuildingPermissionCard.addBonus(new DirectBonus(BonusType.HELPERS, 4));
        seasideBuildingPermissionCard.addAllowedTown(new Town("SeasideBPCTest"));
        seaField.getBuildingPermissionCardsDeck().addCard(seasideBuildingPermissionCard);
    }

    /**
     * Setting up mainland field ({@link Town}s and {@link BuildingPermissionCard}s deck)
     */
    private void setupMainlandField() {
        MapField mainlandField = configuration.getMap().getMainlandField();
        mainlandField.setFieldType(Field.MAINLAND);
        mainlandField.replaceTownInSector(3, new Town("mainlandTown3"));
        mainlandField.getTownInSector(3).setSector(3);
        mainlandField.getTownInSector(3).setColor(TownColor.KING);
        mainlandField.getTownInSector(3).setHasKing(true);
        mainlandField.getTownInSector(3).addBonus(new DirectBonus(BonusType.NOBSTEPS, 11));
        mainlandField.getTownInSector(3).addConnectedAnchor(new Anchor(3));

        mainlandField.setBuildingPermissionCardsDeck(new Deck());
        BuildingPermissionCard mainlandBuildingPermissionCard = new BuildingPermissionCard();
        mainlandBuildingPermissionCard.addBonus(new DirectBonus(BonusType.CARDS, 2));
        mainlandBuildingPermissionCard.addAllowedTown(new Town("MainlandBPCTest"));
        mainlandField.getBuildingPermissionCardsDeck().addCard(mainlandBuildingPermissionCard);
    }

    /**
     * Setting up mountains field ({@link Town}s and {@link BuildingPermissionCard}s deck)
     */
    private void setuopMountainsField() {
        MapField mountainField = configuration.getMap().getMountainField();
        mountainField.setFieldType(Field.MOUNTAINS);
        mountainField.replaceTownInSector(1, new Town("mountainTown1"));
        mountainField.getTownInSector(1).setSector(1);
        mountainField.getTownInSector(1).setColor(TownColor.ORANGE);
        mountainField.getTownInSector(1).setHasKing(false);
        mountainField.getTownInSector(1).addBonus(new DirectBonus(BonusType.COINS, 3));
        mountainField.getTownInSector(1).addConnectedAnchor(new Anchor(5));

        mountainField.setBuildingPermissionCardsDeck(new Deck());
        BuildingPermissionCard mountainsBuildingPermissionCard = new BuildingPermissionCard();
        mountainsBuildingPermissionCard.addBonus(new DirectBonus(BonusType.NOBSTEPS, 1));
        mountainsBuildingPermissionCard.addAllowedTown(new Town("MountainBPCTest"));
        mountainField.getBuildingPermissionCardsDeck().addCard(mountainsBuildingPermissionCard);
    }

    /**
     * Setting up {@link NobilityRoute} and its cell 0
     */
    private void setupNobilityRoute() {
        configuration.setNobilityRoute(new NobilityRoute(NOBILITY_ROUTE_SIZE));

        configuration.getNobilityRoute().getCellAtIndex(0).setIndex(0);
        configuration.getNobilityRoute().getCellAtIndex(0).addBonus(new DirectBonus(BonusType.HELPERS, 7));
        configuration.getNobilityRoute().getCellAtIndex(0).addBonus(new IndirectBonus(BonusType.PERMISSIONCARDBONUSES));
    }
}
