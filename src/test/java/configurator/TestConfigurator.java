package configurator;

import game.Configuration;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Card;
import game.cards.Deck;
import org.junit.Assert;
import org.junit.Test;
import util.JSONElementNotFoundException;
import util.JSONHelper;

import java.util.List;

public class TestConfigurator {
    private String jsonTestConfiguration;
    private Configuration configurationTest;

    @Test
    public void testConfigurationParser() {
        setTestJsonConfiguration();
        try {
            configurationTest = JSONHelper.ConfigurationParser.configurationFromJSonConfig(jsonTestConfiguration);
        } catch (JSONElementNotFoundException e) {
            e.printStackTrace();
        }
        testMap(configurationTest);
        testNobilityRoute(configurationTest);
    }

    private void testMap(Configuration configuration) {
        testSeaside(configuration.getMap().getSeaField());
        testMainland(configuration.getMap().getMainlandField());
        testMountains(configuration.getMap().getMountainField());
    }

    private void testSeaside(MapField seaField) {
        Assert.assertEquals("Type of seaField should be SEASIDE", Field.SEASIDE, seaField.getFieldType());
        testSeasideTowns(seaField.getAllTowns(false));
        testSeasideDeck(seaField.getBuildingPermissionCardsDeck());
    }

    private void testSeasideTowns(List<Town> towns) {
        Town town = towns.get(0);
        Assert.assertEquals("Town name should be seasideTest", "seasideTest", town.getName());
        Assert.assertEquals("Town color should be BLUE", TownColor.BLUE, town.getColor());
        Assert.assertEquals("Town sector should be 0", 0, town.getSector().intValue());
        Assert.assertTrue("seasideTest should not have the king on it", !town.getHasKing());
        Assert.assertTrue("seasideTest should not have connections with towns", town.getConnectedTowns().isEmpty());
        testSeasideTestBonuses(town.getBonusList().get(0));
        testSeasideTestAnchorsConnections(town.getConnectedAnchors().get(0));
    }

    private void testSeasideTestBonuses(DirectBonus bonus) {
        Assert.assertEquals("Quantity of seasideTest bonuses should be 1", 1, bonus.getElementsQuantity());
        Assert.assertEquals("Bonus type should be HELPERS", BonusType.HELPERS, bonus.getElementsType());
    }

    private void testSeasideTestAnchorsConnections(Anchor anchor) {
        Assert.assertEquals("seasideTest should be connected to Anchor 0", 0, anchor.getLevel());
    }

    private void testSeasideDeck(Deck deck) {

        /*for (Card card:deck.getCards()){
            BuildingPermissionCard bbb = (BuildingPermissionCard) card;
            System.out.println(bbb.getBonusList().get(0).getElementsQuantity() + bbb.getBonusList().get(0).getElementsType().toString() + bbb.getAllowedTowns().get(0).getName());
        }*/
        for(Card card:deck.getCards()){
            BuildingPermissionCard bcard = (BuildingPermissionCard)card;
            Assert.assertTrue("Number of towns permitted on every card should be 1",bcard.getAllowedTowns().size() == 1);
            //System.out.println(bcard.getAllowedTowns().get(0).getName());
            //System.out.println(((BuildingPermissionCard)configurationTest.getMap().getMainlandField().getBuildingPermissionCardsDeck().getCardAtIndex(0)).getAllowedTowns().get(0).getName());
            Assert.assertTrue("Every card in seaside Deck should have building permission on seasideTest",bcard.getAllowedTowns().get(0).getName().equals("seasideTest"));
            Assert.assertEquals("Every bonus in the card in seaside Deck should have quantity set to 1",1,bcard.getBonusList().get(0).getElementsQuantity());
            Assert.assertEquals("Every bonus in the card in seaside Deck should be HELPERS type",BonusType.HELPERS,bcard.getBonusList().get(0).getElementsType());
        }

    }

    private void testMainland(MapField mainlandField) {
        Assert.assertEquals("Type of seaField should be MAINLAND", Field.MAINLAND, mainlandField.getFieldType());
        Assert.assertEquals("Number of cards in Mainland deck should be 1", 1, mainlandField.getBuildingPermissionCardsDeck().getCards().size());
        testMainlandTowns(mainlandField.getAllTowns(false));
        testMainlandDeck(mainlandField.getBuildingPermissionCardsDeck());
    }

    private void testMainlandTowns(List<Town> towns) {
        Town town = towns.get(0);
        Assert.assertEquals("Town name should be mainlandTest", "mainlandTest", town.getName());
        Assert.assertEquals("Town color should be ORANGE", TownColor.ORANGE, town.getColor());
        Assert.assertEquals("Town sector should be 0", 0, town.getSector().intValue());
        Assert.assertTrue("mainlandTest should not have the king on it", !town.getHasKing());
        Assert.assertTrue("mainlandTest should not have connections with towns", town.getConnectedTowns().isEmpty());
        Assert.assertEquals("Number of anchors connected to mainlandTest should be 2", 2, town.getConnectedAnchors().size());
        testMainlandTestBonuses(town.getBonusList().get(0));
        testMainlandTestAnchorsConnections(town.getConnectedAnchors());
    }

    private void testMainlandTestBonuses(DirectBonus bonus) {
        Assert.assertEquals("Quantity of mainlandTest bonuses should be 1", 1, bonus.getElementsQuantity());
        Assert.assertEquals("Bonus type should be HELPERS", BonusType.HELPERS, bonus.getElementsType());
    }

    private void testMainlandTestAnchorsConnections(List<Anchor> anchors) {
        Assert.assertEquals("mainlandTest should be connected to Anchor 0", 0, anchors.get(0).getLevel());
        Assert.assertEquals("mainlandTest should be connected also to Anchor 3", 3, anchors.get(1).getLevel());
    }

    private void testMainlandDeck(Deck deck) {
        for(Card card:deck.getCards()){
            BuildingPermissionCard bcard = (BuildingPermissionCard)card;
            Assert.assertTrue("Number of towns permitted on every card should be 1",bcard.getAllowedTowns().size() == 1);
            Assert.assertTrue("Every card in seaside Deck should have building permission on mainlandTest",bcard.getAllowedTowns().get(0).getName().equals("mainlandTest"));
            Assert.assertEquals("Every bonus in the card in seaside Deck should have quantity set to 1",1,bcard.getBonusList().get(0).getElementsQuantity());
            Assert.assertEquals("Every bonus in the card in seaside Deck should be HELPERS type",BonusType.HELPERS,bcard.getBonusList().get(0).getElementsType());
        }
    }

    private void testMountains(MapField mountainField) {
        Assert.assertEquals("Type of seaField should be MOUNTAINS", Field.MOUNTAINS, mountainField.getFieldType());
        testMountainsTowns(mountainField.getAllTowns(false));
        testMountainsDeck(mountainField.getBuildingPermissionCardsDeck());
    }

    private void testMountainsTowns(List<Town> towns) {
        Town town = towns.get(0);
        Assert.assertEquals("Town name should be testMountains", "testMountains", town.getName());
        Assert.assertEquals("Town color should be KING", TownColor.KING, town.getColor());
        Assert.assertEquals("Town sector should be 0", 0, town.getSector().intValue());
        Assert.assertTrue("testMountains should have the king on it", town.getHasKing());
        Assert.assertTrue("testMountains should not have connections with towns", town.getConnectedTowns().isEmpty());
        Assert.assertTrue("testMountains bonuses list should be empty", town.getBonusList().isEmpty());
        testMountainsAnchorConnections(town.getConnectedAnchors().get(0));
    }

    private void testMountainsAnchorConnections(Anchor anchor) {
        Assert.assertEquals("testMountains should be connected to Anchor 3", 3, anchor.getLevel());
    }

    private void testMountainsDeck(Deck deck) {
        for(Card card:deck.getCards()){
            BuildingPermissionCard bcard = (BuildingPermissionCard)card;
            Assert.assertTrue("Number of towns permitted on every card should be 1",bcard.getAllowedTowns().size() == 1);
            Assert.assertTrue("Every card in seaside Deck should have building permission on testMountains",bcard.getAllowedTowns().get(0).getName().equals("testMountains"));
            Assert.assertEquals("Every bonus in the card in seaside Deck should have quantity set to 1",1,bcard.getBonusList().get(0).getElementsQuantity());
            Assert.assertEquals("Every bonus in the card in seaside Deck should be HELPERS type",BonusType.HELPERS,bcard.getBonusList().get(0).getElementsType());
        }
    }

    private void testNobilityRoute(Configuration configuration) {
        DirectBonus bonus = (DirectBonus) configuration.getNobilityRoute().getBonusesOfCell(0).get(0);
        Assert.assertEquals("Bonus type of the bonus in Nobility Route should be HELPERS",BonusType.HELPERS,bonus.getElementsType());
        Assert.assertEquals("Bonus quantity of the bonus in Nobility Route should be 1",1,bonus.getElementsQuantity());
        }

    private void setTestJsonConfiguration() {
        jsonTestConfiguration = "{\"nobilityRoute\":{\"cells\":[{\"cellIndex\":\"0\",\"numberOfBonuses\":\"1\",\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]},{\"cellIndex\":\"1\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"2\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"3\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"4\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"5\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"6\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"7\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"8\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"9\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"10\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"11\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"12\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"13\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"14\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"15\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"16\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"17\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"18\",\"numberOfBonuses\":\"0\",\"bonuses\":[]},{\"cellIndex\":\"19\",\"numberOfBonuses\":\"0\",\"bonuses\":[]}]},\"map\":{\"mapFields\":[{\"zone\":\"SEASIDE\",\"towns\":[{\"townName\":\"seasideTest\",\"townColor\":\"BLUE\",\"numberOfBonuses\":\"1\",\"townSector\":\"0\",\"connectionsWithAnchors\":[\"0\"],\"hasKing\":\"false\",\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}],\"connectionsWithTowns\":[]}],\"Deck\":{\"permissionCards\":[{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"seasideTest\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]},{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"seasideTest\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]},{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"seasideTest\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]}]},\"numberOfTowns\":\"1\"},{\"zone\":\"MAINLAND\",\"towns\":[{\"townName\":\"mainlandTest\",\"townColor\":\"ORANGE\",\"numberOfBonuses\":\"1\",\"townSector\":\"0\",\"connectionsWithAnchors\":[\"0\",\"3\"],\"hasKing\":\"false\",\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}],\"connectionsWithTowns\":[]}],\"Deck\":{\"permissionCards\":[{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"mainlandTest\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]},{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"mainlandTest\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]},{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"mainlandTest\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]}]},\"numberOfTowns\":\"1\"},{\"zone\":\"MOUNTAINS\",\"towns\":[{\"townName\":\"testMountains\",\"townColor\":\"KING\",\"numberOfBonuses\":\"0\",\"townSector\":\"0\",\"connectionsWithAnchors\":[\"3\"],\"hasKing\":\"true\",\"bonuses\":[],\"connectionsWithTowns\":[]}],\"Deck\":{\"permissionCards\":[{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"testMountains\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]},{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"testMountains\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]},{\"numberOfBonuses\":\"1\",\"permittedTowns\":[\"testMountains\"],\"bonuses\":[{\"quantity\":\"1\",\"type\":\"HELPERS\",\"isDirect\":\"true\"}]}]},\"numberOfTowns\":\"1\"}]}, \"randomBonuses\":\"false\"}";
    }
}
