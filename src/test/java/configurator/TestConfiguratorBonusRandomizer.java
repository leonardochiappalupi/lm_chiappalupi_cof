package configurator;

import game.Configuration;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import org.junit.Assert;
import org.junit.Test;
import view.configurator.ConfiguratorController;

import java.util.ArrayList;
import java.util.List;

import static constants.ConfigurationConstants.NUMBER_OF_FIELDS;
import static constants.ConfigurationConstants.NUMBER_OF_SECTORS_PER_FIELD;

public class TestConfiguratorBonusRandomizer {
    private Configuration configuration;

    @Test
    public void testToJsonConfigurationParser() {
        setupInitialConfiguration();

        Town seaFieldTown1 = Town.cloneTown(configuration.getMap().getSeaField().getAllTowns(false).get(0));
        Town seaFieldTown2 = Town.cloneTown(configuration.getMap().getSeaField().getAllTowns(false).get(1));
        Town mountainFieldTown0 = Town.cloneTown(configuration.getMap().getMountainField().getAllTowns(false).get(0));

        configuration = ConfiguratorController.randomizeBonuses(configuration, true);

        Assert.assertFalse("Bonuses should not correspond", bonusesCorrespond(seaFieldTown1, configuration.getMap().getSeaField().getAllTowns(false).get(0)));
        Assert.assertFalse("Bonuses should not correspond", bonusesCorrespond(seaFieldTown2, configuration.getMap().getSeaField().getAllTowns(false).get(1)));
        Assert.assertFalse("Bonuses should not correspond", bonusesCorrespond(mountainFieldTown0, configuration.getMap().getMountainField().getAllTowns(false).get(0)));
        Assert.assertEquals("King town should still have 0 bonuses", 0, configuration.getMap().getMainlandField().getAllTowns(false).get(0).getBonusList().size());
    }

    private boolean bonusesCorrespond(Town initialTown, Town shuffledTown) {
        if (initialTown.getBonusList().size() != shuffledTown.getBonusList().size()) return false;
        for (int i = 0; i < initialTown.getBonusList().size(); i++) {
            if (!initialTown.getBonusList().get(i).toString().equals(shuffledTown.getBonusList().get(i).toString()))
                return false;
        }

        return true;
    }

    private void setupInitialConfiguration() {
        configuration = new Configuration();
        configuration.setMap(new Map());
        for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
            configuration.getMap().setMapField(new MapField(NUMBER_OF_SECTORS_PER_FIELD), i);
            configuration.getMap().getMapField(i).setFieldType(i == Field.SEASIDE.getIndex() ? Field.SEASIDE : (i == Field.MAINLAND.getIndex() ? Field.MAINLAND : Field.MOUNTAINS));
        }

        List<Town> townsToAdd = new ArrayList<>();
        townsToAdd.add(new Town("a", TownColor.BLUE));
        townsToAdd.add(new Town("b", TownColor.GOLD));
        configuration.getMap().getSeaField().setAllTowns(townsToAdd);
        configuration.getMap().getSeaField().getAllTowns(false).get(0).setHasKing(false);
        configuration.getMap().getSeaField().getAllTowns(false).get(1).setHasKing(false);
        configuration.getMap().getSeaField().getAllTowns(false).get(0).setSector(0);
        configuration.getMap().getSeaField().getAllTowns(false).get(1).setSector(1);

        List<Town> townsToAdd2 = new ArrayList<>();
        townsToAdd2.add(new Town("k", TownColor.KING));
        configuration.getMap().getMainlandField().setAllTowns(townsToAdd2);
        configuration.getMap().getMainlandField().getAllTowns(false).get(0).setHasKing(true);
        configuration.getMap().getMainlandField().getAllTowns(false).get(0).setSector(0);

        List<Town> townsToAdd3 = new ArrayList<>();
        townsToAdd3.add(new Town("c", TownColor.SILVER));
        configuration.getMap().getMountainField().setAllTowns(townsToAdd3);
        configuration.getMap().getMountainField().getAllTowns(false).get(0).setHasKing(false);
        configuration.getMap().getMountainField().getAllTowns(false).get(0).setSector(0);

        Town seaFieldTown1 = configuration.getMap().getSeaField().getAllTowns(false).get(0);
        seaFieldTown1.addBonus(new DirectBonus(BonusType.CARDS, 4));
        seaFieldTown1.addBonus(new DirectBonus(BonusType.CARDS, 7));
        seaFieldTown1.addBonus(new DirectBonus(BonusType.NOBSTEPS, 2));

        Town seaFieldTown2 = configuration.getMap().getSeaField().getAllTowns(false).get(1);
        seaFieldTown2.addBonus(new DirectBonus(BonusType.COINS, 1));
        seaFieldTown2.addBonus(new DirectBonus(BonusType.HELPERS, 7));

        Town mountainFieldTown = configuration.getMap().getMountainField().getAllTowns(false).get(0);
        mountainFieldTown.addBonus(new DirectBonus(BonusType.SUPPACTION, 6));
        mountainFieldTown.addBonus(new DirectBonus(BonusType.HELPERS, 11));
        mountainFieldTown.addBonus(new DirectBonus(BonusType.COINS, 9));
        mountainFieldTown.addBonus(new DirectBonus(BonusType.NOBSTEPS, 1));
    }
}
