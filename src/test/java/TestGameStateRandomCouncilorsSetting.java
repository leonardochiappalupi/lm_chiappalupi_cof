import constants.ConfigurationConstants;
import game.Configuration;
import game.GameState;
import game.board.Map;
import game.board.MapField;
import game.cards.PoliticsColor;
import game.pawns.Councilor;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class TestGameStateRandomCouncilorsSetting {
    private GameState gameState;

    @Test
    public void testRandomCouncilorsSetting(){
        setGameState();
        gameState.setRandomCouncilors();
        Assert.assertEquals("Number of available councilors should be 20",20,gameState.getAvailableCouncilors().size());
        for (int i = 0; i < ConfigurationConstants.NUMBER_OF_FIELDS; i++) {
            MapField field = gameState.getMap().getMapField(i);
            Assert.assertEquals("Number of councilors in the council of the MapField number " + String.valueOf(i) + " should be 4",4,field.getCouncil().getCouncilors().size());
            testRandomness();
        }
    }

    private void setGameState(){
        Configuration configuration = new Configuration();
        Map map = new Map(new MapField(1),new MapField(1),new MapField(1));
        configuration.setMap(map);
        gameState = new GameState(configuration,3);
    }

    /**
     * Tests the randomness level of the {@link GameState#setRandomCouncilors()} method.
     * The randomnessLevel is increased by 1 every time a council has all the councilor of the same color.
     * It is highly improbable to have more than 2 councils with all the councilors of the same color.
     */
    private void testRandomness(){
        int randomnessLevel = 0;
        for (int i = 0; i < ConfigurationConstants.NUMBER_OF_FIELDS; i++) {
            List<Councilor> councilors= gameState.getMap().getMapField(i).getCouncil().getCouncilors();
            PoliticsColor color0 = councilors.get(0).getColor();
            PoliticsColor color1 = councilors.get(1).getColor();
            PoliticsColor color2 = councilors.get(2).getColor();
            PoliticsColor color3 = councilors.get(3).getColor();
            if(color0 == color1 && color1 == color2 && color2 == color3)
                randomnessLevel++;
        }
            List<Councilor> kingCouncilors= gameState.getKingCouncil().getCouncilors();
            PoliticsColor color0 = kingCouncilors.get(0).getColor();
            PoliticsColor color1 = kingCouncilors.get(1).getColor();
            PoliticsColor color2 = kingCouncilors.get(2).getColor();
            PoliticsColor color3 = kingCouncilors.get(3).getColor();
            if(color0 == color1 && color1 == color2 && color2 == color3)
                randomnessLevel++;
        Assert.assertTrue("The level of randomness should be 0,1 or 2, it is highly improbable to have a higher randomness level",randomnessLevel<=2);
    }
}
