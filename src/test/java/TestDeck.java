import game.cards.*;
import org.junit.Assert;
import org.junit.Test;

public class TestDeck {
    private Deck deck = new Deck();
    @Test
    public void deckTest(){
        setDeck();
        testDeckCreation();
        testDeckMethods();
    }

    private void testDeckCreation(){
        Assert.assertEquals("Number of cards in the deck should be 2",2,deck.getCards().size());
        Assert.assertEquals("The first card should be a BuildingPermissionCard",BuildingPermissionCard.class,deck.getCardAtIndex(0).getClass());
        Assert.assertEquals("The first card should have a price of 13 coins",13,deck.getCardAtIndex(0).getPrice());
        Assert.assertEquals("The second card should be a PoliticsCard",PoliticsCard.class,deck.getCardAtIndex(1).getClass());
        Assert.assertEquals("The second card should have a price of 15 coins",15,deck.getCardAtIndex(1).getPrice());
    }

    private void testDeckMethods(){
        //Testing draw(true)
        Assert.assertEquals("The card drawn without retain should be the BuildingPermissionCard",BuildingPermissionCard.class,deck.draw(true).getClass());
        //Testing draw(false) (default)
        Card card = deck.draw();
        String description = card.toString();
        Assert.assertTrue("After drawing retaining the card, deck dimension should be 1",deck.getCards().size()==1);
        Assert.assertEquals("After drawing retaining the card, the first card should be a politics card",PoliticsCard.class,deck.getCardAtIndex(0).getClass());
        deck.addCard(card);
        //Testing getCardFromDescription()
        Assert.assertEquals("Method getCardFromDescription should return a BuildingPermissionCard",BuildingPermissionCard.class,deck.getCardFromString(description).getClass());
        //Emptying the deck
        deck.draw();
        deck.draw();
        //Creating a complex deck to test the shuffle() method
        for (int i = 0; i < 50; i++) {
            Card cardToAdd = new BuildingPermissionCard();
            cardToAdd.setPrice(i);
            deck.addCard(cardToAdd);
        }
        deck.shuffle();
        boolean isOrdered = true;
        for (int i = 0; i < deck.getCards().size() && isOrdered; i++) {
            if(deck.getCardAtIndex(i).getPrice()!=i)
                isOrdered=false;
        }
        Assert.assertTrue("After shuffling the deck it should not be ordered (not impossible)",!isOrdered);


    }

    private void setDeck(){
        Card politicsCard = new PoliticsCard(PoliticsColor.JOLLY);
        Card buildingPermissionCard = new BuildingPermissionCard();

        politicsCard.setPrice(15);
        buildingPermissionCard.setPrice(13);

        ((BuildingPermissionCard)buildingPermissionCard).setDescription("Test");

        deck.addCardAtIndex(buildingPermissionCard,0);
        deck.addCard(politicsCard);
    }
}
