package logic;

import client.GameCommands;
import game.Configuration;
import game.board.Council;
import game.board.Field;
import game.board.NobilityRoute;
import game.bonus.DirectBonus;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import game.cards.KingBonusTile;
import game.cards.TownColorBonusTile;
import game.logic.Phase;
import game.logic.Player;
import game.logic.actions.Action;
import game.market.ItemToSell;
import game.pawns.Councilor;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;


public class TestGameCommands implements GameCommands {
    private List<DirectBonus> gotBonuses = new ArrayList<>();
    private int allowedActions = 0;


    public int getAllowedActions() {
        return allowedActions;
    }

    public List<DirectBonus> getGotBonuses() {
        return gotBonuses;
    }

    @Override
    public void setupGameState(Configuration configuration, int numberOfPlayers) throws RemoteException {

    }

    @Override
    public void notifyPhase(Phase phase, String player) throws RemoteException {

    }

    @Override
    public void enableUser() throws RemoteException {

    }

    @Override
    public void disableUser() throws RemoteException {

    }

    @Override
    public void rollbackToLastValidGameState() throws RemoteException {

    }

    @Override
    public void markCurrentGameStateAsValid() throws RemoteException {

    }

    @Override
    public void updateOrAddPlayer(Player player) throws RemoteException {

    }

    @Override
    public void updateAvailableCouncilors(List<Councilor> councilors) throws RemoteException {

    }

    @Override
    public void updateAvailableHelpers(int numOfHelpers) throws RemoteException {

    }

    @Override
    public void addEmporiumInTown(String townName, Player owningPlayer) throws RemoteException {

    }

    @Override
    public void moveKingToTown(String townName) throws RemoteException {

    }

    @Override
    public void setupNobilityRoute(NobilityRoute nobilityRoute) throws RemoteException {

    }

    @Override
    public void updateCouncil(Field field, Council council) throws RemoteException {

    }

    @Override
    public void updateFieldCards(Field field, Deck buildingPermitDeck, BuildingPermissionCard left, BuildingPermissionCard right, boolean isBonusTileOnBoard) throws RemoteException {

    }

    @Override
    public void updateKingTiles(List<TownColorBonusTile> townColorBonusTiles, KingBonusTile[] kingBonusTiles) throws RemoteException {

    }

    @Override
    public void updateKingCouncil(Council kingCouncil) throws RemoteException {

    }

    @Override
    public void updatePoliticsCardsDeck(Deck deck) throws RemoteException {

    }

    @Override
    public void updateMarketItemsToSell(List<ItemToSell> itemsToSells) throws RemoteException {

    }

    @Override
    public void turnTimeout(int timeRemaining) throws IOException {

    }

    @Override
    public void reportActionCompleted(Action action) throws RemoteException {

    }

    @Override
    public void onItemBought(String uniqueId) throws RemoteException {

    }

    @Override
    public void allowOneMoreAction() throws RemoteException {
        allowedActions++;
    }

    @Override
    public void onBonusReceived(IBonus bonus, Action context, Player player) {
        if (bonus instanceof DirectBonus)
            gotBonuses.add((DirectBonus) bonus);
    }
}
