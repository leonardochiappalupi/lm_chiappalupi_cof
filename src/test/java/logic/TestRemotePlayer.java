package logic;

import game.bonus.IndirectBonus;
import game.logic.flows.BonusProcessor;
import server.GameRoom;
import server.entities.RemotePlayer;

public class TestRemotePlayer extends RemotePlayer {
    TestRemotePlayer(String username, GameRoom room) {
        super(username, room);
    }

    @Override
    public void startGame() {

    }

    @Override
    public void startConfiguration() {

    }

    @Override
    public void dispatchChatMessage(String jsonChatElement) {

    }

    @Override
    public void dispatchLogMessage(String jsonLogElement) {

    }

    @Override
    public void dispatchLobbyUpdate(String jsonLobbyUpdate) {

    }

    @Override
    public void startIndirectBonusProcess(IndirectBonus bonus, BonusProcessor.IndirectBonusAsyncHandler handler) {

    }
}
