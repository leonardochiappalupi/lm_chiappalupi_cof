package logic;

import client.Client;
import client.presentation.Pick;
import game.GameState;
import game.board.*;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.flows.Flow;
import org.junit.Assert;
import org.junit.Test;
import server.GameRoom;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestChangePermitCardsFlow {
    private RulesBroker rulesBroker;
    private GameState buddyGameState = new GameState();
    private Flow flow;
    private TestGameCommands tgc;
    ArrayList<BuildingPermissionCard> cardsToAdd = new ArrayList<>();

    @Test
    public void testPermitChange() {
        setBuddyGameState();
        GameRoom room = new GameRoom("id");
        RemotePlayer rp = new TestRemotePlayer("buddy", room);
        tgc = new TestGameCommands();
        rp.setGameCommandsBroadcaster(tgc);
        rp.setPickManager(new TestPick());
        rulesBroker = new RulesBroker(room);
        rulesBroker.setMainLocalState(buddyGameState);
        Client.setUsername("buddy");
        flow = rulesBroker.buildAction(Action.SWAP_PERMIT, rp);
        flow.start();
        flow.start();
        flow.start();

        Assert.assertEquals("Player should have 0 helpers from 3", 0, buddyGameState.getPlayerWithUsername("buddy").getHelpers().size());
        /**Seaside test**/
        Assert.assertEquals("Seaside left card should be the old first in the deck",cardsToAdd.get(2),buddyGameState.getMap().getSeaField().getLeftShownPermissionCard());
        Assert.assertEquals("Seaside right card should be the old second in the deck",cardsToAdd.get(3),buddyGameState.getMap().getSeaField().getRightShownPermissionCard());
        Assert.assertEquals("Seaside permit deck should have old shown left card",cardsToAdd.get(1),buddyGameState.getMap().getSeaField().getBuildingPermissionCardsDeck().getCardAtIndex(0));
        Assert.assertEquals("Seaside permit deck should have old shown left card",cardsToAdd.get(0),buddyGameState.getMap().getSeaField().getBuildingPermissionCardsDeck().getCardAtIndex(1));
        /**Mainland test**/
        Assert.assertEquals("Mainland left card should be the old first in the deck",cardsToAdd.get(2),buddyGameState.getMap().getMainlandField().getLeftShownPermissionCard());
        Assert.assertEquals("Mainland right card should be the old second in the deck",cardsToAdd.get(3),buddyGameState.getMap().getMainlandField().getRightShownPermissionCard());
        Assert.assertEquals("Mainland permit deck should have old shown left card",cardsToAdd.get(1),buddyGameState.getMap().getMainlandField().getBuildingPermissionCardsDeck().getCardAtIndex(0));
        Assert.assertEquals("Mainland permit deck should have old shown left card",cardsToAdd.get(0),buddyGameState.getMap().getMainlandField().getBuildingPermissionCardsDeck().getCardAtIndex(1));
        /**Mountain test**/
        Assert.assertEquals("Mountain left card should be the old first in the deck",cardsToAdd.get(2),buddyGameState.getMap().getMountainField().getLeftShownPermissionCard());
        Assert.assertEquals("Mountain right card should be the old second in the deck",cardsToAdd.get(3),buddyGameState.getMap().getMountainField().getRightShownPermissionCard());
        Assert.assertEquals("Mountain permit deck should have old shown left card",cardsToAdd.get(1),buddyGameState.getMap().getMountainField().getBuildingPermissionCardsDeck().getCardAtIndex(0));
        Assert.assertEquals("Mountain permit deck should have old shown left card",cardsToAdd.get(0),buddyGameState.getMap().getMountainField().getBuildingPermissionCardsDeck().getCardAtIndex(1));

    }

    private void setBuddyGameState() {

        MapField seaside = new MapField(6);
        MapField mainland = new MapField(6);
        MapField mountain = new MapField(6);
        Deck seasideDeck = new Deck();
        Deck mainlandDeck = new Deck();
        Deck mountainDeck = new Deck();
        seaside.setBuildingPermissionCardsDeck(seasideDeck);
        mainland.setBuildingPermissionCardsDeck(mainlandDeck);
        mountain.setBuildingPermissionCardsDeck(mountainDeck);
        Map buddyMap = new Map(seaside, mainland, mountain);
        buddyGameState.setMap(buddyMap);
        BuildingPermissionCard c1 = new BuildingPermissionCard();
        BuildingPermissionCard c2 = new BuildingPermissionCard();
        BuildingPermissionCard c3 = new BuildingPermissionCard();
        BuildingPermissionCard c4 = new BuildingPermissionCard();
        c1.addAllowedTown(new Town("a"));
        c2.addAllowedTown(new Town("b"));
        c3.addAllowedTown(new Town("c"));
        c4.addAllowedTown(new Town("d"));
        cardsToAdd.add(c1); /**This is the initial left card**/
        cardsToAdd.add(c2); /**This is the initial right card**/
        cardsToAdd.add(c3); /**This should become the left shown card**/
        cardsToAdd.add(c4); /**This should become the right shown card**/
        seasideDeck.addMultipleCards(cardsToAdd);
        mainlandDeck.addMultipleCards(cardsToAdd);
        mountainDeck.addMultipleCards(cardsToAdd);
        seaside.setLeftShownPermissionCard((BuildingPermissionCard) seasideDeck.draw(false));
        seaside.setRightShownPermissionCard((BuildingPermissionCard) seasideDeck.draw(false));
        mainland.setLeftShownPermissionCard((BuildingPermissionCard) mainlandDeck.draw(false));
        mainland.setRightShownPermissionCard((BuildingPermissionCard) mainlandDeck.draw(false));
        mountain.setLeftShownPermissionCard((BuildingPermissionCard) mountainDeck.draw(false));
        mountain.setRightShownPermissionCard((BuildingPermissionCard) mountainDeck.draw(false));
        Player buddyPlayer = new Player();
        buddyPlayer.setUsername("buddy");
        buddyPlayer.addHelpers(3);
        buddyGameState.addPlayer(buddyPlayer);


    }

    private class TestPick implements Pick {
        int iteration = 0;

        @Override
        public void pickACouncil(Action action, boolean includeKing) throws RemoteException {
            try {
                switch (iteration) {
                    case 0:
                        flow.onBoardItemsPicked(BoardItemType.COUNCIL, Field.SEASIDE);
                        break;
                    case 1:
                        flow.onBoardItemsPicked(BoardItemType.COUNCIL, Field.MAINLAND);
                        break;
                    case 2:
                        flow.onBoardItemsPicked(BoardItemType.COUNCIL, Field.MOUNTAINS);
                        break;
                }
            } catch (GameLogicException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
            iteration++;
        }

        @Override
        public void pickACouncilor(Action action) throws RemoteException {

        }

        @Override
        public void pickUpTo4PoliticsCards(Action action) throws RemoteException {

        }

        @Override
        public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {

        }

        @Override
        public void pickYourPermissionCard(Action action) throws RemoteException {

        }

        @Override
        public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {

        }

        @Override
        public void pickTownsToMoveKing(Action action) throws RemoteException {

        }

        @Override
        public void pickEmporiumTown(Action action) throws RemoteException {

        }
    }
}
