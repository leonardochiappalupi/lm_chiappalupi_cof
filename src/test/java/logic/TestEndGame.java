package logic;

import client.Client;
import client.presentation.Pick;
import logic.TestGameCommands;
import logic.TestRemotePlayer;
import game.GameState;
import game.board.*;
import game.cards.BuildingPermissionCard;
import game.cards.CardType;
import game.cards.PoliticsCard;
import game.cards.PoliticsColor;
import game.logic.Phase;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.flows.Flow;
import game.pawns.Emporium;
import org.junit.Assert;
import org.junit.Test;
import server.GameRoom;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;

public class TestEndGame {
    private RulesBroker rulesBroker;
    private GameState buddyGameState = new GameState();
    private Flow flow;
    GameRoom room = new GameRoom("id");

    /**This test case has one player first and one second in the nobility route**/
    @Test
    public void testNormalEndGame() {
        setupGameState();
        setupConnections();
        flow.start();

        Assert.assertEquals("Room phase should be END_GAME", Phase.END_GAME, room.getRoomPhase());
        Assert.assertEquals("Buddy player should be at position 3 (win)", 3, buddyGameState.getPlayerWithUsername("buddy").getWinPosition());

        Player winningPlayer = rulesBroker.checkEndGameConditions();
        Assert.assertEquals("Winning player is buddy","buddy",winningPlayer.getUsername());
        Assert.assertEquals("Buddy player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy").getWinPosition());
        Assert.assertEquals("Buddy2 player should be at position 2 (win)",2,buddyGameState.getPlayerWithUsername("buddy2").getWinPosition());


    }

    /**This test has 2 even first players in the nobility route**/
    @Test
    public void testEvenFirstEndGame() {
        setupGameState();
        setupConnections();
        buddyGameState.getPlayerWithUsername("buddy2").setNobilityPosition(3);
        flow.start();


        Assert.assertEquals("Room phase should be END_GAME", Phase.END_GAME, room.getRoomPhase());
        Assert.assertEquals("Buddy player should be at position 3 (win)", 3, buddyGameState.getPlayerWithUsername("buddy").getWinPosition());

        Player winningPlayer = rulesBroker.checkEndGameConditions();
        Assert.assertEquals("Winning player is buddy","buddy",winningPlayer.getUsername());
        Assert.assertEquals("Buddy player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy").getWinPosition());
        Assert.assertEquals("Buddy2 player should be at position 5 (win)",5,buddyGameState.getPlayerWithUsername("buddy2").getWinPosition());


    }

    /**This test has one first player and 2 even second players**/
    @Test
    public void testEvenSecondEndGame() {
        setupGameState();
        setupConnections();
        Player buddy3=new Player();
        buddy3.setUsername("buddy3");
        buddy3.setNobilityPosition(1);
        buddyGameState.addPlayer(buddy3);
        flow.start();


        Assert.assertEquals("Room phase should be END_GAME", Phase.END_GAME, room.getRoomPhase());
        Assert.assertEquals("Buddy player should be at position 3 (win)", 3, buddyGameState.getPlayerWithUsername("buddy").getWinPosition());

        Player winningPlayer = rulesBroker.checkEndGameConditions();
        Assert.assertEquals("Winning player is buddy","buddy",winningPlayer.getUsername());
        Assert.assertEquals("Buddy player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy").getWinPosition());
        Assert.assertEquals("Buddy2 player should be at position 2 (win)",2,buddyGameState.getPlayerWithUsername("buddy2").getWinPosition());
        Assert.assertEquals("Buddy3 player should be at position 2 (win)",2,buddyGameState.getPlayerWithUsername("buddy3").getWinPosition());


    }

    /**This test case has 2 even second players and 2 even first players**/
    @Test
    public void testEvenFirstSecondEndGame() {
        setupGameState();
        setupConnections();

        Player buddy3=new Player();
        buddy3.setUsername("buddy3");
        buddy3.setNobilityPosition(1);
        buddyGameState.addPlayer(buddy3);

        Player buddy4=new Player();
        buddy4.setUsername("buddy4");
        buddy4.setNobilityPosition(3);
        buddyGameState.addPlayer(buddy4);

        flow.start();


        Assert.assertEquals("Room phase should be END_GAME", Phase.END_GAME, room.getRoomPhase());
        Assert.assertEquals("Buddy player should be at position 3 (win)", 3, buddyGameState.getPlayerWithUsername("buddy").getWinPosition());

        Player winningPlayer = rulesBroker.checkEndGameConditions();
        Assert.assertEquals("Winning player is buddy","buddy",winningPlayer.getUsername());
        Assert.assertEquals("Buddy player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy").getWinPosition());
        Assert.assertEquals("Buddy2 player should be at position 0 (win)",0,buddyGameState.getPlayerWithUsername("buddy2").getWinPosition());
        Assert.assertEquals("Buddy3 player should be at position 0 (win)",0,buddyGameState.getPlayerWithUsername("buddy3").getWinPosition());
        Assert.assertEquals("Buddy4 player should be at position 5 (win)",5,buddyGameState.getPlayerWithUsername("buddy4").getWinPosition());


    }

    /**This test case has one player first and one second in the nobility route, but after final computations
     * they should go at the same win position. So the winner should be decided by permission cards and politics ones**/
    @Test
    public void testFullEvenEndGame() {
        setupGameState();
        setupConnections();
        buddyGameState.getPlayerWithUsername("buddy2").setWinPosition(9);
        buddyGameState.getPlayerWithUsername("buddy2").addPoliticsCard(new PoliticsCard(PoliticsColor.BLACK));
        flow.start();

        Assert.assertEquals("Room phase should be END_GAME", Phase.END_GAME, room.getRoomPhase());
        Assert.assertEquals("Buddy player should be at position 3 (win)", 3, buddyGameState.getPlayerWithUsername("buddy").getWinPosition());

        Player winningPlayer = rulesBroker.checkEndGameConditions();
        Assert.assertEquals("Winning player is buddy2","buddy2",winningPlayer.getUsername());
        Assert.assertEquals("Buddy player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy").getWinPosition());
        Assert.assertEquals("Buddy2 player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy2").getWinPosition());


    }

    /**This test case has one player first and one second in the nobility route, but after final computations
     * they should go at the same win position. They also have the same number of PC and helpers, so the winner should
     * be buddy (not buddy2), because it has more emporiums and triggered the END_GAME phase**/
    @Test
    public void testFullEvenSameItemsEndGame() {
        setupGameState();
        setupConnections();
        buddyGameState.getPlayerWithUsername("buddy2").setWinPosition(9);
        buddyGameState.getPlayerWithUsername("buddy").addPoliticsCard(new PoliticsCard(PoliticsColor.WHITE));
        buddyGameState.getPlayerWithUsername("buddy2").addPoliticsCard(new PoliticsCard(PoliticsColor.BLACK));
        flow.start();

        Assert.assertEquals("Room phase should be END_GAME", Phase.END_GAME, room.getRoomPhase());
        Assert.assertEquals("Buddy player should be at position 3 (win)", 3, buddyGameState.getPlayerWithUsername("buddy").getWinPosition());

        Player winningPlayer = rulesBroker.checkEndGameConditions();
        Assert.assertEquals("Winning player is buddy","buddy",winningPlayer.getUsername());
        Assert.assertEquals("Buddy player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy").getWinPosition());
        Assert.assertEquals("Buddy2 player should be at position 11 (win)",11,buddyGameState.getPlayerWithUsername("buddy2").getWinPosition());


    }


    private void setupConnections() {

        RemotePlayer rp = new TestRemotePlayer("buddy", room);
        rp.setPickManager(new TestPick());
        rp.setGameCommandsBroadcaster(new TestGameCommands());
        rulesBroker = new RulesBroker(room);
        rulesBroker.setMainLocalState(buddyGameState);
        Client.setUsername("buddy");
        flow = rulesBroker.buildAction(Action.EMPORIUM_PERMIT, rp);


    }


    private void setupGameState() {
        Town a = new Town("a", TownColor.SILVER);
        Town b = new Town("b", TownColor.GOLD);
        Town c = new Town("c", TownColor.ORANGE);
        MapField seaside = new MapField(1);
        MapField mainland = new MapField(1);
        MapField mountain = new MapField(1);
        seaside.replaceTownInSector(0, a);
        mainland.replaceTownInSector(0, b);
        mountain.replaceTownInSector(0, c);
        Map buddyMap = new Map(seaside, mainland, mountain);
        buddyGameState.setMap(buddyMap);
        Player buddyPlayer = new Player();
        buddyPlayer.setUsername("buddy");
        a.addEmporium(new Emporium(buddyPlayer));
        BuildingPermissionCard buddyCard = new BuildingPermissionCard();
        buddyCard.addAllowedTown(b);
        buddyPlayer.addPermissionCards(buddyCard);
        buddyPlayer.setNobilityPosition(3);
        buddyGameState.addPlayer(buddyPlayer);

        Player buddyPlayer2 = new Player();
        buddyPlayer2.setUsername("buddy2");
        buddyPlayer2.setNobilityPosition(1);
        buddyGameState.addPlayer(buddyPlayer2);

    }

    private class TestPick implements Pick {

        @Override
        public void pickACouncil(Action action, boolean includeKing) throws RemoteException {

        }

        @Override
        public void pickACouncilor(Action action) throws RemoteException {

        }

        @Override
        public void pickUpTo4PoliticsCards(Action action) throws RemoteException {

        }

        @Override
        public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {

        }

        @Override
        public void pickYourPermissionCard(Action action) throws RemoteException {
            try {
                flow.onCardsPicked(CardType.BUILDINGPERMISSION, rulesBroker.getThisPlayer(flow).getBuildingPermissionCards(true).get(0));
            } catch (GameLogicException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {

        }

        @Override
        public void pickTownsToMoveKing(Action action) throws RemoteException {

        }

        @Override
        public void pickEmporiumTown(Action action) throws RemoteException {

        }
    }
}
