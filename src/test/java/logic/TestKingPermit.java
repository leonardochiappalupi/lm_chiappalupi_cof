package logic;

import client.Client;
import client.presentation.Pick;
import game.GameState;
import game.board.*;
import game.cards.*;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.NotEnoughCreditsException;
import game.logic.flows.Flow;
import game.pawns.Councilor;
import game.pawns.Emporium;
import org.junit.Assert;
import org.junit.Test;
import server.GameRoom;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;

public class TestKingPermit {
    private GameState buddyGameState = new GameState();
    private RulesBroker rulesBroker;
    private Flow flow;
    private Town a;
    private Town b;
    private Town c;
    GameRoom room = new GameRoom("id");
    int iteration;

    @Test
    public void regularKingMove() {
        iteration = 0;
        setupGameState();
        setupConnections();
        rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").setCoins(4);
        rulesBroker.markCurrentStateAsValid();
        flow.start();
        Assert.assertEquals("King is in town c", "c", rulesBroker.getMainLocalState().getMap().getTownWithKing().getName());
        Assert.assertEquals("Buddy player has no more coins", 0, rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").getCoins());
        Assert.assertEquals("Town c has one emporium", 1, rulesBroker.getMainLocalState().getMap().getTownWithKing().getEmporiums().size());
        Assert.assertEquals("Buddy player has one emporium", 1, rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").getEmporiums().size());
        Assert.assertEquals("That emporium owner is buddy player", "buddy", rulesBroker.getMainLocalState().getMap().getTownWithKing().getEmporiums().get(0).getOwningPlayer().getUsername());
    }

    @Test
    public void notEnoughCoinsMove() {
        iteration = 1;
        setupGameState();
        setupConnections();
        rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").setCoins(3);
        rulesBroker.markCurrentStateAsValid();
        flow.start();
        Assert.assertEquals("King is in town a", "a", rulesBroker.getMainLocalState().getMap().getTownWithKing().getName());
        Assert.assertEquals("Buddy player has same coins", 3, rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").getCoins());
        Assert.assertEquals("Town c has no emporium", 0, rulesBroker.getMainLocalState().getMap().getTownWithKing().getEmporiums().size());
        Assert.assertEquals("Buddy player has no emporium", 0, rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").getEmporiums().size());
    }

    @Test
    public void notEnoughHelperMove() {
        iteration = 2;
        setupGameState();
        setupConnections();
        c.addEmporium(new Emporium(new Player("buddy2")));
        rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").setCoins(4);
        rulesBroker.markCurrentStateAsValid();
        flow.start();
        Assert.assertEquals("King is in town a", "a", rulesBroker.getMainLocalState().getMap().getTownWithKing().getName());
        Assert.assertEquals("Buddy player has same coins", 4, rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").getCoins());
        Assert.assertEquals("Town c has no emporium", 0, rulesBroker.getMainLocalState().getMap().getTownWithKing().getEmporiums().size());
        Assert.assertEquals("Buddy player has no emporium", 0, rulesBroker.getMainLocalState().getPlayerWithUsername("buddy").getEmporiums().size());

    }

    private void setupGameState() {
        a = new Town("a", TownColor.KING);
        b = new Town("b", TownColor.GOLD);
        c = new Town("c", TownColor.ORANGE);
        a.setHasKing(true);
        MapField seaside = new MapField(1);
        MapField mainland = new MapField(1);
        MapField mountain = new MapField(1);
        seaside.replaceTownInSector(0, a);
        mainland.replaceTownInSector(0, b);
        mountain.replaceTownInSector(0, c);
        Map buddyMap = new Map(seaside, mainland, mountain);
        buddyGameState.setMap(buddyMap);
        Council kingCouncil = new Council();
        kingCouncil.addCouncilor(new Councilor(PoliticsColor.BLACK));
        kingCouncil.addCouncilor(new Councilor(PoliticsColor.WHITE));
        kingCouncil.addCouncilor(new Councilor(PoliticsColor.BLUE));
        kingCouncil.addCouncilor(new Councilor(PoliticsColor.ORANGE));
        buddyGameState.setKingCouncil(kingCouncil);
        Player buddyPlayer = new Player();
        buddyPlayer.setUsername("buddy");
        buddyPlayer.addPoliticsCard(new PoliticsCard(PoliticsColor.BLACK));
        buddyPlayer.addPoliticsCard(new PoliticsCard(PoliticsColor.WHITE));
        buddyPlayer.addPoliticsCard(new PoliticsCard(PoliticsColor.BLUE));
        buddyPlayer.addPoliticsCard(new PoliticsCard(PoliticsColor.ORANGE));
        buddyGameState.addPlayer(buddyPlayer);

    }

    private void setupConnections() {

        RemotePlayer rp = new TestRemotePlayer("buddy", room);
        rp.setPickManager(new TestPick());
        rp.setGameCommandsBroadcaster(new TestGameCommands());
        rulesBroker = new RulesBroker(room);
        rulesBroker.setMainLocalState(buddyGameState);
        rulesBroker.markCurrentStateAsValid();
        Client.setUsername("buddy");
        flow = rulesBroker.buildAction(Action.EMPORIUM_KING, rp);
    }

    private class TestPick implements Pick {

        @Override
        public void pickACouncil(Action action, boolean includeKing) throws RemoteException {

        }

        @Override
        public void pickACouncilor(Action action) throws RemoteException {

        }

        @Override
        public void pickUpTo4PoliticsCards(Action action) throws RemoteException {
            try {
                flow.onCardsPicked(CardType.POLITICS, buddyGameState.getPlayerWithUsername("buddy").getPoliticsCard().toArray(new Card[0]));
            } catch (GameLogicException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {

        }

        @Override
        public void pickYourPermissionCard(Action action) throws RemoteException {

        }

        @Override
        public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {

        }

        @Override
        public void pickTownsToMoveKing(Action action) throws RemoteException {
            try {
                flow.onBoardItemsPicked(BoardItemType.TOWN, a, b, c);
            } catch (GameLogicException e) {
                e.printStackTrace();
                switch (iteration) {
                    case 0:
                        break;
                    case 1:
                        Assert.assertTrue("Throws exception is NotEnoughCredits", e instanceof NotEnoughCreditsException);
                        Assert.assertEquals("Thrown exception is due to coins", "coins", ((NotEnoughCreditsException) e).getCreditType());
                        break;
                    case 2:
                        Assert.assertTrue("Throws exception is NotEnoughCredits", e instanceof NotEnoughCreditsException);
                        Assert.assertEquals("Thrown exception is due to helpers", "helpers", ((NotEnoughCreditsException) e).getCreditType());
                        break;
                }
            }
        }

        @Override
        public void pickEmporiumTown(Action action) throws RemoteException {

        }
    }
}
