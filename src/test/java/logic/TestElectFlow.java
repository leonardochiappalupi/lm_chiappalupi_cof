package logic;

import client.Client;
import client.presentation.Pick;
import game.GameState;
import game.board.*;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsColor;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.flows.Flow;
import game.pawns.Councilor;
import game.pawns.PawnType;
import org.junit.Assert;
import org.junit.Test;
import server.GameRoom;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;

import static game.board.Field.SEASIDE;

public class TestElectFlow {
    private RulesBroker rulesBroker;
    private GameState buddyGameState = new GameState();
    private Flow flow;

    @Test
    public void test() {
        setBuddyGameState();
        GameRoom room = new GameRoom("id");
        RemotePlayer rp = new TestRemotePlayer("buddy", room);
        rp.setPickManager(new TestPick());
        rp.setGameCommandsBroadcaster(new TestGameCommands());

        rulesBroker = new RulesBroker(room);
        rulesBroker.setMainLocalState(buddyGameState);
        Client.setUsername("buddy");

        flow = rulesBroker.buildAction(Action.ELECT, rp);
        flow.start();
    }

    private class TestPick implements Pick {

        @Override
        public void pickACouncil(Action action, boolean includeKing) throws RemoteException {
            try {
                flow.onBoardItemsPicked(BoardItemType.COUNCIL, SEASIDE);
            } catch (GameLogicException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void pickACouncilor(Action action) throws RemoteException {

            flow.onPawnsPicked(PawnType.COUNCILOR, buddyGameState.getAvailableCouncilors().get(0).getColor());

            Assert.assertEquals("Coins should be 4 from 0", 4, buddyGameState.getPlayerWithUsername("buddy").getCoins());
            Assert.assertTrue("Available councilors list should contain one councilor", buddyGameState.getAvailableCouncilors().size() == 1);
            Assert.assertEquals("The only available councilor should be VIOLET", PoliticsColor.BLUE, buddyGameState.getAvailableCouncilors().get(0).getColor());
            Assert.assertTrue("Seaside council should have a white councilor as first", buddyGameState.getMap().getSeaField().getCouncil().getCounclilorNumber(0).getColor().equals(PoliticsColor.WHITE));

        }

        @Override
        public void pickUpTo4PoliticsCards(Action action) throws RemoteException {

        }

        @Override
        public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {

        }

        @Override
        public void pickYourPermissionCard(Action action) throws RemoteException {

        }

        @Override
        public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {

        }

        @Override
        public void pickTownsToMoveKing(Action action) throws RemoteException {

        }

        @Override
        public void pickEmporiumTown(Action action) throws RemoteException {

        }
    }

    private void setBuddyGameState() {
        Council seasideCouncil = new Council();
        Council mainlandCouncil = new Council();
        Council mountainCouncil = new Council();
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.BLUE));
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.ORANGE));
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.PINK));
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.VIOLET));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.BLUE));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.ORANGE));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.PINK));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.VIOLET));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.BLUE));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.ORANGE));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.PINK));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.VIOLET));
        MapField seaside = new MapField(0);
        MapField mainland = new MapField(0);
        MapField mountain = new MapField(0);
        seaside.setCouncil(seasideCouncil);
        mainland.setCouncil(mainlandCouncil);
        mountain.setCouncil(mountainCouncil);
        Map buddyMap = new Map(seaside, mainland, mountain);
        buddyGameState.setMap(buddyMap);
        buddyGameState.addAvailableCouncilor(new Councilor(PoliticsColor.WHITE));
        Player buddyPlayer = new Player();
        buddyPlayer.setUsername("buddy");
        buddyPlayer.setCoins(0);
        buddyGameState.addPlayer(buddyPlayer);

    }
}
