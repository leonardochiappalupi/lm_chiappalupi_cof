package logic;

import client.Client;
import game.GameState;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.flows.Flow;
import org.junit.Assert;
import org.junit.Test;
import server.GameRoom;
import server.entities.RemotePlayer;

public class TestAdditionalMainActionFlow {

    private RulesBroker rulesBroker;
    private GameState buddyGameState = new GameState();
    private Flow flow;
    private TestGameCommands tgc;

    @Test
    public void testPermitAcquire() {
        setBuddyGameState();
        GameRoom room = new GameRoom("id");
        RemotePlayer rp = new TestRemotePlayer("buddy", room);
        tgc=new TestGameCommands();
        rp.setGameCommandsBroadcaster(tgc);
        rulesBroker = new RulesBroker(room);
        rulesBroker.setMainLocalState(buddyGameState);
        Client.setUsername("buddy");
        flow = rulesBroker.buildAction(Action.ADDITIONAL_MAIN, rp);
        flow.start();

        Assert.assertEquals("Client should have one remaining action",1,tgc.getAllowedActions());
        Assert.assertEquals("Player should have 3 helpers less",0,buddyGameState.getPlayerWithUsername("buddy").getHelpers().size());

    }

    private void setBuddyGameState(){
        Player buddyPlayer=new Player();
        buddyPlayer.setUsername("buddy");
        buddyPlayer.addHelpers(3);
        buddyGameState.addPlayer(buddyPlayer);
    }
}
