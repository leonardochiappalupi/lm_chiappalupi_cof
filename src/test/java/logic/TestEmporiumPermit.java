package logic;

import client.Client;
import client.presentation.Pick;
import game.GameState;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.cards.*;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.flows.Flow;
import game.pawns.Emporium;
import org.junit.Assert;
import org.junit.Test;
import server.GameRoom;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class TestEmporiumPermit {
    private GameRoom room = new GameRoom("id");
    private RulesBroker rulesBroker;
    private GameState buddyGameState = new GameState();
    private List<DirectBonus> boardBonuses = new ArrayList<>();
    private TestGameCommands tgc = new TestGameCommands();
    private Flow flow;

    @Test
    public void testEmporiumPermit() {
        setBuddyGameState();
        rulesBroker = new RulesBroker(room);
        rulesBroker.setMainLocalState(buddyGameState);
        Client.setUsername("buddy");
        RemotePlayer rp = new TestRemotePlayer("buddy", room);
        room.addPlayer(rp);
        rp.setPickManager(new TestPick());
        rp.setGameCommandsBroadcaster(tgc);
        flow = rulesBroker.buildAction(Action.EMPORIUM_PERMIT, rp);
        flow.start();
    }

    private class TestPick implements Pick {

        @Override
        public void pickACouncil(Action action, boolean includeKing) throws RemoteException {

        }

        @Override
        public void pickACouncilor(Action action) throws RemoteException {

        }

        @Override
        public void pickUpTo4PoliticsCards(Action action) throws RemoteException {

        }

        @Override
        public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {

        }

        @Override
        public void pickYourPermissionCard(Action action) throws RemoteException {
            try {
                flow.onCardsPicked(CardType.BUILDINGPERMISSION, rulesBroker.getThisPlayer(flow).getBuildingPermissionCards(true).get(0));
            } catch (GameLogicException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {
            Town xTown = null;
            for (Town t : rulesBroker.getMainLocalState().getMap().getAllTowns())
                if (t.getName().equals("x"))
                    xTown = t;
            try {
                flow.onBoardItemsPicked(BoardItemType.TOWN, xTown);
            } catch (GameLogicException e) {
                e.printStackTrace();
            }

            Assert.assertTrue("Player's building permission cards deck should be empty", rulesBroker.getThisPlayer(flow).getBuildingPermissionCards(false).isEmpty());
            Assert.assertTrue("Town X should now have an emporium of this player", rulesBroker.getMainLocalState().getMap().getAllTowns().stream()
                    .filter(town -> town.getName().equals("x"))
                    .findAny()
                    .get()
                    .hasEmporiumOfPlayer(rulesBroker.getThisPlayer(flow)));
            Assert.assertEquals("Player has used 2 helpers", 4, rulesBroker.getThisPlayer(flow).getHelpers().size());
            Assert.assertTrue("Players has received all bonuses of towns connected to the one he chose and with an emporium of his ones", tgc.getGotBonuses().containsAll(boardBonuses));

        }

        @Override
        public void pickTownsToMoveKing(Action action) throws RemoteException {

        }

        @Override
        public void pickEmporiumTown(Action action) throws RemoteException {

        }
    }


    private void setBuddyGameState() {
        Deck buddyPCDeck = new Deck();
        buddyPCDeck.addCard(new PoliticsCard(PoliticsColor.WHITE));
        buddyPCDeck.addCard(new PoliticsCard(PoliticsColor.ORANGE));
        buddyPCDeck.addCard(new PoliticsCard(PoliticsColor.PINK));
        buddyPCDeck.addCard(new PoliticsCard(PoliticsColor.BLUE));
        buddyGameState.setPoliticsCardsDeck(buddyPCDeck);

        buddyGameState.addAvailableHelpers(3);

        Player buddyPlayer = new Player();
        Player otherPlayer = new Player();
        Player anotherPlayer = new Player();

        buddyPlayer.setUsername("buddy");
        otherPlayer.setUsername("other");
        anotherPlayer.setUsername("another");

        buddyPlayer.setCoins(0);
        buddyPlayer.addHelpers(3);

        buddyGameState.addPlayer(buddyPlayer);
        buddyGameState.addPlayer(otherPlayer);
        buddyGameState.addPlayer(anotherPlayer);
        buddyGameState.setNobilityRoute(new NobilityRoute(20));

        MapField seaside = new MapField(6);
        MapField mainland = new MapField(6);
        MapField mountain = new MapField(6);

        Town a = new Town("a", TownColor.BLUE);
        Town b = new Town("b", TownColor.GOLD);
        Town c = new Town("c", TownColor.ORANGE);
        Town d = new Town("d", TownColor.SILVER);
        Town e = new Town("e", TownColor.KING);
        Town x = new Town("x", TownColor.SILVER);

        a.addBonus(new DirectBonus(BonusType.NOBSTEPS, 1));
        b.addBonus(new DirectBonus(BonusType.CARDS, 4));
        c.addBonus(new DirectBonus(BonusType.HELPERS, 3));
        d.addBonus(new DirectBonus(BonusType.WINSTEPS, 3));
        e.addBonus(new DirectBonus(BonusType.COINS, 10));
        x.addBonus(new DirectBonus(BonusType.SUPPACTION, 1));

        Anchor buddyAnchor = new Anchor(0);
        buddyAnchor.setLeftField(mainland);
        buddyAnchor.setRightField(mountain);

        a.addConnectedAnchor(buddyAnchor);
        b.addConnectedAnchor(buddyAnchor);
        b.addConnectedTown(x);
        b.addConnectedTown(c);
        c.addConnectedTown(b);
        c.addConnectedTown(e);
        x.addConnectedTown(b);
        x.addConnectedTown(d);

        mainland.replaceTownInSector(3, b);
        mainland.replaceTownInSector(5, c);
        mainland.replaceTownInSector(0, d);
        mainland.replaceTownInSector(4, e);
        mainland.replaceTownInSector(1, x);
        mountain.replaceTownInSector(2, a);

        x.addEmporium(new Emporium(otherPlayer));
        x.addEmporium(new Emporium(anotherPlayer));
        a.addEmporium(new Emporium(buddyPlayer));
        b.addEmporium(new Emporium(buddyPlayer));
        c.addEmporium(new Emporium(buddyPlayer));
        d.addEmporium(new Emporium(buddyPlayer));
        e.addEmporium(new Emporium(buddyPlayer));


        BuildingPermissionCard buddyCard = new BuildingPermissionCard();
        buddyCard.addAllowedTown(x);
        buddyCard.addAllowedTown(a);
        buddyCard.addAllowedTown(b);
        buddyPlayer.addPermissionCards(buddyCard);


        Map buddyMap = new Map(seaside, mainland, mountain);
        buddyGameState.setMap(buddyMap);

        for (Town t : buddyGameState.getMap().getAllTowns())
            boardBonuses.addAll(t.getBonusList());


    }
}
