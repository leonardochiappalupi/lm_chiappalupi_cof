package logic;

import client.Client;
import client.presentation.Pick;
import game.GameState;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.cards.*;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.flows.Flow;
import game.pawns.Councilor;
import org.junit.Assert;
import org.junit.Test;
import server.GameRoom;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.ArrayList;

import static game.board.Field.SEASIDE;

public class TestAcquirePermitFlow {

    private RulesBroker rulesBroker;
    private GameState buddyGameState = new GameState();
    private BuildingPermissionCard permitCardShouldHave;
    private Flow flow;

    @Test
    public void testPermitAcquire() {
        setBuddyGameState();
        GameRoom room = new GameRoom("id");
        RemotePlayer rp = new TestRemotePlayer("buddy", room);
        rp.setPickManager(new TestPick());
        rp.setGameCommandsBroadcaster(new TestGameCommands());
        rulesBroker = new RulesBroker(room);
        rulesBroker.setMainLocalState(buddyGameState);
        Client.setUsername("buddy");
        flow = rulesBroker.buildAction(Action.ACQUIRE_PERMIT, rp);
        flow.start();
    }

    private class TestPick implements Pick {

        @Override
        public void pickACouncil(Action action, boolean includeKing) throws RemoteException {
            try {
                flow.onBoardItemsPicked(BoardItemType.COUNCIL, SEASIDE);
            } catch (GameLogicException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void pickACouncilor(Action action) throws RemoteException {

        }

        @Override
        public void pickUpTo4PoliticsCards(Action action) throws RemoteException {
            try {
                flow.onCardsPicked(CardType.POLITICS, new PoliticsCard(PoliticsColor.BLUE),
                        new PoliticsCard(PoliticsColor.ORANGE),
                        new PoliticsCard(PoliticsColor.JOLLY));
            } catch (GameLogicException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {
            try {
                permitCardShouldHave = rulesBroker.getMainLocalState().getMap().getSeaField().getLeftShownPermissionCard();
                flow.onCardsPicked(CardType.BUILDINGPERMISSION, permitCardShouldHave);

                System.out.println("ACTION COMPLETED");
                Assert.assertEquals("Player should have only one politics card", 1, rulesBroker.getThisPlayer(flow).getPoliticsCard().size());
                Assert.assertEquals("Player's only politics card should be the one won as a bonus", PoliticsColor.PINK, rulesBroker.getThisPlayer(flow).getPoliticsCard().get(0).getColor());
                Assert.assertEquals("Player should have a permission card that was on the board as left one", permitCardShouldHave, rulesBroker.getThisPlayer(flow).getBuildingPermissionCards(false).get(0));
                Assert.assertEquals("Player should have 10 coins (bonus)", 10, rulesBroker.getThisPlayer(flow).getCoins());
                Assert.assertEquals("Player should have 3 helpers (bonus)", 3, rulesBroker.getThisPlayer(flow).getHelpers().size());
                //Assert.assertEquals("Player should be in position 2 (from 0) of nob. route", 2, rulesBroker.getThisPlayer(flow).getNobilityPosition());
                Assert.assertEquals("Player should be in position 4 (from 0) of win. route", 4, rulesBroker.getThisPlayer(flow).getWinPosition());

            } catch (GameLogicException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void pickYourPermissionCard(Action action) throws RemoteException {

        }

        @Override
        public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {

        }

        @Override
        public void pickTownsToMoveKing(Action action) throws RemoteException {

        }

        @Override
        public void pickEmporiumTown(Action action) throws RemoteException {

        }
    }

    private void setBuddyGameState() {
        Council seasideCouncil = new Council();
        Council mainlandCouncil = new Council();
        Council mountainCouncil = new Council();
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.BLUE));
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.ORANGE));
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.PINK));
        seasideCouncil.addCouncilor(new Councilor(PoliticsColor.VIOLET));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.BLUE));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.ORANGE));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.PINK));
        mainlandCouncil.addCouncilor(new Councilor(PoliticsColor.VIOLET));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.BLUE));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.ORANGE));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.PINK));
        mountainCouncil.addCouncilor(new Councilor(PoliticsColor.VIOLET));
        MapField seaside = new MapField(0);
        MapField mainland = new MapField(0);
        MapField mountain = new MapField(0);
        seasideCouncil.setField(seaside);
        mainlandCouncil.setField(mainland);
        mountainCouncil.setField(mountain);
        seaside.setCouncil(seasideCouncil);
        mainland.setCouncil(mainlandCouncil);
        mountain.setCouncil(mountainCouncil);

        Deck seasidePermitDeck = new Deck();
        ArrayList<BuildingPermissionCard> cards = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            BuildingPermissionCard buddyCard = new BuildingPermissionCard();
            buddyCard.addAllowedTown(new Town());
            buddyCard.addBonus(new DirectBonus(BonusType.HELPERS, 3));
            buddyCard.addBonus(new DirectBonus(BonusType.CARDS, 1));
            //buddyCard.addBonus(new DirectBonus(BonusType.NOBSTEPS, 2));
            buddyCard.addBonus(new DirectBonus(BonusType.WINSTEPS, 4));
            buddyCard.addBonus(new DirectBonus(BonusType.COINS, 10));
            cards.add(buddyCard);
        }
        seasidePermitDeck.addMultipleCards(cards);
        seaside.setLeftShownPermissionCard((BuildingPermissionCard) seasidePermitDeck.draw(false));
        seaside.setRightShownPermissionCard((BuildingPermissionCard) seasidePermitDeck.draw(false));
        seaside.setBuildingPermissionCardsDeck(seasidePermitDeck);
        Map buddyMap = new Map(seaside, mainland, mountain);
        buddyGameState.setMap(buddyMap);
        Player buddyPlayer = new Player();
        buddyPlayer.setUsername("buddy");
        buddyPlayer.setCoins(5);
        buddyPlayer.addPoliticsCard(new PoliticsCard(PoliticsColor.BLUE));
        buddyPlayer.addPoliticsCard(new PoliticsCard(PoliticsColor.ORANGE));
        buddyPlayer.addPoliticsCard(new PoliticsCard(PoliticsColor.JOLLY));
        buddyGameState.addPlayer(buddyPlayer);
        buddyGameState.getPoliticsCardsDeck().addCard(new PoliticsCard(PoliticsColor.PINK));
        buddyGameState.addAvailableHelpers(3);

    }
}
