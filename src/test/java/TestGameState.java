import constants.ConfigurationConstants;
import game.Configuration;
import game.GameState;
import game.board.Map;
import game.board.MapField;
import game.board.NobilityRoute;
import game.board.TownColor;
import game.cards.KingBonusTile;
import game.cards.PoliticsColor;
import game.cards.TownColorBonusTile;
import game.pawns.Councilor;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * This class tests only the private methods called by the constructor of GameState
 */
public class TestGameState {
    private GameState gameState;
    @Test
    public void testGameState(){
        setGameState();
        testDefaultValues();
    }
    private void testDefaultValues(){
        Assert.assertEquals("Number of available helpers should be 24",24,gameState.getAvailableHelpers().size());
        for(int i = 0;i< ConfigurationConstants.NUMBER_OF_FIELDS;i++) {
            MapField mapField = gameState.getMap().getMapField(i);
            Assert.assertEquals("Number of winsteps in each MapFieldBonusTile should be 5", 5, mapField.getBonusTile().getWinSteps());
            Assert.assertEquals("Field type in each MapFieldBonusTile should be the one it is on",mapField.getFieldType(),mapField.getBonusTile().getField());
        }
        testTownColorBonusTiles();
        testKingBonusTiles();
        testAvailableCouncilors();
        Assert.assertEquals("Number of available councilors should be 36",36,gameState.getAvailableCouncilors().size());
    }


    private void testTownColorBonusTiles(){
        TownColorBonusTile blueTile = gameState.getTownColorBonusTile(TownColor.BLUE);
        TownColorBonusTile orangeTile = gameState.getTownColorBonusTile(TownColor.ORANGE);
        TownColorBonusTile goldTile = gameState.getTownColorBonusTile(TownColor.GOLD);
        TownColorBonusTile silverTile = gameState.getTownColorBonusTile(TownColor.SILVER);

        Assert.assertEquals("Number of winsteps on blueTile should be 5",5,blueTile.getWinSteps());
        Assert.assertEquals("Number of winsteps on orangeTile should be 5",8,orangeTile.getWinSteps());
        Assert.assertEquals("Number of winsteps on goldTile should be 5",20,goldTile.getWinSteps());
        Assert.assertEquals("Number of winsteps on silverTile should be 5",12,silverTile.getWinSteps());

    }

    private void testKingBonusTiles(){
        KingBonusTile firstBonusTile = gameState.getKingBonusTiles()[0];
        KingBonusTile secondBonusTile = gameState.getKingBonusTiles()[1];
        KingBonusTile thirdBonusTile = gameState.getKingBonusTiles()[2];
        KingBonusTile fourthBonusTile = gameState.getKingBonusTiles()[3];
        KingBonusTile fifthBonusTile = gameState.getKingBonusTiles()[4];

        Assert.assertEquals("Number of winsteps on the first KingBonusTile should be 25",25,firstBonusTile.getWinSteps());
        Assert.assertEquals("Number of winsteps on the second KingBonusTile should be 18",18,secondBonusTile.getWinSteps());
        Assert.assertEquals("Number of winsteps on the third KingBonusTile should be 12",12,thirdBonusTile.getWinSteps());
        Assert.assertEquals("Number of winsteps on the fourth KingBonusTile should be 7",7,fourthBonusTile.getWinSteps());
        Assert.assertEquals("Number of winsteps on the fifth KingBonusTile should be 3",3,fifthBonusTile.getWinSteps());
    }

    private void testAvailableCouncilors(){
        ArrayList<Councilor> councilors = (ArrayList<Councilor>)gameState.getAvailableCouncilors();
        int numberOfCouncilors = councilors.size();

        for (int i = 0; i < numberOfCouncilors; i++)
                Assert.assertEquals("Colour on councilor number " + String.valueOf(i) + " should be " + PoliticsColor.values()[i/ConfigurationConstants.NUMBER_OF_COUNCILOR_COLORS].toString(),PoliticsColor.values()[i/ConfigurationConstants.NUMBER_OF_COUNCILOR_COLORS], councilors.get(i).getColor());
    }

    private void setGameState() {
        Configuration configuration = setConfiguration();
        gameState = new GameState(configuration,3);
    }
    private Configuration setConfiguration(){
        Configuration configuration = new Configuration();
        configuration.setMap(setMap());
        configuration.setNobilityRoute(setNobilityRoute());
        return configuration;
    }
    private Map setMap(){
        MapField seaside = new MapField(1);
        MapField mainland = new MapField(1);
        MapField mountains = new MapField(1);
        return new Map(seaside,mainland,mountains);
    }
    private NobilityRoute setNobilityRoute(){
        NobilityRoute nobilityRoute = new NobilityRoute(5);
        return nobilityRoute;
    }
}
