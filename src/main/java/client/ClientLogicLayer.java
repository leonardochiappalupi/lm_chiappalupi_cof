package client;

import game.Configuration;
import game.GameState;
import game.board.Council;
import game.board.Field;
import game.board.MapField;
import game.board.NobilityRoute;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import game.cards.KingBonusTile;
import game.cards.TownColorBonusTile;
import game.logic.Phase;
import game.logic.Player;
import game.logic.actions.Action;
import game.market.ItemToSell;
import game.market.Market;
import game.pawns.Councilor;
import game.pawns.Emporium;
import org.apache.commons.lang3.SerializationUtils;

import java.util.ArrayList;
import java.util.List;


public class ClientLogicLayer implements GameCommands {

    private Phase phase = Phase.PREGAME;
    private GameState localGameState;
    private GameState lastValidGameState;
    private int remainingMainActions;
    private boolean secondaryActionsEnabled = false;
    private List<ItemToSell> currentItemsForSale = new ArrayList<>();
    private String currentPlayerUsername;
    private int emporiumsToWin;

    public int getEmporiumsToWin() {
        return emporiumsToWin;
    }

    public void setEmporiumsToWin(int emporiumsToWin) {
        this.emporiumsToWin = emporiumsToWin;
    }

    public String getCurrentPlayerUsername() {
        return currentPlayerUsername;
    }

    public boolean areSecondaryActionsEnabled() {
        return secondaryActionsEnabled;
    }

    public Phase getCurrentPhase() {
        return phase;
    }

    public List<ItemToSell> getCurrentItemsForSale() {
        return currentItemsForSale;
    }

    public int getRemainingMainActions() {
        return remainingMainActions;
    }

    public GameState getCurrentLocalState() {
        return localGameState;
    }

    public Player getThisPlayer() {
        return localGameState.getPlayerWithUsername(Client.getUsername());
    }

    @Override
    public void setupGameState(Configuration configuration, int numberOfPlayers) {
        this.localGameState = new GameState(configuration, numberOfPlayers);
        emporiumsToWin = (int) Math.round(configuration.getMap().getAllTowns().size() * 0.66);
    }

    @Override
    public void notifyPhase(Phase phase, String currentPlayer) {
        this.phase = phase;
        this.currentPlayerUsername = currentPlayer;
        Client.getPresentationBridge().onEnabledChanged(phase, currentPlayer);
    }

    @Override
    public void enableUser() {

        switch (phase) {
            case GAME:
                remainingMainActions = 1;
                secondaryActionsEnabled = true;
                break;
            case MARKET_SELL:
                break;
            case MARKET_BUY:
                break;
            default:
                break;
        }
        Client.getPresentationBridge().onPlayerEnabled(phase);
    }

    @Override
    public void disableUser() {
        remainingMainActions = 0;
        secondaryActionsEnabled = false;
        Client.getPresentationBridge().onPlayerDisabled();

    }

    @Override
    public void rollbackToLastValidGameState() {
        localGameState = SerializationUtils.clone(lastValidGameState);
        Client.getPresentationBridge().updatePlayersItems();
        Client.getPresentationBridge().updateOtherPlayers();

    }


    @Override
    public void markCurrentGameStateAsValid() {
        lastValidGameState = SerializationUtils.clone(localGameState);
    }

    @Override
    public void updateOrAddPlayer(Player player) {
        Player oldPlayer = localGameState.getPlayerWithUsername(player.getUsername());
        int playerIndex = localGameState.getPlayers().indexOf(oldPlayer);
        if (playerIndex != -1) {
            localGameState.getPlayers().set(playerIndex, player);
            if (phase != Phase.PREGAME) {
                Client.getPresentationBridge().updateOtherPlayers();
                Client.getPresentationBridge().updatePlayersItems();
                /**
                 * For updating nobility route (its position is inside Player)
                 */
                Client.getPresentationBridge().updateKingBoard();
            }
        } else
            localGameState.addPlayer(player);
    }

    @Override
    public void updateAvailableCouncilors(List<Councilor> councilors) {
        localGameState.clearAvailableCouncilors();
        localGameState.addAvailableCouncilors(councilors);
        if (phase != Phase.PREGAME)
            Client.getPresentationBridge().updateBoardItems();
    }

    @Override
    public void updateAvailableHelpers(int numOfHelpers) {
        localGameState.clearAvailableHelpers();
        localGameState.addAvailableHelpers(numOfHelpers);
        if (phase != Phase.PREGAME)
            Client.getPresentationBridge().updateBoardItems();
    }

    @Override
    public void addEmporiumInTown(String townName, Player owningPlayer) {
        Player localPlayer = localGameState.getPlayerWithUsername(owningPlayer.getUsername());
        localGameState.getMap().getTownWithName(townName).addEmporium(new Emporium(localPlayer));
        Client.getPresentationBridge().updateEmporiums();
    }

    @Override
    public void moveKingToTown(String townName) {
        localGameState.getMap().getTownWithKing().setHasKing(false);
        localGameState.getMap().getTownWithName(townName).setHasKing(true);
        Client.getPresentationBridge().updateKingTownShown();
    }

    @Override
    public void setupNobilityRoute(NobilityRoute nobilityRoute) {
        localGameState.setNobilityRoute(nobilityRoute);
    }

    @Override
    public void updateCouncil(Field field, Council council) {
        localGameState.getMap().getMapField(field).setCouncil(council);
        if (phase != Phase.PREGAME)
            Client.getPresentationBridge().updateCouncils();
    }

    @Override
    public void updateFieldCards(Field receivedField, Deck buildingPermitDeck, BuildingPermissionCard left, BuildingPermissionCard right, boolean hasTileBeenDrawn) {
        MapField field = localGameState.getMap().getMapField(receivedField);
        if (buildingPermitDeck != null)
            field.setBuildingPermissionCardsDeck(buildingPermitDeck);
        if (left != null && right != null) {
            field.setLeftShownPermissionCard(left);
            field.setRightShownPermissionCard(right);
        }
        field.getBonusTile().setHasBeenDrawn(hasTileBeenDrawn);
        Client.getPresentationBridge().updateFieldsCards();
    }

    @Override
    public void updateKingTiles(List<TownColorBonusTile> townColorBonusTiles, KingBonusTile[] kingBonusTiles) {
        localGameState.replaceAllTownColorBonusTiles(townColorBonusTiles);
        System.arraycopy(kingBonusTiles, 0, localGameState.getKingBonusTiles(), 0, localGameState.getKingBonusTiles().length);
        Client.getPresentationBridge().updateKingBoard();
    }

    @Override
    public void updateKingCouncil(Council kingCouncil) {
        localGameState.setKingCouncil(kingCouncil);
        if (phase != Phase.PREGAME)
            Client.getPresentationBridge().updateCouncils();
    }

    @Override
    public void updatePoliticsCardsDeck(Deck deck) {
        localGameState.getPoliticsCardsDeck().emptyDeck();
        localGameState.getPoliticsCardsDeck().addMultipleCards(deck.getCards());
    }

    @Override
    public void updateMarketItemsToSell(List<ItemToSell> itemToSells) {
        currentItemsForSale.clear();
        currentItemsForSale.addAll(itemToSells);
    }

    @Override
    public void turnTimeout(int timeRemaining) {
        Client.getPresentationBridge().turnTimeout(timeRemaining);
    }

    @Override
    public void reportActionCompleted(Action action) {
        if (action.isMain())
            remainingMainActions--;
        else secondaryActionsEnabled = false;
        Client.getPresentationBridge().onActionCompleted(action);
    }

    @Override
    public void onItemBought(String itemId) {
        Client.getPresentationBridge().onItemBought(Market.getItemById(currentItemsForSale, itemId));
        currentItemsForSale.removeIf(item -> item.getUniqueId().equals(itemId));
    }

    @Override
    public void allowOneMoreAction() {
        remainingMainActions++;
    }

    @Override
    public void onBonusReceived(IBonus bonus, Action context, Player player) {
        Client.getPresentationBridge().onBonusProcessed(bonus, context, player);
    }
}
