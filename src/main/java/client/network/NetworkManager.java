package client.network;

import game.logic.actions.Action;
import game.logic.flows.PickedCallbacks;
import game.market.ItemToSell;

import java.util.List;

/**
 * This class is used for all the network communications. Its implementations feature rmi or socket connection, but rely
 * on this same abstract class to create a unique layer between the presentation one and the internet, thus being
 * connection mode-agnostic.
 */
public abstract class NetworkManager {
    private String address;
    private PickedCallbacks currentPickedCallbacks;

    public NetworkManager(String address) {
        this.address = address;
    }

    public PickedCallbacks getCurrentPickedCallbacks() {
        return currentPickedCallbacks;
    }

    public void setCurrentPickedCallbacks(PickedCallbacks currentPickedCallbacks) {
        this.currentPickedCallbacks = currentPickedCallbacks;
    }

    /**
     * Uploads a chat update (JSON formatted string) to server.
     *
     * @param jsonUpdate JSON formatter update string
     */
    public abstract void remoteChatUpdate(String jsonUpdate);

    /**
     * Enables this class to listen lobby updates (when in lobby phase)
     */
    public abstract void enableLobbyUpdate();

    /**
     * Queries the server to know if this user is a game leader.
     * @return True if this user is game leader for its room.
     */
    public abstract boolean amILeader();

    /**
     * Invokes the configuration of a new game room with these maximum number of users and timeout
     *
     * @param maxUsers The maximum number of users allowed in the room
     * @param timeout  The timeout (in seconds) before the game starts (after at least another player joined this room)
     */
    public abstract boolean createGameForUser(int maxUsers, int timeout, int turnTimeout, String jsonConfig);

    /**
     * Asks the server to create a user with this username (after checking if it is available)
     * @param username The username to assign to the newly created player
     */
    public abstract void createUser(String username);

    /**
     * Used to know the username of the current creator of the room
     * @return The username of the game creator
     */
    public abstract String getGameLeader();

    /**
     * Check if this username is already used on the server
     *
     * @param username The username to be checked
     * @return True if the username is available
     */
    public abstract boolean checkUsernameAvailability(String username);

    /**
     * This is used because the NetworkManager is initially created without a username. When the player is created
     * and its username assigned, this method is called to notify the object that it can now reference to
     * {@link client.Client#getUsername()}
     */
    public void notifyUsernameChanged() {

    }

    /**
     * Invoked to start a game action
     * @param which The action to start
     */
    public abstract void startAction(Action which);

    /**
     * Invoked to draw a card
     */
    public abstract void draw();

    /**
     * Invoked to notify the server that this player wants to end its turn
     */
    public abstract void endTurn();

    /**
     * Called when the presentation layer has ended game preparation and loading
     */
    public abstract void onClientReady();

    /**
     * Used during the market phase, when the user has choosen which items to sell
     * @param items A list containing {@link ItemToSell} objects for sale
     */
    public abstract void sendItemsToSell(List<ItemToSell> items);

    /**
     * Called when the user wants to buy something
     * @param item What the user wants to buy
     */
    public abstract void buyItem(ItemToSell item);

    /**
     * Called when, during an action, a user asks to cancel it.
     */
    public abstract void cancelAction();

}
