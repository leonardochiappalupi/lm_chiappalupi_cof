package client.network.socket;


import client.Client;
import client.network.NetworkManager;
import constants.ConnectionConstants;
import game.Configuration;
import game.logic.actions.Action;
import game.market.ItemToSell;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constants.ConnectionConstants.SocketMessages.*;

/**
 * The Socket version of {@link NetworkManager}. The network management here is all made using messages, i.e. TAGs,
 * that help identifying the objects on the ObjectStream. Listening on the socket for events is done
 * asynchronously by starting threads that do not overhead the main one.
 */
public class SocketNetworkManager extends NetworkManager {

    private Socket socket;
    private final ObjectInputStream input;
    private final ObjectOutputStream output;
    private LobbyIncomingLooper lobbyIncomingLooper = new LobbyIncomingLooper();
    private MainIncomingLooper mainIncomingLooper = new MainIncomingLooper();

    /**
     * Creates a new SocketNetworkManager that uses the provided address to connect. Three fundamental objects
     * are hereby stored: a Socket reference that will trigger the persistent server waiting on accept(); an
     * ObjectInputStream, that we will use to receive serialized objects; and an ObjectOutputStream, that we will
     * use to sent TAGs and serialized objects.
     * Finally, we store a new {@link SocketPickedCallbacks} instance, that we will use to call picked callbacks
     * methods to hide which connection we are using.
     *
     * @param address The address to use for the connection
     * @throws IOException If something goes wrong when creating the socket.
     */
    public SocketNetworkManager(String address) throws IOException {
        super(address);
        //COMM INIT
        socket = new Socket();
        ObjectInputStream is = null;
        ObjectOutputStream os = null;

        try {
            socket.setSoTimeout(1000);
            socket.connect(new InetSocketAddress(address, ConnectionConstants.GAME_PERSISTENT_SOCKET_PORT), 1000);
            is = new ObjectInputStream(socket.getInputStream());
            os = new ObjectOutputStream(socket.getOutputStream());
            Logger.getGlobal().log(Level.INFO, String.format("[SOCKET NM]\t Connected using address %s on port %d",
                    address, ConnectionConstants.GAME_PERSISTENT_SOCKET_PORT));

            setCurrentPickedCallbacks(new SocketPickedCallbacks(this));
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, String.format("[SOCKET NM]\t Cannot connect using address %s on port %d!\n" +
                            "Terminating...",
                    address, ConnectionConstants.GAME_PERSISTENT_SOCKET_PORT));

            Logger.getGlobal().log(Level.SEVERE, "[SOCKET NM]\t (Error is: " + e.getMessage() +
                    ")\n");
            throw e;
        } finally {
            input = is;
            output = os;
        }

    }

    @Override
    public boolean amILeader() {
        try {
            String leaderTag = input.readUTF();
            switch (leaderTag) {
                case SOCKET_IS_LEADER:
                    return true;
                case SOCKET_IS_REGULAR:
                    return false;
                default:
                    return false;
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            return false;
        }
    }

    @Override
    public boolean createGameForUser(int maxUsers, int timeout, int turnTimeout, String jsonConfig) {


        try {
            sendTag(SOCKET_MAX_PLAYERS);
            output.writeInt(maxUsers);
            output.flush();

            sendTag(SOCKET_REQUESTED_TIMEOUT);
            output.writeInt(timeout);
            output.flush();

            sendTag(SOCKET_REQUESTED_TURNTIMEOUT);
            output.writeInt(turnTimeout);
            output.flush();

            sendTag(SOCKET_UPLOAD_CONFIGURATION);
            sendTag(jsonConfig);
            String response = input.readUTF();

            enableLobbyUpdate();
            return response.equals(SOCKET_GENERAL_OK);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            return false;
        }

    }

    @Override
    public void createUser(String username) {
        sendTag(SOCKET_CREATE_PLAYER);
        sendTag(username);
    }

    @Override
    public String getGameLeader() {

        try {
            sendTag(SOCKET_GET_GAMELEADER);
            String gameLeader = input.readUTF();
            enableLobbyUpdate();
            return gameLeader;
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            return null;
        }
    }

    /**
     * Starts {@link LobbyIncomingLooper}
     */
    @Override
    public void enableLobbyUpdate() {
        lobbyIncomingLooper.start();
    }

    @Override
    public void remoteChatUpdate(String jsonUpdate) {
        sendTag(SOCKET_JSON);
        sendTag(jsonUpdate);

    }


    @Override
    public boolean checkUsernameAvailability(String username) {
        try {
            output.writeUTF(SOCKET_CHECK_USERNAME);
            output.writeUTF(username);
            output.flush();
            return !input.readUTF().equals(SOCKET_USERNAME_EXISTING);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            return false;
        }
    }

    @Override
    public void startAction(Action which) {

        sendTag(SOCKET_START_ACTION);
        sendObject(which);

    }

    @Override
    public void draw() {
        sendTag(SOCKET_DRAW);
    }

    @Override
    public void endTurn() {
        sendTag(SOCKET_END_TURN);
    }

    @Override
    public void onClientReady() {
        sendTag(SOCKET_CLIENT_READY);
    }

    @Override
    public void sendItemsToSell(List<ItemToSell> items) {
        sendTag(SOCKET_ITEMS_TOSELL);
        ArrayList<ItemToSell> itemsToSend = (ArrayList<ItemToSell>) items;
        sendObject(itemsToSend);
    }

    @Override
    public void buyItem(ItemToSell item) {
        sendTag(SOCKET_ITEM_SOLD);
        sendObject(item);

    }

    @Override
    public void cancelAction() {
        sendTag(SOCKET_CANCEL);
    }

    /**
     * This looper thread is used in-game to listen for updates and messages from the server. We do not make any
     * processing directly here, but we delegate all the sorting of messages and updates to a more appropriate
     * dedicated object, that is a {@link SocketCommandsProcessor}.
     */
    private class MainIncomingLooper extends Thread {
        boolean isGame = true;

        @Override
        public void run() {
            while (isGame) {
                String command;
                try {
                    if (input.available() > 0)
                        synchronized (input) {
                            command = input.readUTF();
                            SocketCommandsProcessor.processSocketMessage(command, input);
                        }
                    else Thread.sleep(100);
                } catch (IOException | InterruptedException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                    this.interrupt();
                }
            }
        }
    }

    /**
     * This looper is similar to the main one but it is dedicated to the lobby phase. When a specific TAG is received,
     * this thread is also in charge of quitting and starting the main one.
     */
    private class LobbyIncomingLooper extends Thread {
        boolean isLobby = true;

        @Override
        public void run() {
            while (isLobby) {
                String command;
                try {
                    if (input.available() > 0) {
                        command = input.readUTF();
                        if (command.equals(SOCKET_JSON)) {
                            String stringCommand = input.readUTF();
                            SocketCommandsProcessor.socketJson(stringCommand);
                        } else if (command.equals(SOCKET_GAME_CONFIGURATION)) {
                            isLobby = false;
                            Client.getPresentationBridge().startLoading();

                            Configuration configuration = (Configuration) input.readObject();
                            int numberOfPlayers = input.readInt();
                            Client.getClientLogicLayer().setupGameState(configuration, numberOfPlayers);
                            mainIncomingLooper.start();

                            this.interrupt();
                        }
                    } else Thread.sleep(100);
                } catch (IOException | ClassNotFoundException | InterruptedException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                    this.interrupt();
                }


            }
        }
    }

    /**
     * Writes a TAG on the output stream.
     *
     * @param tag The TAG to send to the server
     */
    private void sendTag(String tag) {
        try {
            synchronized (output) {
                output.writeUTF(tag);
                output.flush();

            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Writes a (serializable) object on the output stream. To avoid the client receiving a cached item twice,
     * we also reset the stream.
     *
     * @param object The serializable object to write on the stream.
     */
    private void sendObject(Serializable object) {
        synchronized (output) {
            try {

                output.writeObject(object);
                output.flush();
                output.reset();
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public void sendPickPicked(SocketPickPicked pickPicked) {
        try {
            synchronized (output) {

                output.writeUTF(ConnectionConstants.SocketMessages.SOCKET_PICKPICKED);
                output.writeObject(pickPicked);
                output.flush();
                output.reset();

            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

}
