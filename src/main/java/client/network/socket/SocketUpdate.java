package client.network.socket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is a "modular" and generic one designed to be a container for all game logic objects and items that
 * need to be shared by the server to keep clients updated. It includes:
 *  - A list of {@link SocketUpdatable} items (for single objects)
 *  - A list of lists containing {@link SocketUpdatable} items (to make the sharing of collections easier)
 *  - A list of arrays containing {@link SocketUpdatable} items (to make the sharing of arrays easier)
 *  - Optional single integer, string and boolean fields to share arguments.
 * Provided methods are used to know which of these elements the object has, and to retrieve the content.
 */
public class SocketUpdate implements Serializable {

    private ArrayList<SocketUpdatable> itemsToUpdate = new ArrayList<>();
    private ArrayList<List<? extends SocketUpdatable>> listsToUpdate = new ArrayList<>();
    private ArrayList<SocketUpdatable[]> arraysToUpdate = new ArrayList<>();
    private Class listsItemsClass;
    private String string;
    private Integer integer;
    private boolean bool;


    public boolean getBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public String getString() {
        return string;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(int integer) {
        this.integer = integer;
    }


    public void addItemToUpdate(SocketUpdatable item) {
        itemsToUpdate.add(item);
    }

    public void addItemToUpdate(List<? extends SocketUpdatable> item) {
        listsToUpdate.add(item);
    }

    public List<SocketUpdatable> getItemsToUpdate() {
        return itemsToUpdate;
    }

    public void addItemToUpdate(SocketUpdatable[] item) {
        arraysToUpdate.add(item);
    }

    public List<List<SocketUpdatable>> getListsToUpdate() {
        List<List<SocketUpdatable>> returnList = new ArrayList<>();
        for (int i = 0; i < listsToUpdate.size(); i++) {
            returnList.add(new ArrayList<>());
            for (int j = 0; j < listsToUpdate.get(i).size(); j++)
                returnList.get(i).add(listsToUpdate.get(i).get(j));
        }
        return returnList;
    }

    public List<SocketUpdatable[]> getArraysToUpdate() {
        return arraysToUpdate;
    }

    public void setString(String string) {
        this.string = string;
    }

    public boolean hasString() {
        return string != null;
    }

    public boolean hasInteger() {
        return integer != null;
    }

    public boolean hasLists() {
        return !listsToUpdate.isEmpty();
    }

    public boolean hasItems() {
        return !itemsToUpdate.isEmpty();
    }

    public boolean hasArrays() {
        return !arraysToUpdate.isEmpty();
    }

    public void setListsItemsClass(Class itemsClass) {
        listsItemsClass = itemsClass;
    }

    public Class getListsItemsClass() {
        return listsItemsClass;
    }


}
