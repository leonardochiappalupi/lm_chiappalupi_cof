package client.network.socket;


import client.Client;
import client.ClientLogicLayer;
import game.board.Council;
import game.board.Field;
import game.board.NobilityRoute;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import game.cards.KingBonusTile;
import game.cards.TownColorBonusTile;
import game.logic.Phase;
import game.logic.Player;
import game.logic.actions.Action;
import game.market.ItemToSell;
import game.pawns.Councilor;

import java.util.List;

public class SocketUpdateProcessor {

    private SocketUpdateProcessor() {
    }

    /**
     * This method is invoked by a {@link SocketCommandsProcessor} when a SocketUpdate object is received.
     * The logic here is to sort and process the SocketUpdate object (that is a generic and modular class), without
     * the need of explicitly stating (using enums) what to update (to avoid emulating RMI using socket).
     * <p>
     * This first level of sorting distinguishes among updates containing lists, single items, or arguments.
     *
     * @param update The container of items to process and update.
     */
    public static void processUpdate(SocketUpdate update) {

        if (update.hasItems())
            processItemsUpdate(update);
        else if (update.hasLists())
            processListsUpdate(update);
        else if (update.hasInteger() || update.hasString())
            processArgsUpdate(update);

    }

    /**
     * If the received SocketUpdate contains single items, this method is used to go deeper. We basically retrieve the
     * first of this elements and, checking its dynamic type (class), we are able to understand which kind of update
     * to call on the {@link ClientLogicLayer}.
     *
     * @param update The SocketUpdate object received.
     */
    private static void processItemsUpdate(SocketUpdate update) {
        ClientLogicLayer logicLayer = Client.getClientLogicLayer();
        SocketUpdatable firstItem = update.getItemsToUpdate().get(0);

        if (firstItem instanceof Player) {
            if (update.getString() == null)
                logicLayer.updateOrAddPlayer((Player) firstItem);
            else
                logicLayer.addEmporiumInTown(update.getString(), (Player) firstItem);
        } else if (firstItem instanceof Field) {
            if (update.getItemsToUpdate().get(1) instanceof Council) {
                logicLayer.updateCouncil((Field) firstItem, (Council) update.getItemsToUpdate().get(1));
            } else if (update.getItemsToUpdate().get(1) instanceof Deck) {
                logicLayer.updateFieldCards((Field) firstItem,
                        (Deck) update.getItemsToUpdate().get(1),
                        (BuildingPermissionCard) update.getItemsToUpdate().get(2),
                        (BuildingPermissionCard) update.getItemsToUpdate().get(3),
                        update.getBool());
            }
        } else if (firstItem instanceof Council) {
            logicLayer.updateKingCouncil((Council) firstItem);
        } else if (firstItem instanceof Deck) {
            logicLayer.updatePoliticsCardsDeck((Deck) firstItem);
        } else if (firstItem instanceof NobilityRoute) {
            logicLayer.setupNobilityRoute((NobilityRoute) firstItem);
        } else if (firstItem instanceof Action) {
            ((SocketPickedCallbacks) Client.getNetworkManager().getCurrentPickedCallbacks()).confirm();
            logicLayer.reportActionCompleted((Action) firstItem);
        } else if (firstItem instanceof Phase)
            logicLayer.notifyPhase((Phase) firstItem, update.getString());
        else if (firstItem instanceof IBonus)
            logicLayer.onBonusReceived((IBonus) firstItem, (Action) update.getItemsToUpdate().get(1), (Player) update.getItemsToUpdate().get(2));
    }

    /**
     * If the received SocketUpdate contains lists, this method is used to go deeper. We basically retrieve the
     * first of this lists and, checking the dynamic type (class) of its items, we are able to understand which kind
     * of update to call on the {@link ClientLogicLayer}.
     *
     * @param update The SocketUpdate object received.
     */
    private static void processListsUpdate(SocketUpdate update) {
        ClientLogicLayer logicLayer = Client.getClientLogicLayer();
        Class listsItemsClass = update.getListsItemsClass();
        List<? extends SocketUpdatable> firstList = update.getListsToUpdate().get(0);

        if (listsItemsClass == Councilor.class) {
            logicLayer.updateAvailableCouncilors((List<Councilor>) firstList);
        } else if (listsItemsClass == TownColorBonusTile.class) {
            logicLayer.updateKingTiles((List<TownColorBonusTile>) firstList,
                    (KingBonusTile[]) update.getArraysToUpdate().get(0));
        } else if (listsItemsClass == ItemToSell.class) {
            logicLayer.updateMarketItemsToSell((List<ItemToSell>) firstList);
        }
    }


    /**
     * If the received SocketUpdate contains only arguments, this method is used to go deeper. We check which
     * arguments are available and then, we are able to understand which kind of update to call on
     * the {@link ClientLogicLayer}.
     *
     * @param update The SocketUpdate object received.
     */
    private static void processArgsUpdate(SocketUpdate update) {
        ClientLogicLayer logicLayer = Client.getClientLogicLayer();

        if (update.hasInteger()) {
            logicLayer.updateAvailableHelpers(update.getInteger());
        } else if (update.hasString()) {
            logicLayer.moveKingToTown(update.getString());
        }
    }

}
