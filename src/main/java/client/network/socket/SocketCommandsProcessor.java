package client.network.socket;


import client.Client;
import constants.JsonConstants;
import game.logic.exceptions.GameLogicException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constants.ConnectionConstants.SocketMessages.*;


public class SocketCommandsProcessor {

    private SocketCommandsProcessor() {
    }

    /**
     * This is the first entry point to sort and shunt updates and requests from the server.
     * All the work is done because during socket communications, we always put on the stream a (string) tag to
     * identify the identity of the request.
     * This methods delegates the actual processing of received items to different classes/methods, depending on
     * how complex the work is. For all game synchronization events, we rely on calling
     * {@link this#gameSynchronizationMessage(String)} that notifies {@link client.ClientLogicLayer} about what to do.
     * For actual game updates, we use {@link SocketUpdate} objects that have a modular structure to be always the
     * same but being full-filled with different items. In this case, we delegate to {@link SocketUpdateProcessor}.
     * Finally, we handle the remaining actions directly, like the ones involving JSON or the ones involving the market.
     *
     * @param message The (string) tag that enables us to understand what we are going to receive. It is choosen
     *                among {@link constants.ConnectionConstants.SocketMessages}
     * @param input   The input stream to use for receiving additional content.
     */
    public static void processSocketMessage(String message, ObjectInputStream input) {

        try {
            switch (message) {
                case SOCKET_JSON:
                    String stringCommand = input.readUTF();
                    socketJson(stringCommand);
                    break;
                case SOCKET_UPDATE:
                    SocketUpdate update = (SocketUpdate) input.readObject();
                    SocketUpdateProcessor.processUpdate(update);
                    break;
                case SOCKET_PICKPICKED:
                    ((SocketPickedCallbacks) Client.getNetworkManager().getCurrentPickedCallbacks()).confirm();
                    SocketPickPicked request = (SocketPickPicked) input.readObject();
                    ((SocketPickedCallbacks) Client.getNetworkManager().getCurrentPickedCallbacks()).setServerRequest(request);
                    request.execute(Client.getPresentationBridge());
                    break;
                case SOCKET_ITEM_SOLD:
                    String itemUniqueId = input.readUTF();
                    Client.getClientLogicLayer().onItemBought(itemUniqueId);
                    break;
                case SOCKET_TURN_REMAINING:
                    int timeRemaining = input.readInt();
                    Client.getClientLogicLayer().turnTimeout(timeRemaining);
                    break;
                case SOCKET_GAMELOGIC_EXCEPTION:
                    GameLogicException e = (GameLogicException) input.readObject();
                    ((SocketPickedCallbacks) Client.getNetworkManager().getCurrentPickedCallbacks()).error(e);
                    break;
                case SOCKET_GAMELOGIC_OK:
                    ((SocketPickedCallbacks) Client.getNetworkManager().getCurrentPickedCallbacks()).confirm();
                    break;
                default:
                    gameSynchronizationMessage(message);
                    break;
            }
        } catch (IOException | ClassNotFoundException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Mirrors some (elementary) game synchronization events from a tag to the relative {@link client.ClientLogicLayer}
     * call.
     *
     * @param message The (string) TAG to process
     */
    private static void gameSynchronizationMessage(String message) {
        switch (message) {
            case SOCKET_GAME_START:
                Client.getPresentationBridge().startGame();
                break;
            case SOCKET_MARKVALID:
                Client.getClientLogicLayer().markCurrentGameStateAsValid();
                break;
            case SOCKET_ROLLBACK:
                Client.getClientLogicLayer().rollbackToLastValidGameState();
                break;
            case SOCKET_ENABLE_USER:
                Client.getClientLogicLayer().enableUser();
                break;
            case SOCKET_DISABLE_USER:
                Client.getClientLogicLayer().disableUser();
                break;
            case SOCKET_ONEMOREACTION:
                Client.getClientLogicLayer().allowOneMoreAction();
                break;
            default:
                break;
        }
    }

    /**
     * Used to sort JSON updates and parse them correctly.
     *
     * @param stringCommand The JSON Formatted string to sort and parse
     */
    public static void socketJson(String stringCommand) {

        JSONObject jsonCommand = new JSONObject(stringCommand);
        if (jsonCommand.has(JsonConstants.Chat.CHAT_ELEMENT_TAG))
            Client.getPresentationBridge().localChatUpdate(stringCommand);
        if (jsonCommand.has(JsonConstants.Log.LOG_ELEMENT_TAG))
            Client.getPresentationBridge().localLogUpdate(stringCommand);
        if (jsonCommand.has(JsonConstants.Lobby.LOBBY_ELEMENT_TAG))
            Client.getPresentationBridge().localLobbyUpdate(stringCommand);
    }
}
