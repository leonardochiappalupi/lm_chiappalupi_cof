package client.network.socket;

import client.presentation.PresentationBridge;
import game.Pickable;
import game.board.Field;
import game.cards.BuildingPermissionCard;
import game.logic.Pickables;
import game.logic.actions.Action;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This "modular" class is used to make pick and picked callbacks communicate between client and server, masking the
 * actual connection. It contains room for picked items and enums to understand requests and contexts, plus an
 * optional argument used in some cases.
 **/
public class SocketPickPicked implements Serializable {

    private Pickables whatToPick;
    private ArrayList<Pickable> pickedItems = new ArrayList<>();
    private Action action;
    private Serializable arg;

    public SocketPickPicked(Pickables whatToPick, Action action, Serializable arg) {
        this.whatToPick = whatToPick;
        this.action = action;
        this.arg = arg;
    }

    public List<Pickable> getPickedItems() {

        return pickedItems;
    }

    public <T extends Pickable> void addPickedItem(T item) {
        pickedItems.add(item);
    }

    public <T extends Pickable> void addPickedItems(List<T> items) {
        pickedItems.addAll(items);
    }

    /**
     * This method is executed upon the first receive of this object. It calls the appropriate pick method on the
     * current {@link PresentationBridge}, providing the arguments it needs using the stored ones.
     * When the server has sent one of these objects, and the client received it, this method is called.
     * @param presentationBridge The actual reference to a valid {@link PresentationBridge}
     */
    public void execute(PresentationBridge presentationBridge) {
        try {
            switch (whatToPick) {
                case ONE_COUNCIL:
                    presentationBridge.pickACouncil(action, (Boolean) arg);
                    break;
                case ONE_COUNCILOR:
                    presentationBridge.pickACouncilor(action);
                    break;
                case ONE_FIELD_PERMIT:
                    presentationBridge.pickFieldPermissionCard((Field) arg, action);
                    break;
                case ONE_YOURS_PERMIT:
                    presentationBridge.pickYourPermissionCard(action);
                    break;
                case UPTOFOUR_PC:
                    presentationBridge.pickUpTo4PoliticsCards(action);
                    break;
                case ONE_PERMIT_TOWN:
                    presentationBridge.pickOneTownFromPermitCard((BuildingPermissionCard) arg, action);
                    break;
                case KING_TOWNS:
                    presentationBridge.pickTownsToMoveKing(action);
                    break;
                case EMPORIUM_TOWN:
                    presentationBridge.pickEmporiumTown(action);
                    break;
                default:
                    break;
            }
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public Pickables getWhatToPick() {
        return whatToPick;
    }

    public void setArg(Serializable arg) {
        this.arg = arg;
    }

    public Serializable getArg() {
        return arg;
    }

    @Override
    public String toString() {
        return "PickPicked for action: " + action + ", send to pick: " + whatToPick;
    }
}
