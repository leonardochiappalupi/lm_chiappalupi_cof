package client.network.socket;

import game.Pickable;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.cards.Card;
import game.cards.CardType;
import game.logic.exceptions.GameLogicException;
import game.logic.flows.PickedCallbacks;
import game.pawns.Pawn;
import game.pawns.PawnType;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to provide callbacks for picking operations during the game. Unlike RMI, a socket connected
 * client has no chance of using these callbacks directly; so this object masks everything and handles the network
 * operations internally. We even report eventual exceptions, so the caller won't be able to see any difference.
 * To use socket correctly, we rely on a custom modular object called {@link SocketPickPicked}. This objects are
 * sent back and forth between the client and the server, having their content updated every time.
 */
public class SocketPickedCallbacks implements PickedCallbacks {

    private SocketNetworkManager socketNetworkManager;
    private SocketPickPicked serverRequest;
    private boolean itemsOk = false;
    private boolean itemsVerified = false;
    private GameLogicException exception;
    private final Object lock = new Object();

    public SocketPickedCallbacks(SocketNetworkManager socketNetworkManager) {
        this.socketNetworkManager = socketNetworkManager;
    }

    /**
     * Stores a received {@link SocketPickPicked} object.
     *
     * @param received The received request
     */
    public void setServerRequest(SocketPickPicked received) {
        this.serverRequest = received;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws RemoteException, GameLogicException {
        sendAndWaitForErrors(items, type);
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns) throws RemoteException {
        try {
            sendAndWaitForErrors(pawns, type);
        } catch (GameLogicException e) {
            /**onPawnsPicked never raises this exception, so we catch it here instead of throwing again**/
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards) throws RemoteException, GameLogicException {
        sendAndWaitForErrors(cards, type);
    }

    /**
     * This method implements the actual goal of this class. We send the SocketPickPicked we received stuffed
     * with the content the user picked, and then we sleep until we are woken up by the network manager.
     * If the answer is positive, we just return from the method, and the caller of the callback will continue;
     * otherwise we raise the exception that we received from the wakeup call, so that the caller can know
     * if the user picked something wrong - or has no enough coins, etc.
     *
     * @param items The Pickable items to sent
     * @param type  A type enum used to identify the sub-type of items inside the same callback.
     * @throws GameLogicException
     */
    private void sendAndWaitForErrors(Pickable[] items, Serializable type) throws GameLogicException {
        sendPickPicked(items, type);
        itemsVerified = false;
        synchronized (lock) {
            try {
                while (!itemsVerified)
                    lock.wait();
            } catch (InterruptedException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
        if (!itemsOk) {
            throw exception;
        }
    }

    /**
     * Writes a SocketPickPicked object on the output stream
     *
     * @param items The items to stuff the object with
     * @param arg   Optional args to include
     */
    private void sendPickPicked(Pickable[] items, Serializable arg) {
        serverRequest.addPickedItems(Arrays.asList(items));
        serverRequest.setArg(arg);
        socketNetworkManager.sendPickPicked(serverRequest);
    }

    /**
     * Called by the {@link SocketCommandsProcessor} when a callback on the server succeeded
     */
    public void confirm() {
        itemsOk = true;
        itemsVerified = true;
        synchronized (lock) {
            lock.notify();
        }
    }

    /**
     * Called by the {@link SocketCommandsProcessor} when a callback on the server raised an exception.
     *
     * @param e Raised exception
     */
    public void error(GameLogicException e) {
        itemsOk = false;
        exception = e;
        itemsVerified = true;
        synchronized (lock) {
            lock.notify();
        }
    }
}
