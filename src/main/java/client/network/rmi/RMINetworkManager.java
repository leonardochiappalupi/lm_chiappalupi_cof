package client.network.rmi;


import client.Client;
import client.network.NetworkManager;
import constants.ConnectionConstants;
import game.logic.actions.Action;
import game.logic.flows.PickedCallbacks;
import game.market.ItemToSell;
import server.entities.IRemotePlayer;
import server.persistent.IPersistentServer;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The RMI version of the Network Manager implementations. Handles all the network operations requests from the client
 * relying upon remote objects, and specifically:
 * - An implementation of {@link IPersistentServer} to perform user login
 * - An implementation of {@link IRemotePlayer} to use game related methods and calls
 * - An implementation of {@link PickedCallbacks} to send things selected by a user after the client has been asked
 * to pick something.
 * This object is also exported upon creation to provide methods to call by the server.
 */
public class RMINetworkManager extends NetworkManager implements ILocalPlayer {

    private IPersistentServer server;
    private IRemotePlayer remotePlayer;
    private Registry registry;
    private boolean lobbyUpdateEnabled = false;

    /**
     * Initializes a new RMINetworkManager instance by saving the address, locating and storing the registry,
     * and locating the current {@link IPersistentServer} instance on the registry.
     *
     * @param address The network address (in the form http:// [...]) to use for connection
     * @throws RemoteException   If there are issues with RMI Registry
     * @throws NotBoundException If this manager cannot find a suitable {@link IPersistentServer}
     */
    public RMINetworkManager(String address) throws RemoteException, NotBoundException {
        super(address);
        try {
            registry = LocateRegistry.getRegistry(address, ConnectionConstants.RMI_REGISTRY_PORT);
            server = (IPersistentServer) registry.lookup("rmi_interface");
            UnicastRemoteObject.exportObject(this, 0);
            Logger.getGlobal().log(Level.INFO, String.format("[RMI NM]\t Connected using address %s on port %d",
                    address, ConnectionConstants.RMI_REGISTRY_PORT));
        } catch (RemoteException | NotBoundException e) {
            Logger.getGlobal().log(Level.SEVERE, String.format("[RMI NM]\t Cannot connect using address %s on port %d!\n" +
                            "Terminating...",
                    address, ConnectionConstants.RMI_REGISTRY_PORT));

            Logger.getGlobal().log(Level.SEVERE, "[RMI NM]\t (Error is: " + e.getMessage() +
                    ")\n");
            throw e;
        }
    }

    @Override
    public void notifyUsernameChanged() {
        try {
            registry.rebind(Client.getUsername(), this);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }

    }


    /**
     * Starts an action by notifying the server via {@link server.entities.RMIRemotePlayer#prepareAction(Action)},
     * storing the current instance of the {@link PickedCallbacks} object and asking the server to start the just
     * created flow.
     *
     * @param which The action to start
     */
    @Override
    public void startAction(Action which) {
        try {
            remotePlayer.prepareAction(which);
            setCurrentPickedCallbacks((PickedCallbacks) registry.lookup(Client.getUsername() + "_pickCallbacks"));
            remotePlayer.startPendingAction();
        } catch (RemoteException | NotBoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Draws a card using {@link IRemotePlayer#draw()}
     */
    @Override
    public void draw() {
        try {
            remotePlayer.draw();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Asks the server to end the current turn of this player by using {@link IRemotePlayer#endTurn()}
     */
    @Override
    public void endTurn() {
        try {
            remotePlayer.endTurn();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }


    @Override
    public void onClientReady() {
        try {
            remotePlayer.onClientReady();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void sendItemsToSell(List<ItemToSell> items) {
        try {
            remotePlayer.sendItemsToSell(items);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void buyItem(ItemToSell item) {
        try {
            remotePlayer.buyItem(item);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void cancelAction() {
        try {
            remotePlayer.cancelAction();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Remotely called by the server when a new chat update is available. This method sends the update to the
     * presentation layer by calling {@link client.presentation.PresentationBridge#localChatUpdate(String)}
     *
     * @param jsonUpdate The JSON-Formatted chat update element
     */
    @Override
    public void localChatUpdate(String jsonUpdate) {
        Client.getPresentationBridge().localChatUpdate(jsonUpdate);
    }

    /**
     * Remotely called by the server when a new log update is available. This method sends the update to the
     * presentation layer by calling {@link client.presentation.PresentationBridge#localLogUpdate(String)}
     *
     * @param jsonUpdate The JSON-Formatted log update element
     */
    @Override
    public void localLogUpdate(String jsonUpdate) {
        Client.getPresentationBridge().localLogUpdate(jsonUpdate);
    }

    /**
     * Remotely called by the server when a new lobby update is available. This method sends the update to the
     * presentation layer by calling {@link client.presentation.PresentationBridge#localLobbyUpdate(String)}
     *
     * @param jsonUpdate The JSON-Formatted lobby update element
     */
    @Override
    public void localLobbyUpdate(String jsonUpdate) {
        if (lobbyUpdateEnabled) {
            Client.getPresentationBridge().localLobbyUpdate(jsonUpdate);
        }

    }

    /**
     * This method is remotely called by the server when the lobby has ended and the game needs to be synchronized
     * among all clients. It notifies the {@link client.presentation.PresentationBridge} instance and then exports
     * the two objects that the server uses for all the game logic related calls: an instance of the object
     * implementing {@link client.GameCommands} and another one providing {@link client.presentation.Pick}
     *
     * @throws RemoteException If something goes wrong when exporting and binding objects
     */
    @Override
    public void startConfiguration() throws RemoteException {
        Client.getPresentationBridge().startLoading();
        UnicastRemoteObject.exportObject(Client.getClientLogicLayer(), 0);
        UnicastRemoteObject.exportObject(Client.getPresentationBridge(), 0);
        registry.rebind(Client.getUsername() + "_logic", Client.getClientLogicLayer());
        registry.rebind(Client.getUsername() + "_pick", Client.getPresentationBridge());
    }

    /**
     * Called after the configuration has succeeded.
     */
    @Override
    public void startGame() {
        Client.getPresentationBridge().startGame();
    }

    /**
     * When a new action starts on the server, this class need to get an updated reference to the corresponding
     * flow, so that we can still use the methods provided by the {@link PickedCallbacks} interface.
     * This method retrieves the actual reference to the flow.
     */
    @Override
    public void changeActiveFlow() {
        try {
            setCurrentPickedCallbacks((PickedCallbacks) registry.lookup(Client.getUsername() + "_pickCallbacks"));
        } catch (RemoteException | NotBoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void remoteChatUpdate(String jsonUpdate) {
        try {
            remotePlayer.uploadChat(jsonUpdate);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }


    @Override
    public void enableLobbyUpdate() {
        lobbyUpdateEnabled = true;
    }

    @Override
    public boolean amILeader() {
        try {
            return server.newUser(Client.getUsername());

        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        return false;
    }

    @Override
    public boolean createGameForUser(int maxUsers, int timeout, int turnTimeout, String jsonConfig) {
        try {
            remotePlayer = (IRemotePlayer) registry.lookup(Client.getUsername() + "_remote");
            enableLobbyUpdate();
            return remotePlayer.configureGameRoom(maxUsers, timeout, turnTimeout, jsonConfig);
        } catch (RemoteException | NotBoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        return false;

    }

    @Override
    public void createUser(String username) {
        /**Creation is done automatically when calling {@link this#amILeader()} **/
    }

    @Override
    public String getGameLeader() {
        try {
            remotePlayer = (IRemotePlayer) registry.lookup(Client.getUsername() + "_remote");
            enableLobbyUpdate();
            return remotePlayer.getGameLeader();
        } catch (RemoteException | NotBoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }


    @Override
    public boolean checkUsernameAvailability(String username) {
        boolean isUsernameAvailable = false;
        try {
            isUsernameAvailable = server.checkUsernameAvailability(username);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        return isUsernameAvailable;
    }

}
