package client.network.rmi;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ILocalPlayer extends Remote {

    void localChatUpdate(String jsonUpdate) throws RemoteException;

    void localLogUpdate(String jsonUpdate) throws RemoteException;

    void localLobbyUpdate(String jsonUpdate) throws RemoteException;

    void startConfiguration() throws RemoteException;

    void startGame() throws RemoteException;

    void changeActiveFlow() throws RemoteException;
}
