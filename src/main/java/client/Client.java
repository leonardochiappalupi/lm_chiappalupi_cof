package client;

import client.network.NetworkManager;
import client.network.rmi.RMINetworkManager;
import client.network.socket.SocketNetworkManager;
import client.presentation.PresentationBridge;
import client.presentation.cli.CLIBridge;
import client.presentation.gui.GUIBridge;
import constants.ConnectionConstants;
import constants.PresentationConstants;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is the one in charge of starting the entire game client-side. It shows the
 * launcher (the controller being {@link view.launcher.StartController}) and it contains the variables that set the
 * configuration of both network connection (rmi/socket) and presentation mode (GUI/Command Line Interface).
 * The static method {@link #initialConfiguration} is called by the controller and creates the actual Client object
 * by configuring it as selected using the launcher. Finally, {@link #launch()} creates the {@link PresentationBridge} instance
 * (that connects the client to the presentation mode selected via an unique collection of methods) and the
 * {@link NetworkManager} instance (that handles the remote requests in a similar manner to the {@link PresentationBridge}
 * object).
 * From here onwards, the game continues via {@link PresentationBridge#startLobbyLogin()} method.
 */

public class Client extends Application {
    private static PresentationBridge presentationBridge;
    private static NetworkManager networkManager;
    private static ClientLogicLayer clientLogicLayer;
    private static String username;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Client.username = username;
        if (networkManager != null)
            networkManager.notifyUsernameChanged();
    }

    private static void launch() {
        presentationBridge.startLobbyLogin();
    }

    public static PresentationBridge getPresentationBridge() {
        return presentationBridge;
    }

    public static NetworkManager getNetworkManager() {
        return networkManager;
    }

    /**
     * This method is called whenever a client selects its connection and interface type.
     */

    public static void initialConfiguration(String connectionModeName, String presentationModeName, String address, boolean music) {


        clientLogicLayer = new ClientLogicLayer();

        try {
            if (connectionModeName.equals(ConnectionConstants.Names.CONN_MODE_RMI))
                networkManager = new RMINetworkManager(address);
            else if (connectionModeName.equals(ConnectionConstants.Names.CONN_MODE_SOCKET))
                networkManager = new SocketNetworkManager(address);

            if (presentationModeName.equals(PresentationConstants.Names.PRES_MODE_CLI))
                presentationBridge = new CLIBridge();
            else if (presentationModeName.equals(PresentationConstants.Names.PRES_MODE_GUI))
                presentationBridge = new GUIBridge(music);

            if (networkManager != null)
                launch();
        } catch (IOException | NotBoundException e) {
            Logger.getGlobal().log(Level.ALL, e.getMessage(), e);
        }
    }

    /**
     * Loads the FXML scene for the launcher and displays it
     *
     * @param primaryStage The window (Stage) passed automatically at application launch
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/layouts/launcher.fxml"));
        primaryStage.setTitle("Council of Four Launcher");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    public static ClientLogicLayer getClientLogicLayer() {
        return clientLogicLayer;
    }

    public static void main(String[] args) throws Exception {
        Application.launch(args);

    }

}
