package client;


import game.Configuration;
import game.board.Council;
import game.board.Field;
import game.board.NobilityRoute;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import game.cards.KingBonusTile;
import game.cards.TownColorBonusTile;
import game.logic.Phase;
import game.logic.Player;
import game.logic.actions.Action;
import game.market.ItemToSell;
import game.pawns.Councilor;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface GameCommands extends Remote {

    /**
     * Store in the client the initial GameState instance configuring it with the provided parameters
     *
     * @param configuration   A configuration parsed by a JSON and created by the game leader
     * @param numberOfPlayers The number of players
     */
    void setupGameState(Configuration configuration, int numberOfPlayers) throws RemoteException;

    /**
     * Notifies the clients about an updated phase. Also sends the new enabled player username.
     *
     * @param phase         The new phase
     * @param currentPlayer The username of the player that has just been enabled to play
     */
    void notifyPhase(Phase phase, String currentPlayer) throws RemoteException;

    /**
     * Notifies the user it's its turn.
     */
    void enableUser() throws RemoteException;

    /**
     * Notifies the user its turn has ended
     */
    void disableUser() throws RemoteException;

    /**
     * When an action flow is interrupted or a player disconnected, this method notifies the clients to revert to last
     * valid stored gamestate (used to synchronize server and clients)
     */
    void rollbackToLastValidGameState() throws RemoteException;

    /**
     * Notifies clients that they can mark the current "temporary" gamestate as valid, as all interruptable actions
     * have ended
     */
    void markCurrentGameStateAsValid() throws RemoteException;

    /**
     * Notifies the client about an updated player, whether new or just changed
     *
     * @param player The updated or new player
     */
    void updateOrAddPlayer(Player player) throws RemoteException;

    /**
     * Sends the client the available councilors on the board side (backup)
     *
     * @param councilors Available councilors
     */
    void updateAvailableCouncilors(List<Councilor> councilors) throws RemoteException;

    /**
     * Sends the client the available number of helpers on the board side (backup)
     *
     * @param numOfHelpers Available helpers
     */
    void updateAvailableHelpers(int numOfHelpers) throws RemoteException;

    /**
     * Called when a new emporium is built in a given town by some player
     *
     * @param townName     The name of the emporium' town
     * @param owningPlayer The player that built the emporium
     */
    void addEmporiumInTown(String townName, Player owningPlayer) throws RemoteException;

    /**
     * Updates the king position
     *
     * @param townName The new town of the king
     */
    void moveKingToTown(String townName) throws RemoteException;

    /**
     * Called only at the beginning to configure nobility route's bonuses
     *
     * @param nobilityRoute The nobility route instance
     */
    void setupNobilityRoute(NobilityRoute nobilityRoute) throws RemoteException;

    /**
     * Changes the councilors in a council
     *
     * @param field   The council' field
     * @param council Updated councilors
     */
    void updateCouncil(Field field, Council council) throws RemoteException;

    /**
     * Updates shown and deck cards, and the bonus tiles, of a field on the board.
     *
     * @param field              The field to update
     * @param buildingPermitDeck The new building permission cards deck
     * @param left               The permission card on the left
     * @param right              The permission card on the right
     * @param isBonusTileOnBoard A boolean to set if the tile has been taken or not
     */
    void updateFieldCards(Field field, Deck buildingPermitDeck, BuildingPermissionCard left, BuildingPermissionCard right, boolean isBonusTileOnBoard) throws RemoteException;

    /**
     * Updates both bonus tiles for an emporium in all towns of the same color and king bonus tiles
     *
     * @param townColorBonusTiles Tiles relative to towns colors
     * @param kingBonusTiles      King bonus tiles
     */
    void updateKingTiles(List<TownColorBonusTile> townColorBonusTiles, KingBonusTile[] kingBonusTiles) throws RemoteException;

    /**
     * Updates the councilors in the king council
     *
     * @param kingCouncil The updated council
     */
    void updateKingCouncil(Council kingCouncil) throws RemoteException;

    /**
     * Updates the politics cards deck
     *
     * @param deck The new politics cards deck
     */
    void updatePoliticsCardsDeck(Deck deck) throws RemoteException;

    /**
     * Stores the list of the current items available for sale
     *
     * @param itemsToSells Items that can be purchased during the current turn' market
     */
    void updateMarketItemsToSell(List<ItemToSell> itemsToSells) throws RemoteException;

    /**
     * Notifies how many time remains before the turn ends
     *
     * @param timeRemaining The remaining time (in seconds)
     */
    void turnTimeout(int timeRemaining) throws IOException;

    /**
     * Notifies when an action has completed successfully
     *
     * @param action Which action has been completed
     */
    void reportActionCompleted(Action action) throws RemoteException;

    /**
     * Called when an item, identified via its unique id, has been processed in a transaction
     *
     * @param uniqueId The ID that identifies the item
     */
    void onItemBought(String uniqueId) throws RemoteException;

    /**
     * Allows one more main action
     */
    void allowOneMoreAction() throws RemoteException;

    /**
     * Tells the client that the relative player got a bonus in the context of an action
     */
    void onBonusReceived(IBonus bonus, Action context, Player player) throws RemoteException;

}
