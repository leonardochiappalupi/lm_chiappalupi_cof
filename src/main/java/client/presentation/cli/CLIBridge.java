package client.presentation.cli;

import client.Client;
import client.presentation.PresentationBridge;
import client.presentation.cli.clithreads.*;
import constants.PresentationConstants.Tags;
import game.board.Field;
import game.bonus.DirectBonus;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.cards.PoliticsColor;
import game.logic.Phase;
import game.logic.Player;
import game.logic.actions.Action;
import game.market.IMarketable;
import game.market.ItemToSell;
import game.pawns.Helper;
import util.JSONElementNotFoundException;
import util.JSONHelper;
import view.configurator.Configurator;

import java.io.IOException;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The CLI version of the presentation layer classes. This class contains and implements all the methods from its
 * superclass, with a caveat: to avoid being TOO verbose when using cli to play, update methods are silenced with a
 * "verbose" boolean set to false.
 * This class uses various threads for every picking operation (see {@link client.presentation.cli.clithreads.CLIThread}
 * and custom private threads for the three main phases of the game (actions, market sell, market buy).
 * These threads are stored in different fields to be able to interrupt them whenever is the case.
 * A custom chat thread is always turned on and off to provide chat output even when no listening thread is active.
 */
public class CLIBridge extends PresentationBridge implements Cancelable {

    private static final Scanner cliScanner = new Scanner(System.in);
    private static final PrintStream cliWriter = System.out;
    private List<String> lobbyUsers;
    private int usersAllowed;
    private int timeout;
    private int turnTimeout;
    private Thread currentThread;
    private Thread currentCancelableThread;
    private Thread chatThread;
    private boolean verbose = false;

    public PrintStream getWriter() {
        return cliWriter;
    }

    /**
     * Main flow of game initialization
     */
    @Override
    public void startLobbyLogin() {
        CLIUtils.printTag(Phase.PREGAME, this);
        login();
        boolean isRoomLeader = Client.getNetworkManager().amILeader();
        lobby(isRoomLeader);
    }

    /**
     * Asks the user for an username and delegates {@link client.network.NetworkManager#checkUsernameAvailability(String)} for checking
     * it. When the user finally gives an available username, it is passed to {@link client.network.NetworkManager} to be saved.
     */
    @Override
    public void login() {
        cliWriter.println("Select desired username:");
        String username = cliScanner.nextLine();
        while (!Client.getNetworkManager().checkUsernameAvailability(username)) {
            cliWriter.println("Username is already taken. Choose another username:");
            username = cliScanner.nextLine();
        }
        Client.getNetworkManager().createUser(username);
        Client.setUsername(username);
    }

    /**
     * If the user is also the room leader, this methods asks the user for room parameters (number of players, timeout)
     * and it delegates the room creation request to the network manager instance. If the user is a regular player,
     * this method gets the current game leader via the network manager instance and shows it via CLI. Finally,
     * the network manager instance is called one more time via {@link client.network.NetworkManager#enableLobbyUpdate()} to start
     * listening for lobby data changes (see also {@link #localLobbyUpdate(String)} for more information.
     *
     * @param isRoomLeader is this user a room leader?
     */
    private void lobby(boolean isRoomLeader) {
        if (isRoomLeader) {
            cliWriter.println("There's no game available so a new game room will be created for you");
            cliWriter.println("Enter the maximum number of players you'd like to join this game (2-10):");
            usersAllowed = CLIUtils.requestBoundedInt(1, 10, "[ERROR] Enter an integer value between 2 and 10");
            cliWriter.println("Enter the timeout after which the game will automatically start (in seconds):");
            cliWriter.println("(Note that the timer will not start unless at least another player joins your game)");
            timeout = CLIUtils.requestBoundedInt(0, 300, "[ERROR] Enter a positive integer value in seconds (below 5 min/300 sec)");
            cliWriter.println("Enter the time each turn should take at max (in seconds):");
            turnTimeout = CLIUtils.requestBoundedInt(0, 300, "[ERROR] Enter a positive integer value in seconds (below 5 min/300 sec)");
            cliWriter.println("Type \"create\" to start the configurator or enter one of these configurations:");
            CLIUtils.printStringCollection(Arrays.asList(availableConfigurations()));
            String operation = CLIUtils.requestSelection(availableConfigurations(),
                    "[ERROR] Type \"create\" to start the configurator or enter one of these configurations:", "create");
            createOrSelectConfiguration(operation);
        } else {
            for (int i = 0; ++i < 100; cliWriter.println()) ;
            String gameLeader = Client.getNetworkManager().getGameLeader();
            cliWriter.println(String.format("You've joined %s's game", gameLeader));
            chatThread = new MainOutgoingLooper(this);
            chatThread.start();
        }
    }

    @Override
    public void localChatUpdate(String jsonUpdate) {
        try {
            String author = JSONHelper.ChatParser.authorFromJsonChatUpdate(jsonUpdate);
            String message = JSONHelper.ChatParser.messageFromJsonChatUpdate(jsonUpdate);
            cliWriter.println(Tags.TAG_CHAT + " " + author + " wrote: " + message);
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }

    }

    @Override
    public void localLogUpdate(String jsonUpdate) {
        try {
            String message = JSONHelper.LogParser.messageFromJsonLogUpdate(jsonUpdate);
            cliWriter.println(Tags.TAG_LOG + " " + message);
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * In CLI mode this method evaluates the contents of the JSON Object received (after parsing) and compares the local
     * users list in the lobby to the one received via the update. The first time (when {@link #lobbyUsers} is null), it also
     * shows the entire list of users currently in the lobby; whenever this method is called again, it only shows new
     * users. See hardcoded strings and logs for more details.
     * This method also shows up to date lobby information (such as room name or game leader username), whenever provided.
     *
     * @param jsonUpdate JSON Formatted update string
     */
    @Override
    public void localLobbyUpdate(String jsonUpdate) {

        try {
            String roomName = JSONHelper.LobbyParser.roomNameFromJsonLobbyUpdate(jsonUpdate);
            cliWriter.println(String.format("[LOBBY] You're into room: %s", roomName));
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }

        try {
            String gameLeader = JSONHelper.LobbyParser.leaderFromJsonLobbyUpdate(jsonUpdate);
            cliWriter.println(String.format("[LOBBY] This room' game leader is %s", gameLeader));
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }

        try {
            int timeRemaining = JSONHelper.LobbyParser.timeoutFromJsonLobbyUpdate(jsonUpdate);
            cliWriter.println(String.format("[LOBBY] Time remaining until game starts: %d seconds", timeRemaining));
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }

        List<String> usersConnected = new ArrayList<>();
        try {
            usersConnected.addAll(JSONHelper.LobbyParser.LobbyUsersFromJsonLobbyUpdate.toArrayList(jsonUpdate));
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }
        if (lobbyUsers == null) {
            lobbyUsers = usersConnected;
            cliWriter.println("[LOBBY] Users connected:");
            CLIUtils.printStringCollection(lobbyUsers);
        } else {
            usersConnected.stream().filter(newUser -> !lobbyUsers.contains(newUser)).forEach(newUser -> cliWriter.println("[LOBBY] New user joined this game: " + newUser));
            lobbyUsers = usersConnected;
        }

    }

    /**
     * This method is used to handle the configuration selection phase. If the user wants to create a new one, we
     * launch the configurator (GUI only), otherwise we ask he/she to pick an existing one and we send it to the server.
     *
     * @param operation The user command.
     */
    private void createOrSelectConfiguration(String operation) {
        String jsonConfiguration;
        if ("create".equals(operation)) {
            Configurator.startGameConfiguration();
        } else {
            jsonConfiguration = getRequestedJsonConfiguration(operation);
            boolean hasSucceeded = Client.getNetworkManager().createGameForUser(usersAllowed, timeout, turnTimeout, jsonConfiguration);
            if (hasSucceeded) {
                for (int i = 0; ++i < 100; cliWriter.println()) ;
                cliWriter.println("A new room with your values has been created for you.\n" +
                        "Waiting for at least another player to join this game before starting the countdown...");
                chatThread = new MainOutgoingLooper(this);
                chatThread.start();
            } else
                cliWriter.println("[ERROR] Sorry! Game creation has failed, the server has encountered an error.");
        }
    }


    @Override
    public void onActionCompleted(Action completedAction) {
        currentCancelableThread = new ChooseActionThread();
        currentCancelableThread.start();
    }

    @Override
    public void onBonusProcessed(IBonus bonus, Action context, Player player) {
        if (bonus instanceof DirectBonus)
            cliWriter.println(String.format("%s received %d %s during action %s (now: %d)",
                    player.getUsername().equals(Client.getUsername()) ? "You " : "Player: " + player.getUsername() + " ",
                    bonus.getElementsQuantity(), bonus.getElementsType().name().toLowerCase(), context.name().toLowerCase(),
                    CLIUtils.newBonusStatus(bonus, player)));
    }

    @Override
    public void configuratorCallback() {
        cliWriter.println("[CONFIGURATIONS UPDATED]\nType \"create\" to start the configurator or enter one of these configurations:");
        CLIUtils.printStringCollection(Arrays.asList(availableConfigurations()));
        String operation = CLIUtils.requestSelection(availableConfigurations(),
                "[ERROR] Type \"create\" to start the configurator or enter one of these configurations:", "create");
        createOrSelectConfiguration(operation);

    }

    @Override
    public void onPlayerEnabled(Phase phase) {
        if (chatThread != null)
            chatThread.interrupt();
        synchronized (cliWriter) {
            switch (phase) {
                case GAME:
                    CLIUtils.printTag(Phase.GAME, CLIBridge.this);
                    cliDraw();
                    currentCancelableThread = new ChooseActionThread();
                    break;
                case MARKET_SELL:
                    CLIUtils.printTag(Phase.MARKET_SELL, CLIBridge.this);
                    currentCancelableThread = new MarketSellThread();
                    break;
                case MARKET_BUY:
                    CLIUtils.printTag(Phase.MARKET_BUY, CLIBridge.this);
                    currentCancelableThread = new MarketBuyThread();
                    break;
                default:
                    break;
            }
            currentCancelableThread.start();
        }

    }

    private void cliDraw() {
        PoliticsCard drawn = (PoliticsCard) Client.getClientLogicLayer().getCurrentLocalState().getPoliticsCardsDeck().draw(true);
        cliWriter.println(String.format("You drew a %s politics card. Added to your hand.", drawn.getColor().name().toLowerCase()));
        Client.getNetworkManager().draw();
    }


    @Override
    public void onPlayerDisabled() {
        if (currentThread != null)
            currentThread.interrupt();
        if (currentCancelableThread != null) {
            currentCancelableThread.interrupt();
        }
        synchronized (cliWriter) {
            chatThread = new MainOutgoingLooper(this);
            chatThread.start();
            cliWriter.println("Your turn has terminated. Please wait for other players to end their turn.");
            cliWriter.println("(Remember that you can enter \"chat\" + [message] to share something)");
        }
    }

    @Override
    public void startGame() {
        Timer timer = new Timer();
        timer.schedule(new Delay(), 1000);
        cliWriter.println("Starting game...");
    }

    @Override
    public void updateBoardItems() {
        if (verbose)
            synchronized (cliWriter) {
                /**Verbose not implemented**/
            }

    }

    @Override
    public void updatePlayersItems() {
        if (verbose)
            synchronized (cliWriter) {
                cliWriter.println(String.format("[YOU (%d/99) coins: %d, helpers: %d]", Client.getClientLogicLayer().getThisPlayer().getWinPosition(),
                        Client.getClientLogicLayer().getThisPlayer().getCoins(),
                        Client.getClientLogicLayer().getThisPlayer().getHelpers().size()));

                cliWriter.println("Politics cards:\t" + Arrays.asList(PoliticsColor.values()).parallelStream()
                        .filter(color -> Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(color).size() > 0)
                        .map(color -> String.format("%s x%d", color.name(), Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(color).size()))
                        .reduce((string1, string2) -> string1 + string2 + " ").orElse("<no politic cards>"));

                cliWriter.println("Permission cards:\t" + Client.getClientLogicLayer().getThisPlayer().getBuildingPermissionCards(true).parallelStream()
                        .map(BuildingPermissionCard::toString)
                        .reduce((string1, string2) -> string1 + string2 + " ").orElse("<no permission cards>"));


            }
    }

    @Override
    public void updateOtherPlayers() {
        if (verbose)
            synchronized (cliWriter) {
                Client.getClientLogicLayer().getCurrentLocalState().getPlayers().forEach(player -> {
                    if (!player.getUsername().equals(Client.getUsername()))
                        cliWriter.println(">\t" + player.toString());
                });
            }
    }

    @Override
    public void updateEmporiums() {
        if (verbose)
            synchronized (cliWriter) {
                cliWriter.println("Emporiums overview:");
                Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllTowns().parallelStream()
                        .filter(town -> town.getEmporiums().size() > 0)
                        .map(town -> town.getName() + " > (" +
                                town.getEmporiums().stream().map(emporium -> emporium.getOwningPlayer().getUsername()).reduce((s1, s2) -> s1 + " " + s2)
                                + ")")
                        .forEach(cliWriter::println);
            }

    }

    @Override
    public void updateCouncils() {
        if (verbose)
            synchronized (cliWriter) {
                cliWriter.println("These are the councils right now:");
                Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllFields().forEach(mapField ->
                        cliWriter.println(mapField.getCouncil().toString())
                );
            }
    }

    @Override
    public void updateFieldsCards() {
        if (verbose)
            synchronized (cliWriter) {
                Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllFields().forEach(mapField ->
                        cliWriter.println(String.format("[%s] L: %s R: %s bonus: (%s)",
                                mapField.getFieldType().name(),
                                mapField.getLeftShownPermissionCard() == null ? "No more cards in the deck" : mapField.getLeftShownPermissionCard().toString(),
                                mapField.getRightShownPermissionCard() == null ? "No more cards in the deck" : mapField.getRightShownPermissionCard().toString(),
                                mapField.getBonusTile().toString()))
                );
            }

    }

    @Override
    public void updateKingBoard() {
        if (verbose)
            synchronized (cliWriter) {
                /**Verbose not implemented**/
            }

    }

    @Override
    public void updateKingTownShown() {
        if (verbose)
            synchronized (cliWriter) {
                cliWriter.println("King is now in: " + Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithKing().getName());
            }
    }


    @Override
    public void startLoading() {
        cliWriter.println("Waiting for other players...");

    }

    @Override
    public void onItemBought(ItemToSell item) {
        cliWriter.println("Item bought!");
    }

    @Override
    public void onEnabledChanged(Phase phase, String newPlayerUsername) {
        if (phase != Phase.END_GAME) {
            if (!newPlayerUsername.equals(Client.getUsername()))
                cliWriter.println(String.format("It's now %s turn!", newPlayerUsername));
        } else {
            new Thread(() -> {
                try {
                    Thread.sleep(500);
                    if (currentThread != null)
                        currentThread.interrupt();
                    if (currentCancelableThread != null)
                        currentCancelableThread.interrupt();
                    for (int i = 0; i < 100; cliWriter.println(), i++) ;
                } catch (InterruptedException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                    Thread.currentThread().interrupt();
                }
                CLIUtils.printTag(Phase.END_GAME, this);
                cliWriter.println("* Press any key to close the game *");
                cliScanner.nextLine();
                System.exit(0);
            }).start();

        }
    }

    @Override
    public void turnTimeout(int timeRemaining) {
        switch (timeRemaining) {
            case 60:
                cliWriter.println("\n[TIMEOUT] One minute left for your turn\n");
                break;
            case 30:
                cliWriter.println("\n[TIMEOUT] 30 seconds left for your turn\n");
                break;
            case 10:
                cliWriter.println("\n[TIMEOUT] Hurry up! 10 seconds left for your turn\n");
                break;
            default:
                break;
        }
    }


    @Override
    public void pickACouncil(Action action, boolean includeKing) throws RemoteException {
        currentThread = new PickACouncilCLI(cliWriter, action, this, includeKing);
        currentThread.start();

    }

    @Override
    public void pickACouncilor(Action action) throws RemoteException {
        currentThread = new PickACouncilorCLI(cliWriter, action, this);
        currentThread.start();

    }

    @Override
    public void pickUpTo4PoliticsCards(Action action) throws RemoteException {
        currentThread = new PickPoliticsCardsCLI(cliWriter, action, this, cliScanner);
        currentThread.start();
    }

    @Override
    public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {
        currentThread = new PickFieldCardCLI(cliWriter, action, this, field);
        currentThread.start();
    }

    @Override
    public void pickYourPermissionCard(Action action) throws RemoteException {
        currentThread = new PickYourPermissionCardCLI(cliWriter, action, this);
        currentThread.start();
    }

    @Override
    public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {
        currentThread = new PickTownFromCard(cliWriter, action, this, card);
        currentThread.start();
    }

    @Override
    public void pickTownsToMoveKing(Action action) throws RemoteException {
        currentThread = new PickTownsKingCLI(cliWriter, action, this, cliScanner);
        currentThread.start();
    }

    @Override
    public void pickEmporiumTown(Action action) throws RemoteException {
        currentThread = new PickEmporiumTownCLI(cliWriter, action, this);
        currentThread.start();
    }

    @Override
    public void cancelWaiting() {
        if (currentThread != null) {
            currentThread.interrupt();
            Client.getNetworkManager().cancelAction();
        }
        currentThread = new ChooseActionThread();
        currentThread.start();
    }

    /**
     * This thread is used to delay the onClientReady callback, which can be faster than the room actually waiting for
     * clients.
     */
    private class Delay extends TimerTask {

        @Override
        public void run() {
            Client.getNetworkManager().onClientReady();
        }
    }

    /**
     * Thread used to ask the user for an action to start. Filters possible (and shown) actions by checking
     * {@link client.ClientLogicLayer} for remaining ones. Uses {@link CLIUtils#requestMappedSelection(String[], String, Cancelable, Map)}
     */
    private class ChooseActionThread extends Thread implements Cancelable {
        @Override
        public void run() {
            Action action = chooseAction();
            if (action != null)
                Client.getNetworkManager().startAction(action);
        }

        private Action chooseAction() {
            boolean mainActionsEnabled = Client.getClientLogicLayer().getRemainingMainActions() > 0;
            boolean secondaryActionsEnabled = Client.getClientLogicLayer().areSecondaryActionsEnabled();
            cliWriter.println("Choose an action to perform, or type \"end\" to terminate your turn.");
            List<String> availableActions = Arrays.asList(Action.values())
                    .stream()
                    .filter(action -> action != Action.BONUS)
                    .filter(action -> (mainActionsEnabled && action.isMain()) || (secondaryActionsEnabled && !action.isMain() && CLIUtils.checkSecondaryActionsPermitted(action)))
                    .map(Action::toString)
                    .collect(Collectors.toList());
            return CLIUtils.requestMappedSelection(availableActions.toArray(new String[0]),
                    "Enter a valid action or code, or type \"end\", or type \"chat\" followed by a message", this,
                    CLIUtils.buildActionsMap(mainActionsEnabled, secondaryActionsEnabled));
        }

        /**
         * If this is called, the user entered the end turn escape tag...
         */
        @Override
        public void cancelWaiting() {
            Client.getNetworkManager().endTurn();
            this.interrupt();
        }
    }

    /**
     * Thread used during the sell phase of the market. It asks the user to choose what to sell and depending
     * on the selection it makes further requests to process the items for sale. Items are only sent to the server
     * when the user ends this phase: if something is selected but the turn timeout happens, input is disabled and
     * nothing is set for sale by this user.
     */
    private class MarketSellThread extends Thread implements Cancelable {
        boolean marketing = true;
        boolean inside = false;
        List<ItemToSell> toSell = new ArrayList<>();

        List<String> options = new ArrayList<>();
        List<String> selectables = new ArrayList<>();
        int politicsCardNumber = Client.getClientLogicLayer().getThisPlayer().getPoliticsCard().size();
        int permitCardsNumber = Client.getClientLogicLayer().getThisPlayer().getBuildingPermissionCards(true).size();
        int helpersNumber = Client.getClientLogicLayer().getThisPlayer().getHelpers().size();

        @Override
        public void run() {

            while (marketing) {
                updateSellable();
                cliWriter.println("Choose the item to sell (type \"end\" to confirm selling):");
                String selection = CLIUtils.requestMappedSelection(options.toArray(new String[0]),
                        "Invalid input: enter a number or type \"end\" to end selling items", this,
                        CLIUtils.buildGenericNumberedMap(selectables));

                IMarketable item = null;

                if (selection != null)
                    switch (selection) {
                        case "pc":
                            item = sellPc();
                            break;
                        case "permit":
                            item = sellPermit();
                            break;
                        case "helper":
                            item = sellHelper();
                            break;
                        default:
                            break;
                    }

                inside = false;
                if (item != null) {
                    cliWriter.println("Enter the price:");
                    int price = CLIUtils.requestBoundedInt(0, 9999, "Enter a valid value...");
                    item.setPrice(price);
                    toSell.add(new ItemToSell(item, Client.getClientLogicLayer().getThisPlayer()));

                    if ("pc".equals(selection))
                        politicsCardNumber--;
                    else if ("permit".equals(selection))
                        permitCardsNumber--;
                    else if ("helper".equals(selection))
                        helpersNumber--;
                }
            }

        }

        private void updateSellable() {
            options.clear();
            selectables.clear();

            if (politicsCardNumber > 0) {
                options.add(String.format("[%d] Politics cards (you have: %d)", options.size() + 1, politicsCardNumber));
                selectables.add("pc");
            }

            if (permitCardsNumber > 0) {
                options.add(String.format("[%d] Building permission cards (you have: %d)", options.size() + 1, permitCardsNumber));
                selectables.add("permit");
            }

            if (helpersNumber > 0) {
                options.add(String.format("[%d] Helpers (you have: %d)", options.size() + 1, helpersNumber));
                selectables.add("helper");
            }
        }

        @Override
        public void cancelWaiting() {

            if (!inside) {
                cliWriter.println("Exiting market...");
                marketing = false;
                Client.getNetworkManager().sendItemsToSell(toSell);
                /**
                 * No need to end the turn since the server automatically does after receiving items to sell
                 */
                this.interrupt();

            }
        }

        @Override
        public void interrupt() {
            super.interrupt();
            marketing = false;
        }

        private IMarketable sellPc() {
            inside = true;
            List<PoliticsCard> playersCards = Client.getClientLogicLayer().getThisPlayer().getPoliticsCard();
            List<String> cardsStrings = new ArrayList<>();
            for (int i = 0; i < playersCards.size(); i++)
                cardsStrings.add(String.format("[%d] %s", i + 1, playersCards.get(i).toString()));

            return CLIUtils.requestMappedSelection(
                    cardsStrings.toArray(new String[0]),
                    "Invalid input: enter a number corrisponding to a card or enter \"end\" to cancel", this,
                    CLIUtils.buildGenericNumberedMap(playersCards));
        }

        private IMarketable sellPermit() {
            inside = true;
            List<BuildingPermissionCard> buildingPermissionCards = Client.getClientLogicLayer().getThisPlayer().getBuildingPermissionCards(true);
            List<String> permitStrings = new ArrayList<>();
            for (int i = 0; i < buildingPermissionCards.size(); i++)
                permitStrings.add(String.format("[%d] %s", i + 1, buildingPermissionCards.get(i).toString()));

            return CLIUtils.requestMappedSelection(
                    permitStrings.toArray(new String[0]),
                    "Invalid input: enter a number corrisponding to a permission card or enter \"end\" to cancel", this,
                    CLIUtils.buildGenericNumberedMap(buildingPermissionCards));
        }

        private IMarketable sellHelper() {
            inside = true;
            if (Client.getClientLogicLayer().getThisPlayer().getHelpers().size() < 0) {
                cliWriter.println("You have no helpers!");
                return null;
            } else return new Helper(1);
        }
    }

    /**
     * Thread used during market buy phase. We filter the items for sale hiding the ones that are from this user, and
     * then we ask to pick the ones to buy. We also notify the user if it tries to buy something too expensive.
     */
    private class MarketBuyThread extends Thread implements Cancelable {
        List<ItemToSell> availableItems;
        boolean marketing = true;


        @Override
        public void run() {
            while (marketing) {
                availableItems = Client.getClientLogicLayer().getCurrentItemsForSale().stream()
                        .filter(item -> !item.getSeller().getUsername().equals(Client.getUsername()))
                        .collect(Collectors.toList());
                cliWriter.println("These are the items you can buy. Choose one or type \"end\":");
                ItemToSell itemToBuy = CLIUtils.requestMappedSelection(CLIUtils.getOptionsStringList(availableItems),
                        "Invalid input: enter a valid number or type \"end\" to terminate the market", this,
                        CLIUtils.buildGenericNumberedMap(availableItems));
                if (itemToBuy != null)
                    if (itemToBuy.getPrice() > Client.getClientLogicLayer().getThisPlayer().getCoins())
                        cliWriter.println(String.format("This item is too expensive! You need %d coins, you have %d", itemToBuy.getPrice(),
                                Client.getClientLogicLayer().getThisPlayer().getCoins()));
                    else {
                        Client.getNetworkManager().buyItem(itemToBuy);
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                            this.interrupt();
                        }
                    }
            }
        }

        @Override
        public void cancelWaiting() {
            marketing = false;
            Client.getNetworkManager().endTurn();
            this.interrupt();
        }

        @Override
        public void interrupt() {
            super.interrupt();
            marketing = false;
        }
    }

    /**
     * This thread is started and interrupted multiple times during a game, and it's used to catch user input
     * starting with "chat". If this is the case, we send a proper JSON chat update to the server.
     */
    public class MainOutgoingLooper extends Thread {
        CLIBridge cliBridge;
        boolean enabled = true;

        public MainOutgoingLooper(CLIBridge cliBridge) {
            this.cliBridge = cliBridge;
        }

        @Override
        public void run() {
            while (enabled) {
                try {
                    trapChat();
                } catch (InterruptedException e) {
                    Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                    this.interrupt();
                }
            }
        }

        @Override
        public void interrupt() {
            super.interrupt();
            enabled = false;
        }

        private void trapChat() throws InterruptedException {
            try {
                if (System.in.available() > 0) {
                    String command = cliScanner.nextLine();
                    if (command.startsWith("chat")) {
                        cliBridge.remoteChatUpdate(JSONHelper.ChatParser.toJsonChatUpdate(Client.getUsername(), command.substring(5, command.length())));
                    }
                } else Thread.sleep(200);
            } catch (IOException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            }
        }
    }

}
