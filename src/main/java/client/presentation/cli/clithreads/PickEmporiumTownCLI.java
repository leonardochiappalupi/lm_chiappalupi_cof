package client.presentation.cli.clithreads;

import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.board.BoardItemType;
import game.board.Town;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PickEmporiumTownCLI extends CLIThread {
    public PickEmporiumTownCLI(PrintStream cliWriter, Action action, Cancelable cliBridge) {
        super(cliWriter, action, cliBridge);
    }

    @Override
    public void run(){
        cliWriter.println("Type the name of one of these towns (or a valid number):");
        List<Town> validTowns = Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllTowns().parallelStream()
                .filter(town -> town.hasEmporiumOfPlayer(Client.getClientLogicLayer().getThisPlayer()))
                .collect(Collectors.toList());
        Town selectedTown = CLIUtils
                .requestMappedSelection(CLIUtils.getOptionsStringList(validTowns),
                        "Invalid input: enter the name or the number of a town, or enter \"end\" to abort", cliBridge,
                        CLIUtils.buildTownMap(validTowns));
        if (selectedTown != null)
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.TOWN, selectedTown);
            } catch (GameLogicException | RemoteException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
    }
}
