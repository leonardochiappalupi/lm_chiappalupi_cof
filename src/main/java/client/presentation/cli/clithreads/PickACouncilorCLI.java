package client.presentation.cli.clithreads;

import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.cards.PoliticsColor;
import game.logic.actions.Action;
import game.pawns.PawnType;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PickACouncilorCLI extends CLIThread {
    public PickACouncilorCLI(PrintStream cliWriter, Action action, Cancelable cliBridge) {
        super(cliWriter, action, cliBridge);
    }

    @Override
    public void run() {
        cliWriter.println("Choose the color of an available councilor (only available are shown): ");

        List<String> options = Arrays.asList(PoliticsColor.values()).parallelStream()
                .filter(color -> Client.getClientLogicLayer().getCurrentLocalState().getAvailableCouncilorsOfColor(color) > 0)
                .map(PoliticsColor::name).collect(Collectors.toList());

        PoliticsColor chosenCouncilor = CLIUtils.requestMappedSelection(options.toArray(new String[0]),
                "Enter a valid color or number, or type \"end\", or type \"chat\" followed by a message", cliBridge,
                CLIUtils.buildPoliticsColorMap(false));

        if (chosenCouncilor != null)
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onPawnsPicked(PawnType.COUNCILOR, chosenCouncilor);
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
    }
}
