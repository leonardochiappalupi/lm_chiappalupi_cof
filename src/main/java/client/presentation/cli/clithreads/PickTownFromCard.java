package client.presentation.cli.clithreads;

import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.board.BoardItemType;
import game.board.Town;
import game.cards.BuildingPermissionCard;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.NotEnoughCreditsException;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PickTownFromCard extends CLIThread {
    private BuildingPermissionCard card;

    public PickTownFromCard(PrintStream cliWriter, Action action, Cancelable cliBridge, BuildingPermissionCard card) {
        super(cliWriter, action, cliBridge);
        this.card = card;
    }

    @Override
    public void run() {
        cliWriter.println("Type the name of one of these towns (or a valid number):");
        List<Town> validTowns = card
                .getAllowedTowns()
                .stream()
                .filter(town -> !town.hasEmporiumOfPlayer(Client.getClientLogicLayer().getThisPlayer()))
                .collect(Collectors.toList());
        Town selectedTown = CLIUtils
                .requestMappedSelection(CLIUtils
                                .getOptionsStringList(validTowns),
                        "Invalid input: enter the name or the number of a town, or enter \"end\" to abort", cliBridge,
                        CLIUtils.buildTownMap(validTowns));
        if (selectedTown != null)
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.TOWN, selectedTown);
            } catch (NotEnoughCreditsException e) {
                cliWriter.println("You don't have enough councilors to pay all the emporiums of other players");
                cliBridge.cancelWaiting();
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            } catch (RemoteException | GameLogicException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
    }
}
