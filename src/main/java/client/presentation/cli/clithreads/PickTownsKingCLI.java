package client.presentation.cli.clithreads;

import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Town;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.NotEnoughCreditsException;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PickTownsKingCLI extends CLIThread {
    Scanner cliScanner;

    public PickTownsKingCLI(PrintStream cliWriter, Action action, Cancelable cliBridge, Scanner cliScanner) {
        super(cliWriter, action, cliBridge);
        this.cliScanner = cliScanner;
    }

    @Override
    public void run() {
        boolean inputEnabled = true;
        Town currentKingTown = Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithKing();
        List<Town> townsSelected = new ArrayList<>();
        townsSelected.add(currentKingTown);
        cliWriter.println("King is currently in town: " + currentKingTown.getName());

        while (inputEnabled) {
            cliWriter.println("Select one of these connected town where to move the king next:");
            Town lastSelected = townsSelected.get(townsSelected.size() - 1);
            List<Town> availableTowns = lastSelected.getConnectedTowns()
                    .parallelStream()
                    .filter(town -> !townsSelected.contains(town) && !town.hasEmporiumOfPlayer(Client.getClientLogicLayer().getThisPlayer()))
                    .collect(Collectors.toList());
            availableTowns.addAll(getTownsConnectedThroughAnchors(lastSelected, townsSelected)
                    .parallelStream()
                    .filter(town -> !town.hasEmporiumOfPlayer(Client.getClientLogicLayer().getThisPlayer()))
                    .collect(Collectors.toList()));

            CLIUtils.printStringCollection(Arrays.asList(CLIUtils.getOptionsStringList(availableTowns)));

            String choice = "";

            Town selected;

            try {
                choice = CLIUtils.breakableNextLine(cliScanner);
            } catch (InterruptedException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                inputEnabled = false;
                this.interrupt();
            }

            if ("end".equals(choice)) {
                inputEnabled = false;
                cliBridge.cancelWaiting();
            } else if ("stop".equals(choice)) {
                inputEnabled = false;
                try {
                    Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.TOWN, townsSelected.toArray(new BoardItem[0]));
                } catch (NotEnoughCreditsException e) {
                    Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                    String creditsType = e.getCreditType();
                    if ("coins".equals(creditsType))
                        cliWriter.println("You don't have enough coins to make the king move through all these towns");
                    else if ("helpers".equals(creditsType))
                        cliWriter.println("You don't have enough helpers to pay for all other emporiums already built");
                    cliBridge.cancelWaiting();
                } catch (RemoteException | GameLogicException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            } else {
                Map<String, Town> map = CLIUtils.buildTownMap(availableTowns);
                if (map.keySet().contains(choice)) {
                    selected = map.get(choice);
                    townsSelected.add(selected);
                    cliWriter.print(townsSelected.get(0).getName());
                    townsSelected.stream().filter(town -> !town.equals(townsSelected.get(0))).forEachOrdered(town -> cliWriter.print(" --> " + town.getName()));
                    cliWriter.println();
                } else
                    cliWriter.println("Input invalid, enter a valid town name/number or type \"end\" or \"stop\"");
            }
        }
    }

    private List<Town> getTownsConnectedThroughAnchors(Town townToCheck, List<Town> selected) {
        List<Town> finalTowns = new ArrayList<>();
        Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllTowns().forEach(town ->
                townToCheck.getConnectedAnchors().forEach(anchor -> {
                    if (town.getConnectedAnchors().contains(anchor) &&
                            !town.getName().equals(townToCheck.getName()) &&
                            selected.stream().filter(town1 -> town1.getName().equals(town.getName())).collect(Collectors.toList()).isEmpty())
                        finalTowns.add(town);
                })
        );
        return finalTowns;
    }
}
