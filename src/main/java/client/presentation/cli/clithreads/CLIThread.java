package client.presentation.cli.clithreads;

import client.presentation.cli.Cancelable;
import game.logic.actions.Action;

import java.io.PrintStream;

/**
 * Concrete implementations of this thread model are used to make the user pick elements during the game. The superclass
 * stores the action (context), a reference to the output stream and a reference to the {@link Cancelable} callback to
 * call when the user wants to exit from this picking action.
 */
public abstract class CLIThread extends Thread{
    Action action;
    PrintStream cliWriter;
    Cancelable cliBridge;

    public CLIThread(PrintStream cliWriter, Action action, Cancelable cliBridge) {
        this.action = action;
        this.cliWriter = cliWriter;
        this.cliBridge=cliBridge;
    }

    @Override
    public void run(){
        /**To be a thread, this has to have a run() method but being abstract we can just leave it empty**/
    }
}
