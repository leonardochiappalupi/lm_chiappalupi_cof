package client.presentation.cli.clithreads;

import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.board.BoardItemType;
import game.board.Field;
import game.cards.PoliticsColor;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PickACouncilCLI extends CLIThread {
    boolean includeKing;

    public PickACouncilCLI(PrintStream cliWriter, Action action, Cancelable cliBridge, boolean includeKing) {
        super(cliWriter, action, cliBridge);
        this.includeKing = includeKing;
    }

    @Override
    public void run() {
        if(action==Action.ELECT) {
            cliWriter.println("\nThese are the politics cards you currently have:");
            cliWriter.println("These are the politics cards you currently have:");
            CLIUtils.printStringCollection(Arrays.asList(PoliticsColor.values()).parallelStream()
                    .filter(color -> Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(color).size() > 0)
                    .map(color -> Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(color).size() + " " + color.name().toLowerCase() + " cards")
                    .collect(Collectors.toList()));
        }
        cliWriter.println("\nThese are the councils right now:");
        Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllFields().forEach(mapField ->
                cliWriter.println(mapField.getCouncil().toString())
        );
        if (includeKing)
            cliWriter.println(Client.getClientLogicLayer().getCurrentLocalState().getKingCouncil().toString());

        String message = "Choose a council among SEASIDE, MAINLAND, MOUNTAINS";
        if (includeKing)
            message += ", KING:";
        else message += ":";

        Field choosenField = CLIUtils.requestMappedSelection(new String[]{message},
                "Enter a valid council name or number, or type \"end\", or type \"chat\" followed by a message", cliBridge,
                CLIUtils.buildCouncilsMap(includeKing));

        if (choosenField != null)
            try {
                if (choosenField != Field.KING)
                    Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.COUNCIL, choosenField);
                else
                    Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.KINGCOUNCIL);

            } catch (GameLogicException | RemoteException e) {
                /**
                 * When calling onBoardItemsPicked for a council, there's no chance of having GameLogicExceptions
                 */
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
    }
}

