package client.presentation.cli.clithreads;

import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.cards.Card;
import game.cards.CardType;
import game.cards.PoliticsCard;
import game.cards.PoliticsColor;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.ItemsNotValidException;
import game.logic.exceptions.NotEnoughCreditsException;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PickPoliticsCardsCLI extends CLIThread {
    private Scanner cliScanner;

    public PickPoliticsCardsCLI(PrintStream cliWriter, Action action, Cancelable cliBridge, Scanner cliScanner) {
        super(cliWriter, action, cliBridge);
        this.cliScanner = cliScanner;
    }

    @Override
    public void run() {
        if (action == Action.EMPORIUM_KING) {
            cliWriter.println("This is the king council right now:");
            cliWriter.println(Client.getClientLogicLayer().getCurrentLocalState().getKingCouncil().toString());
        }

        cliWriter.println("These are the politics cards you currently have:");
        List<String> options = Arrays.asList(PoliticsColor.values()).parallelStream()
                .filter(color -> Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(color).size() > 0)
                .map(color -> Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(color).size() + " " + color.name().toLowerCase() + " cards")
                .collect(Collectors.toList());
        CLIUtils.printStringCollection(options);
        cliWriter.println("Choose 1 -> 4 politics cards, (enter \"stop\" to end selection or \"end\" to abort):");

        try {
            readCards();
        } catch (InterruptedException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            this.interrupt();
        }


    }

    private void readCards() throws InterruptedException {
        PoliticsCard cardSelected;
        boolean inputEnabled = true;
        List<Card> choices = new ArrayList<>();
        int numberOfPickedCards = 0;
        while (numberOfPickedCards < 4 && inputEnabled) {

            String choice = "";
            choice = CLIUtils.breakableNextLine(cliScanner);

            if ("stop".equals(choice)) {
                break;
            } else if ("end".equals(choice)) {
                inputEnabled = false;
                cliBridge.cancelWaiting();
            } else {
                PoliticsColor selectedColor = PoliticsColor.getColorFromString(choice.toUpperCase());
                List<PoliticsCard> cardsOfThisColor = new ArrayList<>();
                if (selectedColor != null)
                    cardsOfThisColor.addAll(Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(selectedColor));
                if (!cardsOfThisColor.isEmpty()) {
                    cardSelected = cardsOfThisColor.get(0);
                    numberOfPickedCards++;
                    choices.add(cardSelected);
                    cliWriter.println("Selected one " + cardSelected.getColor().name().toLowerCase() + " card;");
                    cliWriter.println("Continue choosing cards or enter \"stop\"");
                } else
                    cliWriter.println("Invalid input, please type one of available colors or enter \"stop\" to end selection");
            }

        }
        if (inputEnabled)
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onCardsPicked(CardType.POLITICS, choices.toArray(new Card[0]));
            } catch (NotEnoughCreditsException e) {
                cliWriter.println("You don't have enough coins to use these cards with this council");
                cliBridge.cancelWaiting();
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            } catch (ItemsNotValidException e) {
                cliWriter.println("The cards you selected do not satisfy the council!");
                cliBridge.cancelWaiting();
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            } catch (RemoteException | GameLogicException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
    }
}
