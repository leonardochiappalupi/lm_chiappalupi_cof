package client.presentation.cli.clithreads;

import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.cards.BuildingPermissionCard;
import game.cards.CardType;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.NotEnoughCreditsException;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PickYourPermissionCardCLI extends CLIThread {
    public PickYourPermissionCardCLI(PrintStream cliWriter, Action action, Cancelable cliBridge) {
        super(cliWriter, action, cliBridge);
    }

    @Override
    public void run() {
        cliWriter.println("Choose one of your permission cards: ");

        List<BuildingPermissionCard> cards = Client.getClientLogicLayer().getThisPlayer().getBuildingPermissionCards(action.equals(Action.BONUS));
        BuildingPermissionCard chosenCard = CLIUtils.requestMappedSelection(
                CLIUtils.getOptionsStringList(cards),
                "Invalid selection. enter a valid number or enter \"end\" to abort", cliBridge,
                CLIUtils.buildGenericNumberedMap(cards)
        );

        if (chosenCard != null)
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onCardsPicked(CardType.BUILDINGPERMISSION, chosenCard);
            } catch (NotEnoughCreditsException e) {
                cliWriter.println("\nYou don't have enough helpers to pay for other players' emporiums");
                cliBridge.cancelWaiting();
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            } catch (RemoteException | GameLogicException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
    }
}
