package client.presentation.cli.clithreads;


import client.Client;
import client.presentation.cli.CLIUtils;
import client.presentation.cli.Cancelable;
import game.board.Field;
import game.board.MapField;
import game.cards.BuildingPermissionCard;
import game.cards.CardType;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PickFieldCardCLI extends CLIThread {
    private Field field;

    public PickFieldCardCLI(PrintStream cliWriter, Action action, Cancelable cliBridge, Field field) {
        super(cliWriter, action, cliBridge);
        this.field = field;
    }

    @Override
    public void run() {
        MapField chosenField = Client.getClientLogicLayer().getCurrentLocalState().getMap().getMapField(field.getIndex());
        List<BuildingPermissionCard> cardList = new ArrayList<>();
        if (chosenField.getLeftShownPermissionCard() != null)
            cardList.add(chosenField.getLeftShownPermissionCard());
        if (chosenField.getRightShownPermissionCard() != null)
            cardList.add(chosenField.getRightShownPermissionCard());

        cliWriter.println(String.format("Choose a %s permission card:", chosenField.getFieldType().name().toLowerCase()));

        BuildingPermissionCard chosenCard = CLIUtils.requestMappedSelection(CLIUtils.getOptionsStringList(cardList),
                "Enter a valid number, or type \"end\", or type \"chat\" followed by a message", cliBridge,
                CLIUtils.buildGenericNumberedMap(cardList));

        if (chosenCard != null)
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onCardsPicked(CardType.BUILDINGPERMISSION, chosenCard);
            } catch (GameLogicException e) {
                Logger.getGlobal().log(Level.INFO, e.getMessage(), e);
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
    }
}
