package client.presentation.cli;


import client.Client;
import constants.PresentationConstants;
import game.board.Field;
import game.board.Town;
import game.bonus.IBonus;
import game.cards.PoliticsColor;
import game.logic.Phase;
import game.logic.Player;
import game.logic.actions.Action;
import util.JSONHelper;

import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CLIUtils {
    private static Scanner cliScanner = new Scanner(System.in);
    private static PrintStream cliWriter = System.out;

    private CLIUtils() {
    }

    /**
     * Waits for an integer user from the user and iterates until the user provides a valid value, depending on the
     * provided paramenters.
     *
     * @param greaterThan  The lower bound of the expected value (excluded).
     * @param lowerThan    The upper bound of the expected value (excluded).
     * @param errorMessage The error message shown when the user provides a non-valid value.
     * @return The final valid value provided by the user.
     */
    public static int requestBoundedInt(int greaterThan, int lowerThan, String errorMessage) {
        boolean isInputInvalid = true;
        int returnInt = 0;
        try {
            returnInt = cliScanner.nextInt();
            if (returnInt > greaterThan && returnInt < lowerThan)
                isInputInvalid = false;
        } catch (InputMismatchException e) {
            Logger.getGlobal().log(Level.INFO, e.getMessage(), e);
            isInputInvalid = true;
        }

        while (isInputInvalid) {
            cliWriter.println(errorMessage);
            cliScanner.nextLine();
            try {
                returnInt = cliScanner.nextInt();
            } catch (InputMismatchException e) {
                Logger.getGlobal().log(Level.INFO, e.getMessage(), e);
                continue;
            }
            if (returnInt > greaterThan && returnInt < lowerThan)
                isInputInvalid = false;
        }
        return returnInt;
    }

    /**
     * Asks the user to choose one of the strings provided in the "options" parameter, or optionally to enter one of
     * the commands provided among the "escapes" ones. Prompts the user with an error message until he enters a valid
     * option.
     *
     * @param options      A list of selectable options.
     * @param errorMessage The error message shown when the user enters a non-valid value.
     * @param escapes      Optional commands that are also valid.
     * @return The final valid option selected by the user (one from either options or escapes).
     */
    public static String requestSelection(String[] options, String errorMessage, String... escapes) {
        cliScanner = new Scanner(System.in);

        while (true) {
            String userAttempt = cliScanner.nextLine();
            if (Arrays.asList(options).contains(userAttempt) || escapes != null && Arrays.asList(escapes).contains(userAttempt))
                return userAttempt;
            else {
                cliWriter.println(errorMessage);
                printStringCollection(Arrays.asList(options));
            }
        }
    }

    /**
     * Asks the user to choose among one of the shown options, but listens for all the inputs stored as the key set of
     * the provided map and returns the associated object when one is entered. Shows an error message if the input is
     * invalid, and calls {@link Cancelable#cancelWaiting()} if the user wants to abort.
     *
     * @param shownOptions The options shows (different from valid inputs)
     * @param errorMessage The message shown when the input is not in the key set of the map
     * @param caller       The reference to the Cancelable thread
     * @param map          The map providing 1:1 match between input and object to return
     * @param <T>
     * @return The object chosen from the user, if this has not been interrupted before.
     */
    public static <T> T requestMappedSelection(String[] shownOptions, String errorMessage, Cancelable caller, Map<String, T> map) {
        cliScanner = new Scanner(System.in);
        boolean inputEnabled = true;

        printStringCollection(Arrays.asList(shownOptions));

        while (inputEnabled) {
            try {
                String selection = breakableNextLine(cliScanner);

                if ("end".equals(selection)) {
                    caller.cancelWaiting();
                    return null;
                }
                boolean chat = consumeChatMessage(selection);
                if (!chat && map.keySet().contains(selection.toUpperCase()))
                    return map.get(selection.toUpperCase());
                else if (!chat) {
                    cliWriter.println(errorMessage);
                    printStringCollection(Arrays.asList(shownOptions));
                }
            } catch (InterruptedException e) {
                inputEnabled = false;
                Thread.currentThread().interrupt();
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            }

        }
        return null;
    }


    /*private static String retrieveChatEnd(String[] shownOptions, String errorMessage, Cancelable caller, Map<String, T> map) throws InterruptedException {
        try {
            if (System.in.available() > 0) {
                String userAttempt = cliScanner.nextLine();
                return userAttempt;

            } else {
                Thread.sleep(100);
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }
        return null;
    }*/

    /**
     * Builds an input-action map to use with {@link CLIUtils#requestMappedSelection(String[], String, Cancelable, Map)}
     *
     * @param main      Whether to include main actions or not
     * @param secondary Whether to include secondary actions or not
     * @return The map that relates inputs to actions
     */
    public static Map<String, Action> buildActionsMap(boolean main, boolean secondary) {
        Map<String, Action> mapToReturn = new HashMap<>();
        for (int i = 0; i < Action.values().length - 1; i++) {
            if (Action.values()[i].isMain() && main || !Action.values()[i].isMain() && secondary && checkSecondaryActionsPermitted(Action.values()[i])) {
                mapToReturn.put(String.valueOf(Action.values()[i].getIndex()), Action.values()[i]);
                mapToReturn.put(Action.values()[i].name(), Action.values()[i]);
            }
        }
        return mapToReturn;
    }

    public static boolean checkSecondaryActionsPermitted(Action actionToCheck) {
        switch (actionToCheck) {

            case ACQUIRE_HELPER:
                return Client.getClientLogicLayer().getThisPlayer().getCoins() >= 3;
            case SWAP_PERMIT:
            case HELPER_ELECT:
                return !Client.getClientLogicLayer().getThisPlayer().getHelpers().isEmpty();
            case ADDITIONAL_MAIN:
                return Client.getClientLogicLayer().getThisPlayer().getHelpers().size() >= 3;

            default:
                return false;
        }
    }

    /**
     * Builds an input-field map to use with {@link CLIUtils#requestMappedSelection(String[], String, Cancelable, Map)}
     *
     * @param includeKing Whether to make the king council selectable or not
     * @return The map that relates inputs to fields
     **/
    public static Map<String, Field> buildCouncilsMap(boolean includeKing) {
        Map<String, Field> mapToReturn = new HashMap<>();
        for (int i = 0; i < Field.values().length; i++) {
            if (Field.values()[i] == Field.KING && includeKing || Field.values()[i] != Field.KING) {
                mapToReturn.put(String.valueOf(i + 1), Field.values()[i]);
                mapToReturn.put(Field.values()[i].name(), Field.values()[i]);
            }
        }
        return mapToReturn;
    }

    /**
     * Buiilds an integer-object map to use with {@link CLIUtils#requestMappedSelection(String[], String, Cancelable, Map)}.
     * This is used for long lists, when the user can only enter indexes of the choices.
     *
     * @param objectToChoose Selectable items
     * @return The map that relates integers to objects
     */
    public static <T> Map<String, T> buildGenericNumberedMap(List<T> objectToChoose) {
        Map<String, T> mapToReturn = new HashMap<>();
        for (int i = 0; i < objectToChoose.size(); i++)
            mapToReturn.put(String.valueOf(i + 1), objectToChoose.get(i));

        return mapToReturn;
    }

    /**
     * Builds an input-politicsColor map to use with {@link CLIUtils#requestMappedSelection(String[], String, Cancelable, Map)}
     *
     * @param includeJolly Whether to make the jolly colour selectable or not
     * @return The map that relates inputs to colors
     **/
    public static Map<String, PoliticsColor> buildPoliticsColorMap(boolean includeJolly) {
        Map<String, PoliticsColor> mapToReturn = new HashMap<>();
        for (int i = 0; i < PoliticsColor.values().length; i++) {
            if (PoliticsColor.values()[i] == PoliticsColor.JOLLY && includeJolly || PoliticsColor.values()[i] != PoliticsColor.JOLLY) {
                mapToReturn.put(String.valueOf(i + 1), PoliticsColor.values()[i]);
                mapToReturn.put(PoliticsColor.values()[i].name(), PoliticsColor.values()[i]);
            }
        }
        return mapToReturn;
    }

    /**
     * Builds an input-towns map to use with {@link CLIUtils#requestMappedSelection(String[], String, Cancelable, Map)}
     *
     * @param towns The list of selectable towns
     * @return The map that relates inputs to towns
     **/
    public static Map<String, Town> buildTownMap(List<Town> towns) {
        Map<String, Town> mapToReturn = new HashMap<>();
        for (int i = 0; i < towns.size(); i++) {
            Town town = towns.get(i);
            mapToReturn.put(String.valueOf(i + 1), town);
            mapToReturn.put(town.getName().toLowerCase(), town);

        }

        return mapToReturn;
    }

    /**
     * Converts a list of objects in a corresponding array that, for each item, has a string with an integer prefix
     * of the kind "[1]" and a string that is the toString() call of that object.
     *
     * @param list A list of object having a valid toString() override
     * @return An array containing items to string with a numbered prefix
     */
    public static String[] getOptionsStringList(List list) {
        String[] result = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            result[i] = String.format("[%d] %s", i + 1, list.get(i).toString());


        return result;
    }

    /**
     * Prints a formatted list of strings. Shows "< no items >" if empty.
     *
     * @param collectionToPrint The list of values to show to the user.
     */

    public static void printStringCollection(Collection<String> collectionToPrint) {
        if (!collectionToPrint.isEmpty())
            for (String s : collectionToPrint) {
                cliWriter.println(">\t" + s);
            }
        else cliWriter.println(">\t< No items >");
    }

    /**
     * When input starts with chat, we send a proper JSON update and we return whether this happened or not.
     *
     * @param input User input
     * @return Did the input contain a chat message or not?
     */
    public static boolean consumeChatMessage(String input) {
        if (input.startsWith("chat")) {
            Client.getNetworkManager().remoteChatUpdate(JSONHelper.ChatParser.toJsonChatUpdate(Client.getUsername(), input.substring(5, input.length())));
            return true;
        } else return false;

    }

    /**
     * Used to generate ASCII arts depending on the game phase
     *
     * @param phase The phase we are entering
     */
    public static void printTag(Phase phase, CLIBridge cliBridge) {
        PrintStream cliWriter = cliBridge.getWriter();
        synchronized (cliBridge.getWriter()) {
            switch (phase) {
                case PREGAME:
                    cliWriter.println(PresentationConstants.Tags.TAG_PREGAME);
                    break;
                case GAME:
                    cliWriter.println(PresentationConstants.Tags.TAG_GAME);
                    break;
                case MARKET_SELL:
                    cliWriter.println(PresentationConstants.Tags.TAG_MARKET_SELL);
                    break;
                case MARKET_BUY:
                    cliWriter.println(PresentationConstants.Tags.TAG_MARKET_BUY);
                    break;
                case END_GAME:
                    cliWriter.println(Client.getClientLogicLayer().getCurrentPlayerUsername().equals(Client.getUsername()) ?
                            PresentationConstants.Tags.TAG_ENDGAME_WON :
                            String.format(PresentationConstants.Tags.TAG_ENDGAME_LOSE, Client.getClientLogicLayer().getCurrentPlayerUsername()));
                    break;
                default:
                    break;

            }

        }
    }

    public static int newBonusStatus(IBonus bonus, Player player) {
        switch (bonus.getElementsType()) {
            case WINSTEPS:
                return player.getWinPosition();
            case NOBSTEPS:
                return player.getNobilityPosition();
            case HELPERS:
                return player.getHelpers().size();
            case CARDS:
                return player.getPoliticsCard().size();
            case COINS:
                return player.getCoins();
            case SUPPACTION:
                return Client.getClientLogicLayer().getRemainingMainActions();
            default:
                return 0;
        }
    }

    /**
     * This is a partial wrapping of the standard {@link Scanner#nextLine()} method to make it interruptable.
     * The if/else part on the input is done because nextLine() is a blocking instruction and cannot be interrupted
     * once started: so if we are blocked waiting for input and we need to interrupt the thread, we won't be able to do
     * so unless nextLine() catches some input from System.in (that is not closeable).
     * Thread.sleep(100) is used to avoid this method in a cycle to take a lot of CPU %, since otherwise it would be
     * for most of the time just a while(false) cycle that is extremely heavy.
     *
     * @return The string entered from the user, if this has not been interrupted before.
     * @throws InterruptedException When we are waiting for input and the enclosing thread is interrupted.
     */
    public static String breakableNextLine(Scanner cliScanner) throws InterruptedException {
        String choice = "";
        boolean inputEnabled = true;
        while (inputEnabled) {
            try {
                if (System.in.available() > 0) {
                    choice = cliScanner.nextLine();
                    inputEnabled = false;
                } else Thread.sleep(100);
            } catch (IOException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                break;
            }

        }
        return choice;
    }
}
