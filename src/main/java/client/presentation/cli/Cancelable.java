package client.presentation.cli;

@FunctionalInterface
public interface Cancelable {

    /**
     * Called to resume a CLI thread that is stuck waiting for user input.
     */
    void cancelWaiting();
}
