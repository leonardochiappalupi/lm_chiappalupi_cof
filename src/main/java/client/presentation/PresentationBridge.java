package client.presentation;


import client.Client;
import client.network.NetworkManager;
import game.bonus.IBonus;
import game.logic.Phase;
import game.logic.Player;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.market.ItemToSell;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class PresentationBridge implements Pick {

    public void remoteChatUpdate(String jsonUpdate) {
        Client.getNetworkManager().remoteChatUpdate(jsonUpdate);
    }

    public static String[] availableConfigurations() {
        File gameRoot = new File("./configurations");
        File[] files = gameRoot.listFiles();
        if (files != null) {
            String[] names = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                names[i] = files[i].getName();
            }
            return names;
        }
        return new String[]{};
    }

    /**
     * Method called by {@link client.Client} to launch the initialization flow. It should at least call
     * {@link #login()} and  {@link NetworkManager#amILeader()}
     */
    public abstract void startLobbyLogin();


    /**
     * Applies a chat update in JSON format to local client and updates the view
     *
     * @param jsonUpdate JSON Formatted update string
     */
    public abstract void localChatUpdate(String jsonUpdate);

    /**
     * Applies a log update in JSON format to local client and updates the view
     *
     * @param jsonUpdate JSON Formatted update string
     */
    public abstract void localLogUpdate(String jsonUpdate);

    /**
     * Applies a lobby update in JSON format to local client and updates the view
     *
     * @param jsonUpdate JSON Formatted update string
     */
    public abstract void localLobbyUpdate(String jsonUpdate);


    /**
     * Asks the user for an username and check whether it is available or not. When a valid username is found,
     * the server will check-in the user and the {@link client.network.NetworkManager} instance will be updated with the
     * final username.
     */
    public abstract void login();

    /**
     * Callback from a {@link RulesBroker} when an action has been completed successfully.
     *
     * @param completedAction The action that has completed successfully.
     */
    public abstract void onActionCompleted(Action completedAction);

    /**
     * Called whenever the user gets the benefits of a bonus. Should only be called when using {@link game.logic.flows.BonusProcessor}.
     * Used in concrete classes to notify the user visually about the bonus just obtained.
     *
     * @param bonus   The bonus object containing {@link game.bonus.BonusType} and quantity.
     * @param context The action during which the bonus is being processsed.
     * @param player  The player that received the bonus
     */
    public abstract void onBonusProcessed(IBonus bonus, Action context, Player player);

    public abstract void configuratorCallback();

    public static String getRequestedJsonConfiguration(String jsonFileToLoad) {
        String jsonConfigurationString = null;
        try {
            JSONParser jsonParser = new JSONParser();
            FileReader fileReader = new FileReader("./configurations/" + jsonFileToLoad);
            JSONObject jsonConfiguration = (JSONObject) jsonParser.parse(fileReader);
            jsonConfigurationString = jsonConfiguration.toString();
            fileReader.close();
        } catch (IOException | ParseException e) {
            Logger.getGlobal().log(Level.INFO, e.getMessage(), e);
        }

        return jsonConfigurationString;
    }

    /**
     * Called when is this player' turn, reporting the phase (PRE_GAME, GAME, MARKET_SELL, MARKET_BUY)
     *
     * @param phase The current phase
     */
    public abstract void onPlayerEnabled(Phase phase);

    /**
     * Called when this player' turn has ended by a timeout or by disconnection
     */
    public abstract void onPlayerDisabled();

    /**
     * Called when the server ended configuring this client and it's now possible to start the game
     */
    public abstract void startGame();

    /**
     * Updates available items (councilors in the backup, helpers, etc.)
     */
    public abstract void updateBoardItems();

    /**
     * Updates player owned items (cards, helpers, coins, etc.)
     */
    public abstract void updatePlayersItems();

    /**
     * Updates other players' items and state (offline, win steps, etc.)
     */
    public abstract void updateOtherPlayers();

    /**
     * Updates emporiums in towns
     */
    public abstract void updateEmporiums();

    /**
     * Updates all councils, including king one
     */
    public abstract void updateCouncils();

    /**
     * Updates left and right shown permit cards, field bonus tiles
     */
    public abstract void updateFieldsCards();

    /**
     * Updates nobility route, town color bonus cards, king bonus tiles
     */
    public abstract void updateKingBoard();

    /**
     * Updates king town indicator
     */
    public abstract void updateKingTownShown();

    /**
     * Closes lobby phase and start loading the game
     */
    public abstract void startLoading();

    /**
     * Callback from server when a market item has been bought
     *
     * @param item The bought item
     */
    public abstract void onItemBought(ItemToSell item);

    /**
     * Notifies that the enabled player has changed
     */
    public abstract void onEnabledChanged(Phase phase, String newPlayerUsername);

    /**
     * Visual feedback for time-related turn events
     */
    public abstract void turnTimeout(int timeRemaining);

}