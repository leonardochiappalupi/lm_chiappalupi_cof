package client.presentation.gui;


import client.Client;
import client.presentation.PresentationBridge;
import constants.JsonConstants;
import game.board.Field;
import game.bonus.DirectBonus;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.logic.Phase;
import game.logic.Pickables;
import game.logic.Player;
import game.logic.actions.Action;
import game.market.ItemToSell;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;
import org.json.JSONObject;
import util.JSONElementNotFoundException;
import util.JSONHelper;
import view.game.GameApplet;
import view.lobby.LobbyController;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The GUI implementation of the presentation layer. This class uses specific JavaFX methods and controllers to get the
 * work done; it mirrors almost all the calls for updates by calling relative refreshing methods on controllers of the
 * main JavaFX Application. This class is also in charge of managing some of the sound effects in the game.
 */
public class GUIBridge extends PresentationBridge {

    private LobbyLogin lobbyLoginApp;
    private ObservableList<String> lobbyUsers = FXCollections.observableArrayList();
    private ObservableList<String> chatMessages = FXCollections.observableArrayList();
    private GameApplet gameJavaFXApplication;
    private static boolean musicEnabled;

    public GUIBridge(boolean music) {
        musicEnabled = music;
    }

    public ObservableList<String> getChatMessages() {
        return chatMessages;
    }

    public ObservableList<String> getLobbyUsers() {
        return lobbyUsers;
    }

    public static boolean isMusicEnabled() {
        return musicEnabled;
    }

    @Override
    public void localChatUpdate(String jsonUpdate) {

        try {
            String author = JSONHelper.ChatParser.authorFromJsonChatUpdate(jsonUpdate);
            String message = JSONHelper.ChatParser.messageFromJsonChatUpdate(jsonUpdate);
            Platform.runLater(() -> chatMessages.add(String.format("%s wrote: %s", author, message)));
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.INFO, e.getMessage(), e);
        }

    }

    @Override
    public void localLogUpdate(String jsonUpdate) {
        //TO IMPLEMENT
    }

    @Override
    public void localLobbyUpdate(String jsonUpdate) {
        JSONObject lobbyUpdate = new JSONObject(jsonUpdate).getJSONObject(JsonConstants.Lobby.LOBBY_ELEMENT_TAG);
        String roomName = null;
        String gameLeader = null;
        Integer maxPlayers = null;
        Integer timeRemaining = null;

        try {
            roomName = JSONHelper.LobbyParser.roomNameFromJsonLobbyUpdate(jsonUpdate);
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }

        try {
            maxPlayers = JSONHelper.LobbyParser.maxUsersFromJsonLobbyUpdate(jsonUpdate);
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }

        try {
            gameLeader = JSONHelper.LobbyParser.leaderFromJsonLobbyUpdate(jsonUpdate);
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }
        try {
            timeRemaining = JSONHelper.LobbyParser.timeoutFromJsonLobbyUpdate(jsonUpdate);
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }

        lobbyLoginApp.getController().updateLobby(gameLeader, timeRemaining, maxPlayers);
        Platform.runLater(() -> {
            lobbyUsers.clear();

            try {
                lobbyUsers.addAll(JSONHelper.LobbyParser.LobbyUsersFromJsonLobbyUpdate.toArrayList(jsonUpdate));
            } catch (JSONElementNotFoundException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
        });


    }

    public void lobby(boolean isRoomLeader) {
        if (!isRoomLeader) {
            Client.getNetworkManager().getGameLeader();
        }
    }

    @Override
    public void startLobbyLogin() {
        login();
    }

    @Override
    public void login() {
        try {
            lobbyLoginApp = new LobbyLogin();
            lobbyLoginApp.start(new Stage());
        } catch (Exception e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void configuratorCallback() {
        lobbyLoginApp.controller.refreshConfigurationsList();
    }

    @Override
    public void onPlayerEnabled(Phase phase) {
        switch (phase) {
            case GAME:
                gameJavaFXApplication.getGameController().yourTurn("your");
                gameJavaFXApplication.getActionsController().setDrawEnabled(true);
                gameJavaFXApplication.getActionsController().setEndTurnEnabled(true);
                break;
            case MARKET_SELL:
                gameJavaFXApplication.getGameController().showMarketSell();
                break;
            case MARKET_BUY:
                gameJavaFXApplication.getGameController().showMarketBuy();
                break;
            default:
                break;
        }

    }

    @Override
    public void onPlayerDisabled() {

        gameJavaFXApplication.getGameController().hideChooser();
        gameJavaFXApplication.getActionsController().setAllEnabled(false);

        if (musicEnabled) {
            AudioClip endTurn = new AudioClip(getClass().getResource("/music/endturn.mp3").toExternalForm());
            endTurn.play();
        }
    }


    @Override
    public void onActionCompleted(Action completedAction) {
        gameJavaFXApplication.getActionsController().refreshAllowedActions();
        setWorkingState(false);
    }

    @Override
    public void onBonusProcessed(IBonus bonus, Action context, Player player) {
        if (bonus instanceof DirectBonus)
            gameJavaFXApplication.getBonusController().showNewBonus((DirectBonus) bonus, context, player);
    }

    @Override
    public void pickACouncil(Action action, boolean includeKing) throws RemoteException {
        gameJavaFXApplication.getGameController().showChooser(Pickables.ONE_COUNCIL, includeKing);
    }

    @Override
    public void pickACouncilor(Action action) throws RemoteException {
        gameJavaFXApplication.getGameController().showChooser(Pickables.ONE_COUNCILOR, null);
    }

    @Override
    public void pickUpTo4PoliticsCards(Action action) throws RemoteException {
        gameJavaFXApplication.getGameController().showChooser(Pickables.UPTOFOUR_PC, null);
    }

    @Override
    public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {
        gameJavaFXApplication.getGameController().showChooser(Pickables.ONE_FIELD_PERMIT, field);
    }

    @Override
    public void pickYourPermissionCard(Action action) throws RemoteException {
        if (action == Action.BONUS)
            gameJavaFXApplication.getGameController().showChooser(Pickables.ONE_YOURS_PERMIT, true);
        else
            gameJavaFXApplication.getGameController().showChooser(Pickables.ONE_YOURS_PERMIT, false);
    }

    @Override
    public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {
        gameJavaFXApplication.getGameController().showChooser(Pickables.ONE_PERMIT_TOWN, card);
    }

    @Override
    public void pickTownsToMoveKing(Action action) throws RemoteException {
        gameJavaFXApplication.getGameController().showChooser(Pickables.KING_TOWNS, null);
    }

    @Override
    public void pickEmporiumTown(Action action) throws RemoteException {
        gameJavaFXApplication.getGameController().showChooser(Pickables.EMPORIUM_TOWN, null);
    }

    /**
     * The JavaFX Application for the lobby stage.
     */
    private class LobbyLogin extends Application {
        LobbyController controller = new LobbyController(GUIBridge.this);
        Stage primaryStage;

        @Override
        public void start(Stage primaryStage) throws Exception {

            this.primaryStage = primaryStage;
            primaryStage.setOnCloseRequest(event -> System.exit(0));
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/lobby.fxml"));
            loader.setController(controller);
            Parent root = loader.load();
            primaryStage.setTitle("Council of Four Configuration");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.sizeToScene();
            primaryStage.show();

        }

        public void close() {
            primaryStage.setOnCloseRequest(null);
            Platform.runLater(() -> primaryStage.close());

        }

        public LobbyController getController() {
            return controller;
        }

    }

    /**
     * Called by the login button of the lobby controller.
     *
     * @param username The username entered in the text field
     * @return Whether the username provided is available or not.
     */
    public boolean validateUsername(String username) {
        boolean isUsernameAvailable = Client.getNetworkManager().checkUsernameAvailability(username);
        if (isUsernameAvailable) {
            Client.getNetworkManager().createUser(username);
            Client.setUsername(username);
            return true;
        } else {
            return false;
        }
    }

    public boolean amILeader() {
        return Client.getNetworkManager().amILeader();
    }


    public boolean configureGame(Integer maxUsers, Integer timeout, Integer turnTimeout, String jsonConfiguration) {
        return Client.getNetworkManager().createGameForUser(maxUsers, timeout, turnTimeout, jsonConfiguration);
    }

    @Override
    public void startGame() {
        gameJavaFXApplication.showMainGui();
    }

    @Override
    public void updateBoardItems() {
        gameJavaFXApplication.getBoardAvailableController().update();

    }

    @Override
    public void updatePlayersItems() {
        gameJavaFXApplication.getPlayerItemsController().update();
    }

    @Override
    public void updateOtherPlayers() {
        gameJavaFXApplication.getSidebarController().update();
    }

    @Override
    public void updateEmporiums() {

        gameJavaFXApplication.getTownsController().updateEmporiums();
        gameJavaFXApplication.getBoardController().updateFieldBonusTiles();
        gameJavaFXApplication.getBoardController().updateKingBonusTiles();
    }

    @Override
    public void updateCouncils() {
        gameJavaFXApplication.getBoardController().updateFieldsCouncilors();
        gameJavaFXApplication.getBoardController().updateKingCouncilors();
    }

    @Override
    public void updateFieldsCards() {
        gameJavaFXApplication.getBoardController().updateShownPermissionCards();
        gameJavaFXApplication.getBoardController().updateFieldBonusTiles();
    }

    @Override
    public void updateKingBoard() {
        gameJavaFXApplication.getBoardController().updateNobilityRoute();
        gameJavaFXApplication.getBoardController().updateTownBonusTiles();
        gameJavaFXApplication.getBoardController().updateKingBonusTiles();
    }

    @Override
    public void updateKingTownShown() {
        gameJavaFXApplication.getTownsController().updateKingPosition();
    }

    @Override
    public void startLoading() {
        gameJavaFXApplication = new GameApplet();
        Platform.runLater(() -> gameJavaFXApplication.start(new Stage()));
        lobbyLoginApp.close();
    }

    @Override
    public void onItemBought(ItemToSell item) {
        gameJavaFXApplication.getGameController().getMarketBuyController().markItemAsSold(item);
    }

    @Override
    public void onEnabledChanged(Phase phase, String newPlayerUsername) {
        if (phase != Phase.END_GAME) {
            if (!newPlayerUsername.equals(Client.getUsername())) {
                gameJavaFXApplication.getGameController().yourTurn(newPlayerUsername);
            }
        } else {
            gameJavaFXApplication.getGameController().endGame(newPlayerUsername.equals(Client.getUsername()));
        }
    }

    @Override
    public void turnTimeout(int timeRemaining) {
        /**For now, just empty**/
    }

    /**
     * Callback called when the JavaFX Application of the main game gui has ended loading.
     */
    public void onAppletReady() {
        Client.getNetworkManager().onClientReady();
    }

    /**
     * Shows a "working" progress and disables commands
     *
     * @param isWorking Whether to enable or disable the prompt
     */
    public void setWorkingState(boolean isWorking) {
        gameJavaFXApplication.setWorking(isWorking);
    }

}
