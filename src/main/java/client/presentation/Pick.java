package client.presentation;

import game.board.Field;
import game.cards.BuildingPermissionCard;
import game.logic.actions.Action;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * These methods are invocated whenever the game expects the user to pick something to proceed in a
 * {@link game.logic.flows.Flow}
 */
public interface Pick extends Remote {

    /**
     * Pick one of the councils, optionally including king' one.
     * @param action Context of the pick request
     * @param includeKing Whether to make the king council selectable or not.
     */
    void pickACouncil(Action action, boolean includeKing) throws RemoteException;

    /**
     * Pick a councilor (that is, pick one of the politics colors excluding jolly)
     * @param action Context of the pick request
     */
    void pickACouncilor(Action action) throws RemoteException;

    /**
     * Pick at most 4 politics card to corrupt a council. This must provide at most 4 cards, and these cards must
     * be available by the user. The implementation may not check the actual card validity.
     * @param action Context of the pick request
     */
    void pickUpTo4PoliticsCards(Action action) throws RemoteException;

    /**
     * Pick one of the two shown permission cards of a field.
     * @param field The field to get the cards from
     * @param action Context of the pick request
     */
    void pickFieldPermissionCard(Field field, Action action) throws RemoteException;

    /**
     * Pick one of your permission cards.
     * @param action Context of the pick request
     */
    void pickYourPermissionCard(Action action) throws RemoteException;

    /**
     * Pick one of the towns whose names are on a specific building permission card.
     * @param card The permission card whose towns are selectable
     * @param action Context of the pick request
     */
    void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException;

    /**
     * Pick a series of town to move the king one after another. The implementation may not check if the user has enough
     * coins to perform the entire path.
     * @param action Context of the pick request
     */
    void pickTownsToMoveKing(Action action) throws RemoteException;

    /**
     * Pick a town among the ones the user has an emporium into.
     * @param action Context of the pick request
     */
    void pickEmporiumTown(Action action) throws RemoteException;
}
