package server;

import server.persistent.RMIServer;
import server.persistent.SocketServer;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.util.ArrayList;


public class Server {
    private ArrayList<GameRoom> rooms = new ArrayList<>();

    /**
     * Returns the first room that has just been created ({@link RoomState#WAITING}) or that is waiting for players
     * ({@link RoomState#TIMEOUT}), and creates a new one if the rooms list is empty.
     *
     * @return First available {@link GameRoom} or newly created one.
     */
    public GameRoom getAvailableRoom() {

        for (GameRoom room : rooms) {
            if (room.getRoomState() == RoomState.WAITING || room.getRoomState() == RoomState.TIMEOUT)
                return room;
        }
        GameRoom newRoom = new GameRoom("room" + rooms.size());
        rooms.add(newRoom);
        return newRoom;
    }

    /**
     * Checks if the desired username is already in use inside any of the game rooms opened so far.
     *
     * @param username The username to check
     * @return True if the username is not in use across the entire server.
     */
    public boolean checkUsernameAvailability(String username) {
        for (GameRoom gr : rooms) {
            if (!gr.checkUsernameAvailability(username))
                return false;
        }
        return true;
    }

    /**
     * One-stop launcher to startLobbyLogin the entire server. Use this main to create a new server instance before launching
     * the clients.
     *
     * @throws IOException
     * @throws AlreadyBoundException
     */
    public static void main(String[] args) throws IOException, AlreadyBoundException {
        new Server().launch();
    }

    private void launch() throws IOException {
        new RMIServer(this).launch();
        new SocketServer(this).launch();
    }
}
