package server;

public enum RoomState {
    NEW, WAITING, TIMEOUT, PLAYING
}
