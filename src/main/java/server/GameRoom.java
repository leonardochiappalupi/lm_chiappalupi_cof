package server;

import client.GameCommands;
import constants.ConfigurationConstants;
import game.Configuration;
import game.GameState;
import game.board.Map;
import game.board.Town;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.logic.Phase;
import game.logic.Player;
import game.logic.RulesBroker;
import game.market.ItemToSell;
import game.market.Market;
import game.pawns.Emporium;
import server.entities.ConnectionLostException;
import server.entities.RemotePlayer;
import util.JSONElementNotFoundException;
import util.JSONHelper;
import view.MyColor;

import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a "room" for hosting a game session. It includes methods to manage users and handle the various
 * phases of the game flow, from creating and adding players to the actual game execution.
 */

public class GameRoom extends Thread {
    private int maxUsersAllowed;
    private String roomId;
    private ArrayList<RemotePlayer> players = new ArrayList<>();
    private RemotePlayer gameLeader;
    private Configuration gameConfiguration;
    private RoomState roomState;
    private Phase roomPhase;
    private int roomTimeout;
    private int turnTimeout;
    private Timer lobbyTimer;
    private Timer turnTimer;
    private String currentPlayerUsername;
    private final Object mainThreadLock = new Object();
    private RulesBroker rulesBroker = new RulesBroker(this);
    private int playersInitialized = 0;
    private boolean initialized = false;

    /**
     * Initializes a new room with the {@link RoomState#NEW} phase tag.
     *
     * @param id The id (name) for the new room
     */
    public GameRoom(String id) {
        roomState = RoomState.NEW;
        this.roomId = id;
        Logger.getGlobal().info(String.format("[GAMEROOM (id: %s)]\tThis room has been initialized and is waiting to be configured", roomId));

    }

    public Phase getRoomPhase() {
        return roomPhase;
    }

    public void setGameConfiguration(String jsonGameConfiguration) {
        try {
            this.gameConfiguration = JSONHelper.ConfigurationParser.configurationFromJSonConfig(jsonGameConfiguration);
        } catch (JSONElementNotFoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    public RoomState getRoomState() {
        return roomState;
    }

    public void setRoomState(RoomState roomState) {
        this.roomState = roomState;
    }

    /**
     * Sets the timeout (in seconds) after which the room will not accept new players and the game will start.
     *
     * @param timeout Timeout in seconds.
     */
    public void setTimeout(int timeout) {
        this.roomTimeout = timeout;
    }

    public int getTimeout() {
        return this.roomTimeout;
    }

    public void setTurnTimeout(int timeout) {
        this.turnTimeout = timeout;
    }

    public int getTurnTimeout() {
        return turnTimeout;
    }

    public void setMaxUsersAllowed(int maxUsersAllowed) {
        this.maxUsersAllowed = maxUsersAllowed;
    }

    public int getMaxUsersAllowed() {
        return maxUsersAllowed;
    }


    /**
     * The actual game logic to start after the lobby session has ended.
     */
    @Override
    public void run() {
        new Thread(this::gameSessionInitialization).start();
        try {
            while (!initialized) {
                synchronized (mainThreadLock) {
                    mainThreadLock.wait();
                }
            }
            roomPhase = Phase.GAME;
            while (roomPhase == Phase.GAME) {
                gameLoop();
                marketSellLoop();
                marketBuyLoop();
            }
        } catch (InterruptedException e) {
            this.interrupt();
            Logger.getGlobal().log(Level.WARNING, String.format("[GAMEROOM (id: %s)]\tThis room has been terminated", roomId));
        }
    }

    private void gameSessionInitialization() {
        rulesBroker.setMainLocalState(gamePrepare(gameConfiguration));
        rulesBroker.markCurrentStateAsValid();
        for (RemotePlayer rp : players) {
            rp.startConfiguration();
            rp.setupGame(gameConfiguration, players.size());
            rp.startGame();
        }
    }

    public String getRoomId() {
        return roomId;
    }

    /**
     * Adds a {@link RemotePlayer} instance to this room users list. If after the addition the room has reached its
     * limits, this methods also starts the actual game flow.
     * If we are adding the second player of the room, the methods starts the timeout countdown by calling
     * {@link #startLobbyCountdown()}
     *
     * @param player The {@link RemotePlayer} instance to add to the players list
     */
    public void addPlayer(RemotePlayer player) {
        if (players.isEmpty()) {
            gameLeader = player;
            roomState = RoomState.WAITING;
        }
        players.add(player);

        player.dispatchLobbyUpdate(JSONHelper.LobbyParser.toJsonLobbyUpdate(gameLeader.getUsername(), roomId, maxUsersAllowed, roomTimeout, toUsersArray(players)));

        if (players.size() >= 2 && roomState == RoomState.WAITING) {
            roomState = RoomState.TIMEOUT;
            Logger.getGlobal().info(String.format("[GAMEROOM (id: %s)]\tThis room has reached at least 2 players: starting countdown from %d seconds", roomId, roomTimeout));
            startLobbyCountdown();
        }

        if (players.size() == maxUsersAllowed) {
            lobbyTimer.cancel();
            Logger.getGlobal().info(String.format("[GAMEROOM (id: %s)]\tThis room has reached the maximum number of players (%d) and will now start its game...", roomId, maxUsersAllowed));
            publishLogMessage(JSONHelper.LogParser.toJsonLogUpdate("Maximum number of players reached. Game will start shortly..."));
            roomState = RoomState.PLAYING;
            this.start();
        }

        Logger.getGlobal().info(String.format("[GAMEROOM (id: %s)]\tPlayer with username: %s has joined this room", roomId, player.getUsername()));
    }

    /**
     * Checks if one of the players of this room uses the provided username
     *
     * @param username The username to check
     * @return True if the username is available.
     */
    public boolean checkUsernameAvailability(String username) {
        for (RemotePlayer p : players) {
            if (p.getUsername().equals(username))
                return false;
        }
        return true;
    }

    public RemotePlayer getGameLeader() {
        return gameLeader;
    }

    /**
     * Sends a chat update to all the players of this room
     *
     * @param jsonChatElement Json formatted chat update
     */
    public void publishChatMessage(String jsonChatElement) {
        for (RemotePlayer rp : players) {
            rp.dispatchChatMessage(jsonChatElement);
        }
    }

    /**
     * Sends a log update to all the players of this room, excluding the ones passed as parameters
     *
     * @param jsonLogMessage   Json formatted log update
     * @param playersToExclude Players that will NOT receive the log update
     */
    public void publishLogMessageExcludingPlayers(String jsonLogMessage, RemotePlayer... playersToExclude) {
        for (RemotePlayer rp : players) {
            boolean isExcluded = false;
            for (RemotePlayer playerToExclude : playersToExclude)
                if (rp.getUsername().equals(playerToExclude.getUsername()))
                    isExcluded = true;
            if (!isExcluded)
                rp.dispatchLogMessage(jsonLogMessage);

        }
    }

    /**
     * Sends a log update to all the players of this room
     *
     * @param jsonLogMessage Json formatted log update
     */
    private void publishLogMessage(String jsonLogMessage) {
        for (RemotePlayer rp : players) {
            if (!rp.isDisconnected())
                rp.dispatchLogMessage(jsonLogMessage);
        }
    }

    private void publishGamePhase() {
        for (RemotePlayer rp : players) {
            if (!rp.isDisconnected())
                rp.changePhase(roomPhase, currentPlayerUsername);
        }
    }


    /**
     * Returns an array of strings containing the usernames of the players of this room
     *
     * @param players List of players
     * @return Array of usernames strings
     */
    private String[] toUsersArray(ArrayList<RemotePlayer> players) {
        String[] users = new String[players.size()];
        for (int i = 0; i < players.size(); i++)
            users[i] = players.get(i).getUsername();

        return users;

    }


    private void startLobbyCountdown() {
        lobbyTimer = new Timer();
        lobbyTimer.scheduleAtFixedRate(new LobbyTimer(), 0, 1000);
    }

    private void startTurnCountdown() {
        turnTimer = new Timer();
        turnTimer.scheduleAtFixedRate(new TurnTimer(), 0, 1000);
    }

    /**
     * Creates the initial game state based on the uploaded configuration object. Sets all players to disabled and first
     * player to enabled (using {@link Player#enabled}). Sends this initial state to every remote player.
     * Sets {@link #currentPlayerUsername} to the relative value.
     *
     * @param configuration The configuration object which the initial game state should be built upon.
     * @return The initial game state (for saving purposes).
     */
    private GameState gamePrepare(Configuration configuration) {
        int numberOfPlayers = players.size();
        if (numberOfPlayers == 2)
            setTwoPlayersEmporiums(configuration);
        GameState initialGameState = new GameState(configuration, numberOfPlayers);
        rulesBroker.setEmporiumsToWin((int) Math.round(configuration.getMap().getAllTowns().size() * 0.66));

        initialGameState.setRandomCouncilors();
        initialGameState.setDefaultPoliticsCardsDeck(numberOfPlayers);

        for (int i = 0; i < numberOfPlayers; i++) {

            Player player = new Player(players.get(i).getUsername());
            player.setCoins(10 + i);
            player.setWinPosition(0);
            player.setNobilityPosition(0);
            player.setEnabled(false);
            player.addHelpers(i);
            player.setMyColor(new MyColor(new Random().nextInt(256), new Random().nextInt(256), new Random().nextInt(256), 1.0));
            /*for (int j = 0; j < 10; j++) {
                Emporium emporium = new Emporium(player);
                player.addEmporium(emporium);
            }*/
            for (int j = 0; j < ConfigurationConstants.NUMBER_OF_POLITICS_CARDS_PER_PLAYER; j++) {
                player.addPoliticsCard((PoliticsCard) initialGameState.getPoliticsCardsDeck().draw());
            }

            initialGameState.addPlayer(player);
        }

        return initialGameState;
    }

    /**
     * This method is called if the number of players is 2. It builds emporiums (of a different color from two players)
     * on the first 9 towns available on the first card of each MapField.
     */
    private void setTwoPlayersEmporiums(Configuration configuration) {
        Map map = configuration.getMap();
        List<Town> townsWithEmporium = new ArrayList<>();
        map.getAllFields().forEach(mapField -> {
            BuildingPermissionCard card = (BuildingPermissionCard) mapField.getBuildingPermissionCardsDeck().draw();
            card.getAllowedTowns().forEach(town -> townsWithEmporium.add(map.getTownWithName(town.getName())));
            mapField.getBuildingPermissionCardsDeck().addCard(card);
        });
        Player fakePlayer = new Player("FAKE");
        fakePlayer.setMyColor(new MyColor(new Random().nextInt(256), new Random().nextInt(256), new Random().nextInt(256), 1.0));
        for (int i = 0; i < 9 && i < townsWithEmporium.size(); i++)
            townsWithEmporium.get(i).addEmporium(new Emporium(fakePlayer));
    }


    public RemotePlayer getRemotePlayerByUsername(String username) {
        for (RemotePlayer rp : players)
            if (rp.getUsername().equals(username))
                return rp;
        return null;
    }

    /**
     * If there is another player after the one with username {@link #currentPlayerUsername}, this methods find its
     * username, otherwise it returns null. This methods skips disconnected players.
     *
     * @return The username of the next player that will play.
     */
    private String getNextPlayerUsername() {
        /*for (int i = 0; i < players.size(); i++)
            if (players.get(i).getUsername().equals(currentPlayerUsername) &&
                    i + 1 < players.size() && !players.get(i + 1).isDisconnected())
                return players.get(i + 1).getUsername();

        return null;*/

        Integer currentPlayerIndex = null;
        for (RemotePlayer p : players)
            if (p.getUsername().equals(currentPlayerUsername)) {
                currentPlayerIndex = players.indexOf(p);
                break;
            }

        if (currentPlayerIndex != null && currentPlayerIndex != players.size() - 1) {
            for (int i = currentPlayerIndex + 1; i < players.size(); i++) {
                if (!players.get(i).isDisconnected())
                    return players.get(i).getUsername();
            }
            return null;
        } else return null;
    }

    private String findFirstOnlinePlayerUsername() {
        for (RemotePlayer rp : players)
            if (!rp.isDisconnected())
                return rp.getUsername();
        return null;
    }

    /**
     * This timer fires when the turn for the user has expired. Since this timer is canceled when the user has ended
     * its operations, if this timer executes run() it means that the user never closed its turn. Hence this timer tries
     * to contact the client and, if this is not possible, the client is marked as disconnected. Either way, this timer
     * notifies the main thread of the room about this user' turn end (calling {@link #mainThreadLock#notify()}
     */
    private class TurnTimer extends TimerTask {

        private int turnTimeoutRemaining = turnTimeout;


        @Override
        public void run() {

            if (turnTimeoutRemaining > 0) {

                turnTimeoutRemaining--;

                if (turnTimeoutRemaining == 10) {
                    Logger.getGlobal().log(Level.INFO, String.format("[GAMEROOM (id: %s)]\t10 seconds left for user: %s", roomId, currentPlayerUsername));
                }

                int stillConnectedPlayers = 0;
                for (RemotePlayer rp : players) {
                    if (!rp.isDisconnected()) {
                        stillConnectedPlayers++;
                        try {
                            rp.turnTimeout(turnTimeoutRemaining);
                        } catch (ConnectionLostException e) {
                            rp.setDisconnected(true);
                            Logger.getGlobal().log(Level.WARNING, "[GAMEROOM]\t" + rp.getUsername() + " is disconnected", e);
                            Player currentPlayer = rulesBroker.getMainLocalState().getPlayerWithUsername(rp.getUsername());
                            currentPlayer.setDisconnected(true);
                            updateEveryone(gc -> gc.updateOrAddPlayer(currentPlayer));
                        }
                    }
                }
                if (stillConnectedPlayers == 0) {
                    Logger.getGlobal().log(Level.INFO, String.format("[GAMEROOM (id: %s)]\tNo player left online for this room. Closing...", roomId));
                    turnTimer.cancel();
                    roomPhase = Phase.END_GAME;
                    GameRoom.this.interrupt();
                }

            } else {
                turnTimer.cancel();

                synchronized (mainThreadLock) {
                    mainThreadLock.notify();
                }
            }


        }

    }

    /**
     * Starts the countdown and updates the lobbies of the players by sending an up-to-date snapshot of the lobby every
     * second. When the countdown has reached zero, this method starts the game launching {@link GameRoom} as a thread
     * and stops the timer itself.
     */
    private class LobbyTimer extends TimerTask {
        private int timeoutRemaining = roomTimeout;

        @Override
        public void run() {
            if (timeoutRemaining > 0) {
                publishLobbyUpdate(JSONHelper.LobbyParser.toJsonLobbyUpdate(null, null, null, timeoutRemaining, toUsersArray(players)));
                timeoutRemaining--;
            } else {
                roomState = RoomState.PLAYING;
                publishLogMessage(JSONHelper.LogParser.toJsonLogUpdate("Game will start shortly..."));
                Logger.getGlobal().info(String.format("[GAMEROOM (id: %s)]\tThis room countdown has ended and game will now start...", roomId));

                GameRoom.this.start();
                lobbyTimer.cancel();
            }
        }

        /**
         * Sends a lobby update to all the players of this room
         *
         * @param jsonLobbyUpdate Json formatted lobby update
         */
        private void publishLobbyUpdate(String jsonLobbyUpdate) {
            for (RemotePlayer rp : players)
                if (!rp.isDisconnected())
                    rp.dispatchLobbyUpdate(jsonLobbyUpdate);
        }
    }

    /**
     * This loop models the game phase for the room. After calling {@link this#setupGamePhase(Phase)} to publish
     * phase and enable the first player, it does - for every player once:
     * - Starts the countdown for the turn and waits to be waken up either from the timer or by the user;
     * - When it wakes up, disables the player and the relative remotePlayer, cancels an eventual pending action,
     * and sends these updated items to everyone;
     * - Finds the next player that should play;
     * - If this player is valid, enables it (both player and remotePlayer) and ends the loop;
     * - Otherwise it changes the phase to {@link Phase#MARKET_SELL} (ending the while).
     */
    private synchronized void gameLoop() throws InterruptedException {
        setupGamePhase(Phase.GAME);
        while (roomPhase == Phase.GAME || roomPhase == Phase.END_GAME) {
            startTurnCountdown();
            synchronized (mainThreadLock) {
                mainThreadLock.wait();
            }
            Logger.getGlobal().log(Level.INFO, String.format("[GAMEROOM (id: %s)]\t Room has woken up...", roomId));

            try {
                getRemotePlayerByUsername(currentPlayerUsername).getGameCommandsBroadcaster().disableUser();
                cancelCurrentAction();

            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
            currentPlayerUsername = getNextPlayerUsername();

            if (currentPlayerUsername != null) {
                try {
                    getRemotePlayerByUsername(currentPlayerUsername).getGameCommandsBroadcaster().enableUser();
                } catch (RemoteException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            } else {
                if (roomPhase == Phase.GAME) {
                    //MARKET SELL
                    roomPhase = Phase.MARKET_SELL;
                } else if (roomPhase == Phase.END_GAME) {
                    Logger.getGlobal().log(Level.INFO, String.format("[GAMEROOM (id: %s)]\tThis room has reached end game", roomId));
                    currentPlayerUsername = rulesBroker.checkEndGameConditions().getUsername();
                    publishGamePhase();
                    if (turnTimer != null)
                        turnTimer.cancel();
                    this.interrupt();
                }
            }
        }

    }

    /**
     * This loop models the market sell phase for the room. After calling {@link this#setupGamePhase(Phase)} to publish
     * phase and enable the first player, it does - for every player once:
     * - Starts the countdown for the turn and waits to be waken up either from the timer or by the user;
     * - When it wakes up, disables the player and the relative remotePlayer, cancels an eventual pending action,
     * and sends these updated items to everyone;
     * - Finds the next player that should play;
     * - If this player is valid, enables it (both player and remotePlayer) and ends the loop;
     * - Otherwise it changes the phase to {@link Phase#MARKET_BUY} (ending the while).
     */
    private synchronized void marketSellLoop() throws InterruptedException {
        Logger.getGlobal().log(Level.INFO, "[GAMEROOM (id: %s)]\tEntering market phase: SELL");
        setupGamePhase(Phase.MARKET_SELL);
        while (roomPhase == Phase.MARKET_SELL) {
            startTurnCountdown();
            synchronized (mainThreadLock) {
                mainThreadLock.wait();
            }
            try {
                getRemotePlayerByUsername(currentPlayerUsername).getGameCommandsBroadcaster().disableUser();
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
            currentPlayerUsername = getNextPlayerUsername();
            if (currentPlayerUsername != null) {
                try {
                    getRemotePlayerByUsername(currentPlayerUsername).getGameCommandsBroadcaster().enableUser();
                } catch (RemoteException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            } else {
                //MARKET BUY
                roomPhase = Phase.MARKET_BUY;
            }
        }
    }

    /**
     * This loop models the market buy phase for the room. After calling {@link this#setupGamePhase(Phase)} to publish
     * phase and enable the first player, it does - for every player once:
     * - Starts the countdown for the turn and waits to be waken up either from the timer or by the user;
     * - When it wakes up, disables the player and the relative remotePlayer, cancels an eventual pending action,
     * and sends these updated items to everyone;
     * - Finds the next player that should play;
     * - If this player is valid, enables it (both player and remotePlayer) and ends the loop;
     * - Otherwise it changes the phase to {@link Phase#GAME} (ending the while).
     */
    private synchronized void marketBuyLoop() throws InterruptedException {
        setupGamePhase(Phase.MARKET_BUY);
        while (roomPhase == Phase.MARKET_BUY) {
            startTurnCountdown();
            synchronized (mainThreadLock) {
                mainThreadLock.wait();
            }
            try {
                getRemotePlayerByUsername(currentPlayerUsername).getGameCommandsBroadcaster().disableUser();
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
            currentPlayerUsername = rulesBroker.getMainLocalState().getMarketInstance().getRandomBuyerUsername();
            if (currentPlayerUsername != null) {
                getRemotePlayerByUsername(currentPlayerUsername).updateItemsToSell(rulesBroker.getMainLocalState().getMarketInstance().getItemsForSale());

                try {
                    getRemotePlayerByUsername(currentPlayerUsername).getGameCommandsBroadcaster().enableUser();
                } catch (RemoteException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            } else {
                //MARKET END
                roomPhase = Phase.GAME;
            }
        }
    }

    /**
     * Set the current phase both on {@link RulesBroker} and using {@link RemotePlayer}, then enables the first user.
     *
     * @param phase
     */
    private void setupGamePhase(Phase phase) {
        switch (phase) {
            case GAME:
                currentPlayerUsername = findFirstOnlinePlayerUsername();
                break;
            case MARKET_SELL:
                currentPlayerUsername = findFirstOnlinePlayerUsername();
                rulesBroker.getMainLocalState().setMarketInstance(new Market(this, rulesBroker));
                break;
            case MARKET_BUY:
                currentPlayerUsername = rulesBroker.getMainLocalState().getMarketInstance().getRandomBuyerUsername();
                getRemotePlayerByUsername(currentPlayerUsername).updateItemsToSell(rulesBroker.getMainLocalState().getMarketInstance().getItemsForSale());
                break;
            default:
                break;
        }
        publishGamePhase();
        try {
            getRemotePlayerByUsername(currentPlayerUsername).getGameCommandsBroadcaster().enableUser();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    public RulesBroker getRulesBroker() {
        return this.rulesBroker;
    }

    /**
     * Calls a method from {@link GameCommands} on every {@link RemotePlayer} still online
     *
     * @param whatToUpdate The lambda/fi containing the call to perform
     */
    public void updateEveryone(AllGameCommand<GameCommands> whatToUpdate) {
        players.stream().filter(rp -> !rp.isDisconnected())
                .forEach(rp -> whatToUpdate.accept(rp.getGameCommandsBroadcaster()));
    }

    public void reportClientReady() {

        playersInitialized++;
        if (playersInitialized >= players.size()) {
            initialized = true;
            synchronized (mainThreadLock) {
                mainThreadLock.notify();
                playersInitialized = -1;
            }
        }

    }

    /**
     * Stores new items for sale from a user.
     *
     * @param calling The RemotePlayer that is asking to add items
     * @param items   Items for sale to add
     */
    public void addItemsToSell(RemotePlayer calling, List<ItemToSell> items) {

        Logger.getGlobal().log(Level.INFO, String.format("[GAMEROOM (id: %s)]\tReceived %d items to sell from player: %s", roomId, items.size(), calling.getUsername()));
        rulesBroker
                .getMainLocalState()
                .getMarketInstance()
                .addItemsToSell(items);
        turnTimer.cancel();

        synchronized (mainThreadLock) {
            mainThreadLock.notify();
        }
    }

    /**
     * Used to trigger the end of the turn when a player asks to.
     */
    public void endTurnOfCurrentPlayer() {

        turnTimer.cancel();
        synchronized (mainThreadLock) {
            mainThreadLock.notify();
        }
    }

    public void buyItemForSale(RemotePlayer buyer, ItemToSell what) {
        rulesBroker.getMainLocalState().getMarketInstance().buy(buyer.getUsername(), what);
    }

    public void cancelCurrentAction() {
        rulesBroker.revertToLastValidGameState();
        updateEveryone(GameCommands::rollbackToLastValidGameState);

    }

    public void notifyEndGameCondition() {
        /*turnTimer.cancel();
        this.interrupt();
        roomPhase = Phase.END_GAME;
        publishGamePhase();
        */
        roomPhase = Phase.END_GAME;
    }

}
