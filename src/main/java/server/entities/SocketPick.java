package server.entities;

import client.network.socket.SocketPickPicked;
import client.presentation.Pick;
import game.board.Field;
import game.cards.BuildingPermissionCard;
import game.logic.Pickables;
import game.logic.actions.Action;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * This class is used to mask connection when a socket one is used and an action needs the user to pick something.
 * Basically, all the methods use a {@link SocketPickPicked} modular object to send an receive pickables.
 */
public class SocketPick implements Pick {

    private SocketRemotePlayer remotePlayer;

    public SocketPick(SocketRemotePlayer remotePlayer) {
        this.remotePlayer = remotePlayer;
    }

    @Override
    public void pickACouncil(Action action, boolean includeKing) throws RemoteException {
        sendPickPickedRequest(action, Pickables.ONE_COUNCIL, includeKing);
    }

    @Override
    public void pickACouncilor(Action action) throws RemoteException {
        sendPickPickedRequest(action, Pickables.ONE_COUNCILOR);
    }

    @Override
    public void pickUpTo4PoliticsCards(Action action) throws RemoteException {
        sendPickPickedRequest(action, Pickables.UPTOFOUR_PC);
    }

    @Override
    public void pickFieldPermissionCard(Field field, Action action) throws RemoteException {
        sendPickPickedRequest(action, Pickables.ONE_FIELD_PERMIT, field);
    }

    @Override
    public void pickYourPermissionCard(Action action) throws RemoteException {
        sendPickPickedRequest(action, Pickables.ONE_YOURS_PERMIT);
    }

    @Override
    public void pickOneTownFromPermitCard(BuildingPermissionCard card, Action action) throws RemoteException {
        sendPickPickedRequest(action, Pickables.ONE_PERMIT_TOWN, card);
    }

    @Override
    public void pickTownsToMoveKing(Action action) throws RemoteException {
        sendPickPickedRequest(action, Pickables.KING_TOWNS);
    }

    @Override
    public void pickEmporiumTown(Action action) throws RemoteException {
        sendPickPickedRequest(action, Pickables.EMPORIUM_TOWN);
    }

    private void sendPickPickedRequest(Action action, Pickables what, Serializable... arg) {
        SocketPickPicked request = null;
        switch (what) {

            case ONE_COUNCILOR:
            case ONE_YOURS_PERMIT:
            case UPTOFOUR_PC:
            case KING_TOWNS:
            case EMPORIUM_TOWN:
                request = new SocketPickPicked(what, action, null);
                break;
            case ONE_PERMIT_TOWN:
            case ONE_FIELD_PERMIT:
            case ONE_COUNCIL:
                request = new SocketPickPicked(what, action, arg[0]);
                break;
            default:
                break;
        }

        remotePlayer.sendPickPicked(request);
    }
}
