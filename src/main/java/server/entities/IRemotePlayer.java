package server.entities;

import game.logic.actions.Action;
import game.market.ItemToSell;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IRemotePlayer extends Remote {

    /**
     * Used remotely by a rmi client to broadcast a chat message (Json formatted)
     *
     * @param jsonMessage JSON formatted chat update
     * @throws RemoteException
     */
    void uploadChat(String jsonMessage) throws RemoteException;

    /**
     * Usually called by a RMI client, configures a room on the server with the specified parameters
     * @param maxUsers The max number of users
     * @param timeout The timeout for the lobby (in seconds)
     * @param turnTimeout The timeout for the turn (in seconds)
     * @param jsonConfig A JSON formatted game configuration
     * @return Whether the room setup has succeeded or not
     */
    boolean configureGameRoom(int maxUsers, int timeout, int turnTimeout, String jsonConfig) throws RemoteException;

    /**
     * Retrieves the room leader' username
     * @return Room creator and manager username
     */
    String getGameLeader() throws RemoteException;

    /**
     * Binds on the registry the necessary objects to perform an action
     * @param actionToStart Which action to start
     * @throws RemoteException
     */
    void prepareAction(Action actionToStart) throws RemoteException;

    /**
     * Start an action prepared calling {@link this#prepareAction(Action)}
     */
    void startPendingAction() throws RemoteException;

    /**
     * Draw a politics card at the beginning of the turn
     */
    void draw() throws RemoteException;

    /**
     * Asks the room to end this player' turn
     */
    void endTurn() throws RemoteException;

    /**
     * Reports to the room that this player/client is ready
     */
    void onClientReady() throws RemoteException;

    /**
     * Receives items for sale from a client and stores them in the room
     * @param items The items for sale by this client
     */
    void sendItemsToSell(List<ItemToSell> items) throws RemoteException;

    /**
     * Processes the transaction of an item by this client
     * @param item The item bought
     */
    void buyItem(ItemToSell item) throws RemoteException;

    /**
     * Aborts the current flow
     */
    void cancelAction() throws RemoteException;
}
