package server.entities;

import client.network.socket.SocketPickPicked;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.cards.Card;
import game.cards.CardType;
import game.logic.Pickables;
import game.logic.exceptions.GameLogicException;
import game.pawns.Pawn;
import game.pawns.PawnType;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constants.ConnectionConstants.SocketMessages.SOCKET_GAMELOGIC_EXCEPTION;

/**
 * Lightweight thread launched to process a received {@link SocketPickPicked} object containing objects
 * picked by a user. It executes the actual callback on the actual flow and reports to the client eventual thrown exceptions.
 * Otherwises sends tags of gamelogic ok.
 */
public class SocketPickHandler extends Thread {
    Pickables what;
    SocketPickPicked response;
    SocketRemotePlayer remotePlayer;

    public SocketPickHandler(Pickables what, SocketPickPicked response, SocketRemotePlayer remotePlayer) {
        this.what = what;
        this.remotePlayer = remotePlayer;
        this.response = response;
    }

    @Override
    public void run() {
        try {
            switch (what) {
                case ONE_COUNCIL:
                case ONE_PERMIT_TOWN:
                case KING_TOWNS:
                    remotePlayer.getCurrentFlow().onBoardItemsPicked((BoardItemType) response.getArg(), response.getPickedItems().toArray(new BoardItem[0]));
                    break;

                case ONE_COUNCILOR:
                    remotePlayer.getCurrentFlow().onPawnsPicked((PawnType) response.getArg(), response.getPickedItems().toArray(new Pawn[0]));
                    break;

                case ONE_FIELD_PERMIT:
                case ONE_YOURS_PERMIT:
                case UPTOFOUR_PC:
                    remotePlayer.getCurrentFlow().onCardsPicked((CardType) response.getArg(), response.getPickedItems().toArray(new Card[0]));
                    break;

                default:
                    break;
            }

        } catch (GameLogicException e) {
            synchronized (remotePlayer.getOutputStream()) {
                reportPickError(e);
            }
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void reportPickError(GameLogicException e) {
        try {
            remotePlayer.getOutputStream().writeUTF(SOCKET_GAMELOGIC_EXCEPTION);
            remotePlayer.getOutputStream().flush();
            remotePlayer.getOutputStream().writeObject(e);
            remotePlayer.getOutputStream().flush();
            remotePlayer.getOutputStream().reset();
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), ex);
        }
    }
}
