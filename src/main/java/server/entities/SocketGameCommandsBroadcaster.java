package server.entities;

import client.GameCommands;
import client.network.socket.SocketUpdate;
import game.Configuration;
import game.board.Council;
import game.board.Field;
import game.board.NobilityRoute;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import game.cards.KingBonusTile;
import game.cards.TownColorBonusTile;
import game.logic.Phase;
import game.logic.Player;
import game.logic.actions.Action;
import game.market.ItemToSell;
import game.pawns.Councilor;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constants.ConnectionConstants.SocketMessages.*;

/**
 * This class masks the used connection in case of a socket one. It provides all the commands from {@link GameCommands},
 * and sends appropriate tags and {@link SocketUpdate} serialized objects when needed.
 */
public class SocketGameCommandsBroadcaster implements GameCommands {

    private ObjectOutputStream output;
    private SocketRemotePlayer remotePlayer;

    public SocketGameCommandsBroadcaster(ObjectOutputStream outputStream, SocketRemotePlayer rp) {
        this.output = outputStream;
        this.remotePlayer = rp;
    }

    @Override
    public void setupGameState(Configuration configuration, int numberOfPlayers) throws RemoteException {
        try {
            synchronized (remotePlayer.getOutputStream()) {
                output.writeObject(configuration);
                output.writeInt(numberOfPlayers);
                output.flush();
                output.reset();
            }

        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }


    @Override
    public void notifyPhase(Phase phase, String newPlayer) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(phase);
        update.setString(newPlayer);
        sendUpdate(update);
    }

    @Override
    public void enableUser() {
        sendTag(SOCKET_ENABLE_USER);

    }

    @Override
    public void disableUser() throws RemoteException {
        sendTag(SOCKET_DISABLE_USER);

    }

    @Override
    public void rollbackToLastValidGameState() {
        sendTag(SOCKET_ROLLBACK);

    }

    @Override
    public void markCurrentGameStateAsValid() {
        sendTag(SOCKET_MARKVALID);

    }

    @Override
    public void updateOrAddPlayer(Player player) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(player);
        sendUpdate(update);

    }

    @Override
    public void updateAvailableCouncilors(List<Councilor> councilors) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(councilors);
        update.setListsItemsClass(Councilor.class);
        sendUpdate(update);
    }

    @Override
    public void updateAvailableHelpers(int numOfHelpers) {
        SocketUpdate update = new SocketUpdate();
        update.setInteger(numOfHelpers);
        sendUpdate(update);
    }

    @Override
    public void addEmporiumInTown(String townName, Player owningPlayer) {
        SocketUpdate update = new SocketUpdate();
        update.setString(townName);
        update.addItemToUpdate(owningPlayer);
        sendUpdate(update);
    }

    @Override
    public void moveKingToTown(String townName) {
        SocketUpdate update = new SocketUpdate();
        update.setString(townName);
        sendUpdate(update);
    }

    @Override
    public void setupNobilityRoute(NobilityRoute nobilityRoute) {

        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(nobilityRoute);
        sendUpdate(update);

    }

    @Override
    public void updateCouncil(Field field, Council council) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(field);
        update.addItemToUpdate(council);
        sendUpdate(update);
    }

    @Override
    public void updateFieldCards(Field field, Deck buildingPermitDeck, BuildingPermissionCard left, BuildingPermissionCard right, boolean isBonusTileOnBoard) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(field);
        update.addItemToUpdate(buildingPermitDeck);
        update.addItemToUpdate(left);
        update.addItemToUpdate(right);
        update.setBool(isBonusTileOnBoard);
        sendUpdate(update);
    }

    @Override
    public void updateKingTiles(List<TownColorBonusTile> townColorBonusTiles, KingBonusTile[] kingBonusTiles) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(townColorBonusTiles);
        update.addItemToUpdate(kingBonusTiles);
        update.setListsItemsClass(TownColorBonusTile.class);
        sendUpdate(update);
    }

    @Override
    public void updateKingCouncil(Council kingCouncil) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(kingCouncil);
        sendUpdate(update);
    }

    @Override
    public void updatePoliticsCardsDeck(Deck deck) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(deck);
        sendUpdate(update);
    }

    @Override
    public void updateMarketItemsToSell(List<ItemToSell> itemsToSells) throws RemoteException {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(itemsToSells);
        update.setListsItemsClass(ItemToSell.class);
        sendUpdate(update);
    }


    @Override
    public void turnTimeout(int timeRemaining) throws IOException {

        synchronized (remotePlayer.getOutputStream()) {
            sendTag(SOCKET_TURN_REMAINING);
            output.writeInt(timeRemaining);
            output.flush();
        }
    }


    @Override
    public void reportActionCompleted(Action action) throws RemoteException {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(action);
        sendUpdate(update);
    }

    @Override
    public void onItemBought(String uniqueId) throws RemoteException {
        try {
            synchronized (remotePlayer.getOutputStream()) {
                sendTag(SOCKET_ITEM_SOLD);
                output.writeUTF(uniqueId);
                output.flush();
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void allowOneMoreAction() throws RemoteException {
        sendTag(SOCKET_ONEMOREACTION);
    }

    @Override
    public void onBonusReceived(IBonus bonus, Action context, Player player) {
        SocketUpdate update = new SocketUpdate();
        update.addItemToUpdate(bonus);
        update.addItemToUpdate(context);
        update.addItemToUpdate(player);
        sendUpdate(update);
    }

    private void sendUpdate(SocketUpdate update) {
        try {
            synchronized (remotePlayer.getOutputStream()) {
                output.writeUTF(SOCKET_UPDATE);
                output.writeObject(update);
                output.flush();
                output.reset();
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    private void sendTag(String tag) {
        try {
            synchronized (remotePlayer.getOutputStream()) {
                output.writeUTF(tag);
                output.flush();
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }
}
