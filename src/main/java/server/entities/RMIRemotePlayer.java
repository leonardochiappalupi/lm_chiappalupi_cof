package server.entities;

import client.GameCommands;
import client.network.rmi.ILocalPlayer;
import client.presentation.Pick;
import game.bonus.IndirectBonus;
import game.logic.actions.Action;
import game.logic.flows.BonusProcessor;
import game.logic.flows.Flow;
import game.logic.flows.IndirectBonusFlow;
import game.market.ItemToSell;
import server.GameRoom;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class RMIRemotePlayer extends RemotePlayer implements IRemotePlayer {

    private ILocalPlayer remotePlayerInterface;


    public RMIRemotePlayer(String username, GameRoom room, ILocalPlayer remoteInterface) throws RemoteException {
        super(username, room);
        this.remotePlayerInterface = remoteInterface;
        UnicastRemoteObject.exportObject(this, 0);
        Logger.getGlobal().info(String.format("[rmi RP]               Remote player object for player: %s exported", username));
    }


    @Override
    public void startGame() {
        try {
            remotePlayerInterface.startGame();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void startConfiguration() {
        try {
            /**
             * RMINetworkManager exports GameCommands and Pick on the Registry
             */
            remotePlayerInterface.startConfiguration();
            gameCommandsBroadcaster = (GameCommands) LocateRegistry.getRegistry().lookup(getUsername() + "_logic");
            pickManager = (Pick) LocateRegistry.getRegistry().lookup(getUsername() + "_pick");

        } catch (RemoteException | NotBoundException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    //FROM SERVER TO THIS PLAYER
    @Override
    public void dispatchChatMessage(String jsonChatElement) {
        try {
            remotePlayerInterface.localChatUpdate(jsonChatElement);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void dispatchLogMessage(String jsonLogElement) {
        try {
            remotePlayerInterface.localLogUpdate(jsonLogElement);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void dispatchLobbyUpdate(String jsonLobbyUpdate) {
        try {
            remotePlayerInterface.localLobbyUpdate(jsonLobbyUpdate);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }

    }

    @Override
    public void startIndirectBonusProcess(IndirectBonus bonus, BonusProcessor.IndirectBonusAsyncHandler handler) {
        try {
            Flow flow = new IndirectBonusFlow(handler, room.getRulesBroker(), this, bonus);
            UnicastRemoteObject.exportObject(flow, 0);
            LocateRegistry.getRegistry().rebind(getUsername() + "_pickCallbacks", flow);
            remotePlayerInterface.changeActiveFlow();
            flow.start();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void onClientReady() {
        room.reportClientReady();
    }

    @Override
    public void sendItemsToSell(List<ItemToSell> items) throws RemoteException {
        room.addItemsToSell(this, items);
    }

    @Override
    public void buyItem(ItemToSell item) throws RemoteException {
        room.buyItemForSale(this, item);
    }

    @Override
    public void cancelAction() throws RemoteException {
        room.cancelCurrentAction();
    }

    //FROM THIS PLAYER TO SERVER
    @Override
    public void uploadChat(String jsonMessage) throws RemoteException {
        room.publishChatMessage(jsonMessage);
    }

    @Override
    public boolean configureGameRoom(int maxUsers, int timeout, int turnTimeout, String jsonConfig) {
        room.setMaxUsersAllowed(maxUsers);
        room.setTimeout(timeout);
        room.setTurnTimeout(turnTimeout);
        room.addPlayer(this);
        room.setGameConfiguration(jsonConfig);
        Logger.getGlobal().info(String.format("[rmi RP]               Player: %s has configured his room", getUsername()));
        return true;
    }

    @Override
    public String getGameLeader() {
        room.addPlayer(this);
        return room.getGameLeader().getUsername();
    }


    @Override
    public void prepareAction(Action action) {
        try {
            currentFlow = room.getRulesBroker().buildAction(action, this);
            UnicastRemoteObject.exportObject(currentFlow, 0);
            LocateRegistry.getRegistry().rebind(getUsername() + "_pickCallbacks", currentFlow);

        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void startPendingAction() {
        currentFlow.start();
    }

    @Override
    public void draw() throws RemoteException {
        room.getRulesBroker().drawPoliticsCard(this);
    }

    @Override
    public void endTurn() throws RemoteException {
        room.endTurnOfCurrentPlayer();
    }
}
