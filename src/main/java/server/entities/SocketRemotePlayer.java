package server.entities;


import client.network.socket.SocketPickPicked;
import constants.ConnectionConstants;
import game.bonus.IndirectBonus;
import game.logic.flows.BonusProcessor;
import game.logic.flows.IndirectBonusFlow;
import server.GameRoom;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constants.ConnectionConstants.SocketMessages.*;

public class SocketRemotePlayer extends RemotePlayer {


    private final ObjectInputStream input;
    private final ObjectOutputStream output;
    private LobbyChatHandler lobbyChatHandler = new LobbyChatHandler();
    private PlayerHandler playerHandler = new PlayerHandler();

    /**
     * Constructor that auto starts a {@link LobbyChatHandler} thread to listen from various events incoming from the
     * client binded to this remote player
     *
     * @param username The username of the user binded to this remote player
     * @param gameRoom The game room this remote player is in
     * @param input    A scanner providing input from client to server
     * @param output   A PrintWriter to send data from server to client
     */
    public SocketRemotePlayer(String username, GameRoom gameRoom, final ObjectInputStream input, final ObjectOutputStream output) {
        super(username, gameRoom);
        this.input = input;
        this.output = output;
        this.gameCommandsBroadcaster = new SocketGameCommandsBroadcaster(output, this);
        this.pickManager = new SocketPick(this);
        lobbyChatHandler.start();

        Logger.getGlobal().info(String.format("[SOCKET RP]            SocketRemotePlayer object for user: %s has been created", username));

        Logger.getGlobal().info(String.format("[SOCKET RP]            Request handler for user: %s is now ready...", username));
    }

    public ObjectInputStream getInput() {
        return input;
    }

    @Override
    public void startGame() {

        try {
            lobbyChatHandler.cancel();
            lobbyChatHandler.join();
            playerHandler.start();
            synchronized (output) {
                output.writeUTF(SOCKET_GAME_START);
                output.flush();
            }
        } catch (IOException | InterruptedException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void startConfiguration() {
        synchronized (output) {
            try {
                output.writeUTF(SOCKET_GAME_CONFIGURATION);
                output.flush();
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    //FROM SERVER TO THIS PLAYER
    @Override
    public synchronized void dispatchChatMessage(String jsonChatElement) {
        sendJson(jsonChatElement);
    }

    @Override
    public synchronized void dispatchLogMessage(String jsonLogElement) {
        sendJson(jsonLogElement);
    }

    @Override
    public synchronized void dispatchLobbyUpdate(String jsonLobbyUpdate) {
        sendJson(jsonLobbyUpdate);
    }

    @Override
    public void startIndirectBonusProcess(IndirectBonus bonus, BonusProcessor.IndirectBonusAsyncHandler handler) {
        currentFlow = new IndirectBonusFlow(handler, room.getRulesBroker(), this, bonus);
        currentFlow.start();
    }


    /**
     * Looper thread used to receive and process chat messages during lobby phase.
     */
    private class LobbyChatHandler extends Thread {
        boolean isInputEnabled = true;

        @Override
        public void run() {
            try {
                mainLoop();
            } catch (IOException | ClassNotFoundException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }

        private void mainLoop() throws IOException, ClassNotFoundException {

            while (isInputEnabled) {
                if (input.available() > 0) {
                    String command = input.readUTF();
                    SocketInputProcessor.process(SocketRemotePlayer.this, command);
                }
            }
        }

        public void cancel() {
            this.isInputEnabled = false;
        }
    }

    /**
     * Looper thread used to receive and process client requests. It uses {@link SocketInputProcessor} (in a similar
     * way to {@link client.network.socket.SocketNetworkManager}, but on server side)
     */
    private class PlayerHandler extends Thread {
        boolean isInputEnabled = true;

        @Override
        public void run() {
            while (isInputEnabled) {

                try {
                    String command = input.readUTF();
                    SocketInputProcessor.process(SocketRemotePlayer.this, command);
                } catch (EOFException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                    this.cancel();
                } catch (IOException | ClassNotFoundException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            }

        }

        public void cancel() {
            this.isInputEnabled = false;
            try {
                input.close();
                this.join();
            } catch (IOException | InterruptedException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }


        }

    }

    /**
     * Writes the JSON tag and the object on the output stream
     *
     * @param jsonToSend The JSON formatted object to send
     */
    private void sendJson(String jsonToSend) {
        try {
            synchronized (output) {
                output.writeUTF(SOCKET_JSON);
                output.flush();
                output.writeUTF(jsonToSend);
                output.flush();
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    public ObjectOutputStream getOutputStream() {
        return output;
    }

    public void sendPickPicked(SocketPickPicked pickPicked) {
        try {
            synchronized (output) {
                output.writeUTF(ConnectionConstants.SocketMessages.SOCKET_PICKPICKED);
                output.writeObject(pickPicked);
                output.flush();
                output.reset();
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

}
