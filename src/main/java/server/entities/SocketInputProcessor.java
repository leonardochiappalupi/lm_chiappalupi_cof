package server.entities;


import client.network.socket.SocketPickPicked;
import game.logic.Pickables;
import game.logic.actions.Action;
import game.logic.flows.Flow;
import game.market.ItemToSell;
import server.GameRoom;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import static constants.ConnectionConstants.SocketMessages.*;

public class SocketInputProcessor {

    private SocketInputProcessor(){}

    public static void process(SocketRemotePlayer remotePlayer, String command) throws IOException, ClassNotFoundException {
        GameRoom room = remotePlayer.getGameRoom();
        ObjectInputStream input = remotePlayer.getInput();

        switch (command) {
            case SOCKET_CLIENT_READY:
                room.reportClientReady();
                break;
            case SOCKET_DRAW:
                room.getRulesBroker().drawPoliticsCard(remotePlayer);
                break;
            case SOCKET_START_ACTION:
                Action which = (Action) input.readObject();
                Flow flow = remotePlayer.getGameRoom().getRulesBroker().buildAction(which, remotePlayer);
                remotePlayer.setCurrentFlow(flow);
                flow.start();
                break;
            case SOCKET_CANCEL:
                room.cancelCurrentAction();
                break;
            case SOCKET_JSON:
                String jsonChat = input.readUTF();
                room.publishChatMessage(jsonChat);
                break;
            case SOCKET_END_TURN:
                room.endTurnOfCurrentPlayer();
                break;
            case SOCKET_ITEM_SOLD:
                ItemToSell item = (ItemToSell) input.readObject();
                room.buyItemForSale(remotePlayer, item);
                break;
            case SOCKET_ITEMS_TOSELL:
                List<ItemToSell> items = (List<ItemToSell>) input.readObject();
                room.addItemsToSell(remotePlayer, items);
                break;
            case SOCKET_PICKPICKED:
                SocketPickPicked response = (SocketPickPicked) input.readObject();
                Pickables what = response.getWhatToPick();
                new SocketPickHandler(what, response, remotePlayer).start();
                break;
            default:
                break;

        }
    }
}

