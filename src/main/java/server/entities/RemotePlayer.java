package server.entities;

import client.GameCommands;
import client.presentation.Pick;
import game.Configuration;
import game.board.MapField;
import game.bonus.IndirectBonus;
import game.logic.Phase;
import game.logic.Player;
import game.logic.flows.BonusProcessor;
import game.logic.flows.Flow;
import game.market.ItemToSell;
import server.GameRoom;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents the client on the server side, regardless of the connection method. The server uses instances
 * of this class to call methods that dispatch updates from the server itself to the client linked with that instance.
 * To send game updates, it relies on an object implementing all the commands from {@link GameCommands}.
 * To make the user pick things, it stores an object providing {@link Pick} commands.
 * This class is the only touch point between clients and the room (and the game itself).
 */
public abstract class RemotePlayer {
    private String username;
    final GameRoom room;
    private boolean disconnected = false;


    GameCommands gameCommandsBroadcaster;
    Pick pickManager;
    Flow currentFlow;

    public RemotePlayer(String username, GameRoom room) {
        this.username = username;
        this.room = room;
    }

    public Flow getCurrentFlow() {
        return currentFlow;
    }

    public void setCurrentFlow(Flow currentFlow) {
        this.currentFlow = currentFlow;
    }

    public GameCommands getGameCommandsBroadcaster() {
        return gameCommandsBroadcaster;
    }

    public void setGameCommandsBroadcaster(GameCommands gameCommandsBroadcaster) {
        this.gameCommandsBroadcaster = gameCommandsBroadcaster;
    }

    /**
     * Notifies the client about the end of the lobby phase and the beginning of the game.
     */
    public abstract void startGame();

    public abstract void startConfiguration();

    public boolean isDisconnected() {
        return disconnected;
    }

    public void setDisconnected(boolean disconnected) {
        this.disconnected = disconnected;
    }

    /**
     * Invoked to send a chat update from the server to the client binded to this remote player
     *
     * @param jsonChatElement JSON formatted chat update
     */
    public abstract void dispatchChatMessage(String jsonChatElement);

    /**
     * Invoked to send a log update from the server to the client binded to this remote player
     *
     * @param jsonLogElement JSON formatted log update
     */
    public abstract void dispatchLogMessage(String jsonLogElement);

    /**
     * Invoked to send a lobby update from the server to the client binded to this remote player
     *
     * @param jsonLobbyUpdate JSON formatted chat update
     */
    public abstract void dispatchLobbyUpdate(String jsonLobbyUpdate);

    public GameRoom getGameRoom() {
        return room;
    }

    public String getUsername() {
        return username;
    }

    /**
     * Notifies the remote player about a new phase of the game
     *
     * @param phase         The new phase
     * @param enabledPlayer The just enabled player username
     */
    public void changePhase(Phase phase, String enabledPlayer) {
        try {
            gameCommandsBroadcaster.notifyPhase(phase, enabledPlayer);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Sends the time remaining for a turn. Used also as a ping to check online/offline clients
     *
     * @param timeoutRemaining The turn time remaining (in seconds)
     * @throws ConnectionLostException Thrown if the client is not reachable anymore
     */
    public void turnTimeout(int timeoutRemaining) throws ConnectionLostException {
        try {
            gameCommandsBroadcaster.turnTimeout(timeoutRemaining);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            throw new ConnectionLostException();
        }
    }

    /**
     * Setups game client-side by providing all the necessary items
     *
     * @param configuration   A configuration object (parsed by a JSON)
     * @param numberOfPlayers The nymber of players that will join the game
     */
    public void setupGame(Configuration configuration, int numberOfPlayers) {
        try {
            gameCommandsBroadcaster.setupGameState(configuration, numberOfPlayers);
            for (Player p : room.getRulesBroker().getMainLocalState().getPlayers())
                gameCommandsBroadcaster.updateOrAddPlayer(p);
            for (MapField field : room.getRulesBroker().getMainLocalState().getMap().getAllFields())
                gameCommandsBroadcaster.updateCouncil(field.getFieldType(), field.getCouncil());
            gameCommandsBroadcaster.updateKingCouncil(room.getRulesBroker().getMainLocalState().getKingCouncil());
            gameCommandsBroadcaster.updatePoliticsCardsDeck(room.getRulesBroker().getMainLocalState().getPoliticsCardsDeck());
            gameCommandsBroadcaster.updateAvailableCouncilors(room.getRulesBroker().getMainLocalState().getAvailableCouncilors());
            gameCommandsBroadcaster.updateAvailableHelpers(room.getRulesBroker().getMainLocalState().getAvailableHelpers().size());
            gameCommandsBroadcaster.markCurrentGameStateAsValid();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public Pick getPickManager() {
        return pickManager;
    }

    public void setPickManager(Pick pickManager) {
        this.pickManager = pickManager;
    }

    /**
     * Sends clients a list of items for sale
     *
     * @param items Items for sale.
     */
    public void updateItemsToSell(List<ItemToSell> items) {
        try {
            gameCommandsBroadcaster.updateMarketItemsToSell(items);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Start processing indirect bonuses, when invoked by a handler thread.
     *
     * @param bonus   The indirect bonus to process
     * @param handler The handler caller thread
     */
    public abstract void startIndirectBonusProcess(IndirectBonus bonus, BonusProcessor.IndirectBonusAsyncHandler handler);
}
