package server.persistent;

import client.network.rmi.ILocalPlayer;
import constants.ConnectionConstants;
import server.GameRoom;
import server.RoomState;
import server.Server;
import server.entities.RMIRemotePlayer;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Persistent RMI server that receives new users. Delegates the handling to a RMIRemotePlayer when user login has
 * completed.
 */
public class RMIServer implements IPersistentServer {
    private Server server;
    private Registry registry;

    public RMIServer(Server mainServer) throws RemoteException {
        this.server = mainServer;
    }

    public void launch() throws RemoteException {
        registry = LocateRegistry.createRegistry(ConnectionConstants.RMI_REGISTRY_PORT);
        registry.rebind("rmi_interface", this);
        UnicastRemoteObject.exportObject(this, 0);
        Logger.getGlobal().info("[rmi PERSISTENT]       rmi Server running correctly...");
    }
    @Override
    public boolean newUser(String username) throws RemoteException {
        Logger.getGlobal().info("[rmi PERSISTENT]       New user is requesting access: " + username);
        ILocalPlayer remotePlayerInterface = null;
        try {
            remotePlayerInterface = (ILocalPlayer) registry.lookup(username);
            Logger.getGlobal().info("[rmi PERSISTENT]       Remote player interface found for user: " + username);
        } catch (NotBoundException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }

        GameRoom availableRoom = server.getAvailableRoom();
        RMIRemotePlayer newPlayer = new RMIRemotePlayer(username, availableRoom, remotePlayerInterface);
        String remotePlayerObjectId = newPlayer.getUsername() + "_remote";
        registry.rebind(remotePlayerObjectId, newPlayer);
        RoomState roomState = availableRoom.getRoomState();
        return roomState == RoomState.NEW;

    }

    @Override
    public boolean checkUsernameAvailability(String username) {
        return server.checkUsernameAvailability(username);
    }

}
