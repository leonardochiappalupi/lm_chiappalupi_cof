package server.persistent;

import java.rmi.*;


public interface IPersistentServer extends Remote {

    boolean newUser(String username) throws RemoteException;

    boolean checkUsernameAvailability(String username) throws RemoteException;
}
