package server.persistent;

import client.presentation.PresentationBridge;
import constants.ConnectionConstants;
import server.GameRoom;
import server.RoomState;
import server.Server;
import server.entities.SocketRemotePlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a persistent socket server to listen for clients that try to connect. It then delegates the
 * user management to a proper {@link server.entities.SocketRemotePlayer} instance.
 */
public class SocketServer {
    private ServerSocket gamePersistentSocket;
    private Server mainServer;
    private AcceptanceHandler acceptanceHandler;
    private boolean isServerEnabled = true;

    /**
     * This constructor starts a {@link NewUserHandler} thread for every client that wants to connect using socket
     *
     * @param mainServer The {@link server.Server} instance reference.
     */
    public SocketServer(Server mainServer) {
        this.mainServer = mainServer;
    }

    public void launch() {
        acceptanceHandler = new AcceptanceHandler();
        acceptanceHandler.start();
        Logger.getGlobal().info("[SOCKET PERSISTENT]\tsocket Server running correctly...");
    }


    private class AcceptanceHandler extends Thread {
        @Override
        public void run() {
            try {
                gamePersistentSocket = new ServerSocket(ConnectionConstants.GAME_PERSISTENT_SOCKET_PORT);

                while (isServerEnabled) {
                    Socket newUserGameSocket = gamePersistentSocket.accept();

                    NewUserHandler newUserThread = new NewUserHandler(newUserGameSocket);
                    newUserThread.start();
                    Logger.getGlobal().info("[SOCKET]               New user handler thread started due to new incoming request...");

                }
            } catch (IOException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
            System.out.println("Accept thread terminated");
        }
    }


    /**
     * This thread registers a new user and calls the {@link server.Server} to get a room for it.
     * Then it delegates further user management to a new {@link server.entities.SocketRemotePlayer} object, calling
     * {@link #createAndAddRemotePlayer(String, GameRoom)}
     */
    private class NewUserHandler extends Thread {
        ObjectInputStream input;
        ObjectOutputStream output;
        GameRoom availableRoom;
        String username;

        public NewUserHandler(Socket newUserGameSocket) {
            try {
                output = new ObjectOutputStream(newUserGameSocket.getOutputStream());
                input = new ObjectInputStream(newUserGameSocket.getInputStream());
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }

        @Override
        public void run() {
            try {
                username = login();
                Logger.getGlobal().info(String.format("[SOCKET]\tUser %s logged in", username));

                availableRoom = mainServer.getAvailableRoom();
                RoomState roomState = availableRoom.getRoomState();

                if (roomState == RoomState.NEW) {
                    receiveRoomConfiguration();

                } else if (roomState == RoomState.WAITING || roomState == RoomState.TIMEOUT) {
                    notifyRegularUser();
                }

                Logger.getGlobal().info(String.format("[SOCKET]\tUser handler thread for user: %s will be terminated;" +
                        "\ta dedicated SocketRemotePlayer will now startLobbyLogin...", username));
                createAndAddRemotePlayer(username, availableRoom);
                this.interrupt();
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }

        /**
         * Fired when the user is the first in a game room, this method receives all the expected parameters for the
         * configuration of the room and finally set the room' state to "WAITING" (to make the room visible for other
         * players).
         *
         * @throws IOException
         */
        private void receiveRoomConfiguration() throws IOException {
            output.writeUTF(ConnectionConstants.SocketMessages.SOCKET_IS_LEADER);
            output.flush();
            Logger.getGlobal().info(String.format("[SOCKET]\tNotified user %s as game leader; waiting for parameters...", username));

            if (input.readUTF().equals(ConnectionConstants.SocketMessages.SOCKET_MAX_PLAYERS)) {
                int maxPlayersAllowed = input.readInt();
                availableRoom.setMaxUsersAllowed(maxPlayersAllowed);
            }
            if (input.readUTF().equals(ConnectionConstants.SocketMessages.SOCKET_REQUESTED_TIMEOUT)) {
                int timeout = input.readInt();
                availableRoom.setTimeout(timeout);
            }
            if (input.readUTF().equals(ConnectionConstants.SocketMessages.SOCKET_REQUESTED_TURNTIMEOUT)) {
                int turnTimeout = input.readInt();
                availableRoom.setTurnTimeout(turnTimeout);
            }
            if (input.readUTF().equals(ConnectionConstants.SocketMessages.SOCKET_UPLOAD_CONFIGURATION)) {
                String jsonConfig = input.readUTF();
                availableRoom.setGameConfiguration(jsonConfig);
            }
            availableRoom.setRoomState(RoomState.WAITING);

            output.writeUTF(ConnectionConstants.SocketMessages.SOCKET_GENERAL_OK);
            output.flush();

            Logger.getGlobal().info(String.format("[SOCKET]               Room -%s- created for user: %s. Max users nr.: %d, Timeout: %d",
                    availableRoom.getRoomId(), username, availableRoom.getMaxUsersAllowed(), availableRoom.getTimeout()));

        }

        /**
         * Notifies a just logged user that he's not a leader and sends out the username of the game leader.
         *
         * @throws IOException
         */
        private void notifyRegularUser() throws IOException {
            output.writeUTF(ConnectionConstants.SocketMessages.SOCKET_IS_REGULAR);
            output.flush();
            Logger.getGlobal().info(String.format("[SOCKET]\tNotified user %s as regular user", username));
            if (input.readUTF().equals(ConnectionConstants.SocketMessages.SOCKET_GET_GAMELEADER)) {
                output.writeUTF(availableRoom.getGameLeader().getUsername());
                output.flush();
            }
        }

        /**
         * The server side implementation of {@link PresentationBridge#login()}
         *
         * @return Username accepted.
         */
        private String login() throws IOException {

            while (true) {
                String attempt = input.readUTF();
                switch (attempt) {
                    case ConnectionConstants.SocketMessages.SOCKET_CHECK_USERNAME:
                        checkUsername();
                        break;
                    case ConnectionConstants.SocketMessages.SOCKET_CREATE_PLAYER:
                        String desiredUsername = input.readUTF();
                        return desiredUsername;
                    default:
                        break;

                }
            }

        }

        public void checkUsername() throws IOException {
            String usernameToCheck = input.readUTF();
            if (!mainServer.checkUsernameAvailability(usernameToCheck)) {
                output.writeUTF(ConnectionConstants.SocketMessages.SOCKET_USERNAME_EXISTING);
                output.flush();
            } else {
                output.writeUTF(ConnectionConstants.SocketMessages.SOCKET_USERNAME_OK);
                output.flush();
            }
        }

        /**
         * Creates a {@link SocketRemotePlayer} object for the new user and adds it to the open game room
         *
         * @param username      The username of the user to add
         * @param availableRoom The {@link GameRoom} reference for this player
         * @return {@link SocketRemotePlayer} representing the user (server side) from now on
         */
        private void createAndAddRemotePlayer(String username, GameRoom availableRoom) {

            SocketRemotePlayer newRemotePlayer = new SocketRemotePlayer(username, availableRoom, input, output);
            availableRoom.addPlayer(newRemotePlayer);
        }
    }


}
