package server;

import java.rmi.RemoteException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Functional interface provided to catch (often fake) RemoteExceptions inside and not outside a lambda,
 * when using {@link GameRoom#updateEveryone(AllGameCommand)}
 * @param <T>
 */
@FunctionalInterface
public interface AllGameCommand<T> extends Consumer<T> {


    @Override
    default void accept(final T elem) {
        try {
            acceptThrows(elem);
        } catch (final RemoteException e) {
            Logger.getGlobal().log(Level.WARNING,e.getMessage(),e);
        }
    }

    void acceptThrows(T elem) throws RemoteException;
}
