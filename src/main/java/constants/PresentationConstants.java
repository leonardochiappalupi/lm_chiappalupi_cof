package constants;


public class PresentationConstants {

    private PresentationConstants() {
    }

    /**
     * This codes are used to identify presentation modes via integer values.
     */
    public static class Codes {
        public static final int PRES_MODE_CLI = 10;
        public static final int PRES_MODE_GUI = 20;

        private Codes() {
        }
    }

    /**
     * This tags are used to identify general presentation events and modes.
     */
    public static class Names {
        public static final String PRES_MODE_CLI = "cli";
        public static final String PRES_MODE_GUI = "gui";

        private Names() {
        }
    }

    /**
     * This tags are used as prefixes to show which type of message we are looking at (via CLI).
     */
    public static class Tags {
        public static final String TAG_CHAT = "[CHAT]";
        public static final String TAG_LOG = "[SERVERLOG]";
        public static final String TAG_PREGAME = "\n  /$$$$$$   /$$$$$$  /$$   /$$ /$$   /$$  /$$$$$$  /$$$$$$ /$$      \n" +
                " /$$__  $$ /$$__  $$| $$  | $$| $$$ | $$ /$$__  $$|_  $$_/| $$      \n" +
                "| $$  \\__/| $$  \\ $$| $$  | $$| $$$$| $$| $$  \\__/  | $$  | $$      \n" +
                "| $$      | $$  | $$| $$  | $$| $$ $$ $$| $$        | $$  | $$      \n" +
                "| $$      | $$  | $$| $$  | $$| $$  $$$$| $$        | $$  | $$      \n" +
                "| $$    $$| $$  | $$| $$  | $$| $$\\  $$$| $$    $$  | $$  | $$      \n" +
                "|  $$$$$$/|  $$$$$$/|  $$$$$$/| $$ \\  $$|  $$$$$$/ /$$$$$$| $$$$$$$$\n" +
                " \\______/  \\______/  \\______/ |__/  \\__/ \\______/ |______/|________/\n" +
                "  /$$$$$$  /$$$$$$$$       /$$$$$$$$ /$$$$$$  /$$   /$$ /$$$$$$$    \n" +
                " /$$__  $$| $$_____/      | $$_____//$$__  $$| $$  | $$| $$__  $$   \n" +
                "| $$  \\ $$| $$            | $$     | $$  \\ $$| $$  | $$| $$  \\ $$   \n" +
                "| $$  | $$| $$$$$         | $$$$$  | $$  | $$| $$  | $$| $$$$$$$/   \n" +
                "| $$  | $$| $$__/         | $$__/  | $$  | $$| $$  | $$| $$__  $$   \n" +
                "| $$  | $$| $$            | $$     | $$  | $$| $$  | $$| $$  \\ $$   \n" +
                "|  $$$$$$/| $$            | $$     |  $$$$$$/|  $$$$$$/| $$  | $$   \n" +
                " \\______/ |__/            |__/      \\______/  \\______/ |__/  |__/   \n" +
                "\nA game by:     Simone Luciani, Daniele Tascini\n" +
                "Software by:   Leonardo Chiappalupi, Giuseppe Cannizzaro, Ivan Bugli\n" +
                "--------------------------------------------------------------------\n";

        public static final String TAG_GAME = "\n-------------------------------------------\n" +
                "  /$$$$$$   /$$$$$$  /$$      /$$ /$$$$$$$$\n" +
                " /$$__  $$ /$$__  $$| $$$    /$$$| $$_____/\n" +
                "| $$  \\__/| $$  \\ $$| $$$$  /$$$$| $$      \n" +
                "| $$ /$$$$| $$$$$$$$| $$ $$/$$ $$| $$$$$   \n" +
                "| $$|_  $$| $$__  $$| $$  $$$| $$| $$__/   \n" +
                "| $$  \\ $$| $$  | $$| $$\\  $ | $$| $$      \n" +
                "|  $$$$$$/| $$  | $$| $$ \\/  | $$| $$$$$$$$\n" +
                " \\______/ |__/  |__/|__/     |__/|________/\n\n" +
                "It's your turn!\n" +
                "--------------------------------------------";
        public static final String TAG_MARKET_SELL = "\n---------------------------------------------------------------\n" +
                " /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$ /$$$$$$$$ /$$$$$$$$\n" +
                "| $$$    /$$$ /$$__  $$| $$__  $$| $$  /$$/| $$_____/|__  $$__/\n" +
                "| $$$$  /$$$$| $$  \\ $$| $$  \\ $$| $$ /$$/ | $$         | $$   \n" +
                "| $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$/  | $$$$$      | $$   \n" +
                "| $$  $$$| $$| $$__  $$| $$__  $$| $$  $$  | $$__/      | $$   \n" +
                "| $$\\  $ | $$| $$  | $$| $$  \\ $$| $$\\  $$ | $$         | $$   \n" +
                "| $$ \\/  | $$| $$  | $$| $$  | $$| $$ \\  $$| $$$$$$$$   | $$   \n" +
                "|__/     |__/|__/  |__/|__/  |__/|__/  \\__/|________/   |__/   \n\n" +
                "You can now sell items you don't need to get coins!\n" +
                "----------------------------------------------------------------\n";
        public static final String TAG_MARKET_BUY = "\n---------------------------------------------------------------\n" +
                " /$$      /$$  /$$$$$$  /$$$$$$$  /$$   /$$ /$$$$$$$$ /$$$$$$$$\n" +
                "| $$$    /$$$ /$$__  $$| $$__  $$| $$  /$$/| $$_____/|__  $$__/\n" +
                "| $$$$  /$$$$| $$  \\ $$| $$  \\ $$| $$ /$$/ | $$         | $$   \n" +
                "| $$ $$/$$ $$| $$$$$$$$| $$$$$$$/| $$$$$/  | $$$$$      | $$   \n" +
                "| $$  $$$| $$| $$__  $$| $$__  $$| $$  $$  | $$__/      | $$   \n" +
                "| $$\\  $ | $$| $$  | $$| $$  \\ $$| $$\\  $$ | $$         | $$   \n" +
                "| $$ \\/  | $$| $$  | $$| $$  | $$| $$ \\  $$| $$$$$$$$   | $$   \n" +
                "|__/     |__/|__/  |__/|__/  |__/|__/  \\__/|________/   |__/   \n\n" +
                "You can now buy items you need from other players!\n" +
                "----------------------------------------------------------------\n";

        public static final String TAG_ENDGAME_WON = "\n-------------------------------------------\n" +
                "                    ,----..            ,--. \n" +
                "           .---.   /   /   \\         ,--.'| \n" +
                "          /. ./|  /   .     :    ,--,:  : | \n" +
                "      .--'.  ' ; .   /   ;.  \\,`--.'`|  ' : \n" +
                "     /__./ \\ : |.   ;   /  ` ;|   :  :  | | \n" +
                " .--'.  '   \\' .;   |  ; \\ ; |:   |   \\ | : \n" +
                "/___/ \\ |    ' '|   :  | ; | '|   : '  '; | \n" +
                ";   \\  \\;      :.   |  ' ' ' :'   ' ;.    ; \n" +
                " \\   ;  `      |'   ;  \\; /  ||   | | \\   | \n" +
                "  .   \\    .\\  ; \\   \\  ',  / '   : |  ; .' \n" +
                "   \\   \\   ' \\ |  ;   :    /  |   | '`--'   \n" +
                "    :   '  |--\"    \\   \\ .'   '   : |       \n" +
                "     \\   \\ ;        `---`     ;   |.'       \n" +
                "      '---\"                   '---'         \n\n" +
                "Congratulations! You won the game!\n" +
                "-------------------------------------------\n";

        public static final String TAG_ENDGAME_LOSE = "\n-------------------------------------------------\n" +
                "   ,--,                                           \n" +
                ",---.'|        ,----..                            \n" +
                "|   | :       /   /   \\     .--.--.        ,---,. \n" +
                ":   : |      /   .     :   /  /    '.    ,'  .' | \n" +
                "|   ' :     .   /   ;.  \\ |  :  /`. /  ,---.'   | \n" +
                ";   ; '    .   ;   /  ` ; ;  |  |--`   |   |   .' \n" +
                "'   | |__  ;   |  ; \\ ; | |  :  ;_     :   :  |-, \n" +
                "|   | :.'| |   :  | ; | '  \\  \\    `.  :   |  ;/| \n" +
                "'   :    ; .   |  ' ' ' :   `----.   \\ |   :   .' \n" +
                "|   |  ./  '   ;  \\; /  |   __ \\  \\  | |   |  |-, \n" +
                ";   : ;     \\   \\  ',  /   /  /`--'  / '   :  ;/| \n" +
                "|   ,/       ;   :    /   '--'.     /  |   |    \\ \n" +
                "'---'         \\   \\ .'      `--'---'   |   :   .' \n" +
                "               `---`                   |   | ,'   \n" +
                "                                       `----'     \n\n" +
                "Oh no! %s won the game! How bad!\n" +
                "-------------------------------------------------\n";


        private Tags() {
        }
    }
}
