package constants;


public class ConnectionConstants {

    public static final int GAME_PERSISTENT_SOCKET_PORT = 80;
    public static final int RMI_REGISTRY_PORT = 1099;

    private ConnectionConstants() {
    }

    /**
     * This codes are used to identify connection modes via integer values.
     */
    public static class Codes {
        public static final int CONN_MODE_RMI = 100;
        public static final int CONN_MODE_SOCKET = 200;

        private Codes() {
        }
    }

    /**
     * This tags are used for various network related events.
     */
    public static class Names {
        public static final String CONN_MODE_RMI = "rmi";
        public static final String CONN_MODE_SOCKET = "socket";

        private Names() {
        }
    }

    public static class SocketMessages {


        public static final String SOCKET_USERNAME_EXISTING = "already_exists";
        public static final String SOCKET_USERNAME_OK = "user_ok";
        public static final String SOCKET_GENERAL_OK = "ok";
        public static final String SOCKET_CHECK_USERNAME = "check";
        public static final String SOCKET_CREATE_PLAYER = "create";
        public static final String SOCKET_UPLOAD_CONFIGURATION = "configuration";
        public static final String SOCKET_REQUESTED_TIMEOUT = "timeout";
        public static final String SOCKET_REQUESTED_TURNTIMEOUT = "turn_timeout";
        public static final String SOCKET_MAX_PLAYERS = "max_players_number";
        public static final String SOCKET_IS_LEADER = "leader";
        public static final String SOCKET_IS_REGULAR = "regular";
        public static final String SOCKET_GET_GAMELEADER = "get_leader";
        public static final String SOCKET_JSON = "json";
        public static final String SOCKET_GAME_START = "game";
        public static final String SOCKET_GAME_CONFIGURATION = "game_config";

        public static final String SOCKET_ENABLE_USER = "enable";
        public static final String SOCKET_DISABLE_USER = "disable";
        public static final String SOCKET_ROLLBACK = "rollback";
        public static final String SOCKET_MARKVALID = "mark_valid";
        public static final String SOCKET_TURN_REMAINING = "turn_remaining";
        public static final String SOCKET_UPDATE = "update";
        public static final String SOCKET_CLIENT_READY = "client_ready";
        public static final String SOCKET_PICKPICKED = "pickpicked";
        public static final String SOCKET_START_ACTION = "start_action";
        public static final String SOCKET_ITEM_SOLD = "item_sold";
        public static final String SOCKET_END_TURN = "end_turn";
        public static final String SOCKET_DRAW = "draw";
        public static final String SOCKET_ITEMS_TOSELL = "items_to_sell";
        public static final String SOCKET_ONEMOREACTION = "one_more_action";
        public static final String SOCKET_GAMELOGIC_EXCEPTION = "gamelogic_exception";
        public static final String SOCKET_GAMELOGIC_OK = "gamelogic_ok";
        public static final String SOCKET_CANCEL = "cancel";


        private SocketMessages() {
        }
    }

    /**
     * Returns connection mode code value via its name
     *
     * @param name The name of the connection method
     * @return The code of the connection method. Returns -1 if the provided name is not valid.
     */
    public static int getCodeFromName(String name) {
        switch (name) {
            case Names.CONN_MODE_RMI:
                return Codes.CONN_MODE_RMI;
            case Names.CONN_MODE_SOCKET:
                return Codes.CONN_MODE_SOCKET;
            default:
                return -1;
        }
    }
}

