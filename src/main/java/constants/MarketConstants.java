package constants;

/**
 * contains mainly tags used by the market
 **/
public class MarketConstants {
    public static final String MARKET_SKIP_CONFIRM_MESSAGE = "Are you sure you want to skip the market phase?";
    public static final String MARKET_SELL_CONFIRM_MESSAGE = "Are you sure you want to sell these items?";

    public static final String PRICE_FORMAT_ERROR_MESSAGE = "The price of the objects you sell must contain digits only";

    private MarketConstants(){}
}
