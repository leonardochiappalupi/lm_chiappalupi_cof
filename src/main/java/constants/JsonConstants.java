package constants;


public class JsonConstants {

    private JsonConstants() {
    }

    /**
     * Json tags to retrive elements from Json formatted chat updates
     */
    public static class Chat {
        public static final String CHAT_ELEMENT_TAG = "chat";
        public static final String AUTHOR_TAG = "author";
        public static final String MESSAGE_TAG = "message";
        public static final String TIMESTAMP_TAG = "timestamp";

        private Chat() {
        }
    }

    /**
     * Json tags to retrive elements from Json formatted log updates
     */
    public static class Log {
        public static final String LOG_ELEMENT_TAG = "log";
        public static final String MESSAGE_TAG = "message";
        public static final String TIMESTAMP_TAG = "timestamp";

        private Log() {
        }
    }

    /**
     * Json tags to retrive elements from Json formatted lobby updates
     */
    public static class Lobby {
        public static final String LOBBY_ELEMENT_TAG = "lobby";
        public static final String GAME_LEADER = "leader";
        public static final String USERS_CONNECTED = "users";
        public static final String TIME_REMAINING = "timeout";
        public static final String ROOM_NAME = "room";
        public static final String USER_TAG = "user";
        public static final String MAX_PLAYERS = "max_players";

        private Lobby() {
        }
    }

    /**
     * Json tags to retrive elements from Json formatted game updates
     */
    public static class Game {

        public static final String SYNCHRO_ELEMENT_TAG = "synchro";
        public static final String TURN_TIME_REMAINING = "remaining";
        public static final String PLAYER_IS_ACTIVE = "player_active";
        public static final String PLAYER_IS_ONLINE = "player_online";

        private Game() {
        }


    }
    public static class ConfigurationConst {
        public static final String MAP_JSON_TAG = "map";
        public static final String MAPFIELD_JSON_TAG = "mapFields";
        public static final String NOBILITY_ROUTE_JSON_TAG = "nobilityRoute";
        public static final String NOBILITY_ROUTE_CELLS_JSON_TAG = "cells";
        public static final String PERMISSION_CARDS_JSON_TAG ="permissionCards";
        public static final String FIELD_TYPE_JSON_TAG = "zone";
        public static final String IS_DIRECT_JSON_TAG ="isDirect";
        public static final String TOWNS_JSON_TAG = "towns";
        public static final String NUMBER_OF_TOWNS_PER_FIELD_TAG = "numberOfTowns";
        public static final String TOWN_NAME_JSON_TAG = "townName";
        public static final String TOWN_SECTOR_JSON_TAG = "townSector";
        public static final String HAS_KING_JSON_TAG = "hasKing";
        public static final String NUMBER_OF_BONUSES_JSON_TAG = "numberOfBonuses";
        public static final String BONUSES_JSON_TAG = "bonuses";
        public static final String BONUS_TYPE_JSON_TAG = "type";
        public static final String QUANTITY_OF_BONUSES_JSON_TAG = "quantity";
        public static final String CONNECTIONS_JSON_TAG = "connections";
        public static final String TOWN_CONNECTIONS_JSON_TAG = "connectionsWithTowns";
        public static final String ANCHOR_CONNECTIONS_JSON_TAG ="connectionsWithAnchors";
        public static final String CELL_INDEX_JSON_TAG ="cellIndex";
        public static final String PERMITTED_TOWNS_JSON_TAG ="permittedTowns";
        public static final String DECK_JSON_TAG ="Deck";
        public static final String TOWN_COLOR_JSON_TAG = "townColor";
        public static final String RANDOM_BONUSES = "randomBonuses";

        private ConfigurationConst() {
        }
    }

}
