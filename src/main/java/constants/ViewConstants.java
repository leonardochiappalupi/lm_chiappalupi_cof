package constants;

import javafx.stage.Screen;

/**
 * Contains tags used mainly for JavaFX (GUI) releated events and IDs.
 */
public class ViewConstants {

    public static final String SOCKET_RADIO_BUTTON_ID = "socketRadioButton";
    public static final String RMI_RADIO_BUTTON_ID = "rmiRadioButton";
    public static final String GUI_RADIO_BUTTON_ID = "guiRadioButton";
    public static final String CLI_RADIO_BUTTON_ID = "cliRadioButton";
    private static final double LAUNCH_VIEW_HEIGHT_RATIO = 0.7;
    private static final double SCREEN_HEIGHT = getScreenHeight();
    public static final double SCREEN_WIDTH = getScreenWidth();
    public static final double LAUNCH_VIEW_HEIGHT = SCREEN_HEIGHT * ViewConstants.LAUNCH_VIEW_HEIGHT_RATIO;

    private ViewConstants(){}

    private static double getScreenHeight() {
        return Screen.getPrimary().getVisualBounds().getHeight();
    }

    private static double getScreenWidth() {

        return Screen.getPrimary().getVisualBounds().getWidth();
    }

}

