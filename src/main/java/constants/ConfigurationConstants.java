package constants;

import java.awt.*;

/**
 * contains mainly tags used by the configurator
 **/
public class ConfigurationConstants {

    public static final int NUMBER_OF_FIELDS = 3;
    public static final int NUMBER_OF_SECTORS_PER_FIELD = 6;
    public static final int NOBILITY_ROUTE_SIZE = 20;
    public static final int NUMBER_OF_COUNCILOR_COLORS = 6;
    public static final int NUMBER_OF_HELPERS_PER_PLAYER = 8;
    public static final int NUMBER_OF_COUNCILORS_PER_PLAYER = 12;
    public static final int NUMBER_OF_POLITICS_CARD_COLORS = 7;
    public static final int NUMBER_OF_POLITICS_CARDS = 23;
    public static final int NUMBER_OF_POLITICS_CARDS_PER_PLAYER = 6;
    public static final int NUMBER_OF_MINIMUM_BUILDING_PERMISSION_CARDS_FACTOR = 3;

    public static final int NUMBER_OF_TOWN_COLORS = 5;

    public static final String GENERIC_ANCHOR = "Anchor";
    public static final String ANCHOR0 = "Anchor 0";
    public static final String ANCHOR1 = "Anchor 1";
    public static final String ANCHOR2 = "Anchor 2";
    public static final String ANCHOR3 = "Anchor 3";
    public static final String ANCHOR4 = "Anchor 4";
    public static final String ANCHOR5 = "Anchor 5";

    public static final String NEW_CARD_DESCRIPTION = "new_card";

    public static final String MISSING_TOWN_NAME_ERROR_MESSAGE = "Please, specify a name for the town before proceeding";
    public static final String KING_COLOR_NOT_AVAILABLE_ERROR_MESSAGE = "The king color had already been selected for another town, please choose another one";
    public static final String TOWN_NAME_STARTING_WITH_SAME_LETTER_ERROR_MESSAGE = "The name you chose starts with the same letter of another one on the board, please change it";
    public static final String NO_ELEMENT_SELECTED_TO_MODIFY_ERROR_MESSAGE = "Choose an element to modify before pressing the button";
    public static final String NO_ELEMENT_SELECTED_TO_DELETE_ERROR_MESSAGE = "Choose an element to delete before pressing the button";
    public static final String WRONG_BONUS_QUANTITY_FORMAT_ERROR_MESSAGE = "Insert a quantity for the bonus before adding it (only digits)";
    public static final String BUILDING_PERMISSION_CARD_MISSING_ATTRIBUTES_ERROR_MESSAGE = "Add at least one bonus and one allowed town to add this card to the deck";
    public static final String NO_TOWN_TO_ADD_ERROR_MESSAGE = "You haven't configured any town yet!";
    public static final String NOT_ENOUGH_TOWNS_ERROR_MESSAGE = "The configuration is not valid; make sure you have configured at least one town per field";
    public static final String NOT_ENOUGH_BUILDING_PERMISSION_CARDS_1_ERROR_MESSAGE = "The configuration is not valid; make sure you have configured at least ";
    public static final String NOT_ENOUGH_BUILDING_PERMISSION_CARDS_2_ERROR_MESSAGE = " building permission cards";
    public static final String NOT_EVERY_TOWN_CONNECTED_ERROR_MESSAGE = "The configuration is not valid; make sure you haven't left any town isolated from others";
    public static final String KING_TOWN_MISSING_ERROR_MESSAGE = "The configuration is not valid; make sure you have configured the king town";
    public static final String NOT_EVERY_TOWN_HAS_BONUSES_ERROR_MESSAGE = "The configuration is not valid; make sure every town has at least one bonus configured";
    public static final String NOT_ENOUGH_RANDOM_BONUSES = "The configuration is not valid; you chose to use the random bonuses feature, so you have to configure a number of bonuses at least equal to the number of towns you already configured";
    public static final String TOWN_IN_USE_ERROR_MESSAGE = "The town you are trying to delete is allowed by a building permission card you\'ve already configured. Either delete that card or remove this town from its list, then retry.";
    public static final String CONFIGURATION_NAME_ALREADY_IN_USE_ERROR_MESSAGE = "The configuration name you chose is already in use, please choose another one";
    public static final String EMPTY_CONFIGURATION_NAME_ERROR_MESSAGE = "Insert a configuration name before proceeding";
    public static final String CANCEL_CONFIGURATION_CONFIRMATION = "Are you sure you want to cancel the current configuration process? Data will be lost permanently.";
    public static final String IMPORT_ALREADY_CONFIGURED_BONUSES_CONFIRMATION = "Do you want to import the bonuses you have already configured in towns?";

    public static final String CONFIGURATIONS_PATH = "./configurations";

    public static final String NO_CONFIGURATION_TO_LOAD_SELECTED_ERROR_MESSAGE = "Select a configuration to load from the drop down chooser before proceeding";

    public static final String DELETE_BONUS_ERROR_MESSAGE = "Select an element to remove before pressing the delete button";
    public static final String MISSING_QUANTITY_ERROR_MESSAGE = "Insert a quantity for the bonus before adding it (only digits)";
    public static final String DELETE_ROUTE_ERROR_MESSAGE = "Select an element to remove before pressing the delete button";

    public static final Color nobilityRouteCellEdited = new Color(200, 230, 201);
    public static final Color nobilityRouteCellNotEdited = new Color(224, 224, 224);

    private ConfigurationConstants() {
    }

}
