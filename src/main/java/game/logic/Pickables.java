package game.logic;

import game.Pickable;
import game.board.Field;
import game.board.Town;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.cards.PoliticsColor;

import java.io.Serializable;

/**
 * This enum lists all the different types of "item pick"
 */

public enum Pickables implements Serializable{
    ONE_COUNCIL(Field.class), ONE_COUNCILOR(PoliticsColor.class), UPTOFOUR_PC(PoliticsCard.class),
    ONE_FIELD_PERMIT(BuildingPermissionCard.class), ONE_YOURS_PERMIT(BuildingPermissionCard.class),
    ONE_PERMIT_TOWN(Town.class), KING_TOWNS(Town.class), EMPORIUM_TOWN(Town.class);

    private Class<? extends Pickable> pickedClass;

    Pickables(Class pickedClass) {
        this.pickedClass = pickedClass;
    }

    Class<? extends Pickable> getPickedClass() {
        return pickedClass;
    }
}
