package game.logic.actions;

import client.network.socket.SocketUpdatable;

import java.io.Serializable;

/**
 * Enum that lists all the possible actions of a player's turn between both {@link MainAction} and {@link SecondaryAction}
 * Indexes give a quick reference to a selected action.
 *
 */
public enum Action implements Serializable, SocketUpdatable {
    ELECT(true, "Elect a new councilor in a council (+4 coins)", 1),
    ACQUIRE_PERMIT(true, "Acquire one of the building permission cards of a field", 2),
    EMPORIUM_PERMIT(true, "Build an emporium in one of the towns on your permission cards", 3),
    EMPORIUM_KING(true, "Build an emporium moving the king", 4),
    ACQUIRE_HELPER(false, "Obtain one helper from the stock (-1 coin)", 5),
    SWAP_PERMIT(false, "Draw new permission cards in one of the councils", 6),
    HELPER_ELECT(false, "Elect a councilor in a council (-3 helper)", 7),
    ADDITIONAL_MAIN(false, "Perform an additional main action", 8),
    BONUS(false, "", 0);

    boolean isMain;
    String description;
    int index;

    Action(boolean isMain, String description, int index) {
        this.isMain = isMain;
        this.description = description;
        this.index = index;
    }

    public boolean isMain() {
        return isMain;
    }

    public String getDescription() {
        return description;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return String.format("[%d][%s] %s;", index, name(), description);
    }

}
