package game.logic;


import client.network.socket.SocketUpdatable;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.cards.PoliticsColor;
import game.pawns.Emporium;
import game.pawns.Helper;
import view.MyColor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class is used by both the Client and the Server to have a quick recap of a player's state.
 */

public class Player implements Serializable, SocketUpdatable, Comparable {
    private int winPosition;
    private int nobilityPosition;
    private ArrayList<Helper> helpers = new ArrayList<>();
    private ArrayList<Emporium> emporiums = new ArrayList<>();
    private ArrayList<BuildingPermissionCard> buildingPermissionCards = new ArrayList<>();
    private ArrayList<PoliticsCard> politicsCard = new ArrayList<>();
    private int coins;
    private String username;
    private boolean enabled = true;
    private boolean disconnected = false;
    private MyColor myColor;

    public Player() {
        //USED FOR TESTING
    }

    public Player(String username) {
        this.username = username;
    }

    public boolean isDisconnected() {
        return disconnected;
    }

    public void setDisconnected(boolean disconnected) {
        this.disconnected = disconnected;
    }

    public void setMyColor(MyColor color) {
        this.myColor = color;
    }

    public MyColor getMyColor() {
        return myColor;
    }

    /**
     * Defines whether this player is enabled to perform actions in this turn or not. Set to true by server only,
     * set to false by player itself at the end of the turn.
     *
     * @return
     */
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


    public void addHelpers(int quantity) {
        for (int i = 0; i < quantity; i++)
            helpers.add(new Helper(1));
    }

    public void removeHelpers(int quantity) {
        for (int i = 0; i < quantity; i++)
            helpers.remove(0);
    }

    public List getEmporiums() {
        return emporiums;
    }


    public void addEmporium(Emporium emporium) {
        this.emporiums.add(emporium);
    }

    public void addPermissionCards(BuildingPermissionCard card) {
        this.buildingPermissionCards.add(card);
    }

    public void addPoliticsCard(PoliticsCard card) {
        this.politicsCard.add(card);
    }

    public List<BuildingPermissionCard> getBuildingPermissionCards(boolean includeCovered) {
        if (includeCovered)
            return buildingPermissionCards;
        else {
            ArrayList<BuildingPermissionCard> cardsToReturn = new ArrayList<>();
            for (BuildingPermissionCard card : buildingPermissionCards)
                if (!card.isFlipped())
                    cardsToReturn.add(card);
            return cardsToReturn;
        }
    }

    public List<PoliticsCard> getPoliticsCard() {
        return politicsCard;
    }

    public int getWinPosition() {
        return winPosition;
    }

    public void setWinPosition(int winPosition) {
        this.winPosition = winPosition;
    }

    public int getNobilityPosition() {
        return nobilityPosition;
    }

    public void setNobilityPosition(int nobilityPosition) {

        this.nobilityPosition = nobilityPosition;
        if (nobilityPosition < 0)
            this.nobilityPosition = 0;
        else if (nobilityPosition > 19)
            this.nobilityPosition = 19;
    }

    public List<Helper> getHelpers() {
        return helpers;
    }


    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void increaseWinPositionBy(int diff) {
        if (diff > 0)
            this.winPosition += diff;
    }

    public void increaseNobilityPositionBy(int diff) {

        this.nobilityPosition += diff;
        if (nobilityPosition > 19)
            this.nobilityPosition = 19;
    }

    public void increaseCoinsBy(int diff) {
        this.coins += diff;
    }

    public void decreaseCoinsBy(int diff) {
        this.coins -= diff;
        if (coins < 0)
            coins = 0;
    }

    public List<PoliticsCard> getPoliticsCardOfColor(PoliticsColor color) {
        return politicsCard.stream().filter(card -> card.getColor().equals(color)).collect(Collectors.toList());
    }

    public BuildingPermissionCard getPermitCardInstance(BuildingPermissionCard card) {
        for (BuildingPermissionCard localCard : buildingPermissionCards) {
            if (localCard.equals(card))
                return localCard;
        }
        return null;
    }

    public void removePoliticsCards(List<PoliticsCard> cards) {
        for (PoliticsCard card : cards) {
            if (!getPoliticsCardOfColor(card.getColor()).isEmpty()) {
                politicsCard.remove(getPoliticsCardOfColor(card.getColor()).get(0));
            }
        }
    }

    @Override
    public String toString() {
        return String.format("[%s]\t[%d/99] coins: %d, nobility: %d, politics: %d, permission: %d",
                getUsername(),
                winPosition,
                coins,
                nobilityPosition,
                politicsCard.size(),
                buildingPermissionCards.size());
    }

    /**
     * This method is used at the end of the game to compare two players according to the game rules.
     *
     * @param o is the other player to compare to.
     * @return is 1 if this player has better statistics, -1 if he has worse statistics, 0 otherwise.
     */

    @Override
    public int compareTo(Object o) {
        Player otherPlayer = (Player) o;
        if (winPosition > otherPlayer.getWinPosition())
            return 1;
        else if (winPosition < otherPlayer.getWinPosition())
            return -1;
        else {
            int thisPlayerCardsAndHelpers = helpers.size() + politicsCard.size();
            int otherPlayerCardsAndHelpers = otherPlayer.getHelpers().size() + otherPlayer.getPoliticsCard().size();

            if (thisPlayerCardsAndHelpers > otherPlayerCardsAndHelpers)
                return 1;
            else if (thisPlayerCardsAndHelpers < otherPlayerCardsAndHelpers)
                return -1;
            else if (emporiums.size() > otherPlayer.getEmporiums().size())
                return 1;
            else if (emporiums.size() < otherPlayer.getEmporiums().size())
                return -1;
            else return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Player)
            return ((Player) o).getUsername().equals(username);
        else return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

