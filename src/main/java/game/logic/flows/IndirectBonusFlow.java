package game.logic.flows;

import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Town;
import game.bonus.DirectBonus;
import game.bonus.IndirectBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Card;
import game.cards.CardType;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IndirectBonusFlow extends Flow {
    private IndirectBonus bonus;
    private BonusProcessor.IndirectBonusAsyncHandler handler;

    public IndirectBonusFlow(BonusProcessor.IndirectBonusAsyncHandler handler, RulesBroker rulesBroker, RemotePlayer remotePlayer, IndirectBonus bonus) {
        super(rulesBroker, remotePlayer);
        this.bonus = bonus;
        this.handler = handler;
        action = Action.BONUS;
    }

    @Override
    public void start() {
        try {
            if (bonus.isCollectsFromBuildingCard()) {
                int playerBuildingCards = rulesBroker.getThisPlayer(this).getBuildingPermissionCards(true).size();
                if (playerBuildingCards > 0) {
                    remotePlayer.getPickManager().pickYourPermissionCard(Action.BONUS);
                } else {
                    Logger.getGlobal().log(Level.INFO, String.format("Skipped processing indirect bonus from card because player" +
                            "has no cards (%s)", rulesBroker.getThisPlayer(this).getUsername()));
                    handler.unlock();
                }

            } else if (bonus.isCollectsFromTown()) {
                double playersEmporiums = rulesBroker.getMainLocalState().getMap().getAllTowns()
                        .parallelStream()
                        .filter(town -> town.hasEmporiumOfPlayer(rulesBroker.getThisPlayer(this)))
                        .count();
                if (playersEmporiums > 0) {
                    remotePlayer.getPickManager().pickEmporiumTown(Action.BONUS);
                } else {
                    Logger.getGlobal().log(Level.INFO, String.format("Skipped processing indirect bonus from town because player" +
                            "has no emporiums (%s)", rulesBroker.getThisPlayer(this).getUsername()));
                    handler.unlock();
                }
            }

        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            handler.unlock();
        }
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws GameLogicException {

        Town selectedTown = (Town) items[0];
        for (DirectBonus dbonus : selectedTown.getBonusList())
            new BonusProcessor(this, rulesBroker, dbonus, Action.BONUS).process();
        handler.unlock();
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns) {
        //No need to pick pawns
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards) throws GameLogicException {

        BuildingPermissionCard card = (BuildingPermissionCard) cards[0];
        for (DirectBonus dbonus : card.getBonusList())
            new BonusProcessor(this, rulesBroker, dbonus, Action.BONUS).process();
        handler.unlock();
    }
}
