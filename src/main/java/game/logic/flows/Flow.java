package game.logic.flows;

import game.board.*;
import game.bonus.DirectBonus;
import game.cards.*;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.ItemsNotValidException;
import game.logic.exceptions.NotEnoughAvailableException;
import game.logic.exceptions.NotEnoughCreditsException;
import game.pawns.Councilor;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



public abstract class Flow implements PickedCallbacks {
    RulesBroker rulesBroker;
    RemotePlayer remotePlayer;
    Action action;

    Flow(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        this.rulesBroker = rulesBroker;
        this.remotePlayer = remotePlayer;
    }

    public RemotePlayer getCreatorRemotePlayer() {
        return remotePlayer;
    }

    public abstract void start();

    void processCards(Card[] cards, Council council) throws NotEnoughCreditsException, ItemsNotValidException {
        PoliticsCard[] politicsCards = new PoliticsCard[cards.length];
        for (int i = 0; i < cards.length; i++) {
            politicsCards[i] = (PoliticsCard) cards[i];
        }
        try {
            int jollyCards = checkCardsCorrectness(politicsCards, council);
            int coinsToDecrease = Math.round((-3.3f * cards.length) + 13.5f);

            coinsToDecrease += jollyCards;
            if (rulesBroker.getThisPlayer(this).getCoins() >= coinsToDecrease) {

                rulesBroker.getThisPlayer(this).decreaseCoinsBy(coinsToDecrease);
                rulesBroker.getThisPlayer(this).removePoliticsCards(Arrays.asList(politicsCards));

                remotePlayer.getGameCommandsBroadcaster().updateOrAddPlayer(rulesBroker.getThisPlayer(this));

            } else
                throw new NotEnoughCreditsException(rulesBroker.getThisPlayer(this).getUsername(), "coins", "using cards for a council");
        } catch (ItemsNotValidException e) {
            Logger.getGlobal().log(Level.INFO, e.getMessage(), e);
            throw e;
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.INFO, e.getMessage(), e);
        }
    }


    private int checkCardsCorrectness(PoliticsCard[] politicsCards, Council council) throws ItemsNotValidException {
        if (politicsCards.length > 4 || politicsCards.length == 0)
            throw new ItemsNotValidException();

        int jollyCards = 0;
        ArrayList<PoliticsCard> tempCards = new ArrayList<>();
        tempCards.addAll(Arrays.asList(politicsCards));
        ArrayList<Councilor> tempCouncil = new ArrayList<>();
        tempCouncil.addAll(council.getCouncilors());

        for (PoliticsCard card : politicsCards) {
            if (card.getColor().equals(PoliticsColor.JOLLY)) {
                jollyCards++;
            } else {
                council.getCouncilors().stream()
                        .filter(councilor -> card.getColor().equals(councilor.getColor()))
                        .forEachOrdered(councilor -> {
                            tempCards.remove(card);
                            tempCouncil.remove(councilor);
                        });
            }

        }
        if (tempCards.isEmpty() || tempCards.size() == jollyCards)
            return jollyCards;
        else throw new ItemsNotValidException();


    }

    ArrayList<DirectBonus> exploreTownGraph(Town fromTown, ArrayList<Town> visited, ArrayList<DirectBonus> bonusList) {

        ArrayList<Town> townsToExplore = new ArrayList<>();
        townsToExplore.addAll(fromTown.getConnectedTowns());
        for (Anchor a : fromTown.getConnectedAnchors()) {
            MapField field = a.getLeftField();
            for (Town t : field.getAllTowns(false))
                if (t.getConnectedAnchors().contains(a))
                    townsToExplore.add(t);
            field = a.getRightField();
            for (Town t : field.getAllTowns(false))
                if (t.getConnectedAnchors().contains(a))
                    townsToExplore.add(t);
        }

        for (Town t : townsToExplore) {
            if (t.hasEmporiumOfPlayer(rulesBroker.getThisPlayer(this)) && !visited.contains(t)) {
                visited.add(t);
                bonusList.addAll(t.getBonusList());
                exploreTownGraph(t, visited, bonusList);
            }

        }

        return bonusList;
    }

    void checkBonusTiles(Town newTown) {

        TownColorBonusTile colorBonusTile = rulesBroker.getMainLocalState().getTownColorBonusTile(newTown.getColor());
        Field fieldOfTown = rulesBroker.getMainLocalState().getMap().getFieldOfTown(newTown);
        FieldBonusTile fieldBonusTile = rulesBroker.getMainLocalState().getMap().getMapField(fieldOfTown).getBonusTile();
        int totalWinSteps = 0;

        //AN EMPORIUM IN ALL TOWNS OF SAME COLOR
        if (!colorBonusTile.hasBeenDrawn()) {
            boolean color = true;
            for (Town t : rulesBroker.getMainLocalState().getMap().getAllTowns()) {
                if (t.getColor()
                        .equals(newTown
                                .getColor()) &&
                        !t.hasEmporiumOfPlayer(rulesBroker
                                .getThisPlayer(this))) {
                    color = false;
                }
            }
            if (color) {
                colorBonusTile.setHasBeenDrawn(true);
                totalWinSteps += colorBonusTile.getWinSteps();

            }
        }


        //AN EMPORIUM IN ALL TOWNS OF A FIELD
        if (fieldBonusTile != null && !fieldBonusTile.hasBeenDrawn()) {
            boolean field = true;

            for (Town t : rulesBroker.getMainLocalState().getMap().getMapField(fieldOfTown).getAllTowns(false)) {

                if (!t.hasEmporiumOfPlayer(rulesBroker.getThisPlayer(this))) {
                    field = false;
                }
            }

            if (field) {
                fieldBonusTile.setHasBeenDrawn(true);
                totalWinSteps += fieldBonusTile.getWinSteps();
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateFieldCards(fieldOfTown, null, null, null, true));
            }
        }

        elaborateTotalWinsteps(totalWinSteps);


    }

    private void elaborateTotalWinsteps(int totalWinSteps) {
        if (totalWinSteps > 0) {
            KingBonusTile kingBonusTile = rulesBroker.getMainLocalState().getNextAvailableKingBonusTile();
            if (kingBonusTile != null) {
                totalWinSteps += kingBonusTile.getWinSteps();
                kingBonusTile.setHasBeenDrawn(true);
            }
            rulesBroker.getThisPlayer(this).increaseWinPositionBy(totalWinSteps);
            remotePlayer.getGameRoom().updateEveryone(gc -> {
                gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this));
                gc.updateKingTiles(rulesBroker.getMainLocalState()
                        .getTownColorBonusTiles(), rulesBroker.getMainLocalState().getKingBonusTiles());
            });
        }
    }

    public void processMultipleBonuses(List<DirectBonus> bonuses) throws NotEnoughAvailableException {
        for (DirectBonus bonus : bonuses) {
            new BonusProcessor(this, rulesBroker, bonus, action).process();
        }
    }


}
