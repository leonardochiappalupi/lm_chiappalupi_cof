package game.logic.flows;

import client.GameCommands;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Council;
import game.board.Field;
import game.cards.Card;
import game.cards.CardType;
import game.cards.PoliticsColor;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.ItemsNotValidException;
import game.pawns.Councilor;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ElectCouncilorWithHelper extends Flow {
    private Council selectedCouncil;
    private Field field;

    public ElectCouncilorWithHelper(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        super(rulesBroker, remotePlayer);
        action = Action.HELPER_ELECT;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws ItemsNotValidException {
        if (type == BoardItemType.COUNCIL && items.length == 1) {
            field = (Field) items[0];
            selectedCouncil = rulesBroker.getMainLocalState().getMap().getMapField(field).getCouncil();
            try {
                remotePlayer.getPickManager().pickACouncilor(action);
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        } else throw new ItemsNotValidException();
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns) {
        if (type == PawnType.COUNCILOR && pawns.length == 1) {
            PoliticsColor pickedCouncilorColor = (PoliticsColor) pawns[0];
            Councilor pickedCouncilor = rulesBroker.getMainLocalState().getCouncilorWithColor(pickedCouncilorColor, true);
            Councilor removedCouncilor = selectedCouncil.insertCouncilor(pickedCouncilor);
            rulesBroker.getMainLocalState().addAvailableCouncilor(removedCouncilor);

            try {
                remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(action);

                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateCouncil(field, selectedCouncil));
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateAvailableCouncilors(rulesBroker.getMainLocalState().getAvailableCouncilors()));
                remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
                remotePlayer.getGameRoom().getRulesBroker().markCurrentStateAsValid();

            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards){
        //No need to pick cards
    }

    @Override
    public void start() {
        rulesBroker.getThisPlayer(this).removeHelpers(1);
        try {
            remotePlayer.getPickManager().pickACouncil(action, true);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
