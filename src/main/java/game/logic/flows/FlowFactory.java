package game.logic.flows;


import game.logic.RulesBroker;
import game.logic.actions.Action;
import server.entities.RemotePlayer;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FlowFactory {
    private RulesBroker rulesBroker;
    private Map<Action, Class> classesMap = new HashMap<>();

    public FlowFactory(RulesBroker rulesBroker) {

        this.rulesBroker = rulesBroker;
        classesMap.put(Action.ELECT, ElectFlow.class);
        classesMap.put(Action.ACQUIRE_PERMIT, AcquirePermissionFlow.class);
        classesMap.put(Action.EMPORIUM_KING, KingPermit.class);
        classesMap.put(Action.EMPORIUM_PERMIT, EmporiumPermit.class);
        classesMap.put(Action.ACQUIRE_HELPER, EngageAssistant.class);
        classesMap.put(Action.SWAP_PERMIT, ChangeBuildingPermissionCards.class);
        classesMap.put(Action.HELPER_ELECT, ElectCouncilorWithHelper.class);
        classesMap.put(Action.ADDITIONAL_MAIN, AddictionalMainAction.class);
    }

    public Flow createFlow(RemotePlayer remotePlayer, Action action) {
       /* switch (action) {
            case ELECT:
                ElectFlow.class.getConstructor(RemotePlayer.class,Action.class).newInstance(remotePlayer,action);
                return new ElectFlow(rulesBroker, remotePlayer);
            case ACQUIRE_PERMIT:
                return new AcquirePermissionFlow(rulesBroker, remotePlayer);
            case EMPORIUM_KING:
                return new KingPermit(rulesBroker, remotePlayer);
            case EMPORIUM_PERMIT:
                return new EmporiumPermit(rulesBroker, remotePlayer);
            case ACQUIRE_HELPER:
                return new EngageAssistant(rulesBroker, remotePlayer);
            case SWAP_PERMIT:
                return new ChangeBuildingPermissionCards(rulesBroker, remotePlayer);
            case HELPER_ELECT:
                return new ElectCouncilorWithHelper(rulesBroker, remotePlayer);
            case ADDITIONAL_MAIN:
                return new AddictionalMainAction(rulesBroker, remotePlayer);
            case BONUS:
            default:
                return null;
        }*/
        Flow instance = null;
        try {
            instance = (Flow) classesMap.get(action).getConstructor(RulesBroker.class, RemotePlayer.class).newInstance(rulesBroker, remotePlayer);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        return instance;
    }

}
