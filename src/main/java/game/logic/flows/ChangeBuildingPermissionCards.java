package game.logic.flows;

import client.GameCommands;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Field;
import game.board.MapField;
import game.cards.BuildingPermissionCard;
import game.cards.Card;
import game.cards.CardType;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChangeBuildingPermissionCards extends Flow {

    public ChangeBuildingPermissionCards(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        super(rulesBroker, remotePlayer);
        action = Action.SWAP_PERMIT;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) {
        if (items.length == 1 && type == BoardItemType.COUNCIL) {
            Field fieldSelected = (Field) items[0];
            MapField mapField = rulesBroker.getMainLocalState().getMap().getMapField(fieldSelected);

            BuildingPermissionCard leftCard = mapField.getLeftShownPermissionCard();
            BuildingPermissionCard rightCard = mapField.getRightShownPermissionCard();

            if (rightCard != null)
                mapField.getBuildingPermissionCardsDeck().addCard(rightCard);

            if (leftCard != null)
                mapField.getBuildingPermissionCardsDeck().addCard(leftCard);

            mapField.drawShownPermissionCard(leftCard);
            mapField.drawShownPermissionCard(rightCard);

            rulesBroker.getThisPlayer(this).removeHelpers(1);
            rulesBroker.getMainLocalState().addAvailableHelpers(1);
            try {
                remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(action);
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
            remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateAvailableHelpers(rulesBroker.getMainLocalState().getAvailableHelpers().size()));
            remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));
            remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateFieldCards(fieldSelected, mapField.getBuildingPermissionCardsDeck(), mapField.getLeftShownPermissionCard(), mapField.getRightShownPermissionCard(), mapField.getBonusTile().hasBeenDrawn()));
            remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
            remotePlayer.getGameRoom().getRulesBroker().markCurrentStateAsValid();


        }
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns) {
        //No need to pick pawns
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards) {
        //No need to pick cards
    }

    @Override
    public void start() {

        try {
            remotePlayer.getPickManager().pickACouncil(action, false);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
