package game.logic.flows;

import client.GameCommands;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Council;
import game.board.Field;
import game.cards.Card;
import game.cards.CardType;
import game.cards.PoliticsColor;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.ItemsNotValidException;
import game.pawns.Councilor;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ElectFlow extends Flow {

    private Field councilField;
    private Council council;

    public ElectFlow(RulesBroker rulesBroker, RemotePlayer remotePlayer) {

        super(rulesBroker, remotePlayer);
        this.action = Action.ELECT;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws ItemsNotValidException {
        if (items.length == 1 && type == BoardItemType.COUNCIL) {
            councilField = (Field) items[0];
            council = rulesBroker.getMainLocalState().getMap().getMapField(councilField).getCouncil();
        } else if (items.length == 1 && type == BoardItemType.KINGCOUNCIL) {
            council = rulesBroker.getMainLocalState().getKingCouncil();

        } else throw new ItemsNotValidException();

        try {
            remotePlayer.getPickManager().pickACouncilor(action);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns) {

        if (pawns.length == 1 && type == PawnType.COUNCILOR) {

            PoliticsColor pickedCouncilorColor = (PoliticsColor) pawns[0];
            Councilor pickedCouncilor = rulesBroker.getMainLocalState().getCouncilorWithColor(pickedCouncilorColor, true);
            Councilor removedCouncilor = council.insertCouncilor(pickedCouncilor);
            rulesBroker.getMainLocalState().addAvailableCouncilor(removedCouncilor);
            rulesBroker.getThisPlayer(this).increaseCoinsBy(4);

            try {
                if (councilField != null)
                    remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateCouncil(councilField, council));
                else
                    remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateKingCouncil(council));
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateAvailableCouncilors(rulesBroker.getMainLocalState().getAvailableCouncilors()));
                remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(action);
                remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
                remotePlayer.getGameRoom().getRulesBroker().markCurrentStateAsValid();

            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }

        }
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards){
        /**No need of this callback here**/
    }

    @Override
    public void start() {
        try {
            remotePlayer.getPickManager().pickACouncil(action, true);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }

    }
}
