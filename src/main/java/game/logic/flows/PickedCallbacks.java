package game.logic.flows;

import game.board.BoardItem;
import game.board.BoardItemType;
import game.cards.Card;
import game.cards.CardType;
import game.logic.exceptions.*;
import game.pawns.Pawn;
import game.pawns.PawnType;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface is implemented by abstract class {@link Flow} which extensions represent every possible action flow.
 * Its implementations are used to give a response to the server whenever a player chooses something from the board.
 */

public interface PickedCallbacks extends Remote{

    void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws RemoteException, GameLogicException;

    void onPawnsPicked(PawnType type, Pawn... pawns) throws RemoteException;

    void onCardsPicked(CardType type, Card... cards) throws RemoteException, GameLogicException;

}
