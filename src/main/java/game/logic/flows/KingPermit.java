package game.logic.flows;

import client.GameCommands;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Town;
import game.bonus.DirectBonus;
import game.cards.Card;
import game.cards.CardType;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.ItemsNotValidException;
import game.logic.exceptions.NotEnoughCreditsException;
import game.pawns.Emporium;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KingPermit extends Flow {

    public KingPermit(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        super(rulesBroker, remotePlayer);
        this.action = Action.EMPORIUM_KING;

    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws GameLogicException {
        if (type != BoardItemType.TOWN || ((Town) items[items.length - 1]).hasEmporiumOfPlayer(rulesBroker.getThisPlayer(this)))
            throw new ItemsNotValidException();

        //THIS EXPECTS AN ARRAY THAT STARTS WITH THE TOWN THAT CURRENTLY HAS THE KING
        int coinsRequired = (items.length - 1) * 2;
        if (rulesBroker.getThisPlayer(this).getCoins() >= coinsRequired) {
            rulesBroker.getThisPlayer(this).decreaseCoinsBy(coinsRequired);

            Town newKingTown = rulesBroker.getMainLocalState().getMap().getTownWithName(((Town) items[items.length - 1]).getName());
            Town oldKingTown = rulesBroker.getMainLocalState().getMap().getTownWithName(((Town) items[0]).getName());

            oldKingTown.setHasKing(false);
            newKingTown.setHasKing(true);

            int helpersRequired = newKingTown.getEmporiums().size();
            if (helpersRequired <= rulesBroker.getThisPlayer(this).getHelpers().size()) {

                rulesBroker.getMainLocalState().addAvailableHelpers(helpersRequired);
                rulesBroker.getThisPlayer(this).removeHelpers(helpersRequired);

                Emporium newEmporium = new Emporium(rulesBroker.getThisPlayer(this));
                newEmporium.setTown(newKingTown);
                rulesBroker.getThisPlayer(this).addEmporium(newEmporium);
                newKingTown.addEmporium(newEmporium);

                if (rulesBroker.getThisPlayer(this).getEmporiums().size() >= rulesBroker.getEmporiumsToWin()) {
                    remotePlayer.getGameRoom().notifyEndGameCondition();
                    rulesBroker.getThisPlayer(this).increaseWinPositionBy(3);
                }

                remotePlayer.getGameRoom().updateEveryone(gc -> gc.addEmporiumInTown(newKingTown.getName(), rulesBroker.getThisPlayer(this)));
                ArrayList<DirectBonus> bonuses = exploreTownGraph(newKingTown, new ArrayList<>(), new ArrayList<>());

                for (DirectBonus b : bonuses)
                    new BonusProcessor(this, rulesBroker, b, action).process();

                checkBonusTiles(newKingTown);

                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateAvailableHelpers(rulesBroker.getMainLocalState().getAvailableHelpers().size()));
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.moveKingToTown(newKingTown.getName()));
                remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
                rulesBroker.markCurrentStateAsValid();
                try {
                    remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(action);
                } catch (RemoteException e) {
                    Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                }


            } else {
                rulesBroker.revertToLastValidGameState();
                remotePlayer.getGameRoom().updateEveryone(GameCommands::rollbackToLastValidGameState);
                throw new NotEnoughCreditsException(rulesBroker.getThisPlayer(this).getUsername(), "helpers", "building emporium");
            }
        } else {
            rulesBroker.revertToLastValidGameState();
            remotePlayer.getGameRoom().updateEveryone(GameCommands::rollbackToLastValidGameState);
            throw new NotEnoughCreditsException(rulesBroker.getThisPlayer(this).getUsername(), "coins", "moving king");
        }


    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns) {
        /**No need of this callback here**/
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards) throws ItemsNotValidException, NotEnoughCreditsException {
        if (type == CardType.POLITICS) {

            try {
                processCards(cards, rulesBroker.getMainLocalState().getKingCouncil());
                remotePlayer.getPickManager().pickTownsToMoveKing(action);
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            } catch (ItemsNotValidException e) {
                remotePlayer.getGameRoom().updateEveryone(GameCommands::rollbackToLastValidGameState);
                rulesBroker.revertToLastValidGameState();
                throw e;
            }
        }
    }

    @Override
    public void start() {
        try {
            remotePlayer.getPickManager().pickUpTo4PoliticsCards(action);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }
}