package game.logic.flows;

import client.GameCommands;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.cards.Card;
import game.cards.CardType;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddictionalMainAction extends Flow {

    public AddictionalMainAction(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        super(rulesBroker, remotePlayer);
        action = Action.ADDITIONAL_MAIN;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items){
        //No need to pick anything
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns){
        //No need to pick anything
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards){
        //No need to pick anything
    }

    @Override
    public void start() {
        /**
         * This expects gui or logic to have already checked if player has >=3 helpers
         */
        rulesBroker.getThisPlayer(this).removeHelpers(3);
        rulesBroker.getMainLocalState().addAvailableHelpers(3);
        try {
            remotePlayer.getGameCommandsBroadcaster().allowOneMoreAction();
            remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(action);
            remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
            remotePlayer.getGameRoom().getRulesBroker().markCurrentStateAsValid();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
