package game.logic.flows;

import client.GameCommands;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.cards.Card;
import game.cards.CardType;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EngageAssistant extends Flow {

    public EngageAssistant(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        super(rulesBroker, remotePlayer);
        action = Action.ACQUIRE_HELPER;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items){
        //No need to pick board items
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns){
        //No need to pick pawns
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards){
        //No need to pick cards
    }

    @Override
    public void start() {
        /**
         * This expects the gui or the game logic to have already checked if player has >=3 coins
         */
        rulesBroker.getMainLocalState().consumeAvailableHelpers(1);
        rulesBroker.getThisPlayer(this).addHelpers(1);
        rulesBroker.getThisPlayer(this).decreaseCoinsBy(3);

        remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));
        remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateAvailableHelpers(rulesBroker.getMainLocalState().getAvailableHelpers().size()));

        try {
            remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(action);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
        remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
        remotePlayer.getGameRoom().getRulesBroker().markCurrentStateAsValid();

    }
}
