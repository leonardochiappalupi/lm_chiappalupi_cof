package game.logic.flows;

import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.bonus.IBonus;
import game.bonus.IndirectBonus;
import game.cards.PoliticsCard;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.NotEnoughAvailableException;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BonusProcessor {
    private DirectBonus bonus;
    private Action context;
    private RemotePlayer remotePlayer;
    private RulesBroker rulesBroker;
    private Flow callingFlow;
    private IndirectBonusAsyncHandler indirectBonusAsyncHandler;

    public BonusProcessor(Flow callingFlow, RulesBroker broker, DirectBonus bonus, Action context) {
        this.rulesBroker = broker;
        this.bonus = bonus;
        this.context = context;
        this.callingFlow = callingFlow;
        this.remotePlayer = callingFlow.getCreatorRemotePlayer();
    }

    public void process() throws NotEnoughAvailableException {

        BonusType type = bonus.getElementsType();
        int quantity = bonus.getElementsQuantity();
        switch (type) {
            case CARDS:
                giveCardsToPlayer(quantity);
                break;
            case HELPERS:
                giveHelpersToPlayer(quantity);
                break;
            case COINS:
                rulesBroker.getThisPlayer(callingFlow).increaseCoinsBy(quantity);
                updateThisPlayerForAll();
                break;
            case WINSTEPS:
                rulesBroker.getThisPlayer(callingFlow).increaseWinPositionBy(quantity);
                updateThisPlayerForAll();
                break;
            case NOBSTEPS:
                rulesBroker.getThisPlayer(callingFlow).increaseNobilityPositionBy(quantity);
                updateThisPlayerForAll();
                additionalBonuses();
                break;
            case SUPPACTION:
                allowOneMoreActionChecked();
                break;
            default:
                break;
        }
        remotePlayer.getGameRoom().updateEveryone(gc -> gc.onBonusReceived(bonus, context, rulesBroker.getThisPlayer(callingFlow)));

    }

    private void additionalBonuses() throws NotEnoughAvailableException {
        List<IBonus> additionalBonuses = rulesBroker.getMainLocalState().getNobilityRoute().getBonusesOfCell(rulesBroker.getThisPlayer(callingFlow).getNobilityPosition());
        for (IBonus ibonus : additionalBonuses) {
            if (ibonus instanceof IndirectBonus) {

                if (indirectBonusAsyncHandler == null)
                    indirectBonusAsyncHandler = new IndirectBonusAsyncHandler();

                indirectBonusAsyncHandler.addBonusToProcess((IndirectBonus) ibonus);

            } else if (ibonus instanceof DirectBonus)
                new BonusProcessor(callingFlow, rulesBroker, (DirectBonus) ibonus, context).process();
        }
        if (indirectBonusAsyncHandler != null)
            indirectBonusAsyncHandler.start();

    }

    private void giveCardsToPlayer(int quantity) throws NotEnoughAvailableException {
        if (quantity <= rulesBroker.getMainLocalState().getPoliticsCardsDeck().getCards().size()) {
            for (int i = 0; i < quantity; i++)
                rulesBroker.getThisPlayer(callingFlow).addPoliticsCard((PoliticsCard) rulesBroker.getMainLocalState().getPoliticsCardsDeck().draw(false));

            remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(callingFlow)));
            remotePlayer.getGameRoom().updateEveryone(gc -> gc.updatePoliticsCardsDeck(rulesBroker.getMainLocalState().getPoliticsCardsDeck()));

        } else throw new NotEnoughAvailableException("PC", "collect bonus PC");
    }

    private void giveHelpersToPlayer(int quantity) throws NotEnoughAvailableException {
        if (quantity <= rulesBroker.getMainLocalState().getAvailableHelpers().size()) {

            rulesBroker.getThisPlayer(callingFlow).addHelpers(quantity);
            rulesBroker.getMainLocalState().consumeAvailableHelpers(quantity);

            remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(callingFlow)));
            remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateAvailableHelpers(rulesBroker.getMainLocalState().getAvailableHelpers().size()));

        } else throw new NotEnoughAvailableException("helpers", "collect bonus helpers");
    }

    private void updateThisPlayerForAll() {
        remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(callingFlow)));
    }


    public class IndirectBonusAsyncHandler extends Thread {
        private List<IndirectBonus> bonusesToProcess = new ArrayList<>();
        private final BonusProcessor bonusProcessor = BonusProcessor.this;
        private boolean locked;

        public void addBonusToProcess(IndirectBonus bonus) {
            bonusesToProcess.add(bonus);
        }


        @Override
        public void run() {
            for (IndirectBonus bonusToProcess : bonusesToProcess) {
                locked = true;
                remotePlayer.startIndirectBonusProcess(bonusToProcess, this);
                while (locked)
                    synchronized (bonusProcessor) {
                        try {
                            bonusProcessor.wait();
                        } catch (InterruptedException e) {
                            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                            Thread.currentThread().interrupt();
                        }
                    }
                try {
                    remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(Action.BONUS);
                } catch (RemoteException e) {
                    Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }

        public void unlock() {
            locked = false;
            synchronized (bonusProcessor) {
                bonusProcessor.notify();
            }
        }
    }

    private void allowOneMoreActionChecked() {
        try {
            remotePlayer.getGameCommandsBroadcaster().allowOneMoreAction();
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
