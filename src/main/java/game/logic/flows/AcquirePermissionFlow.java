package game.logic.flows;

import client.GameCommands;
import game.board.*;
import game.cards.BuildingPermissionCard;
import game.cards.Card;
import game.cards.CardType;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.ItemsNotValidException;
import game.logic.exceptions.NotEnoughCreditsException;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AcquirePermissionFlow extends Flow {

    private Field field;
    private Council council;

    public AcquirePermissionFlow(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        super(rulesBroker, remotePlayer);
        this.action = Action.ACQUIRE_PERMIT;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws GameLogicException {
        if (items.length == 1 && type == BoardItemType.COUNCIL) {
            this.field = (Field) items[0];
            this.council = rulesBroker.getMainLocalState().getMap().getMapField(field).getCouncil();
            try {
                remotePlayer.getPickManager().pickUpTo4PoliticsCards(action);
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        } else throw new ItemsNotValidException();
    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns) {
        /**
         * No need of this callback here
         */
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards) throws GameLogicException {
        try {
            if (type == CardType.POLITICS) {
                processCardsOrThrow(cards);
                remotePlayer.getPickManager().pickFieldPermissionCard(field, action);

            } else if (type == CardType.BUILDINGPERMISSION && cards[0] != null) {

                BuildingPermissionCard card = (BuildingPermissionCard) cards[0];
                MapField mapField = council.getField();
                mapField.drawShownPermissionCard(card);

                rulesBroker.getThisPlayer(this).addPermissionCards(card);
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));

                processMultipleBonuses(card.getBonusList());

                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateFieldCards(mapField.getFieldType(),
                        mapField.getBuildingPermissionCardsDeck(), mapField.getLeftShownPermissionCard(), mapField.getRightShownPermissionCard(), mapField.getBonusTile().hasBeenDrawn()));
                remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(Action.ACQUIRE_PERMIT);
                remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
                remotePlayer.getGameRoom().getRulesBroker().markCurrentStateAsValid();

            } else {
                remotePlayer.getGameRoom().updateEveryone(GameCommands::rollbackToLastValidGameState);
                rulesBroker.revertToLastValidGameState();
                throw new ItemsNotValidException();
            }
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void start() {
        try {
            remotePlayer.getPickManager().pickACouncil(action, false);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
    }

    public void processCardsOrThrow(Card... cards) throws GameLogicException {
        try {
            processCards(cards, council);
        } catch (NotEnoughCreditsException e) {
            remotePlayer.getGameRoom().updateEveryone(GameCommands::rollbackToLastValidGameState);
            rulesBroker.revertToLastValidGameState();
            throw e;
        }
    }


}
