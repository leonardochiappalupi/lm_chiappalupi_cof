package game.logic.flows;


import client.GameCommands;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Town;
import game.bonus.DirectBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Card;
import game.cards.CardType;
import game.logic.RulesBroker;
import game.logic.actions.Action;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.ItemsNotValidException;
import game.logic.exceptions.NotEnoughCreditsException;
import game.pawns.Emporium;
import game.pawns.Pawn;
import game.pawns.PawnType;
import server.entities.RemotePlayer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmporiumPermit extends Flow {

    private BuildingPermissionCard permissionCard;

    public EmporiumPermit(RulesBroker rulesBroker, RemotePlayer remotePlayer) {
        super(rulesBroker, remotePlayer);
        this.action = Action.EMPORIUM_PERMIT;
    }

    @Override
    public void onBoardItemsPicked(BoardItemType type, BoardItem... items) throws GameLogicException {
        if (items.length == 1 && type == BoardItemType.TOWN) {
            Town townWhereToBuild = rulesBroker.getMainLocalState().getMap().getTownWithName(((Town) items[0]).getName());
            int helpersNeeded = townWhereToBuild.getEmporiums().size();

            if (helpersNeeded > rulesBroker.getThisPlayer(this).getHelpers().size()) {
                remotePlayer.getGameRoom().updateEveryone(GameCommands::rollbackToLastValidGameState);
                rulesBroker.revertToLastValidGameState();
                throw new NotEnoughCreditsException(rulesBroker.getThisPlayer(this).getUsername(), "helpers", "building emporium");
            } else {

                rulesBroker.getMainLocalState().addAvailableHelpers(helpersNeeded);
                rulesBroker.getThisPlayer(this).removeHelpers(helpersNeeded);

                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateAvailableHelpers(rulesBroker.getMainLocalState().getAvailableHelpers().size()));
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));

                Emporium newEmporium=new Emporium(rulesBroker.getThisPlayer(this));
                newEmporium.setTown(townWhereToBuild);
                rulesBroker.getThisPlayer(this).addEmporium(newEmporium);
                townWhereToBuild.addEmporium(newEmporium);

                if(rulesBroker.getThisPlayer(this).getEmporiums().size()>=rulesBroker.getEmporiumsToWin()){
                    remotePlayer.getGameRoom().notifyEndGameCondition();
                    rulesBroker.getThisPlayer(this).increaseWinPositionBy(3);
                }


                remotePlayer.getGameRoom().updateEveryone(gc -> gc.addEmporiumInTown(townWhereToBuild.getName(), rulesBroker.getThisPlayer(this)));
                /**
                 * Clients are updated directly inside BonusProcessor
                 */
                ArrayList<DirectBonus> connectedBonuses = exploreTownGraph(townWhereToBuild, new ArrayList<>(), new ArrayList<>());
                for (DirectBonus db : connectedBonuses) {
                    new BonusProcessor(this, rulesBroker, db, action).process();
                }

                checkBonusTiles(townWhereToBuild);

                rulesBroker.getThisPlayer(this).getPermitCardInstance(permissionCard).flipCovered();
                remotePlayer.getGameRoom().updateEveryone(gc -> gc.updateOrAddPlayer(rulesBroker.getThisPlayer(this)));
                rulesBroker.markCurrentStateAsValid();
                remotePlayer.getGameRoom().updateEveryone(GameCommands::markCurrentGameStateAsValid);
                try {
                    remotePlayer.getGameCommandsBroadcaster().reportActionCompleted(action);
                } catch (RemoteException e) {
                    Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                }
            }
        } else throw new ItemsNotValidException();

    }

    @Override
    public void onPawnsPicked(PawnType type, Pawn... pawns){
        /**No need of this callback here**/
    }

    @Override
    public void onCardsPicked(CardType type, Card... cards) throws GameLogicException {
        if (cards.length == 1 && type == CardType.BUILDINGPERMISSION) {
            permissionCard = (BuildingPermissionCard) cards[0];
            List<Town> allowedTowns = permissionCard.getAllowedTowns();
            if (allowedTowns.size() > 1)
                try {
                    remotePlayer.getPickManager().pickOneTownFromPermitCard(permissionCard, action);
                } catch (RemoteException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            else onBoardItemsPicked(BoardItemType.TOWN, allowedTowns.get(0));
        } else throw new ItemsNotValidException();

    }

    @Override
    public void start() {
        try {
            remotePlayer.getPickManager().pickYourPermissionCard(action);
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

}
