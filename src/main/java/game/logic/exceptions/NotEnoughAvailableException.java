package game.logic.exceptions;

import java.io.Serializable;

/**
 * This exception is thrown every time some action or bonus requires to get from the available fields of the game state
 * more objects than currently available on the board itself. These objects can either be politics cards or helpers.
 */
public class NotEnoughAvailableException extends GameLogicException implements Serializable {
    private final String message = "There are not enough %s available to perform operation: %s";
    private final String availableType;
    private final String operation;

    public NotEnoughAvailableException(String availableType, String operation) {
        this.availableType = availableType;
        this.operation = operation;
    }

    @Override
    public String getMessage() {
        return String.format(message, availableType, operation);
    }

    public String getAvailableType() {
        return availableType;
    }

    public String getOperation() {
        return operation;
    }

}
