package game.logic.exceptions;

import java.io.Serializable;

public abstract class GameLogicException extends Exception implements Serializable {
}
