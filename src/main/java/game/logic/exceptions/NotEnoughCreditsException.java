package game.logic.exceptions;

import java.io.Serializable;

/**
 * This exception is thrown every time a game action requires the player to have more game objects than currently
 * owned by the player itself. These "credits" can be either helpers or coins.
 */
public class NotEnoughCreditsException extends GameLogicException implements Serializable {

    private final String message = "Player %s has not enough %s to perform operation: %s";
    private final String username;
    private final String creditType;
    private final String operation;

    public NotEnoughCreditsException(String username, String creditType, String operation) {
        this.username = username;
        this.creditType = creditType;
        this.operation = operation;
    }

    @Override
    public String getMessage() {
        return String.format(message, username, creditType, operation);
    }

    public String getCreditType() {
        return creditType;
    }

    public String getUsername() {
        return username;
    }

    public String getOperation() {
        return operation;
    }

}
