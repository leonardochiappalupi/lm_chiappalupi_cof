package game.logic;

import client.GameCommands;
import game.GameState;
import game.cards.PoliticsCard;
import game.logic.actions.Action;
import game.logic.flows.Flow;
import game.logic.flows.FlowFactory;
import org.apache.commons.lang3.SerializationUtils;
import server.GameRoom;
import server.entities.RemotePlayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class stores the game logic for the server. It behaves as at first entry point for managing the game state,
 * starting {@link Flow}, and more.
 */
public class RulesBroker {

    private FlowFactory flowFactory = new FlowFactory(this);
    private GameState mainLocalState;
    private GameState lastValidLocalState;
    private GameRoom room;
    private int emporiumsToWin;

    public RulesBroker(GameRoom room) {
        this.room = room;
    }

    public int getEmporiumsToWin() {
        return emporiumsToWin;
    }

    public void setEmporiumsToWin(int emporiumsToWin) {
        this.emporiumsToWin = emporiumsToWin;
    }

    public Player getThisPlayer(Flow callingFlow) {
        return mainLocalState
                .getPlayerWithUsername(callingFlow
                        .getCreatorRemotePlayer()
                        .getUsername());
    }

    public GameRoom getRoom() {
        return room;
    }

    public GameState getMainLocalState() {
        return mainLocalState;
    }

    public void setMainLocalState(GameState mainLocalState) {
        this.mainLocalState = mainLocalState;
    }


    public Flow buildAction(Action action, RemotePlayer remotePlayer) {
        return flowFactory.createFlow(remotePlayer, action);
    }

    public void revertToLastValidGameState() {
        mainLocalState = SerializationUtils.clone(lastValidLocalState);
    }

    public void markCurrentStateAsValid() {
        lastValidLocalState = SerializationUtils.clone(mainLocalState);
    }


    public void drawPoliticsCard(RemotePlayer requestingPlayer) {
        Player player = mainLocalState.getPlayerWithUsername(requestingPlayer.getUsername());
        PoliticsCard drawnCard = (PoliticsCard) mainLocalState.getPoliticsCardsDeck().draw();
        player.getPoliticsCard().add(drawnCard);
        room.updateEveryone(gameCommands -> gameCommands.updateOrAddPlayer(player));
        markCurrentStateAsValid();
        room.updateEveryone(GameCommands::markCurrentGameStateAsValid);
    }

    public Player checkEndGameConditions() {

        List<Player> firstPlayers = new ArrayList<>();
        List<Player> secondPlayers = new ArrayList<>();

        for (Player p : mainLocalState.getPlayers()) {
            if (firstPlayers.isEmpty() || p.getNobilityPosition() > firstPlayers.get(0).getNobilityPosition()) {
                firstPlayers.clear();
                firstPlayers.add(p);
            } else if (p.getNobilityPosition() == firstPlayers.get(0).getNobilityPosition())
                firstPlayers.add(p);
            else if (secondPlayers.isEmpty() || p.getNobilityPosition() > secondPlayers.get(0).getNobilityPosition()) {
                secondPlayers.clear();
                secondPlayers.add(p);
            } else if (p.getNobilityPosition() == secondPlayers.get(0).getNobilityPosition())
                secondPlayers.add(p);
        }
        if (firstPlayers.size() > 1) {
            for (Player p : firstPlayers)
                p.increaseWinPositionBy(5);
        } else {
            firstPlayers.get(0).increaseWinPositionBy(5);
            for (Player p : secondPlayers)
                p.increaseWinPositionBy(2);
        }

        List<Player> mostPermitPlayers = new ArrayList<>();
        for (Player p : mainLocalState.getPlayers()) {
            if (mostPermitPlayers.isEmpty() || p.getBuildingPermissionCards(true).size() > mostPermitPlayers.get(0).getBuildingPermissionCards(true).size()) {
                mostPermitPlayers.clear();
                mostPermitPlayers.add(p);
            }
        }
        for (Player p : mostPermitPlayers) {
            p.increaseWinPositionBy(3);
        }
        List<Player> ordered = mainLocalState.getPlayers();
        Collections.sort(ordered);
        Player winningPlayer = ordered.get(ordered.size() - 1);

        for (Player p : ordered)
            room.updateEveryone(gc -> gc.updateOrAddPlayer(p));

        markCurrentStateAsValid();

        return winningPlayer;


    }

}
