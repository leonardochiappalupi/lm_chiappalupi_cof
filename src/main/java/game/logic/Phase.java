package game.logic;


import client.network.socket.SocketUpdatable;

import java.io.Serializable;

public enum Phase implements Serializable, SocketUpdatable {
    PREGAME, GAME, MARKET_SELL, MARKET_BUY, END_GAME
}
