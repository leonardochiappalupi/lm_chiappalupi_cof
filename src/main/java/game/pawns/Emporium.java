package game.pawns;

import game.logic.Player;
import game.board.Town;

import java.io.Serializable;

public class Emporium implements Serializable {
    private Player owningPlayer;
    private Town town;

    public Emporium(Player owningPlayer) {
        this.owningPlayer = owningPlayer;
    }

    public Player getOwningPlayer() {
        return owningPlayer;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }


}
