package game.pawns;

import game.market.IMarketable;

import java.io.Serializable;

public class Helper implements IMarketable, Pawn, Serializable {

    private int quantity;
    private int price;


    public Helper(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "Helper";
    }

}
