package game.pawns;

import java.io.Serializable;

public enum PawnType implements Serializable {HELPER, COUNCILOR}
