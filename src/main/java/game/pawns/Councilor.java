package game.pawns;

import client.network.socket.SocketUpdatable;
import game.cards.PoliticsColor;

import java.io.Serializable;

public class Councilor implements Serializable, SocketUpdatable {

    private PoliticsColor color;

    public Councilor(PoliticsColor color) {
        this.color = color;
    }

    public PoliticsColor getColor() {
        return color;
    }

    public void setColor(PoliticsColor color) {
        this.color = color;
    }
}
