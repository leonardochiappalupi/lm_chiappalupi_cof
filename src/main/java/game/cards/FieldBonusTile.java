package game.cards;


import game.board.Field;

import java.io.Serializable;

/**
 * This class represents those tiles positioned on each {@link game.board.MapField}
 * Each tile can be obtained once during the game when a player builds an emporium on each town.
 */
public class FieldBonusTile extends BonusTile implements Serializable {

    private Field field;

    public void setField(Field field) {
        this.field = field;
    }

    public Field getField() {
        return field;
    }

}
