package game.cards;


import client.network.socket.SocketUpdatable;
import game.board.Town;
import game.bonus.DirectBonus;
import game.market.IMarketable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents those cards acquired and used by players to build emporiums on the pointed cities.
 * Three decks of BuildingPermissionCards are stacked on each MapField and acquired during the game by the players.
 */

public class BuildingPermissionCard extends Card implements IMarketable, Serializable, SocketUpdatable {

    private String description;
    private ArrayList<DirectBonus> bonusList = new ArrayList<>();
    private ArrayList<Town> allowedTowns = new ArrayList<>();
    private boolean isFlipped = false;

    public boolean isFlipped() {
        return isFlipped;
    }

    public void flipCovered() {
        isFlipped = true;
    }

    public void flipUncovered() {
        isFlipped = false;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<DirectBonus> getBonusList() {
        return new ArrayList<>(bonusList);
    }

    public void addBonus(DirectBonus bonus) {
        this.bonusList.add(bonus);
    }

    public List<Town> getAllowedTowns() {
        return new ArrayList<>(allowedTowns);
    }

    public void addAllowedTown(Town town) {
        this.allowedTowns.add(town);
    }

    public void removeBonus(int index) {
        this.bonusList.remove(index);
    }

    public void removeAllowedTown(int index) {
        this.allowedTowns.remove(index);
    }


    @Override
    public boolean equals(Object other) {

        if (other instanceof BuildingPermissionCard) {
            BuildingPermissionCard card = (BuildingPermissionCard) other;

            if (allowedTowns.size() != card.allowedTowns.size() || bonusList.size() != card.getBonusList().size())
                return false;

            for (int i = 0; i < allowedTowns.size(); i++) {
                if (!allowedTowns.get(i)
                        .getName()
                        .equals(card.getAllowedTowns()
                                .get(i)
                                .getName()))
                    return false;
            }

            for (int i = 0; i < bonusList.size(); i++) {
                if (!(bonusList.get(i).getElementsType() == card.getBonusList().get(i).getElementsType() &&
                        bonusList.get(i).getElementsQuantity() == card.getBonusList().get(i).getElementsQuantity()))
                    return false;
            }

            return true;

        } else return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        String stringToReturn = "[ ";

        for (Town allowedTown : allowedTowns) {
            stringToReturn += allowedTown.getName().toUpperCase().charAt(0) + " ";
        }
        stringToReturn += "] bonuses:";
        for (DirectBonus bonus : bonusList) {
            stringToReturn += " (" + bonus.toString() + ")";
        }
        return stringToReturn;
    }

}
