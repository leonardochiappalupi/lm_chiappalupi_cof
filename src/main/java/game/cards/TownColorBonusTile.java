package game.cards;


import client.network.socket.SocketUpdatable;
import game.board.TownColor;

import java.io.Serializable;

/**
 * This class represents those tiles positioned on each town of the map.
 * A TownColorBonusTile can be obtained every time a player builds an emporium on each town of the same color.
 */
public class TownColorBonusTile extends BonusTile implements Serializable, SocketUpdatable {

    private TownColor color = TownColor.KING;

    public void setTownColor(TownColor color) {
        this.color = color;
    }

    public TownColor getTownColor() {
        return color;
    }

}
