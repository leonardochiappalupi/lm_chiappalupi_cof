package game.cards;


import java.io.Serializable;

/**
 * This class represents those rectangular tiles positioned on the map that give the player its bonuses when obtained.
 * There are three types of BonusTiles represented by {@link KingBonusTile}, {@link FieldBonusTile} and {@link TownColorBonusTile}
 */
abstract class BonusTile implements Serializable {
    private int winSteps = 0;
    private boolean hasBeenDrawn = false;

    public int getWinSteps() {
        return winSteps;
    }

    public void setWinSteps(int winSteps) {
        this.winSteps = winSteps;
    }

    public boolean hasBeenDrawn() {
        return hasBeenDrawn;
    }

    public void setHasBeenDrawn(boolean hasBeenDrawn) {
        this.hasBeenDrawn = hasBeenDrawn;
    }

    @Override
    public String toString() {
        return "WINSTEPS x " + winSteps;
    }
}
