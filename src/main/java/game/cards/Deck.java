package game.cards;


import client.network.socket.SocketUpdatable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Deck implements Serializable, SocketUpdatable {

    private List<Card> cards = new ArrayList<>();

    /**
     * Adds a card to the deck. It is added under all the others. Use {@link this#addCardAtIndex(Card, int)} to get
     * more control on positions
     *
     * @param card The card to add
     */
    public void addCard(Card card) {
        this.cards.add(card);
    }

    /**
     * Adds multiple cards in this deck. Since cards are ordered 0 -> size from top to bottom, this method add passed
     * cards in this very order: so the first card passed will be the topmost one
     *
     * @param cards A collection containing cards to add
     */
    public void addMultipleCards(Collection<? extends Card> cards) {
        this.cards.addAll(cards);
    }

    public Card getCardAtIndex(int index) {
        return this.cards.get(index);
    }

    public void setCard(Card card, int index) {
        this.cards.set(index, card);
    }

    public int indexOf(Card card) {
        return this.cards.indexOf(card);
    }

    public void addCardAtIndex(Card card, int index) {
        this.cards.add(index, card);
    }

    public void removeCard(Card card) {
        this.cards.remove(card);
    }

    public void removeCardAtIndex(int index) {
        this.cards.remove(index);
    }

    /**
     * Method used to get a BuildingPermissionCard knowing its toString representation only
     *
     * @param description The description to look for
     * @return A {@link BuildingPermissionCard} object with the description given
     */
    public BuildingPermissionCard getCardFromString(String description) {
        return (BuildingPermissionCard) cards.stream()
                .filter(card -> card instanceof BuildingPermissionCard)
                .filter(buildingPermissionCard -> buildingPermissionCard.toString().equals(description))
                .findFirst()
                .orElse(null);
    }

    public List<Card> getCards() {
        return new ArrayList<>(cards);
    }

    public void emptyDeck() {
        cards.clear();
    }

    public void shuffle() {
        int randomIndex;
        ArrayList<Card> shuffledDeck = new ArrayList<>();
        while (!cards.isEmpty()) {
            do {
                randomIndex = new Random().nextInt(cards.size());
            } while (cards.get(randomIndex) == null);
            shuffledDeck.add(cards.get(randomIndex));
            cards.remove(randomIndex);
        }

        cards = shuffledDeck;
    }

    /**
     * Used to draw from a deck (card with lowest index is at the top of the deck). If you decide to retain the card,
     * this is useful to peek the first card.
     *
     * @param retainCard Do you want to keep the top card inside the deck?
     * @return Drawn card.
     */
    public Card draw(boolean retainCard) {
        Card drawedCard;
        if (cards.size() > 0) {
            drawedCard = cards.get(0);
            if (!retainCard)
                cards.remove(0);
            return drawedCard;
        } else return null;
    }

    /**
     * Draws a card that is removed from the deck. Equivalent to {@link Deck#draw(boolean false)}
     *
     * @return The drawn card.
     */
    public Card draw() {
        return draw(false);
    }
}
