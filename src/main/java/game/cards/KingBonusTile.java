package game.cards;


import client.network.socket.SocketUpdatable;

import java.io.Serializable;

/**
 * This class represents those tiles that can be obtained every time a player obtains a {@link FieldBonusTile} or a {@link TownColorBonusTile}
 * There are 5 different KingBonusTiles created by default at the beginning of a game.
 */
public class KingBonusTile extends BonusTile implements Serializable, SocketUpdatable {
    private int progressiveNumeral;

    public int getProgressiveNumeral() {
        return progressiveNumeral;
    }

    public void setProgressiveNumeral(int progressiveNumeral) {
        this.progressiveNumeral = progressiveNumeral;
    }

}
