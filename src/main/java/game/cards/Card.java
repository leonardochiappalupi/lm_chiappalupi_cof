package game.cards;

import game.Pickable;
import game.market.IMarketable;

import java.io.Serializable;

/**
 * Class that denotes a generic card of the game.
 * Extended by {@link BuildingPermissionCard} and {@link PoliticsCard}
 */
public abstract class Card implements IMarketable, Serializable, Pickable {
    private int price;

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

}
