package game.cards;


import game.pawns.Pawn;
import javafx.scene.paint.Color;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Enum that lists all the available colors of {@link PoliticsCard} and {@link game.pawns.Councilor}
 */
public enum PoliticsColor implements Serializable, Pawn {
    BLACK(Color.BLACK), PINK(Color.HOTPINK), ORANGE(Color.ORANGE), BLUE(Color.DODGERBLUE), VIOLET(Color.PURPLE), WHITE(Color.WHITE), JOLLY(Color.BEIGE);

    private Color color;

    PoliticsColor(Color color) {
        this.color = color;
    }

    public Color getFXColor() {
        return color;
    }

    public static PoliticsColor getColorFromString(String string) {
        return Arrays.asList(PoliticsColor.values())
                .parallelStream()
                .filter(color -> color.name().equalsIgnoreCase(string))
                .findFirst()
                .orElse(null);
    }
}
