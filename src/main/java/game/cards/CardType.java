package game.cards;

import java.io.Serializable;

public enum CardType implements Serializable{
    POLITICS, BUILDINGPERMISSION
}
