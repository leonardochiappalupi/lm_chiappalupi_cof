package game.cards;


import game.Pickable;

import java.io.Serializable;

/**
 * This class represents the physical Politics cards available to elect a {@link game.pawns.Councilor}
 * One PoliticsCard is drawn by each player at the beginning of his turn
 */
public class PoliticsCard extends Card implements Serializable, Pickable {

    private PoliticsColor color;

    public PoliticsCard(PoliticsColor color) {
        this.color = color;
    }

    public void setColor(PoliticsColor color) {
        this.color = color;
    }

    public PoliticsColor getColor() {
        return color;
    }

    @Override
    public String toString(){
        return "Politics Color: " + color.name().toUpperCase();
    }
}
