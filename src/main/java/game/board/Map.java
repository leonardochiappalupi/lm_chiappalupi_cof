package game.board;

import game.cards.BuildingPermissionCard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Map implements Serializable {
    private ArrayList<Anchor> listOfAnchors = new ArrayList<>();
    private MapField seaField;
    private MapField mainlandField;
    private MapField mountainField;

    public Map() {
        setAnchors();
    }

    public Map(MapField seaField, MapField mainlandField, MapField mountainField) {

        this.seaField = seaField;
        this.mainlandField = mainlandField;
        this.mountainField = mountainField;
    }

    /**
     * This method sets the default {@link Anchor} to the map
     */

    private void setAnchors() {
        listOfAnchors.clear();
        for (int i = 0; i < 6; i++) {
            Anchor anchorToAdd = new Anchor(i);
            if (i <= 2) {
                anchorToAdd.setRightField(mainlandField);
                anchorToAdd.setLeftField(seaField);
            } else {
                anchorToAdd.setRightField(mountainField);
                anchorToAdd.setLeftField(mainlandField);
            }
            listOfAnchors.add(anchorToAdd);
        }
    }

    public List<Town> getAllTowns() {
        List<Town> towns = new ArrayList<>();
        towns.addAll(seaField.getAllTowns(false));
        towns.addAll(mainlandField.getAllTowns(false));
        towns.addAll(mountainField.getAllTowns(false));

        return towns;
    }

    public List<BuildingPermissionCard> getAllBuildingPermissionCards() {
        List<BuildingPermissionCard> buildingPermissionCards = new ArrayList<>();
        buildingPermissionCards.addAll(seaField.getBuildingPermissionCardsDeck().getCards().stream().map(card -> (BuildingPermissionCard) card).collect(Collectors.toList()));
        buildingPermissionCards.addAll(mainlandField.getBuildingPermissionCardsDeck().getCards().stream().map(card -> (BuildingPermissionCard) card).collect(Collectors.toList()));
        buildingPermissionCards.addAll(mountainField.getBuildingPermissionCardsDeck().getCards().stream().map(card -> (BuildingPermissionCard) card).collect(Collectors.toList()));
        return buildingPermissionCards;
    }

    public MapField getSeaField() {
        return seaField;
    }

    private void setSeaField(MapField seaField) {
        this.seaField = seaField;
        setAnchors();
    }

    public MapField getMainlandField() {
        return mainlandField;
    }

    private void setMainlandField(MapField mainlandField) {
        this.mainlandField = mainlandField;
        setAnchors();
    }

    public MapField getMountainField() {
        return mountainField;
    }

    public Anchor getAnchorByIndex(int index) {
        return listOfAnchors.get(index);
    }

    private void setMountainField(MapField mountainField) {
        this.mountainField = mountainField;
        setAnchors();
    }


    public List<MapField> getAllFields() {
        List<MapField> fields = new ArrayList<>();

        fields.add(seaField);
        fields.add(mainlandField);
        fields.add(mountainField);

        return fields;
    }

    public MapField getMapField(int index) {
        return index == Field.SEASIDE.getIndex() ? seaField : index == Field.MAINLAND.getIndex() ? mainlandField : mountainField;
    }

    public MapField getMapField(Field field) {
        switch (field) {
            case SEASIDE:
                return getSeaField();
            case MAINLAND:
                return getMainlandField();
            case MOUNTAINS:
                return getMountainField();
            default:
                return null;
        }
    }

    public void setMapField(MapField mapToAdd, int i) {
        if (i == Field.SEASIDE.getIndex())
            setSeaField(mapToAdd);
        else if (i == Field.MAINLAND.getIndex())
            setMainlandField(mapToAdd);
        else
            setMountainField(mapToAdd);

    }

    public Town getTownWithName(String name) {
        for (Town town : getAllTowns()) {
            if (town.getName().equals(name))
                return town;
        }
        return null;
    }

    public Town getTownWithKing() {
        for (Town town : getAllTowns()) {
            if (town.getHasKing())
                return town;
        }
        return null;
    }

    public List<TownConnection> getTownConnections() {
        List<TownConnection> connections = new ArrayList<>();
        for (Town t : getAllTowns()) {
            for (Town connectedTown : t.getConnectedTowns())
                if (connections.stream().noneMatch(connection -> connection.hasTowns(t, connectedTown)) && !t.getName().equals(connectedTown.getName()))
                    connections.add(new TownConnection(t, connectedTown));
        }
        return connections;
    }

    /**
     * This method returns the MapField which contains the town received.
     * @param town is the Town you want to know the MapField of.
     * @return the {@link Field} containing the town received.
     */
    public Field getFieldOfTown(Town town) {
        for (Town t : seaField.getAllTowns(false))
            if (t.getName().equals(town.getName()))
                return Field.SEASIDE;
        for (Town t : mainlandField.getAllTowns(false))
            if (t.getName().equals(town.getName()))
                return Field.MAINLAND;
        for (Town t : mountainField.getAllTowns(false))
            if (t.getName().equals(town.getName()))
                return Field.MOUNTAINS;
        return null;
    }
}
