package game.board;


import client.network.socket.SocketUpdatable;
import game.pawns.Councilor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Council implements Serializable, SocketUpdatable {

    public static final int COUNCIL_SIZE = 4;
    private ArrayList<Councilor> councilors = new ArrayList<>(COUNCIL_SIZE);
    private MapField field;

    public MapField getField() {
        return field;
    }

    public void setField(MapField field) {
        this.field = field;
    }

    public Councilor insertCouncilor(Councilor councilor) {
        this.councilors.add(0, councilor);
        return this.councilors.remove(councilors.size() - 1);
    }

    public Councilor getCounclilorNumber(int index) {
        return councilors.get(index);
    }

    public List<Councilor> getCouncilors() {
        return new ArrayList<>(councilors);
    }

    public void addCouncilor(Councilor councilor) {
        councilors.add(0, councilor);
    }

    @Override
    public String toString() {
        String stringToReturn;
        if (field != null)
            stringToReturn = field.getFieldType().toString() + " region council: ";
        else
            stringToReturn = "King council: ";
        for (Councilor councilor : councilors) {
            try {
                stringToReturn += councilor
                        .getColor()
                        .name() + " ";
            } catch (NullPointerException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            }
        }
        return stringToReturn;
    }
}
