package game.board;

import game.bonus.IBonus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NobilityRouteCell implements Serializable {
    private int index;
    private List<IBonus> bonuses = new ArrayList<>();

    NobilityRouteCell() {
        this.index = -1; /** default value that the cell contains before being configured **/
    }

    public NobilityRouteCell(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<IBonus> getBonuses() {
        return new ArrayList<>(bonuses);
    }

    public void setBonuses(List<IBonus> bonuses) {
        this.bonuses = bonuses;
    }

    public void addBonus(IBonus bonus) {
        bonuses.add(bonus);
    }

    public void removeBonus(int index) {
        bonuses.remove(index);
    }
}
