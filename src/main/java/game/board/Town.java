package game.board;

import game.bonus.DirectBonus;
import game.logic.Player;
import game.pawns.Emporium;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Town implements Serializable, BoardItem {
    private String name;
    private boolean hasKing;
    private TownColor color;
    private Integer sector;
    private ArrayList<Town> connected = new ArrayList<>();
    private ArrayList<DirectBonus> bonusList = new ArrayList<>();
    private ArrayList<Emporium> emporiums = new ArrayList<>();
    private ArrayList<Anchor> connectedAnchors = new ArrayList<>();

    public Town() {//EMPTY BUILDER
        name = "";
    }

    public Town(Integer sector) {
        this.sector = sector;
    }

    public Town(String name) {
        this.name = name;
    }

    public Town(String name, TownColor color) {
        this.name = name;
        this.color = color;
    }

    public Integer getSector() {
        return sector;
    }

    public void setSector(Integer sector) {
        this.sector = sector;
    }

    public void addConnectedAnchor(Anchor anchor) {
        connectedAnchors.add(anchor);
    }

    public void removeConnectedAnchor(Anchor anchor) {
        connectedAnchors.removeAll(
                connectedAnchors.parallelStream().filter(anchor1 -> anchor1.getLevel() == anchor.getLevel()).collect(Collectors.toList()));
    }

    public List<Anchor> getConnectedAnchors() {
        return new ArrayList<>(connectedAnchors);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getHasKing() {
        return hasKing;
    }

    public void setHasKing(boolean hasKing) {
        this.hasKing = hasKing;
    }

    public TownColor getColor() {
        return color;
    }

    public void setColor(TownColor color) {
        this.color = color;
    }

    public void addConnectedTown(Town town) {
        this.connected.add(town);
    }

    public void removeConnectedTown(Town town) {
        this.connected.remove(town);
    }

    public List<Town> getConnectedTowns() {
        return new ArrayList<>(connected);
    }

    public void addBonus(DirectBonus bonus) {
        this.bonusList.add(bonus);
    }

    public void removeBonus(int index) {
        this.bonusList.remove(index);
    }

    public List<DirectBonus> getBonusList() {
        return new ArrayList<>(bonusList);
    }

    public void clearBonusList() {
        bonusList.clear();
    }

    public void addEmporium(Emporium emporium) {
        emporiums.add(emporium);
    }

    public boolean hasEmporiumOfPlayer(Player owningPlayer) {
        for (Emporium e : emporiums)
            if (e.getOwningPlayer().getUsername().equals(owningPlayer.getUsername()))
                return true;
        return false;
    }

    public List<Emporium> getEmporiums() {
        return new ArrayList<>(emporiums);
    }

    public int getRowFromSector() {
        return sector / 2;
    }

    public int getColumnFromSector() {
        return sector % 2;
    }

    @Override
    public String toString() {
        String stringToReturn = String.format("%s (%s)", name, color.name().toLowerCase());
        if (hasKing)
            stringToReturn += " (K)";
        stringToReturn += "\n\tBonuses:\t ";
        for (DirectBonus bonus : getBonusList()) {
            stringToReturn += String.format("(%s) ", bonus.toString());
        }
        stringToReturn += "\n\tEmporiums:\t ";
        if (emporiums.isEmpty())
            stringToReturn += "< none >";
        else
            for (Emporium emporium : emporiums)
                if (!"FAKE".equals(emporium.getOwningPlayer().getUsername()))
                    stringToReturn += emporium.getOwningPlayer().getUsername() + " ";
        return stringToReturn;
    }

    public static Town cloneTown(Town town) {
        Town clonedTown = new Town(town.getName(), town.getColor());
        clonedTown.setHasKing(town.getHasKing());
        clonedTown.setSector(town.getSector());
        town.getBonusList().stream()
                .forEach(clonedTown::addBonus);
        town.getConnectedTowns().stream()
                .forEach(clonedTown::addConnectedTown);
        town.getConnectedAnchors().stream()
                .forEach(clonedTown::addConnectedAnchor);

        return clonedTown;
    }
}
