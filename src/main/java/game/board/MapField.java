package game.board;


import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import game.cards.FieldBonusTile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class represents a region of the map. There are three regions with an arbitrary number of customizable towns.
 * Each MapField contains all the objects related to it.
 * so it has a reference to its {@link BuildingPermissionCard} deck and related shown cards, {@link Town} list,
 * its {@link Council} and its {@link FieldBonusTile}.
 */

public class MapField implements Serializable, BoardItem {
    private Deck buildingPermissionCardsDeck = new Deck();
    private List<Town> towns = new ArrayList<>();
    private BuildingPermissionCard leftShownPermissionCard;
    private BuildingPermissionCard rightShownPermissionCard;
    private Council council;
    private Field fieldType;
    private FieldBonusTile bonusTile;


    public MapField(int size) {
        for (int i = 0; i < size; i++)
            towns.add(null);
    }

    public void setTowns(List<Town> towns) {
        this.towns = towns;
    }

    public FieldBonusTile getBonusTile() {
        return bonusTile;
    }

    public void setBonusTile(FieldBonusTile bonusTile) {
        this.bonusTile = bonusTile;
    }

    public Deck getBuildingPermissionCardsDeck() {
        return buildingPermissionCardsDeck;
    }

    public void setBuildingPermissionCardsDeck(Deck buildingPermissionCardsDeck) {
        this.buildingPermissionCardsDeck = buildingPermissionCardsDeck;

    }

    public Council getCouncil() {
        return council;
    }

    public void setCouncil(Council council) {
        this.council = council;
    }

    public Field getFieldType() {
        return fieldType;
    }

    public void setFieldType(Field fieldType) {
        this.fieldType = fieldType;
    }

    public Town getTownInSector(int sector) {
        return towns.get(sector);
    }

    public List<Town> getAllTowns(boolean includeNullTowns) {
        List<Town> toReturn;

        if (!includeNullTowns) {
            Stream<Town> townStream = towns.stream();
            toReturn = townStream
                    .filter(town -> town != null)
                    .collect(Collectors.toList());
            townStream.close();
        } else
            toReturn = towns;

        return toReturn;
    }

    public void replaceTownInSector(int sector, Town town) {
        towns.set(sector, town);
    }

    public void setAllTowns(List<Town> towns) {
        this.towns = towns;
    }

    public BuildingPermissionCard getLeftShownPermissionCard() {
        return leftShownPermissionCard;
    }

    public void setLeftShownPermissionCard(BuildingPermissionCard leftShownPermissionCard) {
        this.leftShownPermissionCard = leftShownPermissionCard;
    }

    public BuildingPermissionCard getRightShownPermissionCard() {
        return rightShownPermissionCard;
    }

    public void setRightShownPermissionCard(BuildingPermissionCard rightShownPermissionCard) {
        this.rightShownPermissionCard = rightShownPermissionCard;
    }

    public void drawShownPermissionCard(BuildingPermissionCard card){
        if (card.equals(rightShownPermissionCard)) {
            rightShownPermissionCard = (BuildingPermissionCard) buildingPermissionCardsDeck.draw(false);
        } else if (card.equals(leftShownPermissionCard)) {
            leftShownPermissionCard = (BuildingPermissionCard) buildingPermissionCardsDeck.draw(false);
        }
    }
}
