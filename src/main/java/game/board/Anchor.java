package game.board;


import java.io.Serializable;

/**
 * This class is used to connect two adjacent MapFields
 * There are six anchors, three connect SeaSide to Mainland, and the other three connect Mainland to Mountains
 */
public class Anchor implements Serializable {
    private MapField leftField = null;
    private MapField rightField = null;
    /**
     * This parameter denotes the position of the anchor on the map.
     * There are six different levels (from 0 to 6)
     * First three levels (top to bottom) on the left side of the map.
     * Second three levels (top to bottom) on the right side of the map.
     */
    private int level;

    public Anchor(int level) {
        this.level = level;
    }

    public MapField getLeftField() {
        return leftField;
    }

    public void setLeftField(MapField leftField) {
        this.leftField = leftField;
    }

    public MapField getRightField() {
        return rightField;
    }

    public void setRightField(MapField rightField) {
        this.rightField = rightField;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Anchor " + this.level;
    }
}
