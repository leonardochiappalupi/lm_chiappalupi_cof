package game.board;


public class TownConnection {
    private Town town1;
    private Town town2;

    public TownConnection(Town town1, Town town2) {
        this.town1 = town1;
        this.town2 = town2;
    }

    public Town getTown1() {
        return town1;
    }

    public Town getTown2() {
        return town2;
    }

    public boolean hasTowns(Town t1, Town t2) {
        return town1.getName().equals(t1.getName()) && town2.getName().equals(t2.getName())
                || town1.getName().equals(t2.getName()) && town2.getName().equals(t1.getName());
    }
}
