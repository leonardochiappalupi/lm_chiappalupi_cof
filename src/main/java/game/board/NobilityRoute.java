package game.board;

import client.network.socket.SocketUpdatable;
import game.bonus.IBonus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class NobilityRoute implements Serializable, SocketUpdatable {

    private List<NobilityRouteCell> cells;

    public NobilityRoute(int initialSize) {
        cells = new ArrayList<>(initialSize);
        for (int i = 0; i < initialSize; i++) {
            this.addCell(new NobilityRouteCell());
        }
    }

    public List<IBonus> getBonusesOfCell(int index) {
        return cells.get(index).getBonuses();
    }

    private void addCell(NobilityRouteCell nobilityRouteCell) {
        this.cells.add(nobilityRouteCell);
    }

    public void setCell(NobilityRouteCell nobilityRouteCell, int index) {
        this.cells.set(index, nobilityRouteCell);
    }

    public void removeCell(int index) {
        this.cells.set(index, new NobilityRouteCell());
    }

    public List<NobilityRouteCell> getCells() {
        return new ArrayList<>(cells);
    }

    public NobilityRouteCell getCellAtIndex(int index) {
        return cells.get(index);
    }
}
