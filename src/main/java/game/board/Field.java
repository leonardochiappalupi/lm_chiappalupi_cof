package game.board;


import client.network.socket.SocketUpdatable;

import java.io.Serializable;

public enum Field implements Serializable, BoardItem, SocketUpdatable {
    SEASIDE("Seaside", 0), MAINLAND("Mainland", 1), MOUNTAINS("Mountains", 2), KING("King", 3);

    private String description;
    private Integer index;

    Field(String description, Integer index) {
        this.description = description;
        this.index = index;
    }

    public String getDescription() {
        return this.description;
    }

    public Integer getIndex() {
        return this.index;
    }

    @Override
    public String toString() {
        return description;
    }
}
