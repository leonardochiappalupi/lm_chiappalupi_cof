package game.board;


import javafx.scene.paint.Color;

import java.io.Serializable;

public enum TownColor implements Serializable {
    BLUE("Blue", Color.rgb(0, 0, 255, 1.0)),
    ORANGE("Orange", Color.rgb(255, 165, 0, 1.0)),
    GOLD("Gold", Color.rgb(255, 215, 0, 1.0)),
    SILVER("Silver", Color.rgb(192, 192, 192, 1.0)),
    KING("King", Color.rgb(238, 130, 238, 1.0));

    private String description;
    private Color color;

    TownColor(String description, Color color) {
        this.description = description;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public String getDescription() {
        return description;
    }
}
