package game;

import constants.ConfigurationConstants;
import game.board.Council;
import game.board.Map;
import game.board.NobilityRoute;
import game.board.TownColor;
import game.cards.*;
import game.logic.Player;
import game.market.Market;
import game.pawns.Councilor;
import game.pawns.Helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class identifies the snapshot of the game, it contains a reference to all the objects needed during every phase
 * A reference to an object of this class is present on both the Server and the Client
 * Through the selected connection, the Client GameState is updated by the Server
 * The Server updates the local GameState every time a game related event happens, but only on the fields actually changed.
 * All the modifications are issued calling one of the {@link client.GameCommands} methods.
 * See {@link client.ClientLogicLayer} for the implementation used of {@link client.GameCommands}, whose methods are
 * directly called using rmi and forwarded using {@link server.entities.SocketGameCommandsBroadcaster} if using socket.
 */
public class GameState implements Serializable {

    private NobilityRoute nobilityRoute;
    private Deck politicsCardsDeck = new Deck();
    private ArrayList<Councilor> availableCouncilors = new ArrayList<>();
    private Council kingCouncil = new Council();
    private KingBonusTile[] kingBonusTiles = new KingBonusTile[5];
    private ArrayList<TownColorBonusTile> townColorBonusTiles = new ArrayList<>();
    private Map map;
    private ArrayList<Helper> availableHelpers = new ArrayList<>();
    private Market marketInstance;
    private ArrayList<Player> players = new ArrayList<>();

    public GameState() {
        //USED FOR TESTING ONLY RIGHT NOW
    }

    public GameState(Configuration configuration, int numberOfPlayers) {
        int numberOfCouncilors = numberOfPlayers * ConfigurationConstants.NUMBER_OF_COUNCILORS_PER_PLAYER;
        setNobilityRoute(configuration.getNobilityRoute());
        setMap(configuration.getMap());
        setFieldBonusTiles();
        setTownColorBonusTiles();
        setKingBonusTiles(createDefaultKingBonusTiles());
        addAvailableHelpers(numberOfPlayers * ConfigurationConstants.NUMBER_OF_HELPERS_PER_PLAYER);
        setAvailableCouncilors(numberOfCouncilors);
    }


    /**
     * This method set the default bonus tiles available after a player builds an emporium on every town of a color
     */
    private void setTownColorBonusTiles() {
        TownColorBonusTile blueTile = new TownColorBonusTile();
        TownColorBonusTile orangeTile = new TownColorBonusTile();
        TownColorBonusTile goldTile = new TownColorBonusTile();
        TownColorBonusTile silverTile = new TownColorBonusTile();

        blueTile.setWinSteps(5);
        blueTile.setTownColor(TownColor.BLUE);
        orangeTile.setWinSteps(8);
        orangeTile.setTownColor(TownColor.ORANGE);
        goldTile.setWinSteps(20);
        goldTile.setTownColor(TownColor.GOLD);
        silverTile.setWinSteps(12);
        silverTile.setTownColor(TownColor.SILVER);

        townColorBonusTiles.add(blueTile);
        townColorBonusTiles.add(orangeTile);
        townColorBonusTiles.add(goldTile);
        townColorBonusTiles.add(silverTile);
    }

    /**
     * This method creates the default king bonus tiles
     *
     * @return the array containing the default bonus tiles
     */
    private KingBonusTile[] createDefaultKingBonusTiles() {

        KingBonusTile[] defaultKingBonusTiles = new KingBonusTile[5];

        KingBonusTile firstBonusTile = new KingBonusTile();
        KingBonusTile secondBonusTile = new KingBonusTile();
        KingBonusTile thirdBonusTile = new KingBonusTile();
        KingBonusTile fourthBonusTile = new KingBonusTile();
        KingBonusTile fifthBonusTile = new KingBonusTile();

        firstBonusTile.setWinSteps(25);
        firstBonusTile.setProgressiveNumeral(1);
        secondBonusTile.setWinSteps(18);
        secondBonusTile.setProgressiveNumeral(2);
        thirdBonusTile.setWinSteps(12);
        thirdBonusTile.setProgressiveNumeral(3);
        fourthBonusTile.setWinSteps(7);
        fourthBonusTile.setProgressiveNumeral(4);
        fifthBonusTile.setWinSteps(3);
        fifthBonusTile.setProgressiveNumeral(5);

        defaultKingBonusTiles[0] = firstBonusTile;
        defaultKingBonusTiles[1] = secondBonusTile;
        defaultKingBonusTiles[2] = thirdBonusTile;
        defaultKingBonusTiles[3] = fourthBonusTile;
        defaultKingBonusTiles[4] = fifthBonusTile;

        return defaultKingBonusTiles;
    }

    private void setFieldBonusTiles() {
        for (int i = 0; i < 3; i++) {
            FieldBonusTile fieldBonusTile = new FieldBonusTile();
            fieldBonusTile.setField(map.getMapField(i).getFieldType());
            fieldBonusTile.setWinSteps(5);
            map.getMapField(i).setBonusTile(fieldBonusTile);
        }
    }

    /**
     * This method creates the default number of available councilors ordered by color.
     *
     * @param numberOfCouncilors is the number of initial councilors, based on the number of players
     *                           throw a coefficient present in {@link ConfigurationConstants}
     */
    private void setAvailableCouncilors(int numberOfCouncilors) {
        for (int i = 0; i < ConfigurationConstants.NUMBER_OF_COUNCILOR_COLORS; i++) {
            for (int j = 0; j < numberOfCouncilors / ConfigurationConstants.NUMBER_OF_COUNCILOR_COLORS; j++) {
                Councilor councilor = new Councilor(PoliticsColor.values()[i]);
                availableCouncilors.add(councilor);
            }
        }

    }

    /**
     * This method takes 16 random available councilors and adds them to every council of the map.
     */
    public void setRandomCouncilors() {
        //setting default random councilors in each mapfield council
        for (int i = 0; i < ConfigurationConstants.NUMBER_OF_FIELDS; i++) {
            Council council = new Council();
            council.setField(getMap().getMapField(i));
            for (int j = 0; j < Council.COUNCIL_SIZE; j++) {
                council.addCouncilor(availableCouncilors.remove(new Random().nextInt(getAvailableCouncilors().size())));
            }
            getMap().getMapField(i).setCouncil(council);
        }
        //setting default random councilors in kingCouncil
        for (int i = 0; i < Council.COUNCIL_SIZE; i++) {
            kingCouncil.addCouncilor(availableCouncilors.remove(new Random().nextInt(getAvailableCouncilors().size())));
        }


    }

    /**
     * This method creates the default politics cards deck.
     *
     * @param numOfPlayers is used to set the number of total politics cards.
     */
    public void setDefaultPoliticsCardsDeck(int numOfPlayers) {
        Deck politicsCardDeck = new Deck();
        for (int i = 0; i < ConfigurationConstants.NUMBER_OF_POLITICS_CARD_COLORS; i++) {
            for (int j = 0; j < numOfPlayers * ConfigurationConstants.NUMBER_OF_POLITICS_CARDS; j++) {
                PoliticsCard politicsCard = new PoliticsCard(PoliticsColor.values()[i]);
                politicsCardDeck.addCard(politicsCard);
            }
        }
        politicsCardDeck.shuffle();
        setPoliticsCardsDeck(politicsCardDeck);

    }


    public List<Councilor> getAvailableCouncilors() {
        return new ArrayList<>(availableCouncilors);
    }

    public void addAvailableCouncilor(Councilor councilor) {
        this.availableCouncilors.add(councilor);
    }

    public void addAvailableCouncilors(List<Councilor> councilors) {
        this.availableCouncilors.addAll(councilors);
    }

    public void consumeAvailableCouncilor(Councilor councilorToConsume) {
        this.availableCouncilors.remove(councilorToConsume);
    }

    public void clearAvailableCouncilors() {
        this.availableCouncilors.clear();
    }

    public KingBonusTile[] getKingBonusTiles() {
        return kingBonusTiles;
    }

    private void setKingBonusTiles(KingBonusTile[] kingBonusTiles) {
        this.kingBonusTiles = kingBonusTiles;
    }

    public List<TownColorBonusTile> getTownColorBonusTiles() {
        return new ArrayList<>(townColorBonusTiles);
    }

    public void replaceAllTownColorBonusTiles(List<TownColorBonusTile> newTiles){
        townColorBonusTiles.clear();
        townColorBonusTiles.addAll(newTiles);
    }


    public List<Helper> getAvailableHelpers() {
        return new ArrayList<>(availableHelpers);
    }

    public void addAvailableHelpers(int quantity) {
        for (int i = 0; i < quantity; i++) {
            availableHelpers.add(new Helper(1));
        }
    }

    public void consumeAvailableHelpers(int quantity) {
        for (int i = 0; i < quantity; i++) {
            availableHelpers.remove(availableHelpers.get(0));
        }
    }

    public void clearAvailableHelpers() {
        availableHelpers.clear();
    }

    public List<Player> getPlayers() {
        return players;
    }

    public NobilityRoute getNobilityRoute() {
        return nobilityRoute;
    }

    public void setNobilityRoute(NobilityRoute nobilityRoute) {
        this.nobilityRoute = nobilityRoute;
    }

    public Deck getPoliticsCardsDeck() {
        return politicsCardsDeck;
    }

    public void setPoliticsCardsDeck(Deck politicsCardsDeck) {
        this.politicsCardsDeck = politicsCardsDeck;
    }

    public Council getKingCouncil() {
        return kingCouncil;
    }

    public void setKingCouncil(Council council) {
        kingCouncil = council;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Market getMarketInstance() {
        return marketInstance;
    }

    public void setMarketInstance(Market marketInstance) {
        this.marketInstance = marketInstance;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public Player getPlayerWithUsername(String username) {
        for (Player p : players) {
            if (p.getUsername().equals(username))
                return p;

        }
        return null;
    }

    public String getActivePlayerUsername() throws IllegalGameStateException {
        for (Player p : players) {
            if (p.isEnabled() && !p.isDisconnected())
                return p.getUsername();
        }
        throw new IllegalGameStateException();
    }

    public TownColorBonusTile getTownColorBonusTile(TownColor color) {
        return townColorBonusTiles.stream().filter(tile -> tile.getTownColor().equals(color)).findFirst().orElse(new TownColorBonusTile());
    }

    /**
     * @param color   indicates the color of the councilor to look for.
     * @param consume indicates if the found councilor has to be removed from the available councilors.
     * @return a single Councilor whit the same passed color.
     */

    public Councilor getCouncilorWithColor(PoliticsColor color, boolean consume) {
        Councilor councilorToReturn = null;
        /*councilorToReturn = availableCouncilors
                .parallelStream()
                .filter(councilor -> councilor.getColor().equals(color))
                .findAny()
                .get(); */
        for (Councilor availableCouncilor : availableCouncilors) {
            if (availableCouncilor.getColor().equals(color)) {
                councilorToReturn = availableCouncilor;
                break;
            }
        }
        if (consume)
            consumeAvailableCouncilor(councilorToReturn);
        return councilorToReturn;
    }

    public long getAvailableCouncilorsOfColor(PoliticsColor color) {
        return availableCouncilors.parallelStream().filter(councilor -> councilor.getColor().equals(color))
                .count();
    }

    public KingBonusTile getNextAvailableKingBonusTile() {
        for (KingBonusTile kbt : kingBonusTiles) {
            if (!kbt.hasBeenDrawn())
                return kbt;
        }
        return null;
    }


}
