package game;


import game.board.Map;
import game.board.NobilityRoute;

import java.io.Serializable;

/**
 * This class represent the configuration of a game.
 * Its instances are used three times, first by the configurator to create a custom configuration
 * and then by both the Client and the Server to create the first GameState (using {@link util.JSONHelper.ConfigurationParser}
 * See {@link view.configurator.Configurator} for the custom configuration.
 */
public class Configuration implements Serializable {
    private Map map;
    private NobilityRoute nobilityRoute;
    private boolean randomBonusRequested;

    public Configuration(){
        this.randomBonusRequested = false;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public NobilityRoute getNobilityRoute() {
        return nobilityRoute;
    }

    public void setNobilityRoute(NobilityRoute nobilityRoute) {
        this.nobilityRoute = nobilityRoute;
    }

    public boolean isRandomBonusRequested() {
        return randomBonusRequested;
    }

    public void setRandomBonusRequested(boolean randomBonusRequested) {
        this.randomBonusRequested = randomBonusRequested;
    }
}
