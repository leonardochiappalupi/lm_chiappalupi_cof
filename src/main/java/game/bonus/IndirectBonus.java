package game.bonus;

import java.io.Serializable;

public class IndirectBonus implements IBonus, Serializable {
    private BonusType elementsType;
    private boolean collectsFromTown;
    private boolean collectsFromBuildingCard;

    public IndirectBonus(BonusType bonusType) {
        this.elementsType = bonusType;
    }

    public boolean isCollectsFromTown() {
        return collectsFromTown;
    }

    public boolean isCollectsFromBuildingCard() {
        return collectsFromBuildingCard;
    }

    public void setCollectsFromTown(boolean collectsFromTown) {
        this.collectsFromTown = collectsFromTown;
    }

    public void setCollectsFromBuildingCard(boolean collectsFromBuildingCard) {
        this.collectsFromBuildingCard = collectsFromBuildingCard;
    }

    @Override
    public int getElementsQuantity() {
        return 0; /** IndirectBonus doesn't need the quantity parameter, but it is necessary to override the method due to the IBonus interface implementation **/
    }

    @Override
    public BonusType getElementsType() {
        return elementsType;
    }

    @Override
    public String toString(){
        return elementsType.getDescription().toUpperCase();
    }
}
