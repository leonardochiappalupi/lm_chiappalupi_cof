package game.bonus;


import java.io.Serializable;

public enum BonusType implements Serializable {
    HELPERS("Helpers"),
    COINS("Coins"),
    CARDS("Cards"),
    SUPPACTION("Suppaction"),
    WINSTEPS("Winsteps"),
    NOBSTEPS("Nobsteps"),
    TOWNBONUSES("Townbonuses"),
    PERMISSIONCARDBONUSES("Permissioncardbonuses");

    private String description;

    BonusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }
}
