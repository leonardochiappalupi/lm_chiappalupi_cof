package game.bonus;

import client.network.socket.SocketUpdatable;

public interface IBonus extends SocketUpdatable {

    int getElementsQuantity();

    BonusType getElementsType();
}
