package game.bonus;

import java.io.Serializable;

/**
 * This class represents bonuses using just {@link BonusType} and its quantity
 */
public class DirectBonus implements IBonus, Serializable {

    private BonusType elementsType;
    private int elementsQuantity;

    public DirectBonus(BonusType elementsType, int elementsQuantity) {
        this.elementsType = elementsType;
        this.elementsQuantity = elementsQuantity;
    }

    @Override
    public int getElementsQuantity() {
        return elementsQuantity;
    }

    @Override
    public BonusType getElementsType() {
        return elementsType;
    }

    @Override
    public String toString() {
        return elementsType.name() + " x " + elementsQuantity;
    }
}
