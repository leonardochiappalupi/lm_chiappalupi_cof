package game.market;


import client.GameCommands;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.logic.Player;
import game.logic.RulesBroker;
import game.pawns.Helper;
import server.GameRoom;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Market implements Serializable {
    private List<ItemToSell> itemsList = new ArrayList<>();
    private List<String> usernamesAvailable = new ArrayList<>();
    private List<String> usernamesAlreadyBuyed = new ArrayList<>();
    private transient RulesBroker rulesBroker;
    private transient GameRoom room;

    public Market(GameRoom room, RulesBroker rulesBroker) {
        this.room = room;
        this.rulesBroker = rulesBroker;
    }

    /**
     * Creates an updated list containing all the players that are still online. Then we remove all the players that
     * already buyed something. Finally we check if there's still someone to make buying things, and if there is
     * we return its name. Otherwise we return null, as expected from {@link GameRoom#marketSellLoop()}
     *
     * @return The username of the next player that should buy things (null if we are done with everyone).
     */
    public String getRandomBuyerUsername() {
        usernamesAvailable.clear();
        for (Player p : rulesBroker.getMainLocalState().getPlayers()) {
            if (!p.isDisconnected()) {
                usernamesAvailable.add(p.getUsername());
            }
        }
        usernamesAvailable.removeAll(usernamesAlreadyBuyed);
        if (usernamesAvailable.isEmpty())
            return null;
        else {
            String usernameToReturn = usernamesAvailable.get(new Random().nextInt(usernamesAvailable.size()));
            usernamesAlreadyBuyed.add(usernameToReturn);
            return usernameToReturn;
        }

    }


    public void addItemsToSell(List<ItemToSell> items) {
        itemsList.addAll(items);
    }

    /**
     * We elaborate the transaction locally by moving coins and items from seller to buyer. Finally, we send the updated
     * instances of both buyer and seller to all the clients. The transaction must end without errors because all
     * controls on requirements (for instance, if you have enough coins) are performed before getting here, usually
     * by the {@link client.presentation.PresentationBridge} on the Client.
     *
     * @param buyerUsername The username of the user that wants to buy this item.
     * @param item          The item to buy.
     */
    public void buy(String buyerUsername, ItemToSell item) {

        Player buyer = rulesBroker.getMainLocalState().getPlayerWithUsername(buyerUsername);
        Player seller = rulesBroker.getMainLocalState().getPlayerWithUsername(item.getSeller().getUsername());

        buyer.decreaseCoinsBy(item.getPrice());
        seller.increaseCoinsBy(item.getPrice());

        if (item.getItem() instanceof BuildingPermissionCard) {

            BuildingPermissionCard card = (BuildingPermissionCard) item.getItem();
            buyer.addPermissionCards(card);
            seller.getBuildingPermissionCards(true).remove(seller.getPermitCardInstance(card));

        } else if (item.getItem() instanceof Helper) {

            seller.removeHelpers(1);
            buyer.addHelpers(1);

        } else if (item.getItem() instanceof PoliticsCard) {

            PoliticsCard card = (PoliticsCard) item.getItem();
            seller.getPoliticsCard().remove(seller.getPoliticsCardOfColor(card.getColor()).get(0));
            buyer.addPoliticsCard(card);

        }

        try {
            room.getRemotePlayerByUsername(buyerUsername).getGameCommandsBroadcaster().onItemBought(item.getUniqueId());
        } catch (RemoteException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }

        itemsList.remove(getItemById(itemsList, item.getUniqueId()));

        room.updateEveryone(gc -> gc.updateOrAddPlayer(buyer));
        room.updateEveryone(gc -> gc.updateOrAddPlayer(seller));
        rulesBroker.markCurrentStateAsValid();
        room.updateEveryone(GameCommands::markCurrentGameStateAsValid);
    }

    public List<ItemToSell> getItemsForSale() {
        return itemsList;
    }

    public static ItemToSell getItemById(List<ItemToSell> itemsList, String id) {
        return itemsList
                .parallelStream()
                .filter(item -> item.getUniqueId().equals(id))
                .findAny()
                .get();
    }
}
