package game.market;

import client.network.socket.SocketUpdatable;
import game.logic.Player;

import java.io.Serializable;
import java.util.Random;

/**
 * This class is used during the Market phase ({@link game.logic.Phase}) to represent a generic item ({@link IMarketable})
 * that can be sold and bought by players.
 */
public class ItemToSell implements Serializable, SocketUpdatable {

    private IMarketable item;
    private int price;
    private Player seller;
    private String uniqueId;

    public ItemToSell(IMarketable item, Player seller) {
        this.item = item;
        price = item.getPrice();
        this.seller = seller;
        uniqueId = seller.getUsername() + item.toString() + price + new Random().nextInt();
    }

    public IMarketable getItem() {
        return item;
    }

    public int getPrice() {
        return price;
    }

    public Player getSeller() {
        return seller;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    @Override
    public String toString() {
        return String.format("%s\t\towner: %s\t\tprice: %d", item.toString(), seller.getUsername(), price);
    }

}
