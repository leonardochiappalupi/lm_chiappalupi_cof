package game.market;

import java.io.Serializable;

public interface IMarketable extends Serializable{

    void setPrice(int price);

    int getPrice();
}
