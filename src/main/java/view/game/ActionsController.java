package view.game;


import client.Client;
import client.presentation.gui.GUIBridge;
import game.logic.actions.Action;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.media.AudioClip;

public class ActionsController {

    private Button elect;
    private Button acquirePermission;
    private Button emporiumPermit;
    private Button kingPermit;
    private Button acquireHelper;
    private Button swapPermit;
    private Button helperElect;
    private Button additionalMain;
    private Button draw;
    private Button endTurn;

    public ActionsController(Parent root) {

        elect = (Button) root.lookup("#elect");
        acquirePermission = (Button) root.lookup("#acquirePermission");
        emporiumPermit = (Button) root.lookup("#emporiumPermit");
        kingPermit = (Button) root.lookup("#kingPermit");

        acquireHelper = (Button) root.lookup("#acquireHelper");
        swapPermit = (Button) root.lookup("#swapPermit");
        helperElect = (Button) root.lookup("#helperElect");
        additionalMain = (Button) root.lookup("#additionalMain");

        draw = (Button) root.lookup("#draw");
        endTurn = (Button) root.lookup("#endTurn");

        setAllEnabled(false);

        linkButtonsToActions();

    }

    public void setMainActionsEnabled(boolean enabled) {
        elect.setDisable(!enabled);
        acquirePermission.setDisable(!enabled);
        emporiumPermit.setDisable(!enabled);
        kingPermit.setDisable(!enabled);
    }

    public void setSecondaryActionsEnabled(boolean enabled) {
        if (!enabled) {
            acquireHelper.setDisable(true);
            swapPermit.setDisable(true);
            helperElect.setDisable(true);
            additionalMain.setDisable(true);
        } else {
            refreshAllowedSecondaryActions();
        }
    }

    public void refreshAllowedActions() {
        if (Client.getClientLogicLayer().getRemainingMainActions() > 0)
            setMainActionsEnabled(true);
        else setMainActionsEnabled(false);
        if (Client.getClientLogicLayer().areSecondaryActionsEnabled())
            refreshAllowedSecondaryActions();
        else setSecondaryActionsEnabled(false);
    }

    private void refreshAllowedSecondaryActions() {
        if (Client.getClientLogicLayer().getThisPlayer().getCoins() >= 3)
            acquireHelper.setDisable(false);
        else acquireHelper.setDisable(true);
        if (!Client.getClientLogicLayer().getThisPlayer().getHelpers().isEmpty()) {
            swapPermit.setDisable(false);
            helperElect.setDisable(false);
            if (Client.getClientLogicLayer().getThisPlayer().getHelpers().size() >= 3)
                additionalMain.setDisable(false);
            else additionalMain.setDisable(true);
        } else {
            swapPermit.setDisable(true);
            helperElect.setDisable(true);
        }
    }

    public void setDrawEnabled(boolean enabled) {
        draw.setDisable(!enabled);
    }

    public void setEndTurnEnabled(boolean enabled) {
        endTurn.setDisable(!enabled);
    }

    public void setAllEnabled(boolean enabled) {
        setMainActionsEnabled(enabled);
        setSecondaryActionsEnabled(enabled);
        setDrawEnabled(enabled);
        setEndTurnEnabled(enabled);
    }

    private void linkButtonsToActions() {

        draw.setOnAction(event -> {
            Client.getNetworkManager().draw();
            draw.setDisable(true);
            refreshAllowedActions();
            if (GUIBridge.isMusicEnabled()) {
                AudioClip drawSound = new AudioClip(getClass().getResource("/music/draw.mp3").toExternalForm());
                drawSound.play();
            }
        });
        endTurn.setOnAction(event ->
                Client.getNetworkManager().endTurn());

        elect.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.ELECT));
        acquirePermission.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.ACQUIRE_PERMIT));
        emporiumPermit.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.EMPORIUM_PERMIT));
        kingPermit.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.EMPORIUM_KING));


        additionalMain.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.ADDITIONAL_MAIN));
        acquireHelper.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.ACQUIRE_HELPER));
        swapPermit.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.SWAP_PERMIT));
        helperElect.setOnAction(event ->
                Client.getNetworkManager().startAction(Action.HELPER_ELECT));
    }


}
