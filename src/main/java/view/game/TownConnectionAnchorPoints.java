package view.game;


public enum TownConnectionAnchorPoints {
    NORTH, SOUTH, EAST, WEST, NE, NW, SE, SW
}
