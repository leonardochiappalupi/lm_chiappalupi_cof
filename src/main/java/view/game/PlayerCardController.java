package view.game;


import client.Client;
import game.logic.Player;
import javafx.animation.*;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.Glow;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class PlayerCardController {

    private Player player;

    @FXML
    private Label pc;
    @FXML
    private Label username;
    @FXML
    private Label coins;
    @FXML
    private Label permit;
    @FXML
    private Label helpers;
    @FXML
    private Rectangle userPawn;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label progressText;
    @FXML
    private VBox userRoot;

    public PlayerCardController(Player player) {
        this.player = player;
    }

    public void initialize() {
        pc.setText(String.valueOf(player.getPoliticsCard().size()));
        username.setText(player.getUsername());
        coins.setText(String.valueOf(player.getCoins()));
        permit.setText(String.valueOf(player.getBuildingPermissionCards(false).size()));
        helpers.setText(String.valueOf(player.getHelpers().size()));
        userPawn.setFill(player.getMyColor().getJavaFXColor());
        progressBar.setProgress((float) player.getEmporiums().size() / Client.getClientLogicLayer().getEmporiumsToWin());
        progressText.setText(String.format("%d", player.getWinPosition()));
    }

    public void update() {
        player = Client.getClientLogicLayer().getCurrentLocalState().getPlayerWithUsername(player.getUsername());
        pc.setText(String.valueOf(player.getPoliticsCard().size()));
        username.setText(player.getUsername());
        coins.setText(String.valueOf(player.getCoins()));
        permit.setText(String.valueOf(player.getBuildingPermissionCards(false).size()));
        helpers.setText(String.valueOf(player.getHelpers().size()));
        userPawn.setFill(player.getMyColor().getJavaFXColor());
        progressBar.setProgress((float) player.getEmporiums().size() / Client.getClientLogicLayer().getEmporiumsToWin());
        progressText.setText(String.format("%d", player.getWinPosition()));
        if (player.isDisconnected())
            userRoot.setOpacity(0.6);
        if (player.getEmporiums().size() >= Client.getClientLogicLayer().getEmporiumsToWin())
            glow();
    }

    public String getUsername() {
        return player.getUsername();
    }

    private void glow() {
        Glow glow = new Glow(0.0);
        userRoot.setEffect(glow);
        KeyFrame kf = new KeyFrame(Duration.millis(1000), new KeyValue(glow.levelProperty(), 0.5, Interpolator.EASE_BOTH));
        Timeline glowTl = new Timeline(kf);
        glowTl.setAutoReverse(true);
        glowTl.setCycleCount(Animation.INDEFINITE);
        glowTl.play();
    }
}
