package view.game;

import client.Client;
import client.presentation.gui.GUIBridge;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GameApplet extends Application {
    private GameControllerMain gameController;
    private GUIBridge guiBridge;
    private Stage primaryStage;
    private BoardAvailableController boardAvailableController;
    private PlayerItemsController playerItemsController;
    private SidebarController sidebarController;
    private ActionsController actionsController;
    private TownsController townsController;
    private BoardController boardController;
    private BonusesController bonusController;
    private Parent root;
    private Scene scene;
    private HBox working;
    private MainGUILoader mainGUILoader;
    private MediaPlayer musicPlayer;

    public GameApplet() {
        this.guiBridge = (GUIBridge) Client.getPresentationBridge();
    }

    public Parent getRoot() {
        return root;
    }

    public BoardAvailableController getBoardAvailableController() {
        return boardAvailableController;
    }

    public PlayerItemsController getPlayerItemsController() {
        return playerItemsController;
    }

    public SidebarController getSidebarController() {
        return sidebarController;
    }

    public ActionsController getActionsController() {
        return actionsController;
    }

    public GameControllerMain getGameController() {
        return gameController;
    }

    public TownsController getTownsController() {
        return townsController;
    }

    public BoardController getBoardController() {
        return boardController;
    }

    public BonusesController getBonusController() {
        return bonusController;
    }


    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/loading.fxml"));
            primaryStage.setScene(new Scene(loader.load()));
            primaryStage.setTitle("Council of Four: the Game");
            primaryStage.setResizable(true);
            primaryStage.show();
            mainGUILoader = new MainGUILoader();
            mainGUILoader.start();

            primaryStage.setOnCloseRequest(windowEvent -> {
                System.exit(0);
                if (musicPlayer != null)
                    musicPlayer.stop();
            });

        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }


    }

    public void showMainGui() {
        mainGUILoader.initializeMainGui();
    }


    public void updateBoard() {
        boardAvailableController.update();
        boardController.updateShownPermissionCards();
        boardController.updateFieldsCouncilors();
        boardController.updateKingCouncilors();
        boardController.updateNobilityRoute();
        boardController.updateTownBonusTiles();
        boardController.updateKingBonusTiles();
        boardController.updateFieldBonusTiles();
    }

    public void updateOthers() {
        sidebarController.update();
    }

    private class MainGUILoader extends Thread {

        @Override
        public void run() {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/main.fxml"));
                gameController = new GameControllerMain(GameApplet.this);
                loader.setController(gameController);
                root = loader.load();
                scene = new Scene(root);
                Parent boardRoot = gameController.getBoardRoot();
                working = (HBox) root.lookup("#working");
                boardAvailableController = new BoardAvailableController(root);
                boardController = new BoardController(boardRoot);
                playerItemsController = new PlayerItemsController(root);
                sidebarController = new SidebarController(root);
                actionsController = new ActionsController(root);
                bonusController = new BonusesController(root);
                townsController = new TownsController(boardRoot);
                initialSetup();
                Platform.runLater(() -> {
                    primaryStage.setScene(scene);
                    townsController.buildAnchorsConnections();
                    townsController.buildTownConnections();
                    gameController.resizeToContent();
                    primaryStage.sizeToScene();
                    guiBridge.onAppletReady();
                });

                if (GUIBridge.isMusicEnabled()) {
                    new Thread(() -> {
                        Media music = new Media(getClass().getResource("/music/main.mp3").toExternalForm());
                        musicPlayer = new MediaPlayer(music);
                        musicPlayer.setCycleCount(MediaPlayer.INDEFINITE);
                        musicPlayer.play();
                    }).start();
                }


            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }

        public void initializeMainGui() {

            /**Alternative async loading, empty for now**/
        }

        private void initialSetup() {
            townsController.buildTowns();
            boardController.buildNobilityRoute();
            updateBoard();
            updateOthers();
            playerItemsController.update();
        }
    }

    public void stopMusic() {
        if (GUIBridge.isMusicEnabled()) {
            //KeyValue volume1 = new KeyValue(musicPlayer.volumeProperty(), 100);
            //KeyFrame volume1kf = new KeyFrame(new Duration(0), volume1);
            KeyValue volume2 = new KeyValue(musicPlayer.volumeProperty(), 0);
            KeyFrame volume2kf = new KeyFrame(Duration.seconds(1), volume2);
            Timeline fadeout = new Timeline(volume2kf);
            fadeout.setOnFinished(event -> {
                musicPlayer.stop();
                musicPlayer.dispose();
            });
            fadeout.play();
        }
    }

    public void setWorking(boolean isWorking) {

        Platform.runLater(()->working.setVisible(isWorking));
    }
}
