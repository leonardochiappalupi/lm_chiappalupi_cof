package view.game;

import game.bonus.IBonus;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class BonusController {
    IBonus bonus;
    @FXML
    ImageView image;
    @FXML
    Label quantity;
    @FXML
    StackPane stack;

    boolean withShadow;

    public BonusController(IBonus bonus, boolean withShadow) {
        this.bonus = bonus;
        this.withShadow = withShadow;
    }

    public void initialize() {
        if (!withShadow)
            stack.setEffect(null);
        applyImage();
        int quantityNumber = bonus.getElementsQuantity();
        if (quantityNumber != 0)
            quantity.setText(String.valueOf(bonus.getElementsQuantity()));
        else
            quantity.setVisible(false);

    }


    private void applyImage() {
        switch (bonus.getElementsType()) {
            case CARDS:
                image.setImage(new Image("/images/bonuses/politics.png"));
                break;
            case HELPERS:
                image.setImage(new Image("/images/bonuses/helper.png"));
                break;
            case WINSTEPS:
                image.setImage(new Image("/images/bonuses/winsteps.png"));
                break;
            case SUPPACTION:
                image.setImage(new Image("/images/bonuses/suppaction.png"));
                break;
            case NOBSTEPS:
                image.setImage(new Image("images/bonuses/nobstep.png"));
                break;
            case COINS:
                image.setImage(new Image("images/bonuses/coin.png"));
                break;
            case PERMISSIONCARDBONUSES:
                image.setImage(new Image("images/bonuses/permitcardbonuses.png"));
                break;
            case TOWNBONUSES:
                image.setImage(new Image("images/bonuses/townbonuses.png"));
                break;
            default:
                break;
        }
    }
}
