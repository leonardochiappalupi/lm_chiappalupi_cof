package view.game;

import client.presentation.cli.CLIUtils;
import game.bonus.DirectBonus;
import game.logic.Player;
import game.logic.actions.Action;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;

import java.util.Timer;
import java.util.TimerTask;

public class BonusesController {
    private boolean shown = false;
    private Timer timer = new Timer();

    private VBox bonuses;

    public BonusesController(Parent root) {
        bonuses = (VBox) root.lookup("#bonuses");
    }

    public void showNewBonus(DirectBonus bonus, Action context, Player player) {
        Platform.runLater(() -> {
            bonuses.getChildren().add(getBonusLine(bonus, context, player));
            if (!shown) {
                shown = true;
                bonuses.setVisible(true);
                bonuses.setOpacity(1.0);
            }
            startTimer();
        });
    }

    private Parent getBonusLine(DirectBonus bonus, Action context, Player player) {
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        hBox.setFillHeight(true);
        hBox.setAlignment(Pos.CENTER);
        hBox.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), null)));
        hBox.setEffect(new DropShadow(BlurType.ONE_PASS_BOX, Color.DARKGRAY, 10, 0, 0, 0));
        Label username = new Label(player.getUsername());
        username.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, Font.getDefault().getSize()));
        hBox.getChildren().add(username);
        hBox.getChildren().add(ElementsBuilder.buildBonus(bonus, false));
        hBox.getChildren().add(new Label("Now: " + CLIUtils.newBonusStatus(bonus, player)));
        hBox.getChildren().add(new Label("(during action: " + context.name().toLowerCase() + ")"));
        hBox.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        return hBox;
    }

    private void startTimer() {
        timer.cancel();
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                hideBonuses();
            }
        };
        timer.schedule(timerTask, 5000);
    }

    private void hideBonuses() {

        Timeline fade = new Timeline();
        KeyValue startV = new KeyValue(bonuses.opacityProperty(), 1.0, Interpolator.EASE_IN);
        KeyFrame startF = new KeyFrame(new Duration(0), startV);
        KeyValue endV = new KeyValue(bonuses.opacityProperty(), 0.0, Interpolator.EASE_IN);
        KeyFrame endF = new KeyFrame(new Duration(1000), endV);
        fade.getKeyFrames().addAll(startF, endF);

        fade.setOnFinished(event -> {
            bonuses.setVisible(false);
            bonuses.getChildren().clear();
            shown = false;
        });
        fade.play();

    }


}
