package view.game.choosers;


import client.Client;
import client.presentation.gui.GUIBridge;
import game.cards.BuildingPermissionCard;
import game.cards.CardType;
import game.logic.exceptions.GameLogicException;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import view.game.ElementsBuilder;
import view.game.GameControllerMain;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PermitCardYoursController {

    @FXML
    TilePane permitCards;
    @FXML
    Button cancel;

    private GameControllerMain gameControllerMain;
    private boolean includeCovered;


    public PermitCardYoursController(GameControllerMain gameControllerMain, boolean includeCovered) {
        this.gameControllerMain = gameControllerMain;
        this.includeCovered = includeCovered;
    }

    public void initialize() {
        cancel.setOnAction(event -> gameControllerMain.cancelChooser());

        for (BuildingPermissionCard card : Client.getClientLogicLayer().getThisPlayer().getBuildingPermissionCards(includeCovered)) {
            Parent cardGUI = ElementsBuilder.buildPermissionCard(card, true);
            cardGUI.setOnMouseClicked(event -> {
                        gameControllerMain.hideChooser();
                        ((GUIBridge) Client.getPresentationBridge()).setWorkingState(true);
                        new Thread(() -> {
                            try {
                                Client.getNetworkManager().getCurrentPickedCallbacks().onCardsPicked(CardType.BUILDINGPERMISSION, card);
                            } catch (RemoteException | GameLogicException e) {
                                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                            }
                        }).start();
                    });
            permitCards.getChildren().add(cardGUI);
        }

    }
}
