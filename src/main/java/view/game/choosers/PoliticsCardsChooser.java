package view.game.choosers;


import client.Client;
import game.cards.Card;
import game.cards.CardType;
import game.cards.PoliticsCard;
import game.cards.PoliticsColor;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.ItemsNotValidException;
import game.logic.exceptions.NotEnoughCreditsException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import util.ErrorMessagePopup;
import view.game.GameControllerMain;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PoliticsCardsChooser {


    @FXML
    private ComboBox blackPcNumber;
    @FXML
    private ComboBox orangePcNumber;
    @FXML
    private ComboBox bluePcNumber;
    @FXML
    private ComboBox violetPcNumber;
    @FXML
    private ComboBox pinkPcNumber;
    @FXML
    private ComboBox whitePcNumber;
    @FXML
    private ComboBox jollyPcNumber;
    @FXML
    private Button confirm;
    @FXML
    private Button cancel;

    private int selected = 0;
    private ArrayList<PoliticsColor> selectedColors = new ArrayList<>();
    private static final int MAX_PC_TO_SELECT = 4;

    private Map<PoliticsColor, Integer> colorMax = new EnumMap<>(PoliticsColor.class);
    private Map<PoliticsColor, ObservableList> colorValues = new EnumMap<>(PoliticsColor.class);
    private Map<PoliticsColor, ComboBox> colorCombo = new EnumMap<>(PoliticsColor.class);

    private GameControllerMain gameController;


    public PoliticsCardsChooser(GameControllerMain controller) {

        gameController = controller;
        for (PoliticsColor color : PoliticsColor.values()) {
            colorMax.put(color, Client.getClientLogicLayer().getThisPlayer().getPoliticsCardOfColor(color).size());
            colorValues.put(color, FXCollections.observableArrayList());
        }
    }

    private void setObservableListFromInt(ObservableList list, int selectableMax) {
        int currentMax = list.size() - 1;
        if (currentMax > selectableMax) {
            list.remove(selectableMax + 1, list.size());
        } else if (currentMax < selectableMax) {
            for (int i = currentMax + 1; i <= selectableMax; i++)
                list.add(String.valueOf(i));
        }
    }

    public void initialize() {

        cancel.setOnAction(event -> gameController.cancelChooser());

        colorCombo.put(PoliticsColor.BLACK, blackPcNumber);
        colorCombo.put(PoliticsColor.BLUE, bluePcNumber);
        colorCombo.put(PoliticsColor.ORANGE, orangePcNumber);
        colorCombo.put(PoliticsColor.VIOLET, violetPcNumber);
        colorCombo.put(PoliticsColor.PINK, pinkPcNumber);
        colorCombo.put(PoliticsColor.WHITE, whitePcNumber);
        colorCombo.put(PoliticsColor.JOLLY, jollyPcNumber);

        for (PoliticsColor color : PoliticsColor.values())
            colorCombo.get(color).getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                if (!selectedColors.contains(color) && !"0".equals(newValue))
                    selectedColors.add(color);
                else if (selectedColors.contains(color) && "0".equals(newValue))
                    selectedColors.remove(color);
                updateSelectable(oldValue, newValue);
            });
        updateLists(true);

        confirm.setOnAction(event -> confirm());


    }

    private void confirm() {
        gameController.hideChooser();
        List<PoliticsCard> pickedCards = new ArrayList<>();

        for (PoliticsColor color : PoliticsColor.values()) {
            for (int i = 0; colorCombo.get(color).getValue() != null && i < Integer.valueOf((String) colorCombo.get(color).getValue()); i++) {
                pickedCards.add(new PoliticsCard(color));
            }
        }

        try {
            Client.getNetworkManager().getCurrentPickedCallbacks().onCardsPicked(CardType.POLITICS, pickedCards.toArray(new Card[0]));
        } catch (ItemsNotValidException e) {
            //CARDS ARE NOT VALID FOR THIS COUNCIL
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            new ErrorMessagePopup("These cards do not satisfy the council. Every card you choose must have a relative " +
                    "councilor to be valid...").show();
            gameController.hideChooser();
        } catch (NotEnoughCreditsException e) {
            //USER DOES NOT HAVE ENOUGH COINS FOR THIS OPERATION
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            new ErrorMessagePopup("You do not have enough coins to satisfy the council.").show();
            gameController.hideChooser();
        } catch (GameLogicException | RemoteException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void updateLists(boolean firstUpdate) {

        for (PoliticsColor color : PoliticsColor.values()) {

            if (!selectedColors.contains(color))
                setObservableListFromInt(colorValues.get(color), colorMax.get(color) > (MAX_PC_TO_SELECT - selected) ? (MAX_PC_TO_SELECT - selected) : colorMax.get(color));

            if (colorValues.get(color).size() > 1)
                colorCombo
                        .get(color)
                        .setDisable(false);
            else colorCombo
                    .get(color)
                    .setDisable(true);

            if (firstUpdate) {
                colorCombo.get(color).setItems(colorValues.get(color));
                colorCombo.get(color).getSelectionModel().selectFirst();
            }
        }
    }

    private void updateSelectable(Object oldValue, Object newValue) {

        if (oldValue != null)
            selected -= Integer.valueOf((String) oldValue);
        if (newValue != null)
            selected += Integer.valueOf((String) newValue);

        updateLists(false);
    }


}
