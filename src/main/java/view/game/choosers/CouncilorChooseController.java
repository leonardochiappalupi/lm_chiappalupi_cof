package view.game.choosers;

import client.Client;
import game.cards.PoliticsColor;
import game.pawns.PawnType;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import view.game.GameControllerMain;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CouncilorChooseController {
    @FXML
    private Label remainingBlack;
    @FXML
    private Label remainingBlue;
    @FXML
    private Label remainingOrange;
    @FXML
    private Label remainingPink;
    @FXML
    private Label remainingViolet;
    @FXML
    private Label remainingWhite;
    @FXML
    private Button black;
    @FXML
    private Button blue;
    @FXML
    private Button orange;
    @FXML
    private Button pink;
    @FXML
    private Button violet;
    @FXML
    private Button white;
    @FXML
    private Button cancel;

    private GameControllerMain gameController;

    public CouncilorChooseController(GameControllerMain gameController) {
        this.gameController = gameController;
    }

    public void initialize() {

        cancel.setOnAction(event -> gameController.cancelChooser());

        long remainingBlackCount = Client.getClientLogicLayer().getCurrentLocalState().getAvailableCouncilorsOfColor(PoliticsColor.BLACK);
        long remainingBlueCount = Client.getClientLogicLayer().getCurrentLocalState().getAvailableCouncilorsOfColor(PoliticsColor.BLUE);
        long remainingOrangeCount = Client.getClientLogicLayer().getCurrentLocalState().getAvailableCouncilorsOfColor(PoliticsColor.ORANGE);
        long remainingPinkCount = Client.getClientLogicLayer().getCurrentLocalState().getAvailableCouncilorsOfColor(PoliticsColor.PINK);
        long remainingVioletCount = Client.getClientLogicLayer().getCurrentLocalState().getAvailableCouncilorsOfColor(PoliticsColor.VIOLET);
        long remainingWhiteCount = Client.getClientLogicLayer().getCurrentLocalState().getAvailableCouncilorsOfColor(PoliticsColor.WHITE);

        remainingBlack.setText(String.valueOf(remainingBlackCount));
        remainingBlue.setText(String.valueOf(remainingBlueCount));
        remainingOrange.setText(String.valueOf(remainingOrangeCount));
        remainingPink.setText(String.valueOf(remainingPinkCount));
        remainingViolet.setText(String.valueOf(remainingVioletCount));
        remainingWhite.setText(String.valueOf(remainingWhiteCount));

        black.setOnAction(getEventHandler(PoliticsColor.BLACK));
        if (remainingBlackCount == 0)
            black.setDisable(true);
        blue.setOnAction(getEventHandler(PoliticsColor.BLUE));
        if (remainingBlueCount == 0)
            blue.setDisable(true);
        orange.setOnAction(getEventHandler(PoliticsColor.ORANGE));
        if (remainingOrangeCount == 0)
            orange.setDisable(true);
        pink.setOnAction(getEventHandler(PoliticsColor.PINK));
        if (remainingPinkCount == 0)
            pink.setDisable(true);
        violet.setOnAction(getEventHandler(PoliticsColor.VIOLET));
        if (remainingVioletCount == 0)
            violet.setDisable(true);
        white.setOnAction(getEventHandler(PoliticsColor.WHITE));
        if (remainingWhiteCount == 0)
            white.setDisable(true);

    }

    private EventHandler<ActionEvent> getEventHandler(PoliticsColor color) {
        return event -> {
            try {

                gameController.hideChooser();
                Client.getNetworkManager().getCurrentPickedCallbacks().onPawnsPicked(PawnType.COUNCILOR, color);

            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }

        };
    }
}
