package view.game.choosers;


import client.Client;
import client.presentation.gui.GUIBridge;
import game.board.Field;
import game.cards.BuildingPermissionCard;
import game.cards.CardType;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.ItemsNotValidException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import util.ErrorMessagePopup;
import view.game.ElementsBuilder;
import view.game.GameControllerMain;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PermitCardFieldController {

    private GameControllerMain controller;
    private Field field;

    @FXML
    private Button left;
    @FXML
    private Button right;
    @FXML
    private Button cancel;

    public PermitCardFieldController(GameControllerMain gameControllerMain, Field field) {
        controller = gameControllerMain;
        this.field = field;
    }

    public void initialize() {

        cancel.setOnAction(event -> controller.cancelChooser());

        BuildingPermissionCard leftCard = Client.getClientLogicLayer().getCurrentLocalState().getMap().getMapField(field).getLeftShownPermissionCard();
        BuildingPermissionCard rightCard = Client.getClientLogicLayer().getCurrentLocalState().getMap().getMapField(field).getRightShownPermissionCard();

        left.setGraphic(ElementsBuilder.buildPermissionCard(leftCard, true));
        right.setGraphic(ElementsBuilder.buildPermissionCard(rightCard, true));

        left.setOnAction(event -> chooseCardAsync(leftCard));

        right.setOnAction(event -> chooseCardAsync(rightCard));

    }

    private void chooseCardAsync(BuildingPermissionCard card) {
        controller.hideChooser();
        ((GUIBridge) Client.getPresentationBridge()).setWorkingState(true);
        new Thread(() -> {
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onCardsPicked(CardType.BUILDINGPERMISSION, card);
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            } catch (ItemsNotValidException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                new ErrorMessagePopup("There are no more cards left, trade to get more!").show();
            } catch (GameLogicException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
        }).start();
    }
}
