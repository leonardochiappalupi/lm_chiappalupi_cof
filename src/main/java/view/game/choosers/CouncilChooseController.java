package view.game.choosers;


import client.Client;
import game.board.BoardItemType;
import game.board.Council;
import game.board.Field;
import game.board.MapField;
import game.logic.exceptions.GameLogicException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import view.game.GameControllerMain;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CouncilChooseController {

    private GameControllerMain gameController;
    private boolean includeKing;

    @FXML
    StackPane root;

    @FXML
    Button seaside;
    @FXML
    Button mainland;
    @FXML
    Button mountain;
    @FXML
    Button king;
    @FXML
    Button cancel;

    public CouncilChooseController(GameControllerMain gameController, boolean includeKing) {
        this.gameController = gameController;
        this.includeKing = includeKing;
    }

    public void initialize() {

        seaside.setOnAction(eventHandlerCreator(BoardItemType.COUNCIL, Field.SEASIDE));
        mainland.setOnAction(eventHandlerCreator(BoardItemType.COUNCIL, Field.MAINLAND));
        mountain.setOnAction(eventHandlerCreator(BoardItemType.COUNCIL, Field.MOUNTAINS));
        king.setOnAction(eventHandlerCreator(BoardItemType.KINGCOUNCIL, null));
        cancel.setOnAction(event -> gameController.cancelChooser());

        for (MapField field : Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllFields())
            for (int i = 1; i <= field.getCouncil().getCouncilors().size(); i++) {
                Color color = field.getCouncil().getCounclilorNumber(i - 1).getColor().getFXColor();
                ImageView councilorImg;
                switch (field.getFieldType()) {
                    case SEASIDE:
                        councilorImg = (ImageView) root.lookup("#seasideC" + i);
                        break;
                    case MAINLAND:
                        councilorImg = (ImageView) root.lookup("#mainlandC" + i);
                        break;
                    case MOUNTAINS:
                        councilorImg = (ImageView) root.lookup("#mountainC" + i);
                        break;
                    default:
                        councilorImg = new ImageView();
                        break;
                }
                councilorImg.setEffect(new Lighting(new Light.Distant(0, 88, color)));
            }

        Council kingCouncil = Client.getClientLogicLayer().getCurrentLocalState().getKingCouncil();
        for (int i = 1; i <= kingCouncil.getCouncilors().size(); i++) {
            Color color = kingCouncil.getCounclilorNumber(i - 1).getColor().getFXColor();
            ImageView councilorImg = (ImageView) root.lookup("#kingC" + i);
            councilorImg.setEffect(new Lighting(new Light.Distant(0, 88, color)));
        }

        if (!includeKing) {
            king.setDisable(true);
        }


    }

    private EventHandler<ActionEvent> eventHandlerCreator(BoardItemType type, Field field) {
        return event -> {
            try {

                gameController.hideChooser();
                Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(type, field);

            } catch (RemoteException | GameLogicException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        };
    }


}
