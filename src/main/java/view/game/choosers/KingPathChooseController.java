package view.game.choosers;


import client.Client;
import client.presentation.gui.GUIBridge;
import game.board.Anchor;
import game.board.BoardItem;
import game.board.BoardItemType;
import game.board.Town;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.ItemsNotValidException;
import game.logic.exceptions.NotEnoughCreditsException;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import util.ErrorMessagePopup;
import view.game.GameControllerMain;
import view.game.TownsController;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KingPathChooseController {

    private TownsController townsController;
    private List<String> selected = new ArrayList<>();
    private Parent root;
    private GameControllerMain controllerMain;
    private Town currentKingTown;
    private Parent confirmPane;

    public KingPathChooseController(GameControllerMain controllerMain, Parent root, TownsController townsController) {
        this.townsController = townsController;
        this.root = root;
        this.controllerMain = controllerMain;
        confirmPane = (HBox) root.lookup("#kingConfirm");
    }

    public void start() {
        currentKingTown = Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithKing();
        selected.add(currentKingTown.getName());


        Button confirmButton = (Button) confirmPane.lookup("#confirmButton");
        confirmButton.setOnAction(event -> confirm());

        confirmPane.setVisible(true);
        townsController.getControllerOfTown(currentKingTown.getName()).mark();
        highlightConnectedTownsExceptSelected(currentKingTown.getName());

    }

    private void confirm() {

        confirmPane.setVisible(false);
        ((GUIBridge) Client.getPresentationBridge()).setWorkingState(true);
        townsController.unDimAll();
        townsController.unMarkAll();

        new Thread(() -> {
            try {
                Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.TOWN, (BoardItem[]) getArrayOfSelectedTowns());
            } catch (RemoteException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            } catch (ItemsNotValidException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                new ErrorMessagePopup("You cannot build an emporium in a town where you already did before").show();
            } catch (NotEnoughCreditsException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                if ("coins".equals(e.getCreditType()))
                    new ErrorMessagePopup("You don't have enough coins for this moves").show();
                else new ErrorMessagePopup("You don't have enough helpers to pay other players' emporiums").show();
            } catch (GameLogicException e) {
                Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
            }
        }).start();

        confirmPane.setVisible(false);
        townsController.unDimAll();
        townsController.unMarkAll();
        townsController.updateKingPosition();
    }


    private void highlightConnectedTownsExceptSelected(String townWhereToStartName) {
        Town townWhereToStart = Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithName(townWhereToStartName);
        List<String> townsToHighlight = new ArrayList<>();
        List<Anchor> connectedAnchors = new ArrayList<>();

        for (Town t : Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllTowns()) {
            if (!selected.contains(t.getName()))
                townsController.getControllerOfTown(t).dim();
        }

        for (Town town : townWhereToStart.getConnectedTowns()) {
            if (!selected.contains(town.getName()) && !town.hasEmporiumOfPlayer(Client.getClientLogicLayer().getThisPlayer())) {
                townsToHighlight.add(town.getName());
            }
        }

        for (Anchor a : townWhereToStart.getConnectedAnchors())
            connectedAnchors.add(a);

        for (Town connected : Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllTowns()) {
            for (Anchor anchor : connectedAnchors)
                if (connected.getConnectedAnchors().contains(anchor) && !selected.contains(connected.getName())
                        && !connected.hasEmporiumOfPlayer(Client.getClientLogicLayer().getThisPlayer())) {
                    townsToHighlight.add(connected.getName());
                }
        }

        for (String townName : townsToHighlight) {
            townsController.getControllerOfTown(townName).unDim();
            townsController.getControllerOfTown(townName).getRoot().setOnMouseClicked(event -> {
                selected.add(townName);
                townsController.getControllerOfTown(townName).mark();
                townsController.getControllerOfTown(townName).getRoot().setOnMouseClicked(null);
                townsController.getControllerOfTown(townName).showKing(true);
                townsController.getControllerOfTown(currentKingTown).showKing(false);
                currentKingTown = Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithName(townName);
                highlightConnectedTownsExceptSelected(townName);
            });
        }
    }

    private Town[] getArrayOfSelectedTowns() {
        List<Town> temp = new ArrayList<>();
        for (String townName : selected)
            temp.add(Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithName(townName));

        return temp.toArray(new Town[0]);
    }


}
