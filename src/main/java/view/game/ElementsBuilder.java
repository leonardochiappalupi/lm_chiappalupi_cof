package view.game;

import client.Client;
import game.board.Town;
import game.bonus.DirectBonus;
import game.bonus.IBonus;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsColor;
import game.logic.Player;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import util.BonusesPopup;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ElementsBuilder {

    private ElementsBuilder() {
    }

    public static Parent buildTown(Town town, List<TownController> controllers) {
        try {
            FXMLLoader loader = new FXMLLoader(Client.getPresentationBridge().getClass().getResource("/layouts/town.fxml"));
            TownController townController = new TownController(town);
            loader.setController(townController);
            controllers.add(townController);
            return loader.load();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            return null;
        }
    }

    public static Parent buildBonus(IBonus bonus, boolean withShadow) {
        try {
            FXMLLoader loader = new FXMLLoader(Client.getPresentationBridge().getClass().getResource("/layouts/bonus_view.fxml"));
            loader.setController(new BonusController(bonus, withShadow));
            return loader.load();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            return null;
        }
    }

    public static Parent buildPlayersCard(Player player, List<PlayerCardController> playerCardControllers) {
        try {
            FXMLLoader loader = new FXMLLoader(Client.getPresentationBridge().getClass().getResource("/layouts/user_item.fxml"));
            PlayerCardController cardController = new PlayerCardController(player);
            loader.setController(cardController);
            playerCardControllers.add(cardController);
            return loader.load();

        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    public static Parent buildPermissionCard(BuildingPermissionCard card, boolean withShadow) {
        Label allowedTowns;
        TilePane bonuses;
        VBox cardPane;
        GridPane plusContainer;
        ImageView plus;
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(Client.getPresentationBridge().getClass().getResource("/layouts/building_permission_card.fxml"));
            root = loader.load();
            allowedTowns = (Label) root.lookup("#allowedTowns");
            bonuses = (TilePane) root.lookup("#bonuses");
            cardPane = (VBox) root.lookup("#card");
            plusContainer = (GridPane) root.lookup("#plusContainer");
            plus = new ImageView("/images/ic_plus_black_48dp.png");
            plus.setFitHeight(16);
            plus.setFitWidth(16);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            return null;
        }

        if (card != null) {
            String allowedTownsLetters = "";
            int townNumber = 1;
            for (Town t : card.getAllowedTowns()) {
                allowedTownsLetters = allowedTownsLetters + t.getName().toUpperCase().charAt(0) + (townNumber != card.getAllowedTowns().size() ? "|" : "");
                townNumber++;
            }
            allowedTowns.setText(allowedTownsLetters);

            int bonusCreated = 0;
            for (DirectBonus b : card.getBonusList()) {
                bonusCreated++;

                if (bonusCreated > 2) {
                    plusContainer.add(plus, 0, 0);
                    plusContainer.setOnMouseClicked(event ->
                            new BonusesPopup(card.getBonusList()).show()
                    );
                    break;
                } else {
                    bonuses.getChildren().add(ElementsBuilder.buildBonus(b, false));
                }
            }

        } else {
            allowedTowns.setText("No cards");
        }
        if (withShadow) {
            DropShadow shadow=new DropShadow(BlurType.ONE_PASS_BOX,Color.DARKGRAY,10,0,0,0);
            cardPane.setEffect(shadow);
        }
        return root;


    }

    public static Parent buildHelperToSell() {
        try {
            FXMLLoader loader = new FXMLLoader(Client.getPresentationBridge().getClass().getResource("/layouts/market/helper_to_sell.fxml"));
            return loader.load();

        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    public static Parent buildPermissionCardToSell(BuildingPermissionCard buildingPermissionCard, boolean withShadow) {
        try {
            FXMLLoader loader = new FXMLLoader(Client.getPresentationBridge().getClass().getResource("/layouts/market/building_permission_card_to_sell.fxml"));
            Parent root = loader.load();

            GridPane buildingPermissionCardContainer = (GridPane) root.lookup("#buildingPermissionCardContainer");
            buildingPermissionCardContainer.add(buildPermissionCard(buildingPermissionCard, withShadow), 0, 0);

            return root;

        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    public static Parent buildPoliticCardToSell(PoliticsColor color) {
        try {
            FXMLLoader loader = new FXMLLoader(Client.getPresentationBridge().getClass().getResource("/layouts/market/politic_card_to_sell.fxml"));
            Parent root = loader.load();

            ImageView politicsCardImage = (ImageView) root.lookup("#politicsCardImage");
            setPoliticsCardImage(color, politicsCardImage);

            return root;

        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    public static Parent buildEmporium() {
        try {
            return FXMLLoader.load(Client.getPresentationBridge().getClass().getResource("/layouts/emporium.fxml"));

        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    private static void setPoliticsCardImage(PoliticsColor color, ImageView politicsCardImage) {
        switch (color) {
            case BLACK:
                politicsCardImage.setImage(new Image("/images/cards/black_pc.png"));
                break;
            case BLUE:
                politicsCardImage.setImage(new Image("/images/cards/blue_pc.png"));
                break;
            case VIOLET:
                politicsCardImage.setImage(new Image("/images/cards/violet_pc.png"));
                break;
            case PINK:
                politicsCardImage.setImage(new Image("/images/cards/pink_pc.png"));
                break;
            case ORANGE:
                politicsCardImage.setImage(new Image("/images/cards/orange_pc.png"));
                break;
            case WHITE:
                politicsCardImage.setImage(new Image("/images/cards/white_pc.png"));
                break;
            case JOLLY:
                politicsCardImage.setImage(new Image("/images/cards/jolly_pc.png"));
                break;
            default:
                break;
        }
    }
}
