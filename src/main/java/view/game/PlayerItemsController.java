package view.game;


import client.Client;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsColor;
import game.logic.Player;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Rectangle;

public class PlayerItemsController {

    private Label username;
    private Label pinkCards;
    private Label orangeCards;
    private Label blueCards;
    private Label violetCards;
    private Label whiteCards;
    private Label blackCards;
    private Label jollyCards;
    private Label helpers;
    private Label coins;
    private TilePane permitCardsFlow;
    private Rectangle yourPawn;
    private ProgressBar yourProgressBar;
    private Label yourProgress;
    private StackPane yourRoot;

    public PlayerItemsController(Node root) {
        pinkCards = (Label) root.lookup("#pink_cards");
        orangeCards = (Label) root.lookup("#orange_cards");
        blueCards = (Label) root.lookup("#blue_cards");
        violetCards = (Label) root.lookup("#violet_cards");
        whiteCards = (Label) root.lookup("#white_cards");
        blackCards = (Label) root.lookup("#black_cards");
        jollyCards = (Label) root.lookup("#jolly_cards");
        helpers = (Label) root.lookup("#yourHelpersNumber");
        coins = (Label) root.lookup("#coins");
        username = (Label) root.lookup("#username");
        yourPawn = (Rectangle) root.lookup("#yourPawn");
        yourProgressBar = (ProgressBar) root.lookup("#yourProgressBar");
        yourProgress = (Label) root.lookup("#yourProgress");
        yourRoot = (StackPane) root.lookup("you");

        permitCardsFlow = (TilePane) ((ScrollPane) root.lookup("#permitCardsPane")).getContent().lookup("#permitCardsFlow");
    }

    public void update() {
        Platform.runLater(() -> refresh());

    }

    private void refresh() {
        Player thisPlayerState = Client.getClientLogicLayer().getThisPlayer();
        pinkCards.setText(String.valueOf(thisPlayerState
                .getPoliticsCard()
                .stream()
                .filter(card -> card.getColor().equals(PoliticsColor.PINK))
                .count()));
        orangeCards.setText(String.valueOf(thisPlayerState
                .getPoliticsCard()
                .stream()
                .filter(card -> card.getColor().equals(PoliticsColor.ORANGE))
                .count()));
        blueCards.setText(String.valueOf(thisPlayerState
                .getPoliticsCard()
                .stream()
                .filter(card -> card.getColor().equals(PoliticsColor.BLUE))
                .count()));
        violetCards.setText(String.valueOf(thisPlayerState
                .getPoliticsCard()
                .stream()
                .filter(card -> card.getColor().equals(PoliticsColor.VIOLET))
                .count()));
        whiteCards.setText(String.valueOf(thisPlayerState
                .getPoliticsCard()
                .stream()
                .filter(card -> card.getColor().equals(PoliticsColor.WHITE))
                .count()));
        blackCards.setText(String.valueOf(thisPlayerState
                .getPoliticsCard()
                .stream()
                .filter(card -> card.getColor().equals(PoliticsColor.BLACK))
                .count()));
        jollyCards.setText(String.valueOf(thisPlayerState.getPoliticsCard()
                .stream()
                .filter(card -> card.getColor().equals(PoliticsColor.JOLLY))
                .count()));
        helpers.setText(String.valueOf(thisPlayerState
                .getHelpers()
                .size()));
        coins.setText(String.valueOf(thisPlayerState
                .getCoins()));
        username.setText(thisPlayerState.getUsername());
        yourPawn.setFill(thisPlayerState.getMyColor().getJavaFXColor());

        permitCardsFlow.getChildren().clear();
        for (BuildingPermissionCard card : thisPlayerState.getBuildingPermissionCards(true)) {
            Node cardGui = ElementsBuilder.buildPermissionCard(card, true);
            permitCardsFlow.getChildren().add(cardGui);
            if (card.isFlipped())
                cardGui.setOpacity(0.5);
        }

        yourProgressBar.setProgress((float) thisPlayerState.getEmporiums().size() / Client.getClientLogicLayer().getEmporiumsToWin());
        yourProgress.setText(String.format("%d", thisPlayerState.getWinPosition()));

    }

}
