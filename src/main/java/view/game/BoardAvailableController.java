package view.game;


import client.Client;
import game.GameState;
import game.cards.PoliticsColor;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Label;

public class BoardAvailableController {

    private Label pinkCouncilors;
    private Label orangeCouncilors;
    private Label blueCouncilors;
    private Label violetCouncilors;
    private Label whiteCouncilors;
    private Label blackCouncilors;
    private Label helpers;

    public BoardAvailableController(Node root) {
        pinkCouncilors = (Label) root.lookup("#councilors_pink");
        orangeCouncilors = (Label) root.lookup("#councilors_orange");
        blueCouncilors = (Label) root.lookup("#councilors_blue");
        violetCouncilors = (Label) root.lookup("#councilors_violet");
        whiteCouncilors = (Label) root.lookup("#councilors_white");
        blackCouncilors = (Label) root.lookup("#councilors_black");
        helpers = (Label) root.lookup("#helpersNumber");
    }

    public void update() {
        Platform.runLater(() ->
                refresh()
        );

    }

    private void refresh() {
        GameState currentGameState = Client.getClientLogicLayer().getCurrentLocalState();
        pinkCouncilors.setText(String.valueOf(currentGameState.getAvailableCouncilors()
                .stream()
                .filter(councilor -> councilor.getColor().equals(PoliticsColor.PINK))
                .count()));
        orangeCouncilors.setText(String.valueOf(currentGameState.getAvailableCouncilors()
                .stream()
                .filter(councilor -> councilor.getColor().equals(PoliticsColor.ORANGE))
                .count()));
        blueCouncilors.setText(String.valueOf(currentGameState.getAvailableCouncilors()
                .stream()
                .filter(councilor -> councilor.getColor().equals(PoliticsColor.BLUE))
                .count()));
        violetCouncilors.setText(String.valueOf(currentGameState.getAvailableCouncilors()
                .stream()
                .filter(councilor -> councilor.getColor().equals(PoliticsColor.VIOLET))
                .count()));
        whiteCouncilors.setText(String.valueOf(currentGameState.getAvailableCouncilors()
                .stream()
                .filter(councilor -> councilor.getColor().equals(PoliticsColor.WHITE))
                .count()));
        blackCouncilors.setText(String.valueOf(currentGameState.getAvailableCouncilors()
                .stream()
                .filter(councilor -> councilor.getColor().equals(PoliticsColor.BLACK))
                .count()));
        helpers.setText(String.valueOf(currentGameState.getAvailableHelpers().size()));
    }
}
