package view.game;

import client.Client;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class GameEndController {

    @FXML
    private Label winner;

    public void initialize() {
        if (winner != null)
            winner.setText(String.format("%s won the game. How bad!", Client.getClientLogicLayer().getCurrentPlayerUsername()));
    }

    @FXML
    public void closeGame() {
        System.exit(0);
    }
}
