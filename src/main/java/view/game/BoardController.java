package view.game;


import client.Client;
import game.GameState;
import game.board.MapField;
import game.board.NobilityRoute;
import game.board.NobilityRouteCell;
import game.board.TownColor;
import game.bonus.IBonus;
import game.cards.FieldBonusTile;
import game.cards.KingBonusTile;
import game.cards.TownColorBonusTile;
import game.logic.Player;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class BoardController {

    private Parent root;
    private AnchorPane seasideLC;
    private AnchorPane seasideRC;
    private AnchorPane mainlandLC;
    private AnchorPane mainlandRC;
    private AnchorPane mountainLC;
    private AnchorPane mountainRC;
    private GridPane nobilityRoute;
    private HBox orangeBonusTile;
    private HBox blueBonusTile;
    private HBox goldBonusTile;
    private HBox silverBonusTile;
    private HBox seasideBonusTile;
    private HBox mainlandBonusTile;
    private HBox mountainBonusTile;

    private List<TilePane> nobilityRoutePlayersCells = new ArrayList<>();

    public BoardController(Parent root) {
        this.root = root;
        seasideLC = (AnchorPane) root.lookup("#seasideLC");
        seasideRC = (AnchorPane) root.lookup("#seasideRC");
        mainlandLC = (AnchorPane) root.lookup("#mainlandLC");
        mainlandRC = (AnchorPane) root.lookup("#mainlandRC");
        mountainLC = (AnchorPane) root.lookup("#mountainLC");
        mountainRC = (AnchorPane) root.lookup("#mountainRC");
        nobilityRoute = (GridPane) root.lookup("#nobilityRoute");
        orangeBonusTile = (HBox) root.lookup("#orangeBonusTile");
        blueBonusTile = (HBox) root.lookup("#blueBonusTile");
        goldBonusTile = (HBox) root.lookup("#goldBonusTile");
        silverBonusTile = (HBox) root.lookup("#silverBonusTile");
        seasideBonusTile = (HBox) root.lookup("#seasideBonusTile");
        mainlandBonusTile = (HBox) root.lookup("#mainlandBonusTile");
        mountainBonusTile = (HBox) root.lookup("#mountainBonusTile");

    }

    public void buildNobilityRoute() {
        NobilityRoute nobilityRouteGame = Client.getClientLogicLayer().getCurrentLocalState().getNobilityRoute();
        for (NobilityRouteCell cell : nobilityRouteGame.getCells()) {
            TilePane bonuses = new TilePane(0, 0);
            bonuses.setPrefColumns(1);
            bonuses.setPrefRows(2);
            bonuses.setPrefHeight(Region.USE_COMPUTED_SIZE);
            bonuses.setPrefWidth(Region.USE_COMPUTED_SIZE);
            for (IBonus bonus : cell.getBonuses())
                bonuses.getChildren().add(ElementsBuilder.buildBonus(bonus, false));
            nobilityRoute.add(bonuses, cell.getIndex(), 1);

            TilePane playersPane = new TilePane(3, 3);
            playersPane.setPrefRows(2);
            playersPane.setPrefColumns(2);
            playersPane.setPrefHeight(Region.USE_COMPUTED_SIZE);
            playersPane.setPrefWidth(Region.USE_COMPUTED_SIZE);
            playersPane.setAlignment(Pos.CENTER);
            playersPane.setTileAlignment(Pos.CENTER);
            nobilityRoute.add(playersPane, cell.getIndex(), 0);
            nobilityRoutePlayersCells.add(playersPane);
        }
    }

    public void updateNobilityRoute() {
        Platform.runLater(() -> {
            for (TilePane p : nobilityRoutePlayersCells) {
                p.getChildren().clear();
            }

            for (Player p : Client.getClientLogicLayer().getCurrentLocalState().getPlayers()) {
                int nobilityPosition = p.getNobilityPosition();
                Rectangle playerPawn = new Rectangle(15, 15, p.getMyColor().getJavaFXColor());
                playerPawn.setArcHeight(8);
                playerPawn.setArcWidth(8);
                nobilityRoutePlayersCells.get(nobilityPosition).getChildren().addAll(playerPawn);
            }
        });


    }

    public void updateTownBonusTiles() {
        Platform.runLater(this::refreshTownBonusTiles);

    }

    private void refreshTownBonusTiles() {
        List<TownColorBonusTile> townColorBonusTiles = Client.getClientLogicLayer().getCurrentLocalState().getTownColorBonusTiles();
        for (TownColorBonusTile tile : townColorBonusTiles) {
            TownColor tileColor = tile.getTownColor();
            String quantity = String.valueOf(Client
                    .getClientLogicLayer()
                    .getCurrentLocalState()
                    .getTownColorBonusTile(tileColor)
                    .getWinSteps());
            switch (tileColor) {
                case ORANGE:
                    ((Label) orangeBonusTile.lookup("#quantity")).setText(quantity);
                    if (tile.hasBeenDrawn())
                        orangeBonusTile.setOpacity(0.4);
                    break;
                case BLUE:
                    ((Label) blueBonusTile.lookup("#quantity")).setText(quantity);
                    if (tile.hasBeenDrawn())
                        blueBonusTile.setOpacity(0.4);
                    break;
                case GOLD:
                    ((Label) goldBonusTile.lookup("#quantity")).setText(quantity);
                    if (tile.hasBeenDrawn())
                        goldBonusTile.setOpacity(0.4);
                    break;
                case SILVER:
                    ((Label) silverBonusTile.lookup("#quantity")).setText(quantity);
                    if (tile.hasBeenDrawn())
                        goldBonusTile.setOpacity(0.4);
                    break;
                default:
                    break;

            }
        }
    }

    public void updateShownPermissionCards() {
        Platform.runLater(() -> {
            GameState currentGameState = Client.getClientLogicLayer().getCurrentLocalState();
            seasideRC.getChildren().clear();
            seasideLC.getChildren().clear();
            mainlandRC.getChildren().clear();
            mainlandLC.getChildren().clear();
            mountainRC.getChildren().clear();
            mountainLC.getChildren().clear();
            seasideLC.getChildren().add(ElementsBuilder.buildPermissionCard(currentGameState.getMap().getSeaField().getLeftShownPermissionCard(), false));
            seasideRC.getChildren().add(ElementsBuilder.buildPermissionCard(currentGameState.getMap().getSeaField().getRightShownPermissionCard(), false));
            mainlandLC.getChildren().add(ElementsBuilder.buildPermissionCard(currentGameState.getMap().getMainlandField().getLeftShownPermissionCard(), false));
            mainlandRC.getChildren().add(ElementsBuilder.buildPermissionCard(currentGameState.getMap().getMainlandField().getRightShownPermissionCard(), false));
            mountainLC.getChildren().add(ElementsBuilder.buildPermissionCard(currentGameState.getMap().getMountainField().getLeftShownPermissionCard(), false));
            mountainRC.getChildren().add(ElementsBuilder.buildPermissionCard(currentGameState.getMap().getMountainField().getRightShownPermissionCard(), false));
        });
    }

    public void updateFieldsCouncilors() {
        Platform.runLater(this::refreshFieldsCouncilors);
    }

    private void refreshFieldsCouncilors() {

        GameState currentGameState = Client.getClientLogicLayer().getCurrentLocalState();
        for (MapField field : currentGameState.getMap().getAllFields()) {
            for (int i = 1; i <= field.getCouncil().getCouncilors().size(); i++) {
                Circle councilor = new Circle();
                switch (field.getFieldType()) {
                    case SEASIDE:
                        councilor = (Circle) root.lookup("#seasideC" + i);
                        break;
                    case MAINLAND:
                        councilor = (Circle) root.lookup("#mainlandC" + i);
                        break;
                    case MOUNTAINS:
                        councilor = (Circle) root.lookup("#mountainC" + i);
                        break;
                    default:
                        break;
                }
                councilor.setFill(field.getCouncil().getCounclilorNumber(i - 1).getColor().getFXColor());
            }
        }
    }

    public void updateKingCouncilors() {
        Platform.runLater(() -> {
            GameState currentGameState = Client.getClientLogicLayer().getCurrentLocalState();
            for (int i = 1; i <= currentGameState.getKingCouncil().getCouncilors().size(); i++) {
                Circle kingCouncilor = (Circle) root.lookup("#kingC" + i);
                kingCouncilor.setFill(currentGameState.getKingCouncil().getCounclilorNumber(i - 1).getColor().getFXColor());
            }
        });

    }

    public void updateKingBonusTiles() {
        Platform.runLater(() -> {
            KingBonusTile[] tiles = Client.getClientLogicLayer().getCurrentLocalState().getKingBonusTiles();
            for (int i = 0; i < tiles.length; i++) {
                HBox tile = (HBox) root.lookup("#kingBonusTile" + i);
                Label quantity = (Label) tile.lookup("#quantity");
                quantity.setText(String.valueOf(tiles[i].getWinSteps()));
                if (tiles[i].hasBeenDrawn())
                    tile.setVisible(false);
                else tile.setVisible(true);
            }
        });
    }

    public void updateFieldBonusTiles() {
        Platform.runLater(this::refreshFieldBonusTiles);

    }

    private void refreshFieldBonusTiles() {
        FieldBonusTile seaside = Client
                .getClientLogicLayer().getCurrentLocalState().getMap().getSeaField().getBonusTile();
        if (seaside.hasBeenDrawn())
            seasideBonusTile.setOpacity(0.5);
        else seasideBonusTile.setOpacity(1.0);
        ((Label) seasideBonusTile.lookup("#quantity")).setText(String.valueOf(seaside.getWinSteps()));

        FieldBonusTile mainland = Client
                .getClientLogicLayer().getCurrentLocalState().getMap().getMainlandField().getBonusTile();
        if (mainland.hasBeenDrawn())
            mainlandBonusTile.setOpacity(0.5);
        else mainlandBonusTile.setOpacity(1.0);
        ((Label) mainlandBonusTile.lookup("#quantity")).setText(String.valueOf(mainland.getWinSteps()));

        FieldBonusTile mountain = Client
                .getClientLogicLayer().getCurrentLocalState().getMap().getMountainField().getBonusTile();
        if (mountain.hasBeenDrawn())
            mountainBonusTile.setOpacity(0.5);
        else mountainBonusTile.setOpacity(1.0);
        ((Label) mountainBonusTile.lookup("#quantity")).setText(String.valueOf(Client
                .getClientLogicLayer().getCurrentLocalState().getMap().getMountainField().getBonusTile().getWinSteps()));
    }


}
