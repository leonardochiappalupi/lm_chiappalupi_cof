package view.game;


import client.Client;
import game.board.Anchor;
import game.board.Field;
import game.board.Map;
import game.board.Town;
import game.bonus.DirectBonus;
import game.logic.Player;
import game.pawns.Emporium;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.effect.Effect;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import util.BonusesPopup;

import java.util.ArrayList;
import java.util.List;

public class TownController {

    Town town;
    @FXML
    VBox bonuses;
    @FXML
    Label townName;
    @FXML
    ImageView image;
    @FXML
    FlowPane emporiums;
    @FXML
    HBox townBg;
    @FXML
    ImageView kingIcon;

    List<Parent> emporiumsRoots = new ArrayList<>();
    private Effect bgEffect;


    public TownController(Town town) {
        this.town = town;
    }

    public String getTownName() {
        return town.getName();
    }

    public void initialize() {
        GridPane plusContainer = (GridPane) townBg.lookup("#plusContainer");
        ImageView plus = new ImageView("/images/ic_plus_black_48dp.png");
        plus.setFitHeight(16);
        plus.setFitWidth(16);

        int bonusCreated = 0;
        for (DirectBonus b : town.getBonusList()) {
            bonusCreated++;

            if (bonusCreated > 2) {
                plusContainer.add(plus, 0, 0);
                plusContainer.setOnMouseClicked(event ->
                        new BonusesPopup(town.getBonusList(), "town").show());
                break;
            } else {
                bonuses.getChildren().add(ElementsBuilder.buildBonus(b, true));
            }
        }

        townName.setText(town.getName());
        image.setEffect(new Lighting(new Light.Distant(0, 89, town.getColor().getColor())));

        for (Player p : Client.getClientLogicLayer().getCurrentLocalState().getPlayers()) {
            Parent emporium = ElementsBuilder.buildEmporium();
            emporiums.getChildren().add(emporium);
            emporiumsRoots.add(emporium);
        }
        for (Emporium e : town.getEmporiums()) {
            if (e.getOwningPlayer().getUsername().equals("FAKE")) {
                Parent additionalSlot = ElementsBuilder.buildEmporium();
                emporiums.getChildren().add(additionalSlot);
                emporiumsRoots.add(additionalSlot);
            }
        }

        if (town.getHasKing())
            kingIcon.setVisible(true);
        else
            kingIcon.setVisible(false);

    }

    public void updateEmporiums() {
        for (Parent p : emporiumsRoots) {
            ImageView emporiumImage = (ImageView) p.lookup("#emporiumIcon");
            emporiumImage.setVisible(false);
        }
        List<Emporium> emporiumsInTown = Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithName(town.getName()).getEmporiums();
        for (Emporium emporium : emporiumsInTown) {
            ImageView emporiumIcon = getFirstEmptyEmporiumIcon();
            emporiumIcon.setEffect(new Lighting(new Light.Distant(0, 89, emporium.getOwningPlayer().getMyColor().getJavaFXColor())));
            emporiumIcon.setVisible(true);
        }
    }

    private ImageView getFirstEmptyEmporiumIcon() {
        for (Parent p : emporiumsRoots) {
            ImageView emporiumImage = (ImageView) p.lookup("#emporiumIcon");
            if (!emporiumImage.isVisible())
                return emporiumImage;
        }
        return null;
    }

    public Point2D getConnectionAnchorPoint(TownConnectionAnchorPoints which) {

        Point2D point = townBg.localToScene(0.0, 0.0, true);

        switch (which) {
            case NORTH:
                point = townBg.localToScene(townBg.getWidth() / 2, 0.0, true);
                break;
            case EAST:
                point = townBg.localToScene(townBg.getWidth(), townBg.getHeight() / 2, true);
                break;
            case SOUTH:
                point = townBg.localToScene(townBg.getWidth() / 2, townBg.getHeight(), true);
                break;
            case WEST:
                point = townBg.localToScene(0, townBg.getHeight() / 2, true);
                break;
            case NE:
                point = townBg.localToScene(townBg.getWidth(), 0.0, true);
                break;
            case SE:
                point = townBg.localToScene(townBg.getWidth(), townBg.getHeight(), true);
                break;
            case NW:
                point = townBg.localToScene(0.0, 0.0, true);
                break;
            case SW:
                point = townBg.localToScene(0.0, townBg.getHeight(), true);
                break;
            default:
                break;
        }
        return point;
    }

    public static TownConnectionAnchorPoints connectionDecider(int startSector, int finalSector) {
        if (startSector % 2 == 0) {
            if (finalSector % 2 == 0) {
                if (finalSector > startSector)
                    if (finalSector - startSector == 1)
                        return TownConnectionAnchorPoints.SOUTH;
                    else
                        return TownConnectionAnchorPoints.WEST;
                else if (startSector - finalSector == 1)
                    return TownConnectionAnchorPoints.NORTH;
                else return TownConnectionAnchorPoints.WEST;
            } else {
                if (finalSector - startSector > 1)
                    return TownConnectionAnchorPoints.SE;
                else if (finalSector - startSector == 1)
                    return TownConnectionAnchorPoints.EAST;
                else return TownConnectionAnchorPoints.NE;
            }
        } else {
            if (finalSector % 2 == 0) {
                if (startSector - finalSector > 1)
                    return TownConnectionAnchorPoints.NW;
                else if (startSector - finalSector == 1)
                    return TownConnectionAnchorPoints.WEST;
                else return TownConnectionAnchorPoints.SW;
            } else {
                if (finalSector > startSector)
                    if (finalSector - startSector == 1)
                        return TownConnectionAnchorPoints.SOUTH;
                    else return TownConnectionAnchorPoints.EAST;
                else if (startSector - finalSector == 1)
                    return TownConnectionAnchorPoints.NORTH;
                else return TownConnectionAnchorPoints.EAST;
            }
        }
    }

    public static TownConnectionAnchorPoints anchorConnectionDecider(Map map, Town town, Anchor anchor) {
        Field fieldOfTown = map.getFieldOfTown(town);
        if (fieldOfTown == Field.SEASIDE)
            return TownConnectionAnchorPoints.EAST;
        if (fieldOfTown == Field.MOUNTAINS)
            return TownConnectionAnchorPoints.WEST;
        if (fieldOfTown == Field.MAINLAND) {
            if (town.getSector() % 2 == 0)
                return TownConnectionAnchorPoints.WEST;
            else return TownConnectionAnchorPoints.EAST;
        }
        return null;
    }

    public void dim() {
        townBg.setOpacity(0.5);
        townBg.setOnMouseClicked(null);
    }

    public void unDim() {
        townBg.setOpacity(1.0);
        townBg.setOnMouseClicked(null);
    }

    public Parent getRoot() {
        return townBg;
    }

    public Town getTown() {
        return town;
    }

    public void showKing(boolean show) {
        if (show)
            kingIcon.setVisible(true);
        else
            kingIcon.setVisible(false);
    }

    public void mark() {
        bgEffect = townBg.getEffect();
        townBg.setEffect(new Lighting(new Light.Distant(0, 88, Color.YELLOW)));
    }

    public void unMark() {
        if (bgEffect != null)
            townBg.setEffect(bgEffect);
    }


}
