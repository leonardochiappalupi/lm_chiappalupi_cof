package view.game.market;

import client.Client;
import client.presentation.gui.GUIBridge;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.market.ItemToSell;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import view.game.GameControllerMain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class MarketBuyController {

    @FXML
    private VBox permissionBox;
    @FXML
    private VBox politicsBox;
    @FXML
    private VBox helpersBox;
    @FXML
    private HBox roothbox;
    @FXML
    private Button endButton;
    @FXML
    private StackPane loading;
    @FXML
    private Label yourCoins;

    private List<ItemToSell> listOfItems = new ArrayList<>();
    private GameControllerMain gameControllerMain;
    private Map<String, ItemToBuyController> itemsControllers = new HashMap<>();

    public MarketBuyController(GameControllerMain gameControllerMain) {
        this.gameControllerMain = gameControllerMain;
    }


    public void initialize() {

        yourCoins.setText("Your Coins: " + Client.getClientLogicLayer().getThisPlayer().getCoins());
        loading.setVisible(true);
        endButton.setOnAction(event -> {

            if (GUIBridge.isMusicEnabled()) {
                AudioClip marketSell = new AudioClip(getClass().getResource("/music/marketclose.mp3").toExternalForm());
                marketSell.play();
            }

            gameControllerMain.hideChooser();
            Client.getNetworkManager().endTurn();


        });
        setItemsToSell(Client.getClientLogicLayer().getCurrentItemsForSale());
    }

    private void setItemsToSell(List<ItemToSell> items) {
        this.listOfItems = items;

        for (ItemToSell item : listOfItems
                .stream()
                .filter(item1 -> !item1.getSeller().getUsername().equals(Client.getUsername()))
                .collect(Collectors.toList())) {
            Platform.runLater(() -> {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/market/itemToBuy.fxml"));
                    ItemToBuyController controller = new ItemToBuyController(item);
                    itemsControllers.put(item.getUniqueId(), controller);
                    loader.setController(controller);
                    GridPane root = loader.load();
                    if (item.getItem().getClass() == BuildingPermissionCard.class)
                        permissionBox.getChildren().add(root);
                    else if (item.getItem().getClass() == PoliticsCard.class)
                        politicsBox.getChildren().add(root);
                    else
                        helpersBox.getChildren().add(root);
                } catch (IOException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            });
        }

        Platform.runLater(() -> loading.setVisible(false));

    }

    public void markItemAsSold(ItemToSell item) {
        Platform.runLater(() -> {
            itemsControllers.get(item.getUniqueId()).setItemAsBuyed();
            yourCoins.setText("Your Coins: " + Client.getClientLogicLayer().getThisPlayer().getCoins());
        });

    }


}
