package view.game.market;

import client.Client;
import client.presentation.gui.GUIBridge;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.market.ItemToSell;
import game.pawns.Helper;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import util.ErrorMessagePopup;
import view.game.ElementsBuilder;


public class ItemToBuyController {
    @FXML
    private GridPane gridPane;
    @FXML
    private Button button;
    @FXML
    private Label label;
    @FXML
    private ImageView image;


    private ItemToSell item;

    public ItemToBuyController(ItemToSell item) {
        this.item = item;
    }

    public void initialize() {

        if (item.getItem().getClass() == BuildingPermissionCard.class) {
            VBox card = (VBox) ElementsBuilder.buildPermissionCard((BuildingPermissionCard) item.getItem(), true);
            gridPane.add(card, 0, 0);
        } else if (item.getItem().getClass() == PoliticsCard.class) {
            PoliticsCard card = (PoliticsCard) item.getItem();
            image = getPCImage(card);
            image.setPreserveRatio(true);
            image.setFitHeight(40);
            gridPane.add((image), 0, 0);
        } else if (item.getItem().getClass() == Helper.class) {
            ImageView helper = new ImageView("/images/bonuses/helper.png");
            helper.setPreserveRatio(true);
            helper.setFitHeight(40);
            gridPane.add(helper, 0, 0);
        }
        label.setText(String.valueOf(item.getItem().getPrice()) + " X ");
        ImageView coin = new ImageView("/images/bonuses/coin.png");
        coin.setPreserveRatio(true);
        coin.setFitHeight(30);
        gridPane.add(coin, 2, 0);

        button.setOnAction(event -> {
            int numberOfCoins = Client.getClientLogicLayer().getThisPlayer().getCoins();
            if (numberOfCoins >= item.getPrice()) {
                Client.getNetworkManager().buyItem(item);
                if (GUIBridge.isMusicEnabled()) {
                    AudioClip marketSell = new AudioClip(getClass().getResource("/music/purchase.mp3").toExternalForm());
                    marketSell.play();
                }
            } else
                new ErrorMessagePopup("You don't have enough coins for that Item!").show();
        });
    }

    public void setItemAsBuyed() {
        gridPane.setOpacity(0.5);
        button.setDisable(true);
    }

    private ImageView getPCImage(PoliticsCard card) {
        ImageView imageView = new ImageView();
        switch (card.getColor()) {
            case BLACK:
                imageView = new ImageView("/images/cards/black_pc.png");
                break;
            case BLUE:
                imageView = new ImageView("/images/cards/blue_pc.png");
                break;
            case VIOLET:
                imageView = new ImageView("/images/cards/violet_pc.png");
                break;
            case PINK:
                imageView = new ImageView("/images/cards/pink_pc.png");
                break;
            case ORANGE:
                imageView = new ImageView("/images/cards/orange_pc.png");
                break;
            case WHITE:
                imageView = new ImageView("/images/cards/white_pc.png");
                break;
            case JOLLY:
                imageView = new ImageView("/images/cards/jolly_pc.png");
                break;
            default:
                break;
        }
        return imageView;
    }

}
