package view.game.market;

import client.Client;
import client.presentation.gui.GUIBridge;
import game.cards.BuildingPermissionCard;
import game.cards.PoliticsCard;
import game.logic.Player;
import game.market.ItemToSell;
import game.pawns.Helper;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.TilePane;
import javafx.scene.media.AudioClip;
import util.ConfirmPopup;
import util.IConfirmPopupHandler;
import util.StringFormatChecker;
import view.game.ElementsBuilder;
import view.game.GameControllerMain;

import java.util.ArrayList;
import java.util.List;

import static constants.MarketConstants.MARKET_SELL_CONFIRM_MESSAGE;
import static constants.MarketConstants.MARKET_SKIP_CONFIRM_MESSAGE;

public class MarketSellController {
    private Player player;

    private List<ItemToSell> helpersToSellList = new ArrayList<>();
    private List<ItemToSell> politicCardsToSellList = new ArrayList<>();
    private List<ItemToSell> buildingPermissionCardsToSellList = new ArrayList<>();
    private ArrayList<ItemToSell> completeToSellList = new ArrayList<>();
    private GameControllerMain gameControllerMain;


    @FXML
    private TilePane buildingPermissionCardList;
    @FXML
    private TilePane politicCardsList;
    @FXML
    private TilePane helpersList;

    public MarketSellController(GameControllerMain gameControllerMain) {
        this.player = Client.getClientLogicLayer().getThisPlayer();
        this.gameControllerMain = gameControllerMain;
    }

    @FXML
    public void sell() {
        IConfirmPopupHandler popupHandler = new IConfirmPopupHandler() {
            @Override
            public void onConfirmation() {
                createHelpersToSellList();
                createPoliticCardsToSellList();
                createBuildingPermissionCardsToSellList();

                completeToSellList.addAll(helpersToSellList);
                completeToSellList.addAll(politicCardsToSellList);
                completeToSellList.addAll(buildingPermissionCardsToSellList);

                closeMarketAndProceed();
            }

            @Override
            public void onDismissal() {
                /** Nothing to do in case of dismissal **/
            }
        };
        new ConfirmPopup(popupHandler, MARKET_SELL_CONFIRM_MESSAGE).show();
    }

    private void closeMarketAndProceed() {
        if (GUIBridge.isMusicEnabled()) {
            AudioClip marketSell = new AudioClip(getClass().getResource("/music/marketclose.mp3").toExternalForm());
            marketSell.play();
        }

        gameControllerMain.hideChooser();
        Client.getNetworkManager().sendItemsToSell(completeToSellList);
    }

    private void createHelpersToSellList() {
        for (int i = 0; i < helpersList.getChildren().size(); i++) {
            TextField price = (TextField) helpersList.getChildren().get(i).lookup("#price");

            if (!price.getText().isEmpty() && new StringFormatChecker().digitsOnly(price.getText())) {
                Helper helper = new Helper(1);
                helper.setPrice(Integer.parseInt(price.getText()));
                ItemToSell itemToSell = new ItemToSell(helper, player);

                helpersToSellList.add(itemToSell);
            }
        }
    }

    private void createPoliticCardsToSellList() {
        for (int i = 0; i < politicCardsList.getChildren().size(); i++) {
            TextField price = (TextField) politicCardsList.getChildren().get(i).lookup("#price");

            if (!price.getText().isEmpty() && new StringFormatChecker().digitsOnly(price.getText())) {
                PoliticsCard politicsCard = new PoliticsCard(player.getPoliticsCard().get(i).getColor());
                politicsCard.setPrice(Integer.parseInt(price.getText()));
                ItemToSell itemToSell = new ItemToSell(politicsCard, player);

                politicCardsToSellList.add(itemToSell);
            }
        }
    }

    private void createBuildingPermissionCardsToSellList() {
        for (int i = 0; i < buildingPermissionCardList.getChildren().size(); i++) {
            TextField price = (TextField) buildingPermissionCardList.getChildren().get(i).lookup("#price");

            if (!price.getText().isEmpty() && new StringFormatChecker().digitsOnly(price.getText())) {
                BuildingPermissionCard buildingPermissionCard;
                buildingPermissionCard = player.getBuildingPermissionCards(false).get(i);
                buildingPermissionCard.setPrice(Integer.parseInt(price.getText()));
                ItemToSell itemToSell = new ItemToSell(buildingPermissionCard, player);

                buildingPermissionCardsToSellList.add(itemToSell);
            }
        }
    }

    @FXML
    public void skip() {
        new ConfirmPopup(new IConfirmPopupHandler() {
            @Override
            public void onConfirmation() {
                completeToSellList.clear();

                closeMarketAndProceed();
            }

            @Override
            public void onDismissal() {
                /** Nothing to do in case of dismissal **/
            }
        }, MARKET_SKIP_CONFIRM_MESSAGE).show();
    }

    private void setupHelpersList() {
        for (int i = 0; i < player.getHelpers().size(); i++) {
            helpersList.getChildren().add(ElementsBuilder.buildHelperToSell());
        }
    }

    private void setupPoliticCardsList() {
        for (PoliticsCard politicsCard : player.getPoliticsCard()) {
            politicCardsList.getChildren().add(ElementsBuilder.buildPoliticCardToSell(politicsCard.getColor()));
        }
    }

    private void setupBuildingPermissionCardsList() {
        for (BuildingPermissionCard buildingPermissionCard : player.getBuildingPermissionCards(false)) {
            buildingPermissionCardList.getChildren().add(ElementsBuilder.buildPermissionCardToSell(buildingPermissionCard, true));
        }
    }

    public void initialize() {
        setupHelpersList();
        setupPoliticCardsList();
        setupBuildingPermissionCardsList();
    }
}
