package view.game;


import client.Client;
import client.presentation.gui.GUIBridge;
import game.board.Anchor;
import game.board.BoardItemType;
import game.board.Town;
import game.board.TownConnection;
import game.cards.BuildingPermissionCard;
import game.logic.Player;
import game.logic.exceptions.GameLogicException;
import game.logic.exceptions.NotEnoughCreditsException;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import util.ErrorMessagePopup;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TownsController {

    private Parent mainPane;
    private Pane connectionsPane;
    private GridPane seasideGrid;
    private GridPane mainlandGrid;
    private GridPane mountainGrid;
    private VBox seaMainAnchors;
    private VBox mainMountainAnchors;
    private List<TownController> townControllers = new ArrayList<>();

    public TownsController(Parent root) {
        this.mainPane = root;
        this.connectionsPane = (Pane) root.lookup("#connectionsPane");
        this.seasideGrid = (GridPane) root.lookup("#seasideGrid");
        this.mainlandGrid = (GridPane) root.lookup("#mainlandGrid");
        this.mountainGrid = (GridPane) root.lookup("#mountainGrid");
        this.seaMainAnchors = (VBox) root.lookup("#seaMainAnchors");
        this.mainMountainAnchors = (VBox) root.lookup("#mainMountainAnchors");
    }


    public void buildTowns() {
        for (Town t : Client.getClientLogicLayer().getCurrentLocalState().getMap().getSeaField().getAllTowns(false)) {
            seasideGrid.add(ElementsBuilder.buildTown(t, townControllers), t.getColumnFromSector(), t.getRowFromSector());
        }
        for (Town t : Client.getClientLogicLayer().getCurrentLocalState().getMap().getMainlandField().getAllTowns(false)) {
            mainlandGrid.add(ElementsBuilder.buildTown(t, townControllers), t.getColumnFromSector(), t.getRowFromSector());
        }

        for (Town t : Client.getClientLogicLayer().getCurrentLocalState().getMap().getMountainField().getAllTowns(false)) {
            mountainGrid.add(ElementsBuilder.buildTown(t, townControllers), t.getColumnFromSector(), t.getRowFromSector());
        }
        updateEmporiums();
    }

    public void buildTownConnections() {

        for (TownConnection tc : Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownConnections()) {
            int sector1 = tc.getTown1().getSector();
            int sector2 = tc.getTown2().getSector();
            TownConnectionAnchorPoints anchorPoint1 = TownController.connectionDecider(sector1, sector2);
            TownConnectionAnchorPoints anchorPoint2 = TownController.connectionDecider(sector2, sector1);

            Point2D scenePoint1 = getControllerOfTown(tc.getTown1()).getConnectionAnchorPoint(anchorPoint1);
            Point2D scenePoint2 = getControllerOfTown(tc.getTown2()).getConnectionAnchorPoint(anchorPoint2);

            if (anchorPoint1 != anchorPoint2) {

                Line line = new Line();
                line.setFill(null);
                line.setStroke(Color.BLACK);
                line.setStrokeWidth(3);

                connectionsPane.getChildren().add(line);

                line.setStartX(connectionsPane.sceneToLocal(scenePoint1).getX());
                line.setStartY(connectionsPane.sceneToLocal(scenePoint1).getY());

                line.setEndX(connectionsPane.sceneToLocal(scenePoint2).getX());
                line.setEndY(connectionsPane.sceneToLocal(scenePoint2).getY());


            } else {
                CubicCurve curve = new CubicCurve();
                curve.setFill(null);
                curve.setStroke(Color.BLACK);
                curve.setStrokeWidth(3);
                curve.getStrokeDashArray().addAll(10d, 10d);

                connectionsPane.getChildren().add(curve);

                curve.setStartX(connectionsPane.sceneToLocal(scenePoint1).getX());
                curve.setStartY(connectionsPane.sceneToLocal(scenePoint1).getY());

                curve.setEndX(connectionsPane.sceneToLocal(scenePoint2).getX());
                curve.setEndY(connectionsPane.sceneToLocal(scenePoint2).getY());

                int controlXdelta = anchorPoint1 == TownConnectionAnchorPoints.EAST ? +30 : -30;

                curve.setControlX1(connectionsPane.sceneToLocal(scenePoint1).getX() + controlXdelta);
                curve.setControlX2(connectionsPane.sceneToLocal(scenePoint2).getX() + controlXdelta);

                curve.setControlY1(connectionsPane.sceneToLocal(scenePoint1).getY());
                curve.setControlY2(connectionsPane.sceneToLocal(scenePoint2).getY());

            }

            Circle startPoint = new Circle(5, Color.DARKGRAY);
            Circle endPoint = new Circle(5, Color.DARKGREY);
            connectionsPane.getChildren().addAll(startPoint, endPoint);

            startPoint.setCenterX(connectionsPane.sceneToLocal(scenePoint1).getX());
            startPoint.setCenterY(connectionsPane.sceneToLocal(scenePoint1).getY());

            endPoint.setCenterX(connectionsPane.sceneToLocal(scenePoint2).getX());
            endPoint.setCenterY(connectionsPane.sceneToLocal(scenePoint2).getY());

        }


    }

    public void buildAnchorsConnections() {

        for (Town t : Client.getClientLogicLayer().getCurrentLocalState().getMap().getAllTowns()) {
            for (Anchor a : t.getConnectedAnchors()) {
                CubicCurve line = new CubicCurve();
                line.setStrokeWidth(3);
                line.setStroke(Color.DARKGRAY);
                line.setFill(null);

                connectionsPane.getChildren().add(line);
                TownConnectionAnchorPoints anchorPoint = TownController.anchorConnectionDecider(Client.getClientLogicLayer().getCurrentLocalState().getMap(), t, a);

                Point2D lineStart = connectionsPane.sceneToLocal(getControllerOfTown(t)
                        .getConnectionAnchorPoint(anchorPoint));
                line.setStartX(lineStart.getX());
                line.setStartY(lineStart.getY());
                line.setControlY1(lineStart.getY());


                Circle anchor = (Circle) mainPane.lookup("#anchor" + a.getLevel());

                double xPosInParent = (anchor.getBoundsInParent().getMaxX() + anchor.getBoundsInParent().getMinX()) / 2.0;
                double yPosInParent = (anchor.getBoundsInParent().getMaxY() + anchor.getBoundsInParent().getMinY()) / 2.0;

                Point2D scenePos;
                if (a.getLevel() <= 2)
                    scenePos = seaMainAnchors.localToScene(xPosInParent, yPosInParent, true);
                else
                    scenePos = mainMountainAnchors.localToScene(xPosInParent, yPosInParent, true);

                Point2D lineEnd = connectionsPane.sceneToLocal(scenePos);
                line.setEndX(lineEnd.getX());
                line.setEndY(lineEnd.getY());
                line.setControlX1((lineEnd.getX() + lineStart.getX()) / 2.0);
                line.setControlX2((lineEnd.getX() + lineStart.getX()) / 2.0);
                line.setControlY2(lineEnd.getY());

                Circle startPoint = new Circle(5, Color.BLACK);
                connectionsPane.getChildren().add(startPoint);
                startPoint.setCenterX(connectionsPane.sceneToLocal(getControllerOfTown(t).getConnectionAnchorPoint(anchorPoint)).getX());
                startPoint.setCenterY(connectionsPane.sceneToLocal(getControllerOfTown(t).getConnectionAnchorPoint(anchorPoint)).getY());


            }
        }
    }

    public TownController getControllerOfTown(Town town) {
        for (TownController controller : townControllers)
            if (controller.getTownName().equals(town.getName()))
                return controller;
        return null;
    }

    public TownController getControllerOfTown(String townName) {
        for (TownController controller : townControllers)
            if (controller.getTownName().equals(townName))
                return controller;
        return null;
    }

    public void updateEmporiums() {
        Platform.runLater(() -> {
            for (TownController tc : townControllers)
                tc.updateEmporiums();
        });

    }

    public void highlightTownsOnAPermitCard(BuildingPermissionCard card) {
        Platform.runLater(() -> {
            for (TownController tc : townControllers) {
                for (Town t : card.getAllowedTowns()) {
                    if (tc.getTownName().equals(t.getName())) {
                        tc.unDim();
                        tc.getRoot().setOnMouseClicked(event -> {
                            unDimAll();
                            ((GUIBridge) Client.getPresentationBridge()).setWorkingState(true);
                            new Thread(() -> {
                                try {
                                    Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.TOWN, tc.getTown());
                                } catch (NotEnoughCreditsException e) {
                                    Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                                    if ("coins".equals(e.getCreditType()))
                                        new ErrorMessagePopup("You don't have enough coins for this moves").show();
                                    else
                                        new ErrorMessagePopup("You don't have enough helpers to pay other players' emporiums").show();
                                } catch (RemoteException | GameLogicException e) {
                                    Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                                }
                            }).start();
                        });
                        break;
                    }
                    tc.dim();
                }
            }
        });

    }

    public void highlighTownsWithEmporiumOfPlayer(Player owner) {
        for (TownController tc : townControllers) {
            if (Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithName(tc.getTownName()).hasEmporiumOfPlayer(owner)) {
                tc.unDim();
                tc.getRoot().setOnMouseClicked(event -> {
                            try {
                                Client.getNetworkManager().getCurrentPickedCallbacks().onBoardItemsPicked(BoardItemType.TOWN, tc.getTown());
                            } catch (GameLogicException | RemoteException e) {
                                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                );
            } else
                tc.dim();
        }
    }

    public void unDimAll() {
        for (TownController tc : townControllers) {
            tc.unDim();
        }
    }

    public void dimAll() {
        for (TownController tc : townControllers) {
            tc.dim();
        }
    }

    public void unMarkAll() {
        for (TownController tc : townControllers) {
            tc.unMark();
        }
    }

    public void updateKingPosition() {
        Town townWithKing = Client.getClientLogicLayer().getCurrentLocalState().getMap().getTownWithKing();
        for (TownController tc : townControllers)
            if (!tc.getTownName().equals(townWithKing.getName()))
                tc.showKing(false);
            else tc.showKing(true);
    }


}
