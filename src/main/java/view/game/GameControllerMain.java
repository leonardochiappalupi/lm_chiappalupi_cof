package view.game;

import client.Client;
import client.presentation.gui.GUIBridge;
import game.board.Field;
import game.cards.BuildingPermissionCard;
import game.logic.Pickables;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.util.Duration;
import view.game.choosers.*;
import view.game.market.MarketBuyController;
import view.game.market.MarketSellController;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameControllerMain {
    @FXML
    private Pane obscurator;
    @FXML
    private Pane connectionsPane;
    @FXML
    private Label yourTurnLabel;
    @FXML
    private VBox boardVBox;
    @FXML
    private StackPane choosePane;
    @FXML
    private ScrollPane boardScroll;

    private Parent currentChooser;

    private GameApplet applet;
    private MarketBuyController marketBuyController;

    public GameControllerMain(GameApplet applet) {
        this.applet = applet;
    }

    public void initialize() {
        obscurator.setVisible(false);
        choosePane.setVisible(false);
    }


    public void yourTurn(String who) {

        Timeline timeline = new Timeline(30);

        KeyValue start = new KeyValue(yourTurnLabel.translateXProperty(), 800);
        KeyValue startObs = new KeyValue(obscurator.opacityProperty(), 0);
        KeyValue slideIn = new KeyValue(yourTurnLabel.translateXProperty(), 0, Interpolator.EASE_OUT);
        KeyValue slideInObs = new KeyValue(obscurator.opacityProperty(), 1);
        KeyValue still = new KeyValue(yourTurnLabel.translateXProperty(), 0);
        KeyValue stillObs = new KeyValue(obscurator.opacityProperty(), 1);
        KeyValue slideOut = new KeyValue(yourTurnLabel.translateXProperty(), -800, Interpolator.EASE_IN);
        KeyValue slideOutObs = new KeyValue(obscurator.opacityProperty(), 0);

        KeyFrame startKf = new KeyFrame(Duration.ZERO, start, startObs);
        KeyFrame slideInKf = new KeyFrame(Duration.millis(250), slideIn, slideInObs);
        KeyFrame stillKf = new KeyFrame(Duration.seconds(2), still, stillObs);
        KeyFrame slideOutKf = new KeyFrame(Duration.millis(2250), slideOut, slideOutObs);

        Platform.runLater(() -> {
            yourTurnLabel.setText(String.format("It's %s turn!", who));
            yourTurnLabel.setVisible(true);
            timeline.getKeyFrames().addAll(startKf, slideInKf, stillKf, slideOutKf);
            timeline.play();
            timeline.setOnFinished(event -> yourTurnLabel.setVisible(false));


            if (GUIBridge.isMusicEnabled()) {
                AudioClip yourTurnAudio = new AudioClip(getClass().getResource("/music/turn.mp3").toExternalForm());
                yourTurnAudio.play();
            }
        });


    }


    public void showChooser(Pickables whatToPick, Object args) {

        switch (whatToPick) {
            case ONE_COUNCIL:
                createNestedChooser("council_choose.fxml", new CouncilChooseController(this, (boolean) args));
                break;
            case ONE_COUNCILOR:
                createNestedChooser("councilor_choose.fxml", new CouncilorChooseController(this));
                break;
            case UPTOFOUR_PC:
                createNestedChooser("politics_choose.fxml", new PoliticsCardsChooser(this));
                break;
            case ONE_FIELD_PERMIT:
                createNestedChooser("permit_choose_field.fxml", new PermitCardFieldController(this, (Field) args));
                break;
            case ONE_YOURS_PERMIT:
                createNestedChooser("permit_choose_yours.fxml", new PermitCardYoursController(this, (Boolean) args));
                break;
            case ONE_PERMIT_TOWN:
                applet.getTownsController().highlightTownsOnAPermitCard((BuildingPermissionCard) args);
                break;
            case KING_TOWNS:
                Platform.runLater(() ->
                        new KingPathChooseController(this, applet.getRoot(), applet.getTownsController()).start()
                );
                break;
            case EMPORIUM_TOWN:
                Platform.runLater(() ->
                        applet.getTownsController().highlighTownsWithEmporiumOfPlayer(Client.getClientLogicLayer().getThisPlayer())
                );
                break;
            default:
                break;
        }

    }

    private void createNestedChooser(String fxmlName, Object controller) {
        Platform.runLater(() -> {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/layouts/choosers/" + fxmlName));
            loader.setController(controller);
            try {
                currentChooser = loader.load();
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
            choosePane.getChildren().add(currentChooser);
            choosePane.setVisible(true);
        });

    }

    public void hideChooser() {

        Platform.runLater(() -> {
            choosePane.getChildren().clear();
            choosePane.setVisible(false);
        });
    }

    public void showMarketSell() {
        Platform.runLater(() -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/market/market_sell.fxml"));
                loader.setController(new MarketSellController(this));
                Parent root = loader.load();
                choosePane.getChildren().add(root);
                choosePane.setVisible(true);

                if (GUIBridge.isMusicEnabled()) {
                    AudioClip marketSell = new AudioClip(getClass().getResource("/music/marketopen.mp3").toExternalForm());
                    marketSell.play();
                }
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        });
    }

    public void showMarketBuy() {
        Platform.runLater(() -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/market/market_Buy.fxml"));
                marketBuyController = new MarketBuyController(this);
                loader.setController(marketBuyController);
                Parent root = loader.load();
                choosePane.getChildren().add(root);
                choosePane.setVisible(true);

                if (GUIBridge.isMusicEnabled()) {
                    AudioClip marketSell = new AudioClip(getClass().getResource("/music/marketopen.mp3").toExternalForm());
                    marketSell.play();
                }
            } catch (IOException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        });
    }

    public MarketBuyController getMarketBuyController() {
        return marketBuyController;
    }

    public void cancelChooser() {
        hideChooser();
        Client.getNetworkManager().cancelAction();
    }

    public Parent getBoardRoot() {
        return boardVBox;
    }

    public void resizeToContent() {
        boardScroll.setPrefSize(boardVBox.getWidth() + 10.0, boardVBox.getHeight() + 10.0);
    }

    public void endGame(boolean isWinner) {
        try {
            Parent paneToAdd;
            AudioClip mediaToPlay;
            if (isWinner) {
                paneToAdd = FXMLLoader.load(getClass().getResource("/layouts/victory.fxml"));
                mediaToPlay = new AudioClip(getClass().getResource("/music/victory.mp3").toExternalForm());
            } else {
                paneToAdd = FXMLLoader.load(getClass().getResource("/layouts/defeat.fxml"));
                mediaToPlay = new AudioClip(getClass().getResource("/music/defeat.mp3").toExternalForm());
            }
            if (paneToAdd != null) {
                Platform.runLater(() -> {
                    choosePane.getChildren().clear();
                    choosePane.getChildren().add(paneToAdd);
                    choosePane.setVisible(true);
                    if (GUIBridge.isMusicEnabled()) {
                        applet.stopMusic();
                        mediaToPlay.play();
                    }
                });

            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }
}

