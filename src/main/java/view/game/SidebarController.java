package view.game;


import client.Client;
import client.presentation.gui.GUIBridge;
import game.logic.Player;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import util.JSONHelper;

import java.util.ArrayList;
import java.util.List;

public class SidebarController {

    private GridPane othersTable;
    private List<PlayerCardController> playerCardControllers = new ArrayList<>();
    private ListView chatList;
    private Button chatSend;
    private TextField chatEdit;
    private int otherPlayersRow = 0;

    public SidebarController(Node root) {
        ScrollPane othersPane = (ScrollPane)
                ((SplitPane) root.lookup("#bottomSplit"))
                        .getItems()
                        .get(0)
                        .lookup("#othersPane");
        othersTable = (GridPane) othersPane
                .getContent()
                .lookup("#othersTable");
        chatList = (ListView)
                ((SplitPane) root.lookup("#bottomSplit"))
                        .getItems()
                        .get(1)
                        .lookup("#chatList");
        chatEdit = (TextField)
                ((SplitPane) root.lookup("#bottomSplit"))
                        .getItems()
                        .get(1)
                        .lookup("#chatEdit");
        chatSend = (Button)
                ((SplitPane) root.lookup("#bottomSplit"))
                        .getItems()
                        .get(1)
                        .lookup("#chatSend");
        chatList.setItems(((GUIBridge) Client.getPresentationBridge()).getChatMessages());
        chatSend.setOnAction(event -> Client.getNetworkManager().remoteChatUpdate(JSONHelper.ChatParser.toJsonChatUpdate(Client.getUsername(), chatEdit.getText())));

    }

    public void update() {
        Platform.runLater(() -> {
            List<Player> currentPlayersState = Client.getClientLogicLayer().getCurrentLocalState().getPlayers();
            Player thisPlayer = Client.getClientLogicLayer().getThisPlayer();
            for (Player p : currentPlayersState) {
                if (!p.getUsername().equals(thisPlayer.getUsername())) {
                    if (getPlayerCardController(p) == null) {
                        othersTable.add(ElementsBuilder.buildPlayersCard(p, playerCardControllers), 0, otherPlayersRow);
                        otherPlayersRow++;
                    } else
                        getPlayerCardController(p).update();
                }
            }
        });
    }

    private PlayerCardController getPlayerCardController(Player player) {
        for (PlayerCardController pc : playerCardControllers) {
            if (pc.getUsername().equals(player.getUsername()))
                return pc;
        }
        return null;
    }
}
