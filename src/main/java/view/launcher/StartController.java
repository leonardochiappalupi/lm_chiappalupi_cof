package view.launcher;

import client.Client;
import constants.ConnectionConstants;
import constants.PresentationConstants;
import constants.ViewConstants;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class controls the launcher fxml.
 * Selected values of connection mode and interface mode are passed to the {@link Client#initialConfiguration} method
 */
public class StartController implements Initializable {

    @FXML
    ToggleGroup interfaceType;
    @FXML
    ToggleGroup connectionType;
    @FXML
    Button startGame;
    @FXML
    AnchorPane anchorPane;
    @FXML
    ImageView launcherImage;
    @FXML
    TextField address;
    @FXML
    CheckBox localhost;
    @FXML
    CheckBox music;

    /**
     * This method takes the string corresponding to the selected connection and interface mode
     * and passes them to the {@link Client#initialConfiguration} method.
     * It also controls that the User selected a connection mode and an interface mode.
     */
    @FXML
    public void loadGame() {

        String connectionModeName;
        String presentationModeName;
        String addressName;

        RadioButton connectionModeSelected = (RadioButton) connectionType.getSelectedToggle();
        RadioButton presentationModeSelected = (RadioButton) interfaceType.getSelectedToggle();

        if (connectionModeSelected.getId().equals(ViewConstants.SOCKET_RADIO_BUTTON_ID))
            connectionModeName = ConnectionConstants.Names.CONN_MODE_SOCKET;
        else
            connectionModeName = ConnectionConstants.Names.CONN_MODE_RMI;


        if (presentationModeSelected.getId().equals(ViewConstants.CLI_RADIO_BUTTON_ID))
            presentationModeName = PresentationConstants.Names.PRES_MODE_CLI;
        else
            presentationModeName = PresentationConstants.Names.PRES_MODE_GUI;

        if (localhost.isSelected())
            addressName = "localhost";
        else addressName = address.getText();

        Stage stage = (Stage) startGame.getScene().getWindow();
        stage.close();


        Client.initialConfiguration(connectionModeName, presentationModeName, addressName, music.isSelected());


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        launcherImage.setFitHeight(ViewConstants.LAUNCH_VIEW_HEIGHT);
        localhost.setOnAction(event -> {
            enableOrDisableLaunchGame();
            address.setDisable(!address.isDisabled());
        });
        address.textProperty().addListener(event -> enableOrDisableLaunchGame());
        interfaceType.selectedToggleProperty().addListener((observable, oldValue, newValue) -> enableOrDisableLaunchGame());
        interfaceType.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            RadioButton selected = (RadioButton) newValue;
            if (selected.getId().equals(ViewConstants.GUI_RADIO_BUTTON_ID))
                music.setDisable(false);
            else music.setDisable(true);
        });
        connectionType.selectedToggleProperty().addListener((observable, oldValue, newValue) -> enableOrDisableLaunchGame());

    }

    private void enableOrDisableLaunchGame() {
        boolean enable;
        enable = interfaceType.getSelectedToggle() != null && connectionType.getSelectedToggle() != null && (!"".equals(address.getText()) || localhost.isSelected());
        startGame.setDisable(!enable);
    }
}
