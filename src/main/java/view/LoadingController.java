package view;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class LoadingController {
    @FXML
    private Label hint;

    private ArrayList<String> suggestions = new ArrayList<>();
    private Timeline fadein;
    private Timeline fadeout;
    private String lastSuggestion;

    public void initialize() {
        fadeout = new Timeline();
        fadeout.getKeyFrames().add(new KeyFrame(new Duration(0), new KeyValue(hint.opacityProperty(), 1)));
        fadeout.getKeyFrames().add(new KeyFrame(new Duration(600), new KeyValue(hint.opacityProperty(), 0, Interpolator.EASE_BOTH)));
        fadein = new Timeline();
        fadein.getKeyFrames().add(new KeyFrame(new Duration(0), new KeyValue(hint.opacityProperty(), 0, Interpolator.EASE_BOTH)));
        fadein.getKeyFrames().add(new KeyFrame(new Duration(600), new KeyValue(hint.opacityProperty(), 1)));
        fadeout.setOnFinished(event -> {
            hint.setText(randomSuggestion());
            fadein.play();
        });

        buildSuggestions();
        hint.setText(randomSuggestion());
        lastSuggestion = hint.getText();
        TimerTask newSuggestion = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> fadeout.play());
            }
        };
        Timer suggestionTimeout = new Timer();
        suggestionTimeout.scheduleAtFixedRate(newSuggestion, 3500, 3500);

    }

    private void buildSuggestions() {
        suggestions.add("Balance your play between emporiums and win points to rule other players");
        suggestions.add("Coins are precious... save them whenever you can!");
        suggestions.add("Count other players items to guess what they are planning");
        suggestions.add("Use the market wisely: it can provide items very hard to get on your own!");
        suggestions.add("Remember to use your secondary actions as well, before ending the turn");
        suggestions.add("Councilors are limited: plan your moves in advance or you will be in troubles");
    }

    private String randomSuggestion() {
        String candidateSuggestion = suggestions.get(new Random().nextInt(suggestions.size()));
        if (candidateSuggestion.equals(lastSuggestion))
            return randomSuggestion();
        else {
            lastSuggestion = candidateSuggestion;
            return candidateSuggestion;
        }
    }
}
