package view.lobby;

import client.Client;
import client.presentation.PresentationBridge;
import client.presentation.gui.GUIBridge;
import game.Configuration;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import util.JSONHelper;
import view.configurator.Configurator;
import view.configurator.IInitialChooserHandler;
import view.configurator.InitialConfiguratorChooser;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LobbyController {
    @FXML
    private VBox mainVbox;
    @FXML
    private Label username;
    @FXML
    private Label timeout;
    @FXML
    private Label maxPlayers;
    @FXML
    private Label leaderUsername;
    @FXML
    private TextField chatEdit;
    @FXML
    private Button chatSend;
    @FXML
    private Label statusbarText;
    @FXML
    private ListView<String> lobbyusersList;
    @FXML
    private ListView<String> chatmessagesList;

    @FXML
    private VBox loginVbox;
    @FXML
    private ProgressIndicator loginLoading;
    @FXML
    private HBox usernameHbox;
    @FXML
    private TextField usernameEdit;
    @FXML
    private Button loginButton;

    @FXML
    private AnchorPane configurationPane;
    @FXML
    private VBox configurationVbox;
    @FXML
    private TextField numplayersEdit;
    @FXML
    private TextField timeoutEdit;
    @FXML
    private TextField turnTimeoutEdit;
    @FXML
    private ComboBox configSelect;
    @FXML
    private Button createconfigButton;
    @FXML
    private Button startButton;
    @FXML
    private StackPane configLoading;

    private GUIBridge presentationBridge;

    public LobbyController(GUIBridge presentationBridge) {
        this.presentationBridge = presentationBridge;
    }

    public void initialize() {

        configurationPane.setVisible(false);
        loginLoading.setVisible(false);
        mainVbox.setEffect(new BoxBlur(10, 10, 2));
        configSelect.setItems(FXCollections.observableList(Arrays.asList(PresentationBridge.availableConfigurations())));

        chatmessagesList.setItems(((GUIBridge) Client.getPresentationBridge()).getChatMessages());
        lobbyusersList.setItems(((GUIBridge) Client.getPresentationBridge()).getLobbyUsers());

        chatSend.setOnAction(event -> {
            presentationBridge.remoteChatUpdate(JSONHelper.ChatParser.toJsonChatUpdate(Client.getUsername(), chatEdit.getText()));
            chatEdit.clear();
        });

        loginButton.setOnAction(event -> login());

        startButton.setOnAction(event -> start());

        createconfigButton.setOnAction(event -> new InitialConfiguratorChooser(new IInitialChooserHandler() {
            @Override
            public void onNewConfiguration() {
                try {
                    Configurator.startGameConfiguration();
                } catch (Exception e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            }

            @Override
            public void onLoadConfiguration(Configuration configurationToLoad) {
                Configurator.loadGameConfiguration(configurationToLoad);
            }
        }).show());
    }

    private void login() {

        usernameHbox.setDisable(true);
        loginLoading.setVisible(true);
        String desiredUsername = usernameEdit.getText();
        boolean isUsernameAvailable = presentationBridge.validateUsername(desiredUsername);
        if (isUsernameAvailable) {
            this.username.setText(desiredUsername);
            boolean amILeader = presentationBridge.amILeader();
            loginVbox.setVisible(false);
            if (amILeader) {
                configurationPane.setVisible(true);
                configLoading.setVisible(false);
            } else {
                presentationBridge.lobby(false);
                mainVbox.setEffect(null);
            }
        } else {
            //SHOW ERROR AND ALLOW FOR RETRY
            usernameEdit.clear();
            usernameHbox.setDisable(false);
            loginLoading.setVisible(false);
        }

    }

    private void start() {
        configLoading.setVisible(true);
        configurationVbox.setDisable(true);
        int maxUsers = Integer.valueOf(numplayersEdit.getText());
        int desiredTimeout = Integer.valueOf(timeoutEdit.getText());
        int desiredTurnTimeout = Integer.valueOf(turnTimeoutEdit.getText());
        String jsonConfiguration = PresentationBridge.getRequestedJsonConfiguration((String) configSelect.getValue());
        boolean hasConfigurationSucceeded = presentationBridge.configureGame(maxUsers, desiredTimeout, desiredTurnTimeout, jsonConfiguration);
        if (hasConfigurationSucceeded) {
            configurationPane.setVisible(false);
            mainVbox.setEffect(null);
        } else {
            configurationVbox.setDisable(false);
            configLoading.setVisible(false);
        }
    }

    public void updateLobby(String leader, int timeout, Integer maxPlayers) {
        Platform.runLater(() -> {
            LobbyController.this.timeout.setText(String.valueOf(timeout));
            if (maxPlayers != null)
                this.maxPlayers.setText(String.valueOf(maxPlayers));
            if (leader != null)
                leaderUsername.setText(leader);
        });

    }

    public void refreshConfigurationsList() {
        configSelect.setItems(FXCollections.observableList(Arrays.asList(PresentationBridge.availableConfigurations())));
    }
}
