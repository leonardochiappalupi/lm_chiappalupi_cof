package view.configurator;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

class SaveConfigurationPopup {
    private ISaveConfiguratorHandler iSaveConfiguratorHandler;

    SaveConfigurationPopup(ISaveConfiguratorHandler iSaveConfiguratorHandler) {
        this.iSaveConfiguratorHandler = iSaveConfiguratorHandler;
    }
    public void show() {
        SaveConfigurationPopupController saveConfigurationPopupController = new SaveConfigurationPopupController(iSaveConfiguratorHandler);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/configurator/save_popup.fxml"));
        loader.setController(saveConfigurationPopupController);
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        Stage primaryStage = new Stage();
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle("Save configuration");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }
}
