package view.configurator;

import client.Client;
import client.presentation.PresentationBridge;
import game.Configuration;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import util.*;
import view.configurator.exceptions.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.ConfigurationConstants.*;

/**
 * The main class of the configurator. Controls everything in the main window and allows to start the others configurator panels.
 */
public class ConfiguratorController implements IConfiguratorHandler {
    private Configuration configuration;
    private boolean randomBonusesRequested;
    private static List<DirectBonus> randomBonusConfigured = new ArrayList<>();
    private boolean isLoaded;

    @FXML
    private AnchorPane rootAnchorPane;
    @FXML
    private AnchorPane rootAnchorPanePermissionCards;
    @FXML
    private AnchorPane rootAnchorPaneNobilityRoute;
    @FXML
    private ListView<String> permissionCardList0;
    @FXML
    private ListView<String> permissionCardList1;
    @FXML
    private ListView<String> permissionCardList2;
    @FXML
    private ListView<String> randomBonusListview;
    @FXML
    private Button confirmConfigurationButton;
    @FXML
    private VBox randomBonusesPanel;
    @FXML
    private ComboBox randomBonusComboBox;
    @FXML
    private TextField quantityRandomBonus;
    @FXML
    private Button removeBonus;
    @FXML
    private Button deletePermissionCard0;
    @FXML
    private Button deletePermissionCard1;
    @FXML
    private Button deletePermissionCard2;
    @FXML
    private Button modifyPermissionCard0;
    @FXML
    private Button modifyPermissionCard1;
    @FXML
    private Button modifyPermissionCard2;

    private final ObservableList seasideCurrentList = FXCollections.observableArrayList();
    private final ObservableList mainlandCurrentList = FXCollections.observableArrayList();
    private final ObservableList mountainsCurrentList = FXCollections.observableArrayList();
    private final ObservableList randomBonusList = FXCollections.observableArrayList();
    private final ObservableList<String> randomBonusTypeList = FXCollections.observableArrayList();

    ConfiguratorController(Configuration loadedConfiguration) {
        if (loadedConfiguration != null) { /** Loaded configuration **/
            this.configuration = loadedConfiguration;

            setTownLists();
            setBuildingPermissionCardsLists();

            this.isLoaded = true;
        } else { /** New configuration **/
            initializeNewConfiguration();

            this.isLoaded = false;
        }
    }

    /**
     * Loads all the towns of the loaded {@link Configuration} into the configurator layout, making them visible
     */
    private void loadAllTowns() {
        configuration.getMap().getAllFields().stream()
                .forEach(mapField -> mapField.getAllTowns(false).stream()
                        .forEach(town -> onUpdateTown(town.getName(), town.getColor(), mapField.getFieldType(), town.getSector())));
    }

    /**
     * Resets the town lists inside the {@link Configuration} object, adding also null towns; this is necessary due to some checks done on the opened town when the AddTown button is pressed
     */
    private void setTownLists() {
        for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
            List<Town> loadedTowns = new ArrayList<>();
            for (int x = 0; x < NUMBER_OF_SECTORS_PER_FIELD; x++) {
                loadedTowns.add(null);
            }

            for (int j = 0; j < configuration.getMap().getMapField(i).getAllTowns(false).size(); j++) {
                int sectorToReplace = configuration.getMap().getMapField(i).getAllTowns(false).get(j).getSector();
                loadedTowns.remove(sectorToReplace);
                loadedTowns.add(sectorToReplace, configuration.getMap().getMapField(i).getAllTowns(false).get(j));
            }

            configuration.getMap().getMapField(i).setAllTowns(loadedTowns);
        }
    }

    /**
     * Resets the {@link BuildingPermissionCard} lists inside the {@link Configuration} object, retrieving them all. Moreover this method adds the cards to the shown lists, making them visible
     */
    private void setBuildingPermissionCardsLists() {
        seasideCurrentList.addAll(configuration.getMap().getSeaField().getBuildingPermissionCardsDeck().getCards().stream()
                .map(card -> card.toString())
                .collect(Collectors.toList()));
        seasideCurrentList.add(configuration.getMap().getSeaField().getLeftShownPermissionCard().toString());
        seasideCurrentList.add(configuration.getMap().getSeaField().getRightShownPermissionCard().toString());
        mainlandCurrentList.addAll(configuration.getMap().getMainlandField().getBuildingPermissionCardsDeck().getCards().stream()
                .map(Object::toString)
                .collect(Collectors.toList()));
        mainlandCurrentList.add(configuration.getMap().getSeaField().getLeftShownPermissionCard().toString());
        mainlandCurrentList.add(configuration.getMap().getSeaField().getRightShownPermissionCard().toString());
        mountainsCurrentList.addAll(configuration.getMap().getMountainField().getBuildingPermissionCardsDeck().getCards().stream()
                .map(Object::toString)
                .collect(Collectors.toList()));
        mountainsCurrentList.add(configuration.getMap().getSeaField().getLeftShownPermissionCard().toString());
        mountainsCurrentList.add(configuration.getMap().getSeaField().getRightShownPermissionCard().toString());

        configuration.getMap().getAllFields().stream()
                .forEach(mapField -> mapField.getBuildingPermissionCardsDeck().getCards().stream()
                        .forEach(card -> ((BuildingPermissionCard) card).setDescription(card.toString())));
        configuration.getMap().getAllFields().stream()
                .forEach(mapField -> {
                    mapField.getLeftShownPermissionCard().setDescription(mapField.getLeftShownPermissionCard().toString());
                    mapField.getBuildingPermissionCardsDeck().addCard(mapField.getLeftShownPermissionCard());
                });
        configuration.getMap().getAllFields().stream()
                .forEach(mapField -> {
                    mapField.getRightShownPermissionCard().setDescription(mapField.getRightShownPermissionCard().toString());
                    mapField.getBuildingPermissionCardsDeck().addCard(mapField.getRightShownPermissionCard());
                });
    }

    /**
     * Loads every {@link NobilityRouteCell} making it visible through the configurator layout
     */
    private void loadNobilityRouteCells() {
        for (NobilityRouteCell nobilityRouteCell : configuration.getNobilityRoute().getCells().stream()
                .filter(cell -> cell.getBonuses().size() > 0)
                .collect(Collectors.toList())) {
            ((Button) rootAnchorPaneNobilityRoute.lookup("#nobilityRouteEditCell" + nobilityRouteCell.getIndex())).setBackground(getNobilityRouteCellButtonBackground(true));
            ((Label) rootAnchorPaneNobilityRoute.lookup("#nobilityRouteBonusesNumber" + nobilityRouteCell.getIndex())).setText(nobilityRouteCell.getBonuses().size() + " bonuses");
        }
    }

    private void initializeNewConfiguration() {
        configuration = new Configuration();
        configuration.setMap(new Map());
        for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
            configuration.getMap().setMapField(new MapField(NUMBER_OF_SECTORS_PER_FIELD), i);
            configuration.getMap().getMapField(i).setFieldType(i == Field.SEASIDE.getIndex() ? Field.SEASIDE : (i == Field.MAINLAND.getIndex() ? Field.MAINLAND : Field.MOUNTAINS));
        }
        configuration.setNobilityRoute(new NobilityRoute(NOBILITY_ROUTE_SIZE));
    }

    /**
     * Starts the {@link Town} configurator window
     *
     * @param field  The current {@link Field}
     * @param sector The current sector
     * @throws IOException
     */
    private void startTownConfiguration(Field field, int sector, boolean randomBonuses) throws IOException {
        TownConfiguratorController townConfiguratorController = new TownConfiguratorController(configuration, field, sector, randomBonuses, this);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/configurator/town_configurator.fxml"));
        loader.setController(townConfiguratorController);

        openProperConfigurator("Town configuration (Field: " + field.getDescription() + ", Sector: " + sector + ")", loader);
    }

    /**
     * Method called when a new {@link Town} has been configured or an existing one has been modified using the town configurator.
     *
     * @param townName  The name of the configured {@link Town}
     * @param townColor The {@link TownColor} of the configured {@link Town}
     * @param field     The {@link Field} containing the configured {@link Town}
     * @param sector    The sector of the {@link Field} containing the configured {@link Town}
     */
    @Override
    public void onUpdateTown(String townName, TownColor townColor, Field field, int sector) {
        String idSuffix = String.valueOf(field.getIndex()) + String.valueOf(sector);

        rootAnchorPane.lookup(("#townImagePlaceholder") + idSuffix).setVisible(false);
        ((Button) rootAnchorPane.lookup("#addTown" + idSuffix)).setText("Modify Town");
        Label name = (Label) rootAnchorPane.lookup("#townName" + idSuffix);
        name.setDisable(false);
        name.setText(townName);
        updateBuildingPermissionCards();
        rootAnchorPane.lookup(("#townImage") + idSuffix).setVisible(true);
        ((Circle) rootAnchorPane.lookup(("#townImageBackground") + idSuffix)).setFill(townColor.getColor());
        Label imageLabel = (Label) rootAnchorPane.lookup("#townImageLabel" + idSuffix);
        imageLabel.setText(townName.substring(0, 1).toUpperCase());
        if (townColor.getDescription().equals(TownColor.BLUE.getDescription()))
            imageLabel.setTextFill(Color.WHITE);
    }

    private void updateBuildingPermissionCards() {
        configuration.getMap().getAllFields().stream()
                .forEach(mapField -> {
                    ObservableList currentList = getCurrentList(mapField.getFieldType().getIndex());
                    currentList.clear();
                    mapField.getBuildingPermissionCardsDeck().getCards().stream()
                            .forEach(card -> currentList.add(card.toString()));
                });
    }

    /**
     * Method called when an existing {@link Town} is deleted
     *
     * @param field  The {@link Field} the {@link Town} has been deleted from
     * @param sector The sector the {@link Town} has been deleted from
     */
    @Override
    public void onDeleteTown(Field field, int sector) {
        String idSuffix = String.valueOf(field.getIndex()) + String.valueOf(sector);

        ((Button) rootAnchorPane.lookup("#addTown" + idSuffix)).setText("Add Town");
        Label name = (Label) rootAnchorPane.lookup("#townName" + idSuffix);
        name.setText("Town name");
        name.setDisable(true);
        rootAnchorPane.lookup("#townImage" + idSuffix).setVisible(false);
        rootAnchorPane.lookup("#townImagePlaceholder" + idSuffix).setVisible(true);
    }

    /**
     * Method called when a new {@link BuildingPermissionCard} has been configured
     *
     * @param buildingPermissionCard The configured {@link BuildingPermissionCard}
     * @param field                  The {@link Field} of the configured {@link BuildingPermissionCard}
     */
    @Override
    public void onBuildingPermissionCardConfigured(BuildingPermissionCard buildingPermissionCard, Field field) {
        ObservableList currentList = getCurrentList(field.getIndex());

        currentList.add(buildingPermissionCard.toString());
        configuration.getMap().getMapField(field.getIndex()).getBuildingPermissionCardsDeck().addCard(buildingPermissionCard);
    }

    /**
     * Method called when an existing {@link BuildingPermissionCard} has been modified
     *
     * @param buildingPermissionCard The modified {@link BuildingPermissionCard}
     * @param field                  The {@link Field} of the modified {@link BuildingPermissionCard}
     * @param selectedCardIndex      The index of the modified card in the list
     */
    @Override
    public void onBuildingPermissionCardModified(BuildingPermissionCard buildingPermissionCard, Field field, int selectedCardIndex) {
        ObservableList currentList = getCurrentList(field.getIndex());

        currentList.set(selectedCardIndex, buildingPermissionCard.toString());
        configuration.getMap().getMapField(field.getIndex()).getBuildingPermissionCardsDeck().setCard(buildingPermissionCard, selectedCardIndex);
    }

    /**
     * Method called when a new {@link NobilityRouteCell} is configured or when an existing one is modified
     *
     * @param nobilityRouteCell The configured or modified {@link NobilityRouteCell}
     */
    @Override
    public void onNobilityRouteCellEdited(NobilityRouteCell nobilityRouteCell) {
        configuration.getNobilityRoute().setCell(nobilityRouteCell, nobilityRouteCell.getIndex());
        ((Button) rootAnchorPaneNobilityRoute.lookup("#nobilityRouteEditCell" + nobilityRouteCell.getIndex())).setBackground(getNobilityRouteCellButtonBackground(true));
        ((Label) rootAnchorPaneNobilityRoute.lookup("#nobilityRouteBonusesNumber" + nobilityRouteCell.getIndex())).setText(nobilityRouteCell.getBonuses().size() + " bonuses");
    }

    /**
     * Method called when an existing {@link NobilityRouteCell} is deleted
     *
     * @param cellIndex The index of the deleted {@link NobilityRouteCell}
     */
    @Override
    public void onNobilityRouteCellDeleted(int cellIndex) {
        configuration.getNobilityRoute().removeCell(cellIndex);
        ((Button) rootAnchorPaneNobilityRoute.lookup("#nobilityRouteEditCell" + cellIndex)).setBackground(getNobilityRouteCellButtonBackground(false));
        ((Label) rootAnchorPaneNobilityRoute.lookup("#nobilityRouteBonusesNumber" + cellIndex)).setText("0 bonuses");
    }

    /**
     * Method used to get a {@link Background} object used for the {@link NobilityRoute} section buttons background. A different Background is returned depending on the isEdited boolean value.
     *
     * @param isEdited The boolean that states if the {@link NobilityRouteCell} has been edited or not (that means it's new or a deleted one)
     * @return A {@link Background} element used than to change the background parameter of the buttons
     */
    private Background getNobilityRouteCellButtonBackground(boolean isEdited) {
        java.awt.Color toUse = isEdited ? nobilityRouteCellEdited : nobilityRouteCellNotEdited;

        return new Background(new BackgroundFill(
                Color.rgb(toUse.getRed(),
                        toUse.getGreen(),
                        toUse.getBlue()), null, null));
    }

    /**
     * Method raised when the confirm configuration button is pressed. It converts, using a method defined in {@link JSONHelper} class, a {@link Configuration} object into a Json string and then
     * saves it to a file in the resources/configuration folder of the project.
     */
    @FXML
    public void confirmConfiguration() {
        int minimumNumberOfBuildingPermissionCards = configuration.getMap().getAllTowns().size() * NUMBER_OF_MINIMUM_BUILDING_PERMISSION_CARDS_FACTOR;

        try {
            configurationIsValid(minimumNumberOfBuildingPermissionCards);

            new SaveConfigurationPopup(new ISaveConfiguratorHandler() {
                @Override
                public void onSave(String configurationName) {
                    save(configurationName);
                }

                @Override
                public void onDismissal() {
                    /** Nothing to do in case of dismissal **/
                }
            }).show();
        } catch (NotEnoughBuildingPermissionCardException e) {
            new ErrorMessagePopup(NOT_ENOUGH_BUILDING_PERMISSION_CARDS_1_ERROR_MESSAGE + minimumNumberOfBuildingPermissionCards + NOT_ENOUGH_BUILDING_PERMISSION_CARDS_2_ERROR_MESSAGE).show();
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        } catch (NotEveryTownConnectedException e) {
            new ErrorMessagePopup(NOT_EVERY_TOWN_CONNECTED_ERROR_MESSAGE).show();
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        } catch (NotEnoughTownsException e) {
            new ErrorMessagePopup(NOT_ENOUGH_TOWNS_ERROR_MESSAGE).show();
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        } catch (KingTownMissingException e) {
            new ErrorMessagePopup(KING_TOWN_MISSING_ERROR_MESSAGE).show();
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        } catch (NotEveryTownHasBonusesException e) {
            new ErrorMessagePopup(NOT_EVERY_TOWN_HAS_BONUSES_ERROR_MESSAGE).show();
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        } catch (NotEnoughRandomBonusesException e) {
            new ErrorMessagePopup(NOT_ENOUGH_RANDOM_BONUSES).show();
            Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
        }
    }

    /**
     * Used to save the current configuration
     *
     * @param configurationName The chosen name
     */
    private void save(String configurationName){
        if (randomBonusesRequested) {
            configuration = randomizeBonuses(configuration, false);
            configuration.setRandomBonusRequested(randomBonusesRequested);
        }

        /** I make sure here not to leave any nobility route cell with default index of -1 **/
        for (int i = 0; i < NOBILITY_ROUTE_SIZE; i++) {
            configuration.getNobilityRoute().getCellAtIndex(i).setIndex(i);
        }

        if (!nameAlreadyInUse(configurationName)) {
            try {
                FileUtils.writeStringToFile(new File(CONFIGURATIONS_PATH + "/" + configurationName + ".json"), JSONHelper.ConfigurationParser.toJsonConfiguration(configuration));
            } catch (IOException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            } finally {
                closeConfigurator();
            }
        } else {
            new ErrorMessagePopup(CONFIGURATION_NAME_ALREADY_IN_USE_ERROR_MESSAGE).show();
        }
    }

    /**
     * Method used to check whether the {@link Configuration} is complete or not; intending for complete that:<br><br>
     * - Are at least present a minimum number of {@link BuildingPermissionCard} obtained this way: number of towns per field * NUMBER_OF_MINIMUM_BUILDING_PERMISSION_CARDS_FACTOR<br><br>
     * - No configured {@link Town} is isolated from others; there has to be at least one path from a town to every other.<br><br>
     * - One of the configured {@link Town} is the king one<br><br>
     * - At least one {@link Town} is configured per field<br><br>
     * - If the random bonus feature is being used, there must be at least a number of bonuses equal to the number of configured towns<br>
     *
     * @param minimumNumberOfBuildingPermissionCards The minimum number of {@link BuildingPermissionCard} that must be configured.
     * @throws NotEnoughBuildingPermissionCardException
     * @throws NotEnoughTownsException
     * @throws KingTownMissingException
     * @throws NotEveryTownConnectedException
     * @throws NotEveryTownHasBonusesException
     * @throws NotEnoughRandomBonusesException
     */
    private void configurationIsValid(int minimumNumberOfBuildingPermissionCards) throws NotEnoughBuildingPermissionCardException, NotEnoughTownsException, KingTownMissingException, NotEveryTownConnectedException, NotEveryTownHasBonusesException, NotEnoughRandomBonusesException {
        if (!randomBonusesRequested)
            everyTownHasAtLeastOneBonus();
        enoughTowns();
        enoughBuildingPermissionCards(minimumNumberOfBuildingPermissionCards);
        kingTownConfigured();
        isEveryTownConnected();
        if (randomBonusesRequested)
            enoughRandomBonuses();
    }

    /**
     * Method used to control if enough {@link BuildingPermissionCard} have been configured
     *
     * @param minimumNumberOfBuildingPermissionCards The minimum number of {@link BuildingPermissionCard} to be configured
     * @throws NotEnoughBuildingPermissionCardException
     */
    private void enoughBuildingPermissionCards(int minimumNumberOfBuildingPermissionCards) throws NotEnoughBuildingPermissionCardException {
        if (configuration.getMap().getAllBuildingPermissionCards().size() < minimumNumberOfBuildingPermissionCards) {
            throw new NotEnoughBuildingPermissionCardException();
        }
    }

    /**
     * Method used to control if enough {@link Town} have been configured
     *
     * @throws NotEnoughTownsException
     */
    private void enoughTowns() throws NotEnoughTownsException {
        if (configuration.getMap().getAllFields().stream().anyMatch(field -> field.getAllTowns(false).size() < 1)) {
            throw new NotEnoughTownsException();
        }
    }

    /**
     * Method used to control if the king {@link Town} has been configured
     *
     * @throws KingTownMissingException
     */
    private void kingTownConfigured() throws KingTownMissingException {
        if (configuration.getMap().getAllTowns().stream().noneMatch(Town::getHasKing)) {
            throw new KingTownMissingException();
        }
    }

    /**
     * Method used to control if every {@link Town} is connected to every other at least via one path
     *
     * @throws NotEveryTownConnectedException
     */
    private void isEveryTownConnected() throws NotEveryTownConnectedException {
        Town startingTown = configuration.getMap().getSeaField().getAllTowns(false).get(0);
        ArrayList<Town> startingList = new ArrayList<>();
        startingList.add(startingTown);

        ArrayList<Town> connectedTowns = exploreTownGraph(startingTown, startingList);

        if (connectedTowns.size() != configuration.getMap().getAllTowns().size()) {
            throw new NotEveryTownConnectedException();
        }
    }

    /**
     * Method used to explore the entire {@link Town} graph via the available connections to obtain a list of all the
     * connected towns. This list will be used by the method above to state if every town is connected to every other.
     * In fact if the list returned by this function equals the list of all configured towns the condition is undoubtedly true.
     * This is a recursive method.
     *
     * @param fromTown The starting {@link Town}
     * @param visited  The list of all visited {@link Town} so far
     * @return The same list of visited {@link Town}
     */
    private ArrayList<Town> exploreTownGraph(Town fromTown, ArrayList<Town> visited) {
        ArrayList<Town> townsToExplore = new ArrayList<>();
        townsToExplore.addAll(fromTown.getConnectedTowns());

        for (Anchor a : fromTown.getConnectedAnchors()) {
            List<Town> leftFieldTownList = a.getLeftField().getAllTowns(false).stream()
                    .filter(t -> t.getConnectedAnchors().stream()
                            .filter(anchor -> anchor.getLevel() == a.getLevel())
                            .findFirst()
                            .orElse(null) != null)
                    .collect(Collectors.toList());
            List<Town> rightFieldTownList = a.getRightField().getAllTowns(false).stream()
                    .filter(t -> t.getConnectedAnchors().stream()
                            .filter(anchor -> anchor.getLevel() == a.getLevel())
                            .findFirst()
                            .orElse(null) != null)
                    .collect(Collectors.toList());

            if (leftFieldTownList != null)
                townsToExplore.addAll(leftFieldTownList);
            if (rightFieldTownList != null)
                townsToExplore.addAll(rightFieldTownList);
        }

        townsToExplore.stream().filter(t -> !visited.contains(t)).forEach(t -> {
            visited.add(t);
            exploreTownGraph(t, visited);
        });

        return visited;
    }

    /**
     * Method used to check if every configured town (except from the king one) has at least one bonus
     *
     * @throws NotEveryTownHasBonusesException
     */
    private void everyTownHasAtLeastOneBonus() throws NotEveryTownHasBonusesException {
        if (configuration.getMap().getAllTowns().stream()
                .anyMatch(town -> town.getBonusList().size() == 0 && !town.getHasKing())) {
            throw new NotEveryTownHasBonusesException();
        }
    }

    /**
     * Method used to check if the number of random bonuses configured is at least equal to the number of configured towns
     *
     * @throws NotEnoughRandomBonusesException
     */
    private void enoughRandomBonuses() throws NotEnoughRandomBonusesException {
        if (randomBonusConfigured.size() < configuration.getMap().getAllTowns().size()) {
            throw new NotEnoughRandomBonusesException();
        }
    }

    /**
     * Method used to check if the chosen configuration name is already in use, to avoid accidental overriding
     *
     * @param fileName The chosen name
     * @return True if the name is already in use, false otherwise
     */
    private boolean nameAlreadyInUse(String fileName) {
        return Arrays.asList(PresentationBridge.availableConfigurations()).stream()
                .anyMatch(file -> file.equals(fileName + ".json"));
    }

    /**
     * Method intended to discard every {@link Configuration} done so far, close the configurator and go back to the game room settings
     */
    @FXML
    public void cancelConfiguration() {
        new ConfirmPopup(new IConfirmPopupHandler() {

            @Override
            public void onConfirmation() {
                configuration = null;
                closeConfigurator();
            }

            @Override
            public void onDismissal() {
                /** Nothing to do in case of dismissal **/
            }

        }, CANCEL_CONFIGURATION_CONFIRMATION).show();
    }

    private void closeConfigurator() {
        Stage stage = (Stage) confirmConfigurationButton.getScene().getWindow();
        stage.close();
        Client.getPresentationBridge().configuratorCallback();
    }

    /**
     * Starts the {@link BuildingPermissionCard} configurator window
     *
     * @param field The chosen {@link Field}
     * @throws IOException
     */
    private void addPermissionCard(int field) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/configurator/permission_card_configurator.fxml"));
        BuildingPermissionCard buildingPermissionCard = new BuildingPermissionCard();
        buildingPermissionCard.setDescription(NEW_CARD_DESCRIPTION);
        PermissionCardConfiguratorController permissionCardConfiguratorController = new PermissionCardConfiguratorController(buildingPermissionCard, configuration, getFieldFromIndex(field), -1, this);
        loader.setController(permissionCardConfiguratorController);
        openProperConfigurator("Permission Card configuration, " + (field == Field.SEASIDE.getIndex() ? Field.SEASIDE.getDescription() : field == Field.MAINLAND.getIndex() ? Field.MAINLAND.getDescription() : Field.MOUNTAINS.getDescription()), loader);
    }

    /**
     * Starts the {@link BuildingPermissionCard} configurator window, loading the information of the selected card
     *
     * @param field The chosen {@link Field}
     * @throws IOException
     */
    private void modifyPermissionCard(int field) throws IOException {
        ListView<Object> selectedFieldListView = (javafx.scene.control.ListView<Object>) rootAnchorPanePermissionCards.lookup("#permissionCardList" + field);
        int index = selectedFieldListView.getSelectionModel().getSelectedIndex();

        if (index >= 0) {
            String selectedCardDescription = selectedFieldListView.getSelectionModel().getSelectedItem().toString();
            Deck deck = configuration.getMap().getMapField(field).getBuildingPermissionCardsDeck();
            BuildingPermissionCard card = deck.getCardFromString(selectedCardDescription);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/configurator/permission_card_configurator.fxml"));
            PermissionCardConfiguratorController permissionCardConfiguratorController = new PermissionCardConfiguratorController(card, configuration, getFieldFromIndex(field), deck.indexOf(card), this);
            loader.setController(permissionCardConfiguratorController);
            openProperConfigurator("Permission Card configuration, " + (field == Field.SEASIDE.getIndex() ? Field.SEASIDE.getDescription() : field == Field.MAINLAND.getIndex() ? Field.MAINLAND.getDescription() : Field.MOUNTAINS.getDescription()), loader);
        } else
            new ErrorMessagePopup(NO_ELEMENT_SELECTED_TO_MODIFY_ERROR_MESSAGE).show();
    }

    /**
     * Removes the selected {@link BuildingPermissionCard} from both the list visible in the configurator window and the {@link Configuration} itself
     *
     * @param field The chosen {@link Field}
     */
    private void removePermissionCard(int field) {
        ListView<Object> selectedFieldListView = (javafx.scene.control.ListView<Object>) rootAnchorPanePermissionCards.lookup("#permissionCardList" + field);
        int index = selectedFieldListView.getSelectionModel().getSelectedIndex();

        if (index >= 0) {
            ObservableList currentList = getCurrentList(field);
            currentList.remove(index);

            configuration.getMap().getMapField(field).getBuildingPermissionCardsDeck().removeCardAtIndex(index);

            if (currentList.size() == 0) disableCurrentButtons(field);
        } else
            new ErrorMessagePopup(NO_ELEMENT_SELECTED_TO_DELETE_ERROR_MESSAGE).show();
    }

    /**
     * Returns the {@link ObservableList} of the selected {@link Field}
     *
     * @param field The selected {@link Field}
     * @return The {@link ObservableList} of the {@link Field} passed as a parameter
     */
    private ObservableList getCurrentList(int field) {
        return field == Field.SEASIDE.getIndex() ? seasideCurrentList : field == Field.MAINLAND.getIndex() ? mainlandCurrentList : mountainsCurrentList;
    }

    /**
     * Method used to disable the correct buttons, verifying which list is empty
     *
     * @param field The field of the list to check
     */
    private void disableCurrentButtons(int field) {
        if (field == Field.SEASIDE.getIndex()) {
            modifyPermissionCard0.setDisable(true);
            deletePermissionCard0.setDisable(true);
        } else if (field == Field.MAINLAND.getIndex()) {
            modifyPermissionCard1.setDisable(true);
            deletePermissionCard1.setDisable(true);
        } else {
            modifyPermissionCard2.setDisable(true);
            deletePermissionCard2.setDisable(true);
        }
    }

    private Field getFieldFromIndex(int index) {
        return index == Field.SEASIDE.getIndex() ? Field.SEASIDE : index == Field.MAINLAND.getIndex() ? Field.MAINLAND : Field.MOUNTAINS;
    }

    @FXML
    public void seasideCardSelected() {
        if (permissionCardList0.getSelectionModel().getSelectedItems().size() > 0) {
            modifyPermissionCard0.setDisable(false);
            deletePermissionCard0.setDisable(false);
        }
    }

    @FXML
    public void mainlandCardSelected() {
        if (permissionCardList1.getSelectionModel().getSelectedItems().size() > 0) {
            modifyPermissionCard1.setDisable(false);
            deletePermissionCard1.setDisable(false);
        }
    }

    @FXML
    public void mountainCardSelected() {
        if (permissionCardList2.getSelectionModel().getSelectedItems().size() > 0) {
            modifyPermissionCard2.setDisable(false);
            deletePermissionCard2.setDisable(false);
        }
    }

    /**
     * Starts the {@link NobilityRouteCell} configurator window
     *
     * @param cellIndex The index of the {@link NobilityRouteCell} to edit
     * @throws IOException
     */
    private void editNobilityRouteCell(int cellIndex) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/configurator/nobility_route_configurator.fxml"));

        NobilityRouteCell chosenCell = configuration.getNobilityRoute().getCellAtIndex(cellIndex);
        NobilityRouteConfiguratorController nobilityRouteConfiguratorController = new NobilityRouteConfiguratorController(chosenCell, cellIndex, this);

        loader.setController(nobilityRouteConfiguratorController);
        openProperConfigurator("Cell " + (cellIndex + 1), loader);
    }

    /**
     * Method called when the random bonuses check is checked or unchecked; used to execute all the necessary operations to use the random bonus feature.
     */
    @FXML
    public void randomBonusesRequested() {
        if (!randomBonusesRequested) {
            randomBonusesRequested = true;
            randomBonusesPanel.setDisable(false);
            removeBonus.setDisable(true);

            if (configuration.getMap().getAllTowns().stream()
                    .anyMatch(town -> town.getBonusList().size() > 0)) {
                new ConfirmPopup(new IConfirmPopupHandler() {
                    @Override
                    public void onConfirmation() {
                        importAlreadyConfiguredBonuses();
                    }

                    @Override
                    public void onDismissal() {
                        /** Nothing to do in case of dismissal **/
                    }
                }, IMPORT_ALREADY_CONFIGURED_BONUSES_CONFIRMATION, "Yes", "No").show();
            }
        } else {
            randomBonusesRequested = false;
            randomBonusesPanel.setDisable(true);
        }
    }

    @FXML
    public void bonusSelected() {
        if (randomBonusListview.getSelectionModel().getSelectedItems().size() > 0) {
            removeBonus.setDisable(false);
        }
    }

    /**
     * Method used to import in the random bonus list being initialized every bonus already configured on single towns. Then every bonus in deleted from towns.
     */
    private void importAlreadyConfiguredBonuses() {
        List<DirectBonus> allBonuses = new ArrayList<>();

        configuration.getMap().getAllTowns().stream()
                .filter(town -> town.getBonusList().size() > 0)
                .forEach(town -> allBonuses.addAll(town.getBonusList()));

        randomBonusConfigured.addAll(allBonuses);

        configuration.getMap().getAllTowns().stream()
                .filter(town -> town.getBonusList().size() > 0)
                .forEach(Town::clearBonusList);

        randomBonusList.addAll(allBonuses.stream().map(DirectBonus::toString).collect(Collectors.toList()));
    }

    /**
     * Method used to add a bonus to the list of the random bonuses
     */
    @FXML
    public void addBonus() {
        if (quantityRandomBonus.getText().isEmpty() || !new StringFormatChecker().digitsOnly(quantityRandomBonus.getText())) { /** If the quantity of the bonus is not specified or its format is wrong I don't add it **/
            new ErrorMessagePopup(WRONG_BONUS_QUANTITY_FORMAT_ERROR_MESSAGE).show();
        } else {
            /** I update the bonus list, using a lambda and a stream to find the correct bonus to pass to DirectBonus class constructor **/
            Stream<BonusType> bonusTypeStream = Arrays.stream(BonusType.values());
            DirectBonus directBonus = new DirectBonus(bonusTypeStream
                    .filter(bonus -> bonus.getDescription().equals(randomBonusComboBox.getValue()))
                    .findFirst()
                    .orElse(null), Integer.parseInt(quantityRandomBonus.getText()));

            randomBonusConfigured.add(directBonus);
            bonusTypeStream.close();
            randomBonusList.add(directBonus.toString());
        }
    }

    /**
     * Method intended to remove a bonus from the list of the random bonuses
     */
    @FXML
    public void removeBonus() {
        int indexOfElementToRemove = randomBonusListview.getSelectionModel().getSelectedIndex();
        if (indexOfElementToRemove >= 0) {
            /** Deleting an element from ObservableList will automatically be deleted from ListView **/
            randomBonusList.remove(indexOfElementToRemove);

            /** I finally remove the bonus also from the card being configured **/
            randomBonusConfigured.remove(indexOfElementToRemove);
        } else {
            new ErrorMessagePopup(NO_ELEMENT_SELECTED_TO_DELETE_ERROR_MESSAGE).show();
        }

        if (randomBonusList.size() == 0) removeBonus.setDisable(true);
    }

    /**
     * This static method is used both to randomize the bonuses configured in the global list of the configurator and to shuffle the bonuses of every configured town of a saved {@link Configuration}.
     * This feature is important because this way it's possible to have dynamic configurations that change town bonuses every time a new game is initialized.
     *
     * @param configuration           The {@link Configuration} to modify
     * @param shuffleContainedBonuses A boolean stating if the passed {@link Configuration} is a new one with a global list of bonus to shuffle or it's a preexisting one being shuffled
     * @return A new shuffled {@link Configuration}, with everything else a part from town bonuses untouched
     */
    public static Configuration randomizeBonuses(Configuration configuration, boolean shuffleContainedBonuses) {
        List<DirectBonus> bonuses;

        bonuses = shuffleContainedBonuses ? getBonusesFromTowns(configuration) : randomBonusConfigured;

        configuration.getMap().getAllTowns().stream()
                .filter(town -> town.getBonusList().size() > 0)
                .forEach(Town::clearBonusList);

        /** To start I put one random bonus in each town, so that every one has at least one **/
        configuration.getMap().getAllTowns().stream()
                .filter(town -> !town.getHasKing())
                .forEach(town -> {
                    DirectBonus randomBonus = bonuses.get(new Random().nextInt(bonuses.size()));
                    town.addBonus(randomBonus);
                    bonuses.remove(randomBonus);
                });

        /** Now I put the remaining bonuses, if any, into random towns **/
        int remainingBonuses = bonuses.size();
        for (int i = 0; i < remainingBonuses; i++) {
            List<MapField> availableMapFields = configuration.getMap().getAllFields().stream()
                    .filter(mapField -> mapField.getAllTowns(false).size() > 1 || (mapField.getAllTowns(false).size() == 1 && mapField.getAllTowns(false).stream().allMatch(town -> !town.getHasKing())))
                    .collect(Collectors.toList());
            MapField randomMapField = availableMapFields.get(new Random().nextInt(availableMapFields.size()));
            DirectBonus randomBonus = bonuses.get(new Random().nextInt(bonuses.size()));
            randomMapField.getTownInSector(getRandomTownSector(randomMapField)).addBonus(randomBonus);
            bonuses.remove(randomBonus);
        }
        return configuration;
    }

    /**
     * Method used to get the sector of a random {@link Town} in which to put the random bonus being processed. The selected {@link Town} must not be the king one, since it cannot have bonuses;
     * for this reason the list is filtered before proceeding.
     *
     * @param mapField The {@link MapField} to select the random {@link Town} from
     * @return The sector of the selected random {@link Town}
     */
    private static int getRandomTownSector(MapField mapField) {
        List<Town> availableTowns = mapField.getAllTowns(false).stream()
                .filter(town -> !town.getHasKing())
                .collect(Collectors.toList());

        Town randomTown = availableTowns.get(new Random().nextInt(availableTowns.size()));
        return randomTown.getSector();
    }

    /**
     * Method used to get the list of every bonus of every {@link Town}. This method is used when shuffling an existing {@link Configuration} flagged as random; this way the otherwise fixed bonuses are
     * shuffled every time a new game room is configured, even using the same {@link Configuration}.
     *
     * @param configuration The {@link Configuration} containing the bonuses to shuffle
     * @return The list of all bonuses
     */
    private static List<DirectBonus> getBonusesFromTowns(Configuration configuration) {
        List<DirectBonus> toReturn = new ArrayList<>();

        configuration.getMap().getAllTowns().stream()
                .forEach(town -> toReturn.addAll(town.getBonusList()));

        return toReturn;
    }

    /**
     * Method used to actually open and show every configurator window, initialized each one with a specific method
     *
     * @param windowTitle The title of the window to show
     * @param loader      The loader, initialized with a specific method for every different configurator window
     * @throws IOException
     */
    private void openProperConfigurator(String windowTitle, FXMLLoader loader) throws IOException {
        Parent root = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle(windowTitle);
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    /**
     * Sets up the listeners for the buttons of the {@link Town} configurator tab and the {@link BuildingPermissionCard} configurator tab
     */
    private void setupButtonListeners() {
        for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
            final int field = i;
            for (int k = 0; k < NUMBER_OF_SECTORS_PER_FIELD; k++) {
                final int sector = k;
                ((Button) rootAnchorPane.lookup("#addTown" + field + sector)).setOnAction(event -> {
                    try {
                        startTownConfiguration(field == Field.SEASIDE.getIndex() ? Field.SEASIDE : (field == Field.MAINLAND.getIndex() ? Field.MAINLAND : Field.MOUNTAINS), sector, randomBonusesRequested);
                    } catch (IOException e) {
                        Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                    }
                });
            }

            ((Button) rootAnchorPanePermissionCards.lookup("#addPermissionCard" + field)).setOnAction(event -> {
                try {
                    addPermissionCard(field);
                } catch (IOException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            });
            ((Button) rootAnchorPanePermissionCards.lookup("#modifyPermissionCard" + field)).setOnAction(event -> {
                try {
                    modifyPermissionCard(field);
                } catch (IOException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            });
            ((Button) rootAnchorPanePermissionCards.lookup("#deletePermissionCard" + field)).setOnAction(event ->
                    removePermissionCard(field));
        }
    }

    /**
     * Sets up the listeners for the buttons of the {@link NobilityRoute} configurator tab
     */
    private void setupNobilityRouteButtonsListeners() {
        for (int i = 0; i < NOBILITY_ROUTE_SIZE; i++) {
            final int cellIndex = i;
            Button nobilityRouteCellButton = (Button) rootAnchorPaneNobilityRoute.lookup("#nobilityRouteEditCell" + cellIndex);
            nobilityRouteCellButton.setBackground(getNobilityRouteCellButtonBackground(false));
            nobilityRouteCellButton.setOnAction(event -> {
                try {
                    editNobilityRouteCell(cellIndex);
                } catch (IOException e) {
                    Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
                }
            });
        }
    }

    /**
     * The initialization method, used to initialize every visible element of the main configurator window
     */
    public void initialize() {
        randomBonusesRequested = false;

        /** Setup of the three listview of the permission card tab **/
        permissionCardList0.setItems(seasideCurrentList);
        permissionCardList1.setItems(mainlandCurrentList);
        permissionCardList2.setItems(mountainsCurrentList);

        /** Setup of the random bonus ListView **/
        randomBonusListview.setItems(randomBonusList);

        /** Setup of the random bonus combo box **/
        randomBonusComboBox.setItems(randomBonusTypeList);
        randomBonusComboBox.setValue(BonusType.HELPERS.getDescription());

        /** I initialize the bonus observable list, avoiding the indirect bonuses, which are not allowed in towns **/
        for (BonusType bonus : BonusType.values()) {
            if (!bonus.getDescription().equals(BonusType.TOWNBONUSES.getDescription()) && !bonus.getDescription().equals(BonusType.PERMISSIONCARDBONUSES.getDescription())) {
                randomBonusTypeList.add(bonus.getDescription());
            }
        }

        /**Button listeners setup **/
        setupButtonListeners();
        setupNobilityRouteButtonsListeners();

        if (isLoaded) {
            loadAllTowns();
            loadNobilityRouteCells();
        }
    }
}
