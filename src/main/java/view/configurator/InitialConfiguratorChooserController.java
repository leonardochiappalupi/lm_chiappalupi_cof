package view.configurator;

import client.presentation.PresentationBridge;
import game.Configuration;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import util.ErrorMessagePopup;
import util.JSONElementNotFoundException;
import util.JSONHelper;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import static constants.ConfigurationConstants.NO_CONFIGURATION_TO_LOAD_SELECTED_ERROR_MESSAGE;

class InitialConfiguratorChooserController {
    private boolean load;
    private IInitialChooserHandler iInitialChooserHandler;

    @FXML
    private VBox rootVbox;
    @FXML
    private ComboBox chooseConfigurationComboBox;
    @FXML
    private Button loadConfiguration;
    @FXML
    private Button newConfiguration;

    InitialConfiguratorChooserController(IInitialChooserHandler iInitialChooserHandler) {
        this.iInitialChooserHandler = iInitialChooserHandler;
    }

    private void dismissPopup() {
        Stage stage = (Stage) rootVbox.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void checkLoadConfiguration() {
        loadConfiguration.setDisable(load);
        chooseConfigurationComboBox.setDisable(load);
        chooseConfigurationComboBox.setItems(FXCollections.observableList(Arrays.asList(PresentationBridge.availableConfigurations())));
        load = !load;
        newConfiguration.setDisable(load);
    }

    @FXML
    public void newConfiguration() {
        dismissPopup();
        iInitialChooserHandler.onNewConfiguration();
    }

    @FXML
    public void loadConfiguration() {
        if (!"< Select the configuration to load >".equals(chooseConfigurationComboBox.getValue())) {
            String jsonConfiguration = PresentationBridge.getRequestedJsonConfiguration((String) chooseConfigurationComboBox.getValue());

            Configuration configuration = null;
            try {
                configuration = JSONHelper.ConfigurationParser.configurationFromJSonConfig(jsonConfiguration);
            } catch (JSONElementNotFoundException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }

            dismissPopup();
            iInitialChooserHandler.onLoadConfiguration(configuration);
        } else {
            new ErrorMessagePopup(NO_CONFIGURATION_TO_LOAD_SELECTED_ERROR_MESSAGE).show();
        }
    }

    public void initialize() {
        load = false;
    }
}
