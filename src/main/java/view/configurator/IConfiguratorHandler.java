package view.configurator;

import game.board.Field;
import game.board.NobilityRouteCell;
import game.board.TownColor;
import game.board.Town;
import game.board.NobilityRoute;
import game.cards.BuildingPermissionCard;

/**
 * This interface is used to communicate between the main configurator window and the others configurator panels used to configure: {@link Town}, {@link BuildingPermissionCard} and {@link NobilityRoute}.
 */
interface IConfiguratorHandler {

    void onUpdateTown(String townName, TownColor townColor, Field field, int sector);

    void onDeleteTown(Field field, int sector);

    void onBuildingPermissionCardConfigured(BuildingPermissionCard buildingPermissionCard, Field field);

    void onBuildingPermissionCardModified(BuildingPermissionCard buildingPermissionCard, Field field, int selectedCardIndex);

    void onNobilityRouteCellEdited(NobilityRouteCell nobilityRouteCell);

    void onNobilityRouteCellDeleted(int cellIndex);
}
