package view.configurator;

interface ISaveConfiguratorHandler {
    void onSave(String configurationName);

    void onDismissal();
}
