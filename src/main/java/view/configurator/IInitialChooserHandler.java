package view.configurator;

import game.Configuration;

public interface IInitialChooserHandler {

    void onNewConfiguration();

    void onLoadConfiguration(Configuration configuration);
}
