package view.configurator;

import game.Configuration;
import game.board.Field;
import game.board.Town;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.cards.BuildingPermissionCard;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.ErrorMessagePopup;
import util.StringFormatChecker;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.ConfigurationConstants.*;

/**
 * This class fully controls the {@link BuildingPermissionCard} configurator panel, called by the main window when a new {@link BuildingPermissionCard} is created or an existing one is modified
 */
class PermissionCardConfiguratorController implements Initializable{
    private IConfiguratorHandler configuratorHandler;
    private BuildingPermissionCard buildingPermissionCard;
    private Configuration configuration;
    private ArrayList<Town> configuredTowns = new ArrayList<>();
    private Field field;
    private int selectedCardIndex;
    private boolean configuratingNewCard;

    @FXML
    private ListView<String> allowedTownList;
    @FXML
    private ListView<String> bonusesList;
    @FXML
    private ComboBox<String> allowedTownComboBox;
    @FXML
    private ComboBox<String> bonusTypeComboBox;
    @FXML
    private TextField bonusQuantity;
    @FXML
    private Button confirm;
    @FXML
    private Button removeTown;
    @FXML
    private Button removeBonus;

    private final ObservableList allowedTownsCurrentList = FXCollections.observableArrayList();
    private final ObservableList<String> allowedTownsList = FXCollections.observableArrayList();

    private final ObservableList bonusCurrentList = FXCollections.observableArrayList();
    private final ObservableList<String> bonusTypeList = FXCollections.observableArrayList();

    PermissionCardConfiguratorController(BuildingPermissionCard buildingPermissionCard, Configuration configuration, Field field, int selectedCardIndex, IConfiguratorHandler configuratorHandler) {
        this.configuratorHandler = configuratorHandler;
        this.buildingPermissionCard = buildingPermissionCard;
        this.configuration = configuration;
        this.field = field;
        this.selectedCardIndex = selectedCardIndex;
    }

    /**
     * Method intended to confirm the configuration and close this panel, if everything is correctly set up
     */
    @FXML
    public void confirm() {
        if (allowedTownsCurrentList.size() > 0 && bonusCurrentList.size() > 0) {
            buildingPermissionCard.setDescription(createPermissionCardDescription());

            closeBuildingPermissionCardConfigurator();
            if (configuratingNewCard) {
                configuratorHandler.onBuildingPermissionCardConfigured(buildingPermissionCard, field);
            } else {
                configuratorHandler.onBuildingPermissionCardModified(buildingPermissionCard, field, selectedCardIndex);
            }
        } else {
            new ErrorMessagePopup(BUILDING_PERMISSION_CARD_MISSING_ATTRIBUTES_ERROR_MESSAGE).show();
        }
    }

    /**
     * Method intended to close this configurator panel
     */
    private void closeBuildingPermissionCardConfigurator() {
        Stage stage = (Stage) confirm.getScene().getWindow();
        stage.close();
    }

    /**
     * Method used to build the string to set as a description of the {@link BuildingPermissionCard} being configured, and then shown in the list representing all the configured cards
     * (in the main configurator window; third tab)
     *
     * @return A formatted string representing the {@link Town}s which the card allow you to build into
     */
    private String createPermissionCardDescription() {
        String description = "Allowed towns: ";

        for (int i = 0; i < buildingPermissionCard.getAllowedTowns().size(); i++) {
            description += buildingPermissionCard.getAllowedTowns().get(i).getName() + ", ";
        }
        description = description.substring(0, description.length() - 2);
        return description;
    }

    /**
     * Method used to add a {@link Town} to the list of the allowed ones of the card currently being configured
     */
    @FXML
    public void addTown() {
        if (allowedTownsList.size() != 0) {
            if (!townAlreadyAdded()) {
                /** Adding a new element to ObservableList will automatically be reflected in ListView **/
                allowedTownsCurrentList.add(allowedTownComboBox.getValue());

                /** I finally update the allowed town list in the card being configured **/
                buildingPermissionCard.addAllowedTown(configuredTowns.stream()
                        .filter(townToCheck -> townToCheck.getName().equals(allowedTownComboBox.getValue()))
                        .findFirst()
                        .orElse(null));
            }
        } else
            new ErrorMessagePopup(NO_TOWN_TO_ADD_ERROR_MESSAGE).show();
    }

    /**
     * Method intended to check if the {@link Town} going to be added is already present in the list of allowed towns of the card, to avoid adding copies.
     *
     * @return True if the {@link Town} going to be added is already present in the list, false otherwise
     */
    private boolean townAlreadyAdded() {
        return buildingPermissionCard.getAllowedTowns().stream()
                .map(Town::getName)
                .collect(Collectors.toList()).contains(allowedTownComboBox.getValue());
    }

    /**
     * Method intended to remove a {@link Town} fhe list of the allowed ones of the card currently being configured
     */
    @FXML
    public void removeTown() {
        /** Deleting an element from ObservableList will automatically be deleted from ListView **/
        int indexOfElementToRemove = allowedTownList.getSelectionModel().getSelectedIndex();
        if (indexOfElementToRemove >= 0) {
            buildingPermissionCard.removeAllowedTown(indexOfElementToRemove);
            allowedTownsCurrentList.remove(indexOfElementToRemove);
        } else {
            new ErrorMessagePopup(NO_ELEMENT_SELECTED_TO_DELETE_ERROR_MESSAGE).show();
        }

        if (allowedTownsCurrentList.size() == 0) removeTown.setDisable(true);
    }

    /**
     * Method used to add a bonus to the list of the bonuses of the card currently being configured
     */
    @FXML
    public void addBonus() {
        if (bonusQuantity.getText().isEmpty() || !new StringFormatChecker().digitsOnly(bonusQuantity.getText())) { /** If the quantity of the bonus is not specified or its format is wrong I don't add it **/
            new ErrorMessagePopup(WRONG_BONUS_QUANTITY_FORMAT_ERROR_MESSAGE).show();
        } else {
            /** I update the bonus list in the card being configured, using a lambda and a stream to find the correct bonus to pass to DirectBonus class constructor **/
            Stream<BonusType> bonusTypeStream = Arrays.stream(BonusType.values());
            DirectBonus directBonus = new DirectBonus(bonusTypeStream
                    .filter(bonus -> bonus.getDescription().equals(bonusTypeComboBox.getValue()))
                    .findFirst()
                    .orElse(null), Integer.parseInt(bonusQuantity.getText()));
            if (!bonusAlreadyAdded(directBonus)) {
                buildingPermissionCard.addBonus(directBonus);
                bonusCurrentList.add(directBonus.toString());
            }
            bonusTypeStream.close();
        }
    }

    /**
     * Method intended to check if the {@link DirectBonus} going to be added is already present in the list of bonuses of the card, to avoid adding copies.
     *
     * @return True if the {@link DirectBonus} going to be added is already present in the list, false otherwise
     */
    private boolean bonusAlreadyAdded(DirectBonus directBonus) {
        return buildingPermissionCard.getBonusList().stream()
                .map(DirectBonus::toString)
                .collect(Collectors.toList()).contains(directBonus.toString());
    }

    /**
     * Method intended to remove a bonus from the list of the bonuses of the card currently being configured
     */
    @FXML
    public void removeBonus() {
        int indexOfElementToRemove = bonusesList.getSelectionModel().getSelectedIndex();
        if (indexOfElementToRemove >= 0) {
            /** Deleting an element from ObservableList will automatically be deleted from ListView **/
            bonusCurrentList.remove(indexOfElementToRemove);

            /** I finally remove the bonus also from the card being configured **/
            buildingPermissionCard.removeBonus(indexOfElementToRemove);
        } else {
            new ErrorMessagePopup(NO_ELEMENT_SELECTED_TO_DELETE_ERROR_MESSAGE).show();
        }

        if (bonusCurrentList.size() == 0) removeBonus.setDisable(true);
    }

    @FXML
    public void bonusSelected() {
        if (bonusesList.getSelectionModel().getSelectedItems().size() > 0) {
            removeBonus.setDisable(false);
        }
    }

    @FXML
    public void townSelected() {
        if (allowedTownList.getSelectionModel().getSelectedItems().size() > 0) {
            removeTown.setDisable(false);
        }
    }

    /**
     * Method intended to setup every element of this configurator panel; it is raised only when needed so that the layout is properly setup only when an old card is modified,
     * leaving it blank instead if a new one is created
     */
    private void setupAll() {
        allowedTownsCurrentList.addAll(buildingPermissionCard.getAllowedTowns().stream()
                .map(Town::getName)
                .collect(Collectors.toList()));

        bonusCurrentList.addAll(buildingPermissionCard.getBonusList().stream().map(DirectBonus::toString).collect(Collectors.toList()));
    }

    /**
     * Method used to initialize every element needed by this configurator panel
     */
    public void initialize() {
        /** Setup of the two listview of the window **/
        allowedTownList.setItems(allowedTownsCurrentList);
        bonusesList.setItems(bonusCurrentList);

        allowedTownComboBox.setItems(allowedTownsList);

        bonusTypeComboBox.setItems(bonusTypeList);
        bonusTypeComboBox.setValue(BonusType.HELPERS.getDescription());

        /** I initialize the bonus observable list, avoiding the indirect bonuses, which are not allowed in the permission cards **/
        for (BonusType bonus : BonusType.values()) {
            if (!bonus.getDescription().equals(BonusType.TOWNBONUSES.getDescription()) && !bonus.getDescription().equals(BonusType.PERMISSIONCARDBONUSES.getDescription())) {
                bonusTypeList.add(bonus.getDescription());
            }
        }

        /** Initialization of the allowed towns observable list for the combobox **/
        for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
            for (int j = 0; j < NUMBER_OF_SECTORS_PER_FIELD; j++) {
                Town t = configuration.getMap().getMapField(i).getTownInSector(j);
                if (t != null) {
                    allowedTownsList.add(t.getName());
                    configuredTowns.add(t);
                }
            }
        }
        /** I set the default value of the allowed towns observable list **/
        if (allowedTownsList.size() > 0) {
            allowedTownComboBox.setValue(allowedTownsList.get(0));
        }

        if (!buildingPermissionCard.getDescription().equals(NEW_CARD_DESCRIPTION)) {
            configuratingNewCard = false;
            setupAll();
        } else {
            configuratingNewCard = true;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initialize();
    }
}


