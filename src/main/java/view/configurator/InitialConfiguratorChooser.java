package view.configurator;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InitialConfiguratorChooser {
    private IInitialChooserHandler iInitialChooserHandler;

    public InitialConfiguratorChooser(IInitialChooserHandler iInitialChooserHandler){
        this.iInitialChooserHandler = iInitialChooserHandler;
    }

    public void show() {
        InitialConfiguratorChooserController initialConfiguratorChooserController = new InitialConfiguratorChooserController(iInitialChooserHandler);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/configurator/initial_configurator_chooser.fxml"));
        loader.setController(initialConfiguratorChooserController);
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        Stage primaryStage = new Stage();
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle("Choose");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }
}
