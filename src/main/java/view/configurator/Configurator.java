package view.configurator;

import game.Configuration;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configurator extends Application {
    private static Configuration configuration;

    /**
     * Loads the FXML scene for the configurator and displays it
     *
     * @throws IOException
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        ConfiguratorController configuratorController = new ConfiguratorController(configuration);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/configurator/configurator.fxml"));
        loader.setController(configuratorController);
        Parent root = loader.load();
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle("Game configuration");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    public static void startGameConfiguration() {
        configuration = null;
        buildConfigurator();
    }

    public static void loadGameConfiguration(Configuration configurationToLoad){
        configuration = configurationToLoad;
        buildConfigurator();
    }

    private static void buildConfigurator(){
        try {
            new Configurator().start(new Stage());
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
