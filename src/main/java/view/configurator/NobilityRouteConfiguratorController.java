package view.configurator;

import game.board.NobilityRouteCell;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.bonus.IBonus;
import game.bonus.IndirectBonus;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.ErrorMessagePopup;
import util.StringFormatChecker;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.ConfigurationConstants.NO_ELEMENT_SELECTED_TO_DELETE_ERROR_MESSAGE;
import static constants.ConfigurationConstants.WRONG_BONUS_QUANTITY_FORMAT_ERROR_MESSAGE;

/**
 * This class controls the single {@link NobilityRouteCell} configuration panel
 */
class NobilityRouteConfiguratorController implements Initializable{
    private IConfiguratorHandler configuratorHandler;
    private NobilityRouteCell nobilityRouteCell;
    private int cellIndex;

    @FXML
    private ListView<String> bonusesList;
    @FXML
    private ComboBox<String> bonusTypeComboBox;
    @FXML
    private TextField bonusQuantity;
    @FXML
    private Button confirm;
    @FXML
    private Button removeBonus;

    private final ObservableList bonusCurrentList = FXCollections.observableArrayList();
    private final ObservableList<String> bonusTypeList = FXCollections.observableArrayList();


    NobilityRouteConfiguratorController(NobilityRouteCell nobilityRouteCell, int cellIndex, IConfiguratorHandler configuratorHandler) {
        this.configuratorHandler = configuratorHandler;
        this.nobilityRouteCell = nobilityRouteCell;
        this.cellIndex = cellIndex;
    }

    /**
     * Closes this configurator panel and calls the proper method in the {@link IConfiguratorHandler} interface
     */
    @FXML
    public void confirm() {
        closeNobilityRouteConfigurator();
        configuratorHandler.onNobilityRouteCellEdited(nobilityRouteCell);
    }

    /**
     * Closes this configurator panel and calls the proper method in the {@link IConfiguratorHandler} interface
     */
    @FXML
    public void delete() {
        closeNobilityRouteConfigurator();
        configuratorHandler.onNobilityRouteCellDeleted(cellIndex);
    }

    /**
     * Simply closes this configurator panel
     */
    private void closeNobilityRouteConfigurator() {
        Stage stage = (Stage) confirm.getScene().getWindow();
        stage.close();
    }

    /**
     * Add the configured bonus to the cell being configured
     */
    @FXML
    public void addBonus() {
        if (bonusQuantity.getText().isEmpty() || !new StringFormatChecker().digitsOnly(bonusQuantity.getText())) { /** If the quantity of the bonus is not specified or its format is wrong I don't add it **/
            new ErrorMessagePopup(WRONG_BONUS_QUANTITY_FORMAT_ERROR_MESSAGE).show();
        } else {
            Stream<BonusType> bonusTypeStream = Arrays.stream(BonusType.values());

            if (isDirectBonus(bonusTypeComboBox.getValue())) {
                DirectBonus directBonus = new DirectBonus(bonusTypeStream
                        .filter(bonus -> bonus.getDescription().equals(bonusTypeComboBox.getValue()))
                        .findFirst()
                        .orElse(null), Integer.parseInt(bonusQuantity.getText()));

                if (!bonusAlreadyAdded(directBonus)) {
                    nobilityRouteCell.addBonus(directBonus);
                    bonusCurrentList.add(directBonus.toString());
                }
            } else {
                IndirectBonus indirectBonus = new IndirectBonus(bonusTypeStream
                        .filter(bonus -> bonus.getDescription().equals(bonusTypeComboBox.getValue()))
                        .findFirst()
                        .orElse(null));

                if (!bonusAlreadyAdded(indirectBonus)) {
                    nobilityRouteCell.addBonus(indirectBonus);
                    bonusCurrentList.add(indirectBonus.toString());
                }
            }

            bonusTypeStream.close();
        }
    }

    /**
     * Method intended to check if the {@link game.bonus.IBonus} going to be added is already present in the list of bonuses of the cell, to avoid adding copies.
     *
     * @return True if the {@link game.bonus.IBonus} going to be added is already present in the list, false otherwise
     */
    private boolean bonusAlreadyAdded(IBonus iBonus) {
        return nobilityRouteCell.getBonuses().stream()
                .map(IBonus::toString)
                .collect(Collectors.toList()).contains(iBonus.toString());
    }

    /**
     * Method used to establish whether the bonus is direct or not passing its description
     *
     * @param bonusDescription The description of the bonus
     * @return True if the bonus is direct, false otherwise
     */
    private boolean isDirectBonus(String bonusDescription) {
        return !(bonusDescription.equals(BonusType.TOWNBONUSES.getDescription()) || bonusDescription.equals(BonusType.PERMISSIONCARDBONUSES.getDescription()));
    }

    /**
     * Removes the selected bonus from the cell being configured
     */
    @FXML
    public void removeBonus() {
        int indexOfElementToRemove = bonusesList.getSelectionModel().getSelectedIndex();
        if (indexOfElementToRemove >= 0) {
            bonusCurrentList.remove(indexOfElementToRemove);

            nobilityRouteCell.removeBonus(indexOfElementToRemove);
        } else {
            new ErrorMessagePopup(NO_ELEMENT_SELECTED_TO_DELETE_ERROR_MESSAGE).show();
        }

        if (bonusCurrentList.size() == 0) removeBonus.setDisable(true);
    }

    /**
     * Method used to disable or enable the {@link TextField} containing the quantity. Due to the fact that {@link IndirectBonus}es don't have a quantity attribute if in the {@link ComboBox} one
     * of them is selected the {@link TextField} must be disabled to prevent the user to insert a value.<br>
     * It is re-enabled when a {@link DirectBonus} is selected.
     */
    @FXML
    public void bonusChanged() {
        bonusQuantity.setText("0");
        bonusQuantity.setDisable(!isDirectBonus(bonusTypeComboBox.getValue()));

        if (isDirectBonus(bonusTypeComboBox.getValue()))
            bonusQuantity.clear();
    }

    @FXML
    public void bonusSelected() {
        if (bonusesList.getSelectionModel().getSelectedItems().size() > 0) {
            removeBonus.setDisable(false);
        }
    }

    /**
     * Method used to setup the bonus list visible to the user, picking already configured bonuses from the selected {@link NobilityRouteCell}, if any is present.
     */
    private void setupAll() {
        bonusCurrentList.addAll(nobilityRouteCell.getBonuses().stream().map(Object::toString).collect(Collectors.toList()));
    }

    /**
     * The initialization method, intended to execute all the operations needed to setup the configurator panel just loaded.
     */
    public void initialize() {
        /** Setup of the two listview of the window **/
        bonusesList.setItems(bonusCurrentList);

        bonusTypeComboBox.setItems(bonusTypeList);
        bonusTypeComboBox.setValue(BonusType.HELPERS.getDescription());

        /** I initialize the bonus observable list **/
        for (BonusType bonus : BonusType.values()) {
            bonusTypeList.add(bonus.getDescription());
        }

        if (nobilityRouteCell.getIndex() != -1) {
            setupAll();
        } else {
            nobilityRouteCell.setIndex(cellIndex);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initialize();
    }
}


