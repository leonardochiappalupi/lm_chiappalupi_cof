package view.configurator;

import game.Configuration;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.cards.BuildingPermissionCard;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import util.ErrorMessagePopup;
import util.StringFormatChecker;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.ConfigurationConstants.*;

/**
 * This class fully controls the {@link Town} configurator panel, called by the main window when a new {@link Town} is created or an existing one is modified
 */
class TownConfiguratorController implements Initializable{
    private IConfiguratorHandler configuratorHandler;
    private Configuration configuration;
    private Field field;
    private Integer sector;
    private Town town;
    private boolean randomBonuses;

    private String[] firstAnchorsSet = {ANCHOR0, ANCHOR1, ANCHOR2};
    private String[] secondAnchorsSet = {ANCHOR3, ANCHOR4, ANCHOR5};
    private String[] allAnchors = Stream.concat(Arrays.stream(firstAnchorsSet), Arrays.stream(secondAnchorsSet)).toArray(String[]::new);
    private ArrayList<Town> otherTowns = new ArrayList<>();
    private boolean townAlreadyExists;

    @FXML
    private VBox rootVBox;
    @FXML
    private Button confirm;
    @FXML
    private Button deleteTown;
    @FXML
    private TextField townName;
    @FXML
    private ListView<String> bonusListView;
    @FXML
    private ComboBox<String> bonusType;
    @FXML
    private TextField bonusQuantity;
    @FXML
    private ListView<String> routeListView;
    @FXML
    private ComboBox<String> routeToward;
    @FXML
    private HBox bonusesManagment;
    @FXML
    private Button removeSelectedBonus;
    @FXML
    private Button removeSelectedRoute;

    private final ObservableList bonusCurrentList = FXCollections.observableArrayList();
    private final ObservableList<String> bonusTypeList = FXCollections.observableArrayList();

    private final ObservableList routeCurrentList = FXCollections.observableArrayList();
    private final ObservableList<String> routeTowardList = FXCollections.observableArrayList();

    TownConfiguratorController(Configuration configuration, Field field, Integer sector, boolean randomBonuses, IConfiguratorHandler configuratorHandler) {
        this.configuration = configuration;
        this.field = field;
        this.sector = sector;
        this.randomBonuses = randomBonuses;
        this.configuratorHandler = configuratorHandler;
    }

    /**
     * Method intended to add a bonus to the {@link Town} being configured
     */
    @FXML
    public void addBonus() {
        if (!bonusQuantity.getText().isEmpty() && new StringFormatChecker().digitsOnly(bonusQuantity.getText())) {
            /** I update the bonus list in the town being configured, using a lambda and a stream to find the correct bonus to pass to DirectBonus class constructor **/
            Stream<BonusType> bonusTypeStream = Arrays.stream(BonusType.values());
            DirectBonus directBonus = new DirectBonus(bonusTypeStream
                    .filter(bonus -> bonus.getDescription().equals(bonusType.getValue()))
                    .findFirst()
                    .orElse(null), Integer.parseInt(bonusQuantity.getText()));
            if (!bonusAlreadyAdded(directBonus)) {
                town.addBonus(directBonus);
                bonusCurrentList.add(directBonus.toString());
            }
            bonusTypeStream.close();
        } else { /** If the quantity of the bonus is not specified or its format is wrong I don't add it **/
            new ErrorMessagePopup(MISSING_QUANTITY_ERROR_MESSAGE).show();
        }
    }

    /**
     * Method intended to check if the {@link DirectBonus} going to be added is already present in the list of bonuses of the town, to avoid adding copies.
     *
     * @return True if the {@link DirectBonus} going to be added is already present in the list, false otherwise
     */
    private boolean bonusAlreadyAdded(DirectBonus directBonus) {
        return town.getBonusList().stream()
                .map(DirectBonus::toString)
                .collect(Collectors.toList()).contains(directBonus.toString());
    }

    /**
     * Method intended to remove a bonus from the {@link Town} being configured
     */
    @FXML
    public void removeSelectedBonus() {
        int indexOfElementToRemove = bonusListView.getSelectionModel().getSelectedIndex();
        if (indexOfElementToRemove >= 0) {
            /** Deleting an element from ObservableList will automatically be deleted from ListView **/
            bonusCurrentList.remove(indexOfElementToRemove);

            /** I finally remove the bonus also from the town being configured **/
            town.removeBonus(indexOfElementToRemove);
        } else {
            new ErrorMessagePopup(DELETE_BONUS_ERROR_MESSAGE).show();
        }

        if (bonusCurrentList.size() == 0) removeSelectedBonus.setDisable(true);
    }

    /**
     * Method used to add a route to the {@link Town} being configured, distinguishing between connections with towns and connections with anchors
     */
    @FXML
    public void addRoute() {
        if (!routeAlreadyAdded()) {
            /** Adding a new element to ObservableList will automatically be reflected in ListView **/
            routeCurrentList.add(routeToward.getValue());

            /** finally I add the route to the town being configured, distinguishing between routes town-to-town and routes towards anchors **/
            String route = routeToward.getValue();
            if (route.startsWith(GENERIC_ANCHOR)) {
                int level = Integer.parseInt(route.subSequence(GENERIC_ANCHOR.length() + 1, route.length()).toString());

                Anchor anchorToAdd = new Anchor(level);
                anchorToAdd.setLeftField(level <= 2 ? configuration.getMap().getSeaField() : configuration.getMap().getMainlandField());
                anchorToAdd.setRightField(level <= 2 ? configuration.getMap().getMainlandField() : configuration.getMap().getMountainField());

                town.addConnectedAnchor(anchorToAdd);
            } else {
                Stream<Town> otherTownsStream = otherTowns.stream();
                Town townToConnect = otherTownsStream
                        .filter(townToCheck -> townToCheck.getName().equals(route))
                        .findFirst()
                        .orElse(null);
                otherTownsStream.close();
                town.addConnectedTown(townToConnect);

                /** I add this town to the connected one as well **/
                otherTownsStream = configuration.getMap().getMapField(field.getIndex()).getAllTowns(false).stream();
                getConnectedTownToModifyAsWell(townToConnect).addConnectedTown(town);
                otherTownsStream.close();
            }
        }
    }

    /**
     * Method used to establish if the route going to be added is already present or not
     *
     * @return True if the route is already in the list, false otherwise
     */
    private boolean routeAlreadyAdded() {
        return town.getConnectedTowns().stream()
                .map(Town::getName)
                .collect(Collectors.toList()).contains(routeToward.getValue())
                ||
                town.getConnectedAnchors().stream()
                        .map(Anchor::toString)
                        .collect(Collectors.toList()).contains(routeToward.getValue());
    }

    /**
     * Method used to remove the selected route
     */
    @FXML
    public void removeSelectedRoute() {
        /** Deleting an element from ObservableList will automatically be deleted from ListView **/
        int indexOfElementToRemove = routeListView.getSelectionModel().getSelectedIndex();
        if (indexOfElementToRemove >= 0) {

            /** I finally remove the route from the town being configured **/
            if (routeCurrentList.get(indexOfElementToRemove).toString().startsWith(GENERIC_ANCHOR)) {
                Stream<Anchor> anchorStream = town.getConnectedAnchors().stream();
                town.removeConnectedAnchor(anchorStream
                        .filter(anchor -> anchor.getLevel() == Integer.parseInt(routeCurrentList.get(indexOfElementToRemove).toString().substring(GENERIC_ANCHOR.length() + 1, routeCurrentList.get(indexOfElementToRemove).toString().length())))
                        .findFirst()
                        .orElse(null));
                anchorStream.close();
            } else {
                Stream<Town> townStream = town.getConnectedTowns().stream();
                Town townToRemove = townStream
                        .filter(townToSearch -> townToSearch.getName().equals(routeCurrentList.get(indexOfElementToRemove).toString()))
                        .findFirst()
                        .orElse(null);
                townStream.close();
                town.removeConnectedTown(townToRemove);

                /** I remove this town from the connected one as well **/
                townStream = configuration.getMap().getMapField(field.getIndex()).getAllTowns(false).stream();
                getConnectedTownToModifyAsWell(townToRemove).removeConnectedTown(town);
                townStream.close();
            }

            routeCurrentList.remove(indexOfElementToRemove);
        } else {
            new ErrorMessagePopup(DELETE_ROUTE_ERROR_MESSAGE).show();
        }

        if (routeCurrentList.size() == 0) removeSelectedRoute.setDisable(true);
    }

    /**
     * Returns the town to modify as a consequence of the addition or removal of a route
     *
     * @param town The town to look for into the configuration
     * @return The connected town passed, inside the configuration
     */
    private Town getConnectedTownToModifyAsWell(Town town) {
        Stream<Town> townStream = configuration.getMap().getMapField(field.getIndex()).getAllTowns(false).stream();
        return townStream
                .filter(t -> t.getName().equals(town.getName()))
                .findFirst()
                .orElse(null);
    }

    private void closeTownConfigurator() {
        Stage stage = (Stage) confirm.getScene().getWindow();
        stage.close();
    }

    /**
     * Method called when the confirmation button of the town configurator il pressed; it closes the configurator panel after having checked all the information inserted are correct
     */
    @FXML
    public void confirm() {
        if (townName.getText().isEmpty()) { /** Error in case of missing town name **/
            new ErrorMessagePopup(MISSING_TOWN_NAME_ERROR_MESSAGE).show();
        } else if (!isTownNameAvailable(townName.getText())) { /** Error in case the town name starts with the same letter as another one on the board **/

            new ErrorMessagePopup(TOWN_NAME_STARTING_WITH_SAME_LETTER_ERROR_MESSAGE).show();
        } else { /** Branch executed if the town name is inserted **/
            town.setName(townName.getText());

            /** I put the configured town into the current sector, overwriting the older one **/
            configuration.getMap().getMapField(field.getIndex()).replaceTownInSector(sector, town);

            closeTownConfigurator();
            configuratorHandler.onUpdateTown(town.getName(), town.getColor(), field, sector);
        }
    }

    /**
     * Method intended to delete the desired {@link Town}, but only if this town is not allowed by any {@link BuildingPermissionCard}
     */
    @FXML
    public void deleteTown() {
        if (configuration.getMap().getMapField(field.getIndex()).getBuildingPermissionCardsDeck().getCards().stream()
                .noneMatch(card -> ((BuildingPermissionCard) card).getAllowedTowns().contains(configuration.getMap().getMapField(field.getIndex()).getTownInSector(sector)))) {

            configuration.getMap().getMapField(field.getIndex()).replaceTownInSector(sector, null);

            closeTownConfigurator();
            configuratorHandler.onDeleteTown(field, sector);
        } else {
            new ErrorMessagePopup(TOWN_IN_USE_ERROR_MESSAGE, 45).show();
        }
    }

    /**
     * Raised when the bonus listView is clicked; it enables the relative button if an element has been chosen
     */
    @FXML
    public void bonusSelected() {
        if (bonusListView.getSelectionModel().getSelectedItems().size() > 0) {
            removeSelectedBonus.setDisable(false);
        }
    }

    /**
     * Raised when the route listView is clicked; it enables the relative button if an element has been chosen
     */
    @FXML
    public void routeSelected() {
        if (routeListView.getSelectionModel().getSelectedItems().size() > 0) {
            removeSelectedRoute.setDisable(false);
        }
    }

    /**
     * Used to check if the chosen name is available, intending for available that no other town name starts with the same letter.
     * In fact every town must have a name starting with a different letter
     *
     * @param name The chosen name
     * @return True if the name if usable, false otherwise
     */
    private boolean isTownNameAvailable(String name) {
        for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
            for (int j = 0; j < NUMBER_OF_SECTORS_PER_FIELD; j++) {
                if (configuration.getMap().getMapField(i).getTownInSector(j) != null &&
                        configuration.getMap().getMapField(i).getTownInSector(j).getName().substring(0, 1).equals(name.substring(0, 1)) &&
                        !configuration.getMap().getMapField(i).getTownInSector(j).getName().equals(town.getName())) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Used to determine if the king color is available. In every map there must be only one town with king color
     *
     * @return True if the king color has not been used yet, false otherwise
     */
    private boolean checkIfKingColorIsAvailable() {
        for (int i = 0; i < NUMBER_OF_FIELDS; i++) {
            for (int j = 0; j < NUMBER_OF_SECTORS_PER_FIELD; j++) {
                if (configuration.getMap().getMapField(i).getTownInSector(j) != null &&
                        configuration.getMap().getMapField(i).getTownInSector(j).getHasKing() &&
                        !town.equals(configuration.getMap().getMapField(i).getTownInSector(j))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Method called on startup; it creates a new {@link Town} if necessary, or it loads the one requested to modify
     *
     * @return A town instance
     */
    private Town getExistingTownOrCreateNew() {
        MapField currentMapField = configuration.getMap().getMapField(field.getIndex());
        ArrayList<Town> towns = (ArrayList<Town>) currentMapField.getAllTowns(true);

        if (towns.get(sector) != null) { /** if the town already exists **/
            townAlreadyExists = true;
            return towns.get(sector);
        } else {
            Town toReturn = new Town(sector);
            toReturn.setColor(TownColor.BLUE);
            townAlreadyExists = false;
            return toReturn;
        }
    }

    /**
     * Used to get a list containing the names of every other configured {@link Town} of the current {@link MapField}
     *
     * @return A list containing the names of every other configured {@link Town} of the current {@link MapField}
     */
    private ArrayList<String> getTownsNamesList() {
        ArrayList<String> townsAlreadyConfigured = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_SECTORS_PER_FIELD; i++) {
            String townNameString;
            Town thisTown = configuration.getMap().getMapField(field.getIndex()).getTownInSector(i);

            if (thisTown != null) {
                townNameString = configuration.getMap().getMapField(field.getIndex()).getTownInSector(i).getName();
                otherTowns.add(thisTown); /** I use this function also to create a list containing all the other configured towns **/
            } else townNameString = "";

            if (!townNameString.isEmpty()) {
                townsAlreadyConfigured.add(townNameString);
            }
        }

        return townsAlreadyConfigured;
    }

    /**
     * Used to disable all the checks imageViews on the color tiles
     */
    private void disableAllChecks() {
        for (int i = 0; i < NUMBER_OF_TOWN_COLORS; i++) {
            rootVBox.lookup("#" + TownColor.values()[i].getDescription().toLowerCase() + "Check").setVisible(false);
        }
    }

    /**
     * Method called on startup to setup what has to be shown the user
     */
    private void setupAll() {
        townName.setText(town.getName());
        townName.setFocusTraversable(false);

        disableAllChecks();
        rootVBox.lookup("#" + town.getColor().getDescription().toLowerCase() + "Check").setVisible(true);
        if (town.getHasKing())
            bonusesManagment.setDisable(true);

        for (int i = 0; i < town.getBonusList().size(); i++) {
            DirectBonus singleBonus = town.getBonusList().get(i);
            bonusCurrentList.add(singleBonus.toString());
        }

        for (int i = 0; i < town.getConnectedTowns().size(); i++) {
            Town singleRoute = town.getConnectedTowns().get(i);

            /** check for town existence; if it has been deleted, delete it from this list too, avoiding it to appear in the listview **/
            if (!configuration.getMap().getMapField(field.getIndex()).getAllTowns(false).contains(singleRoute)) {
                town.removeConnectedTown(singleRoute);
                singleRoute = null;
                i--;
            }

            if (singleRoute != null) {
                routeCurrentList.add(singleRoute.getName());
            }
        }

        for (Anchor singleAnchor : town.getConnectedAnchors()) {
            routeCurrentList.add(allAnchors[singleAnchor.getLevel()]);
        }
    }

    /**
     * Used to set the correct color, retrieving it from the {@link Town} instance being modified
     *
     * @param check The index of the check to check
     */
    private void setCheck(int check) {
        disableAllChecks();
        rootVBox.lookup("#" + TownColor.values()[check].getDescription().toLowerCase() + "Check").setVisible(true);
        if (bonusesManagment.isDisabled())
            bonusesManagment.setDisable(false);
        town.setHasKing(false);

        town.setColor(TownColor.values()[check]);
    }

    private void colorSelectorsSetup(){
        /** Setup of the color selector listeners **/
        for (int i = 0; i < NUMBER_OF_TOWN_COLORS; i++) {
            final int check = i;
            rootVBox.lookup("#color" + TownColor.values()[i].getDescription()).setOnMouseClicked(event -> {

                /** I check whether the king color is available or not and I set the hasKing flag as a consequence **/
                if (TownColor.values()[check].equals(TownColor.KING)) {
                    if (checkIfKingColorIsAvailable()) {
                        setCheck(check);
                        town.setHasKing(true);

                        /** The king town has no bonuses, so I erase and disable the bonus list in case some were added **/
                        bonusCurrentList.clear();
                        town.clearBonusList();
                        bonusesManagment.setDisable(true);
                    } else {
                        new ErrorMessagePopup(KING_COLOR_NOT_AVAILABLE_ERROR_MESSAGE).show();
                    }
                } else {
                    setCheck(check);
                }
            });
        }
    }

    public void initialize() {
        /** Setup of the two listview of the window **/
        bonusListView.setItems(bonusCurrentList);
        routeListView.setItems(routeCurrentList);

        bonusType.setItems(bonusTypeList);
        bonusType.setValue(BonusType.HELPERS.getDescription());

        routeToward.setItems(routeTowardList);
        routeToward.setValue(field.equals(Field.SEASIDE) || field.equals(Field.MAINLAND) ? ANCHOR0 : ANCHOR3);

        colorSelectorsSetup();

        /** I initialize the bonus observable list, avoiding the indirect bonuses, which are not allowed in the towns **/
        for (BonusType bonus : BonusType.values()) {
            if (!bonus.getDescription().equals(BonusType.TOWNBONUSES.getDescription()) && !bonus.getDescription().equals(BonusType.PERMISSIONCARDBONUSES.getDescription())) {
                bonusTypeList.add(bonus.getDescription());
            }
        }

        /** I retrieve the town (if it already exists) or I create a new one (if the cell is empty) **/
        town = getExistingTownOrCreateNew();

        /** I create the list of all the town already set up in the current field, retrieving them in the configuration **/
        ArrayList<String> townsAlreadyConfigured = getTownsNamesList();

        /** With the list just retrieved I now initialize the observable list of the connections, removing the current city name if present **/
        routeTowardList.addAll(townsAlreadyConfigured);
        if (routeTowardList.contains(town.getName()))
            routeTowardList.remove(town.getName());
        /** Finally I add to the same observable list the anchors **/
        if (field.equals(Field.SEASIDE) || field.equals(Field.MAINLAND))
            routeTowardList.addAll(firstAnchorsSet);
        if (field.equals(Field.MAINLAND) || field.equals(Field.MOUNTAINS))
            routeTowardList.addAll(secondAnchorsSet);

        /** Finally I setup all the elements in the window just created with the town information (only if it already existed before) **/
        if (townAlreadyExists) {
            setupAll();
        }

        if (randomBonuses)
            bonusesManagment.setDisable(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initialize();
    }
}


