package view.configurator;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import util.ErrorMessagePopup;

import static constants.ConfigurationConstants.EMPTY_CONFIGURATION_NAME_ERROR_MESSAGE;

public class SaveConfigurationPopupController {
    private ISaveConfiguratorHandler iSaveConfiguratorHandler;

    @FXML
    private VBox rootVBox;
    @FXML
    private TextField configurationName;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;

    SaveConfigurationPopupController(ISaveConfiguratorHandler iSaveConfiguratorHandler) {
        this.iSaveConfiguratorHandler = iSaveConfiguratorHandler;
    }

    @FXML
    public void save() {
        dismissPopup();
        if (!"".equals(configurationName.getText())) {
            iSaveConfiguratorHandler.onSave(configurationName.getText());
        } else {
            new ErrorMessagePopup(EMPTY_CONFIGURATION_NAME_ERROR_MESSAGE).show();
        }
    }

    @FXML
    public void cancel() {
        dismissPopup();
        iSaveConfiguratorHandler.onDismissal();
    }

    private void dismissPopup() {
        Stage stage = (Stage) rootVBox.getScene().getWindow();
        stage.close();
    }

    public void initialize() {
        //Used to initialize the popup; nothing to assign
    }
}
