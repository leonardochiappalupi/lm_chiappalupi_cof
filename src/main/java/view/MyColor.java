package view;

import javafx.scene.paint.Color;

import java.io.Serializable;

public class MyColor implements Serializable {

    private int red;
    private int green;
    private int blue;
    private double opacity;

    public MyColor(int r, int g, int b, double alpha) {
        this.red = r;
        this.green = g;
        this.blue = b;
        this.opacity = alpha;
    }

    public Color getJavaFXColor() {
        return Color.rgb(red, green, blue, opacity);
    }

}
