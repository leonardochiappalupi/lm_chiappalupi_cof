package util;

import constants.ConfigurationConstants;
import constants.JsonConstants;
import constants.JsonConstants.Chat;
import constants.JsonConstants.ConfigurationConst;
import constants.JsonConstants.Lobby;
import constants.JsonConstants.Log;
import game.Configuration;
import game.board.*;
import game.bonus.BonusType;
import game.bonus.DirectBonus;
import game.bonus.IBonus;
import game.bonus.IndirectBonus;
import game.cards.BuildingPermissionCard;
import game.cards.Deck;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import view.configurator.ConfiguratorController;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static constants.JsonConstants.ConfigurationConst.*;

/**
 * This class is intended as a converter between Council of Four data and Json formatted strings representing them (or
 * vice-versa). The provided methods are helpers to create proper Json objects using static one-line methods, as well as
 * getting desired information without bothering on the actual Json update structure. This implementation allow easy
 * and fast addition of new data transmitted and also allows to avoid serialization completely. It is also more network
 * efficient as it allows to send "incomplete" Json object containing only the actual updated information (instead of
 * having one method for each possible change or worse, to send entire game updates every time).
 */
public class JSONHelper {

    private JSONHelper() {
    }

    public static class ChatParser {

        private ChatParser() {
        }

        public static String authorFromJsonChatUpdate(String jsonChatUpdate) throws JSONElementNotFoundException {

            JSONObject chatUpdate = new JSONObject(jsonChatUpdate).getJSONObject(Chat.CHAT_ELEMENT_TAG);
            if (chatUpdate.has(Chat.AUTHOR_TAG))
                return chatUpdate.getString(Chat.AUTHOR_TAG);
            else throw new JSONElementNotFoundException("String: " + Chat.AUTHOR_TAG + " not found!");
        }

        public static String messageFromJsonChatUpdate(String jsonChatUpdate) throws JSONElementNotFoundException {
            JSONObject chatUpdate = new JSONObject(jsonChatUpdate).getJSONObject(Chat.CHAT_ELEMENT_TAG);
            if (chatUpdate.has(Chat.MESSAGE_TAG))
                return chatUpdate.getString(Chat.MESSAGE_TAG);
            else throw new JSONElementNotFoundException("String: " + Chat.MESSAGE_TAG + " not found!");
        }

        /**
         * Returns a Json formatted string representing a chat update from an author containing a certain message
         *
         * @param author  The author' username
         * @param message The actual chat message
         * @return Json formatted string
         */
        public static String toJsonChatUpdate(String author, String message) {
            JSONObject chatUpdate = new JSONObject();
            JSONObject chatElement = new JSONObject();
            chatElement.put(Chat.AUTHOR_TAG, author);
            chatElement.put(Chat.MESSAGE_TAG, message);
            chatUpdate.put(Chat.CHAT_ELEMENT_TAG, chatElement);
            return chatUpdate.toString();
        }
    }

    public static class LogParser {

        private LogParser() {
        }

        public static String messageFromJsonLogUpdate(String jsonLogUpdate) throws JSONElementNotFoundException {
            JSONObject logUpdate = new JSONObject(jsonLogUpdate).getJSONObject(Log.LOG_ELEMENT_TAG);
            if (logUpdate.has(Log.MESSAGE_TAG))
                return logUpdate.getString(Log.MESSAGE_TAG);
            else throw new JSONElementNotFoundException("String: " + Chat.MESSAGE_TAG + " not found!");
        }

        /**
         * Returns a Json formatted string representing a log update from server containing a certain message
         *
         * @param message The actual log message
         * @return Json formatted string
         */
        public static String toJsonLogUpdate(String message) {
            JSONObject logUpdate = new JSONObject();
            JSONObject logElement = new JSONObject();
            logElement.put(Log.MESSAGE_TAG, message);
            logUpdate.put(Log.LOG_ELEMENT_TAG, logElement);
            return logUpdate.toString();
        }
    }

    /**
     * This class is used by a Client to convert a json configuration to a Configuration java object and viceversa
     */
    public static class ConfigurationParser {

        private static JSONObject jsonConfiguration;
        private static JSONArray jsonMapFields;
        private static JSONObject jsonNobilityRouteBonuses;
        private static Configuration finalConfig = new Configuration();
        private static Map map = new Map();


        private ConfigurationParser() {
        }

        /**
         * This is the static method that converts a json string to a Configuration.
         *
         * @param jsonConfig is a String containing the json configuration.
         * @return the configuration created with parameters taken from the json string.
         */

        public static Configuration configurationFromJSonConfig(String jsonConfig) throws JSONElementNotFoundException {
            try {
                jsonConfiguration = new JSONObject(jsonConfig);
                jsonMapFields = jsonConfiguration.getJSONObject(MAP_JSON_TAG).getJSONArray(MAPFIELD_JSON_TAG);
                jsonNobilityRouteBonuses = jsonConfiguration.getJSONObject(NOBILITY_ROUTE_JSON_TAG);
                setConfigurationMap();
                finalConfig.setNobilityRoute(nobilityRouteFromJsonConfig());
                finalConfig.setRandomBonusRequested(jsonConfiguration.getBoolean(RANDOM_BONUSES));
            } catch (JSONException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
            if (finalConfig.isRandomBonusRequested())
                finalConfig = ConfiguratorController.randomizeBonuses(finalConfig, true);

            return finalConfig;
        }

        private static void setConfigurationMap() {
            for (int i = 0; i < ConfigurationConstants.NUMBER_OF_FIELDS; i++) {
                MapField mapFieldToAdd = new MapField(jsonMapFields
                        .getJSONObject(i)
                        .getJSONArray(TOWNS_JSON_TAG)
                        .length());
                mapFieldToAdd.setFieldType(Field.valueOf(jsonMapFields
                        .getJSONObject(i)
                        .getString(FIELD_TYPE_JSON_TAG)));
                map.setMapField(mapFieldToAdd, i);
            }
            setMapFieldTowns(map);


            for (int i = 0; i < ConfigurationConstants.NUMBER_OF_FIELDS; i++) {
                Deck deckToAdd = new Deck();
                int deckDimension = jsonMapFields
                        .getJSONObject(i)
                        .getJSONObject(DECK_JSON_TAG)
                        .getJSONArray(PERMISSION_CARDS_JSON_TAG)
                        .length();
                for (int j = 0; j < deckDimension; j++) {
                    JSONArray bonuses = jsonMapFields
                            .getJSONObject(i)
                            .getJSONObject(DECK_JSON_TAG)
                            .getJSONArray(PERMISSION_CARDS_JSON_TAG)
                            .getJSONObject(j)
                            .getJSONArray(BONUSES_JSON_TAG);
                    JSONArray permittedTowns = jsonMapFields
                            .getJSONObject(i)
                            .getJSONObject(DECK_JSON_TAG)
                            .getJSONArray(PERMISSION_CARDS_JSON_TAG)
                            .getJSONObject(j)
                            .getJSONArray(PERMITTED_TOWNS_JSON_TAG);

                    BuildingPermissionCard cardToAdd = setBuildingPermissionCard(bonuses, permittedTowns);
                    deckToAdd.addCard(cardToAdd);
                }
                deckToAdd.shuffle();

                if (!deckToAdd.getCards().isEmpty())
                    map.getMapField(i).setLeftShownPermissionCard((BuildingPermissionCard) deckToAdd.draw(false));
                if (!deckToAdd.getCards().isEmpty())
                    map.getMapField(i).setRightShownPermissionCard((BuildingPermissionCard) deckToAdd.draw(false));

                map.getMapField(i).setBuildingPermissionCardsDeck(deckToAdd);
            }
            finalConfig.setMap(map);
        }

        private static void setMapFieldTowns(Map map) {
            for (int i = 0; i < ConfigurationConstants.NUMBER_OF_FIELDS; i++) {
                ArrayList<Town> towns = new ArrayList<>(jsonMapFields.getJSONObject(i).getJSONArray(TOWNS_JSON_TAG).length());
                for (int k = 0; k < jsonMapFields.getJSONObject(i).getJSONArray(TOWNS_JSON_TAG).length(); k++) {
                    Town town = new Town();
                    setTownName(town, i, k);
                    setTownSector(town, i, k);
                    setTownColor(town, i, k);
                    setTownHasKing(town, i, k);
                    setTownBonuses(town, i, k);
                    if (jsonMapFields
                            .getJSONObject(i)
                            .getJSONArray(TOWNS_JSON_TAG)
                            .getJSONObject(k)
                            .has(ANCHOR_CONNECTIONS_JSON_TAG))
                        setTownAnchors(town, i, k);
                    towns.add(town);
                }
                setTownConnections(towns, i);
                map.getMapField(i).setAllTowns(towns);
            }
        }

        private static void setTownName(Town town, int mapFieldIndex, int townIndex) {
            town.setName(jsonMapFields.getJSONObject(mapFieldIndex).getJSONArray(TOWNS_JSON_TAG)
                    .getJSONObject(townIndex)
                    .getString(TOWN_NAME_JSON_TAG));
        }

        private static void setTownSector(Town town, int mapFieldIndex, int townIndex) {
            town.setSector(jsonMapFields.getJSONObject(mapFieldIndex).getJSONArray(TOWNS_JSON_TAG)
                    .getJSONObject(townIndex)
                    .getInt(TOWN_SECTOR_JSON_TAG));
        }

        private static void setTownColor(Town town, int mapFieldIndex, int townIndex) {
            town.setColor(TownColor.valueOf(jsonMapFields
                    .getJSONObject(mapFieldIndex)
                    .getJSONArray(TOWNS_JSON_TAG)
                    .getJSONObject(townIndex)
                    .getString(TOWN_COLOR_JSON_TAG)));
        }

        private static void setTownHasKing(Town town, int mapFieldIndex, int townIndex) {
            town.setHasKing(jsonMapFields.getJSONObject(mapFieldIndex).getJSONArray(TOWNS_JSON_TAG)
                    .getJSONObject(townIndex)
                    .getBoolean(HAS_KING_JSON_TAG));
        }

        private static void setTownBonuses(Town town, int mapFieldIndex, int townIndex) {
            JSONArray bonuses = jsonMapFields.getJSONObject(mapFieldIndex)
                    .getJSONArray(TOWNS_JSON_TAG)
                    .getJSONObject(townIndex)
                    .getJSONArray(BONUSES_JSON_TAG);
            int numberOfBonuses = bonuses.length();
            for (int k = 0; k < numberOfBonuses; k++) {
                DirectBonus bonusToAdd = new DirectBonus(BonusType.valueOf(bonuses
                        .getJSONObject(k)
                        .getString(BONUS_TYPE_JSON_TAG)), bonuses
                        .getJSONObject(k)
                        .getInt(QUANTITY_OF_BONUSES_JSON_TAG));
                town.addBonus(bonusToAdd);
            }
        }

        private static void setTownConnections(ArrayList<Town> towns, int mapFieldIndex) {
            for (int k = 0; k < jsonMapFields.getJSONObject(mapFieldIndex).getJSONArray(TOWNS_JSON_TAG).length(); k++) {
                int numOfConnections = jsonMapFields
                        .getJSONObject(mapFieldIndex)
                        .getJSONArray(TOWNS_JSON_TAG)
                        .getJSONObject(k)
                        .getJSONArray(TOWN_CONNECTIONS_JSON_TAG)
                        .length();

                for (int i = 0; i < numOfConnections; i++) {
                    for (int j = 0; j < jsonMapFields.getJSONObject(mapFieldIndex).getJSONArray(TOWNS_JSON_TAG).length(); j++) {
                        if (jsonMapFields.getJSONObject(mapFieldIndex)
                                .getJSONArray(TOWNS_JSON_TAG)
                                .getJSONObject(k)
                                .getJSONArray(TOWN_CONNECTIONS_JSON_TAG)
                                .getString(i).equals(towns.get(j).getName())) {

                            towns.get(k).addConnectedTown(towns.get(j));
                        }
                    }
                }
            }
        }

        private static void setTownAnchors(Town town, int mapFieldIndex, int townIndex) {
            int numOfConnections = jsonMapFields
                    .getJSONObject(mapFieldIndex)
                    .getJSONArray(TOWNS_JSON_TAG)
                    .getJSONObject(townIndex)
                    .getJSONArray(ANCHOR_CONNECTIONS_JSON_TAG)
                    .length();
            for (int i = 0; i < numOfConnections; i++) {
                if (jsonMapFields.getJSONObject(mapFieldIndex)
                        .getJSONArray(TOWNS_JSON_TAG)
                        .getJSONObject(townIndex)
                        .has(ANCHOR_CONNECTIONS_JSON_TAG)) {
                    int anchorIndex = jsonMapFields.getJSONObject(mapFieldIndex)
                            .getJSONArray(TOWNS_JSON_TAG)
                            .getJSONObject(townIndex)
                            .getJSONArray(ANCHOR_CONNECTIONS_JSON_TAG)
                            .getInt(i);
                    town.addConnectedAnchor(map
                            .getAnchorByIndex(anchorIndex));
                }
            }

        }

        private static NobilityRoute nobilityRouteFromJsonConfig() {
            NobilityRoute finalNobilityRoute = new NobilityRoute(ConfigurationConstants.NOBILITY_ROUTE_SIZE);
            for (int i = 0; i < jsonNobilityRouteBonuses.getJSONArray(NOBILITY_ROUTE_CELLS_JSON_TAG).length(); i++) {
                int index = jsonNobilityRouteBonuses
                        .getJSONArray(NOBILITY_ROUTE_CELLS_JSON_TAG)
                        .getJSONObject(i)
                        .getInt(CELL_INDEX_JSON_TAG);
                NobilityRouteCell cell = new NobilityRouteCell(index);
                for (int k = 0; k < jsonNobilityRouteBonuses.getJSONArray(NOBILITY_ROUTE_CELLS_JSON_TAG).getJSONObject(i).getJSONArray(BONUSES_JSON_TAG).length(); k++) {
                    IBonus bonusToAdd;
                    JSONObject bonus = jsonNobilityRouteBonuses
                            .getJSONArray(NOBILITY_ROUTE_CELLS_JSON_TAG)
                            .getJSONObject(i)
                            .getJSONArray(BONUSES_JSON_TAG)
                            .getJSONObject(k);
                    boolean isDirect = bonus.getBoolean(IS_DIRECT_JSON_TAG);
                    BonusType bonusType = BonusType.valueOf(bonus.getString(BONUS_TYPE_JSON_TAG));
                    if (isDirect) {
                        int quantity = bonus.getInt(QUANTITY_OF_BONUSES_JSON_TAG);
                        bonusToAdd = new DirectBonus(bonusType, quantity);
                    } else {
                        bonusToAdd = new IndirectBonus(bonusType);
                        if (bonusType == BonusType.PERMISSIONCARDBONUSES) {
                            ((IndirectBonus) bonusToAdd).setCollectsFromBuildingCard(true);
                            ((IndirectBonus) bonusToAdd).setCollectsFromTown(false);
                        } else if (bonusType == BonusType.TOWNBONUSES) {
                            ((IndirectBonus) bonusToAdd).setCollectsFromBuildingCard(false);
                            ((IndirectBonus) bonusToAdd).setCollectsFromTown(true);
                        }
                    }


                    cell.addBonus(bonusToAdd);

                }
                finalNobilityRoute.setCell(cell, index);

            }
            return finalNobilityRoute;
        }

        private static BuildingPermissionCard setBuildingPermissionCard(JSONArray bonuses, JSONArray permittedTowns) {
            BuildingPermissionCard card = new BuildingPermissionCard();

            setBuildingPermissionCardBonuses(bonuses, card);
            setBuildingPermissionCardPermittedTowns(permittedTowns, card);

            return card;
        }

        private static void setBuildingPermissionCardBonuses(JSONArray bonuses, BuildingPermissionCard card) {
            for (int k = 0; k < bonuses.length(); k++) {

                int quantity = bonuses.getJSONObject(k).getInt(QUANTITY_OF_BONUSES_JSON_TAG);
                DirectBonus bonusToAdd = new DirectBonus(BonusType.valueOf(bonuses
                        .getJSONObject(k)
                        .getString(BONUS_TYPE_JSON_TAG)), quantity);
                card.addBonus(bonusToAdd);
            }
        }

        private static void setBuildingPermissionCardPermittedTowns(JSONArray permittedTowns, BuildingPermissionCard card) {
            for (int i = 0; i < permittedTowns.length(); i++) {
                String townName = permittedTowns.getString(i);
                card.addAllowedTown(searchTownInAllFields(townName));
            }
        }

        private static Town searchTownInAllFields(String name) {
            for (int i = 0; i < ConfigurationConstants.NUMBER_OF_FIELDS; i++)
                for (int k = 0; k < map.getMapField(i).getAllTowns(false).size(); k++)
                    if (map.getMapField(i).getAllTowns(false).get(k).getName().equals(name))
                        return map.getMapField(i).getAllTowns(false).get(k);
            return null;
        }

        /**
         * Returns a Json formatted string representing an entire game configuration (using specific methods defined below for each object)
         *
         * @param configuration The configuration to parse
         * @return Json formatted string
         */
        public static String toJsonConfiguration(Configuration configuration) {
            JSONObject jsonConfiguration = new JSONObject();

            JSONObject jsonMap = toJsonMap(configuration.getMap());
            JSONObject jsonNobilityRoute = toJsonNobilityRoute(configuration.getNobilityRoute());

            jsonConfiguration.put(ConfigurationConst.MAP_JSON_TAG, jsonMap);
            jsonConfiguration.put(ConfigurationConst.NOBILITY_ROUTE_JSON_TAG, jsonNobilityRoute);
            jsonConfiguration.put(ConfigurationConst.RANDOM_BONUSES, configuration.isRandomBonusRequested() ? "true" : "false");

            return jsonConfiguration.toString();
        }

        private static JSONObject toJsonMap(Map map) {
            JSONObject jsonMap = new JSONObject();

            JSONArray jsonFields = new JSONArray();

            for (MapField field : map.getAllFields()) {
                jsonFields.put(toJsonMapField(field));
            }

            jsonMap.put(ConfigurationConst.MAPFIELD_JSON_TAG, jsonFields);

            return jsonMap;
        }

        private static JSONObject toJsonMapField(MapField mapField) {
            JSONObject jsonMapField = new JSONObject();

            JSONArray jsonTowns = new JSONArray();
            JSONObject jsonDeck = toJsonDeck(mapField.getBuildingPermissionCardsDeck());

            mapField.getAllTowns(false).stream().forEach(town -> jsonTowns.put(toJsonTown(town)));

            jsonMapField.put(ConfigurationConst.FIELD_TYPE_JSON_TAG, mapField.getFieldType().getDescription().toUpperCase());
            jsonMapField.put(ConfigurationConst.NUMBER_OF_TOWNS_PER_FIELD_TAG, String.valueOf(mapField.getAllTowns(false).size()));
            jsonMapField.put(ConfigurationConst.TOWNS_JSON_TAG, jsonTowns);
            jsonMapField.put(ConfigurationConst.DECK_JSON_TAG, jsonDeck);

            return jsonMapField;
        }

        private static JSONObject toJsonTown(Town town) {
            JSONObject jsonTown = new JSONObject();

            JSONArray jsonBonuses = new JSONArray();
            JSONArray jsonConnectionsWithTowns = new JSONArray();
            JSONArray jsonConnectionWithAnchors = new JSONArray();

            for (DirectBonus bonus : town.getBonusList()) {
                jsonBonuses.put(toJsonBonus(bonus));
            }
            for (Town connectionWithTown : town.getConnectedTowns()) {
                jsonConnectionsWithTowns.put(connectionWithTown.getName());
            }
            for (Anchor connectionWithAnchor : town.getConnectedAnchors()) {
                jsonConnectionWithAnchors.put(String.valueOf(connectionWithAnchor.getLevel()));
            }

            jsonTown.put(ConfigurationConst.TOWN_SECTOR_JSON_TAG, String.valueOf(town.getSector()));
            jsonTown.put(ConfigurationConst.TOWN_NAME_JSON_TAG, town.getName());
            jsonTown.put(ConfigurationConst.TOWN_COLOR_JSON_TAG, town.getColor().getDescription().toUpperCase());
            jsonTown.put(ConfigurationConst.HAS_KING_JSON_TAG, String.valueOf(town.getHasKing()));
            jsonTown.put(ConfigurationConst.NUMBER_OF_BONUSES_JSON_TAG, String.valueOf(town.getBonusList().size()));
            jsonTown.put(ConfigurationConst.BONUSES_JSON_TAG, jsonBonuses);
            jsonTown.put(ConfigurationConst.TOWN_CONNECTIONS_JSON_TAG, jsonConnectionsWithTowns);
            jsonTown.put(ConfigurationConst.ANCHOR_CONNECTIONS_JSON_TAG, jsonConnectionWithAnchors);

            return jsonTown;
        }

        private static JSONObject toJsonBonus(IBonus bonus) {
            JSONObject jsonBonus = new JSONObject();

            jsonBonus.put(ConfigurationConst.QUANTITY_OF_BONUSES_JSON_TAG, String.valueOf(bonus.getElementsQuantity()));
            jsonBonus.put(ConfigurationConst.BONUS_TYPE_JSON_TAG, bonus.getElementsType().getDescription().toUpperCase());
            if (bonus instanceof DirectBonus) {
                jsonBonus.put(ConfigurationConst.IS_DIRECT_JSON_TAG, "true");
            } else if (bonus instanceof IndirectBonus) {
                jsonBonus.put(ConfigurationConst.IS_DIRECT_JSON_TAG, "false");
            }

            return jsonBonus;
        }

        private static JSONObject toJsonDeck(Deck deck) {
            JSONObject jsonDeck = new JSONObject();

            JSONArray jsonPermissionCards = new JSONArray();

            for (BuildingPermissionCard permissionCard : deck.getCards().stream().map(card -> (BuildingPermissionCard) card).collect(Collectors.toList())) {
                jsonPermissionCards.put(toJsonPermissionCard(permissionCard));
            }

            jsonDeck.put(ConfigurationConst.PERMISSION_CARDS_JSON_TAG, jsonPermissionCards);

            return jsonDeck;
        }

        private static JSONObject toJsonPermissionCard(BuildingPermissionCard permissionCard) {
            JSONObject jsonPermissionCard = new JSONObject();

            JSONArray jsonPermittedTowns = new JSONArray();
            JSONArray jsonBonuses = new JSONArray();

            for (Town permittedTown : permissionCard.getAllowedTowns()) {
                jsonPermittedTowns.put(permittedTown.getName());
            }
            for (DirectBonus bonus : permissionCard.getBonusList()) {
                jsonBonuses.put(toJsonBonus(bonus));
            }

            jsonPermissionCard.put(ConfigurationConst.NUMBER_OF_BONUSES_JSON_TAG, String.valueOf(permissionCard.getBonusList().size()));
            jsonPermissionCard.put(ConfigurationConst.PERMITTED_TOWNS_JSON_TAG, jsonPermittedTowns);
            jsonPermissionCard.put(ConfigurationConst.BONUSES_JSON_TAG, jsonBonuses);

            return jsonPermissionCard;
        }

        private static JSONObject toJsonNobilityRoute(NobilityRoute nobilityRoute) {
            JSONObject jsonNobilityRoute = new JSONObject();

            JSONArray jsonCells = new JSONArray();
            for (NobilityRouteCell cell : nobilityRoute.getCells()) {
                jsonCells.put(toJsonNobilityRouteCell(cell));
            }

            jsonNobilityRoute.put(ConfigurationConst.NOBILITY_ROUTE_CELLS_JSON_TAG, jsonCells);

            return jsonNobilityRoute;
        }

        private static JSONObject toJsonNobilityRouteCell(NobilityRouteCell cell) {
            JSONObject jsonCell = new JSONObject();

            JSONArray jsonBonuses = new JSONArray();

            for (IBonus bonus : cell.getBonuses()) {
                jsonBonuses.put(toJsonBonus(bonus));
            }

            jsonCell.put(ConfigurationConst.CELL_INDEX_JSON_TAG, String.valueOf(cell.getIndex()));
            jsonCell.put(ConfigurationConst.NUMBER_OF_BONUSES_JSON_TAG, String.valueOf(cell.getBonuses().size()));
            jsonCell.put(ConfigurationConst.BONUSES_JSON_TAG, jsonBonuses);

            return jsonCell;
        }
    }

    public static class LobbyParser {

        private LobbyParser() {
        }

        public static String roomNameFromJsonLobbyUpdate(String jsonLobbyUpdate) throws JSONElementNotFoundException {
            JSONObject lobbyUpdate = new JSONObject(jsonLobbyUpdate).getJSONObject(Lobby.LOBBY_ELEMENT_TAG);
            if (lobbyUpdate.has(Lobby.ROOM_NAME))
                return lobbyUpdate.getString(Lobby.ROOM_NAME);
            else throw new JSONElementNotFoundException("String: " + Lobby.ROOM_NAME + " not found!");
        }

        public static String leaderFromJsonLobbyUpdate(String jsonLobbyUpdate) throws JSONElementNotFoundException {
            JSONObject lobbyUpdate = new JSONObject(jsonLobbyUpdate).getJSONObject(Lobby.LOBBY_ELEMENT_TAG);
            if (lobbyUpdate.has(Lobby.GAME_LEADER))
                return lobbyUpdate.getString(Lobby.GAME_LEADER);
            else throw new JSONElementNotFoundException("String: " + Lobby.GAME_LEADER + " not found!");
        }

        public static int timeoutFromJsonLobbyUpdate(String jsonLobbyUpdate) throws JSONElementNotFoundException {
            JSONObject lobbyUpdate = new JSONObject(jsonLobbyUpdate).getJSONObject(Lobby.LOBBY_ELEMENT_TAG);
            if (lobbyUpdate.has(Lobby.TIME_REMAINING))
                return lobbyUpdate.getInt(Lobby.TIME_REMAINING);
            else throw new JSONElementNotFoundException("String: " + Lobby.TIME_REMAINING + " not found!");
        }

        public static int maxUsersFromJsonLobbyUpdate(String jsonLobbyUpdate) throws JSONElementNotFoundException {
            JSONObject lobbyUpdate = new JSONObject(jsonLobbyUpdate).getJSONObject(Lobby.LOBBY_ELEMENT_TAG);
            if (lobbyUpdate.has(Lobby.MAX_PLAYERS))
                return lobbyUpdate.getInt(Lobby.MAX_PLAYERS);
            else throw new JSONElementNotFoundException("String: " + Lobby.MAX_PLAYERS + " not found!");
        }

        public static class LobbyUsersFromJsonLobbyUpdate {

            private LobbyUsersFromJsonLobbyUpdate() {
            }

            public static String[] toStringArray(String jsonLobbyUpdate) throws JSONElementNotFoundException {
                JSONObject lobbyUpdate = new JSONObject(jsonLobbyUpdate).getJSONObject(Lobby.LOBBY_ELEMENT_TAG);
                if (lobbyUpdate.has(Lobby.USERS_CONNECTED)) {
                    JSONArray jsonUsers = lobbyUpdate.getJSONArray(Lobby.USERS_CONNECTED);
                    String[] usersConnected = new String[jsonUsers.length()];
                    for (int i = 0; i < jsonUsers.length(); i++) {
                        usersConnected[i] = jsonUsers.getJSONObject(i).getString(Lobby.USER_TAG);
                    }
                    return usersConnected;
                } else throw new JSONElementNotFoundException("String: " + Lobby.USERS_CONNECTED + " not found!");
            }

            public static List<String> toArrayList(String jsonLobbyUpdate) throws JSONElementNotFoundException {
                JSONObject lobbyUpdate = new JSONObject(jsonLobbyUpdate).getJSONObject(Lobby.LOBBY_ELEMENT_TAG);
                if (lobbyUpdate.has(Lobby.USERS_CONNECTED)) {
                    JSONArray jsonUsers = lobbyUpdate.getJSONArray(Lobby.USERS_CONNECTED);
                    ArrayList<String> usersConnected = new ArrayList<>();
                    for (int i = 0; i < jsonUsers.length(); i++) {
                        usersConnected.add(i, jsonUsers.getJSONObject(i).getString(JsonConstants.Lobby.USER_TAG));
                    }
                    return usersConnected;
                } else throw new JSONElementNotFoundException("String: " + Lobby.USERS_CONNECTED + " not found!");
            }
        }

        /**
         * Returns a Json formatted string representing a lobby update
         *
         * @param gameLeader     The room leader' username (nullable)
         * @param roomName       The room name (or ID) (nullable)
         * @param timeRemaining  The time remaining (in seconds) before the room will close and the game startLobbyLogin (nullable)
         * @param usersConnected The list of the users connected, passed as array of usernames (strings)
         * @return Json formatted string
         */
        public static String toJsonLobbyUpdate(String gameLeader, String roomName, Integer maxUsers, int timeRemaining, String... usersConnected) {
            JSONObject lobbyUpdate = new JSONObject();
            JSONObject lobbyElement = new JSONObject();
            if (gameLeader != null)
                lobbyElement.put(Lobby.GAME_LEADER, gameLeader);
            if (roomName != null)
                lobbyElement.put(Lobby.ROOM_NAME, roomName);
            if (maxUsers != null)
                lobbyElement.put(Lobby.MAX_PLAYERS, maxUsers);

            lobbyElement.put(Lobby.TIME_REMAINING, timeRemaining);
            JSONArray users = new JSONArray();
            for (String user : usersConnected) {
                JSONObject jsonUser = new JSONObject();
                jsonUser.put(Lobby.USER_TAG, user);
                users.put(jsonUser);
            }
            lobbyElement.put(Lobby.USERS_CONNECTED, users);

            lobbyUpdate.put(Lobby.LOBBY_ELEMENT_TAG, lobbyElement);
            return lobbyUpdate.toString();
        }
    }
}
