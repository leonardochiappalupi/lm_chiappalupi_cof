package util;

import java.util.regex.Pattern;

public class StringFormatChecker {
    private final String regex = "\\d+";
    private Pattern pattern = Pattern.compile(regex);

    public boolean digitsOnly(String string){
        return pattern.matcher(string).matches();
    }
}
