package util;

public class JSONElementNotFoundException extends Exception {

    JSONElementNotFoundException(String message){
        super(message);
    }
}
