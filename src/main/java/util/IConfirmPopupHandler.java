package util;

public interface IConfirmPopupHandler {
    void onConfirmation();

    void onDismissal();
}
