package util;

import game.bonus.DirectBonus;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import view.game.ElementsBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used in GUI only. Creates a popup showing all the available bonus for a specific card or town.
 * It is used when there are too many bonuses to be shown on a single card or town.
 */
public class BonusesPopup {

    private List<DirectBonus> bonuses = new ArrayList<>();
    private String townOrCard;

    public BonusesPopup(List<DirectBonus> bonuses) {
        this.bonuses = bonuses;
        this.townOrCard = "card"; //default value
    }

    public BonusesPopup(List<DirectBonus> bonuses, String townOrCard) {
        this.bonuses = bonuses;
        this.townOrCard = townOrCard;
    }

    public void show() {
        Stage primaryStage = new Stage();
        TownBonusesContainerController containerController = new TownBonusesContainerController(bonuses, primaryStage);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/town_bonuses_container.fxml"));
        loader.setController(containerController);
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle("Bonuses of this " + townOrCard);
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }
}


class TownBonusesContainerController implements Initializable {
    @FXML
    private TilePane tilePane;
    @FXML
    private Button closeButton;
    private Stage stage;
    private List<DirectBonus> bonuses;

    TownBonusesContainerController(List<DirectBonus> bonuses, Stage stage) {
        this.stage = stage;
        this.bonuses = bonuses;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (DirectBonus bonus : bonuses)
            tilePane
                    .getChildren()
                    .add(ElementsBuilder.buildBonus(bonus, true));

        closeButton.setOnAction(event -> stage.close());
    }
}