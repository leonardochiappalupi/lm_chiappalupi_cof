package util;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

class ConfirmPopupController implements Initializable {
    private String confirmMessage;
    private String positiveButton;
    private String negativeButton;
    private IConfirmPopupHandler confirmPopupHandler;

    @FXML
    private
    VBox rootVBox;
    @FXML
    private Label confirmMessageLabel;
    @FXML
    private Button confirmButton;
    @FXML
    private Button cancelButton;

    ConfirmPopupController(IConfirmPopupHandler confirmPopupHandler, String confirmMessage, String positiveButton, String negativeButton) {
        this.confirmMessage = confirmMessage;
        this.positiveButton = positiveButton;
        this.negativeButton = negativeButton;
        this.confirmPopupHandler = confirmPopupHandler;
    }

    @FXML
    public void confirm() {
        dismissPopup();
        confirmPopupHandler.onConfirmation();
    }

    @FXML
    public void cancel() {
        dismissPopup();
        confirmPopupHandler.onDismissal();
    }

    private void dismissPopup() {
        Stage stage = (Stage) rootVBox.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        confirmMessageLabel.setText(confirmMessage);
        if(positiveButton!=null && negativeButton!=null){
            confirmButton.setText(positiveButton);
            cancelButton.setText(negativeButton);
        }
    }
}

public class ConfirmPopup{
    private String confirmMessage;
    private IConfirmPopupHandler confirmPopupHandler;
    private String positiveButton;
    private String negativeButton;

    public ConfirmPopup(IConfirmPopupHandler confirmPopupHandler, String confirmMessage) {
        this.confirmMessage = confirmMessage;
        this.confirmPopupHandler = confirmPopupHandler;
        this.positiveButton = null;
        this.negativeButton = null;
    }

    public ConfirmPopup(IConfirmPopupHandler confirmPopupHandler, String confirmMessage, String positiveButton, String negativeButton) {
        this.confirmMessage = confirmMessage;
        this.confirmPopupHandler = confirmPopupHandler;
        this.positiveButton = positiveButton;
        this.negativeButton = negativeButton;
    }

    public void show() {
        ConfirmPopupController confirmPopupController = new ConfirmPopupController(confirmPopupHandler, confirmMessage, positiveButton, negativeButton);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/confirm_popup.fxml"));
        loader.setController(confirmPopupController);
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
        }
        Stage primaryStage = new Stage();
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle("Confirm");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }
}

