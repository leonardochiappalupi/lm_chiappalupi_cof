package util;

import client.Client;
import client.presentation.gui.GUIBridge;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

class ErrorMessagePopupController implements Initializable {
    private int averageNumberOfCharacterPerLine;
    private String errorMessage;

    @FXML
    private VBox rootVBox;
    @FXML
    private Label errorMessageLabel;

    public ErrorMessagePopupController(String errorMessage, int averageNumberOfCharacterPerLine) {
        this.errorMessage = errorMessage;
        this.averageNumberOfCharacterPerLine = averageNumberOfCharacterPerLine;
    }

    @FXML
    public void dismissPopup() {
        Stage stage = (Stage) rootVBox.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorMessage = wrapTextIfNeeded(errorMessage);
        errorMessageLabel.setText(errorMessage);
    }

    private String wrapTextIfNeeded(String text) {
        List<String> words = Arrays.asList(text.split(" "));
        String wrappedText = text;
        int partialCharacterCount = -1;
        int lines = 1;
        int offset = 0;

        for (String word : words) {
            partialCharacterCount += word.length();
            partialCharacterCount++;
            if (partialCharacterCount >= averageNumberOfCharacterPerLine * lines + offset) {
                partialCharacterCount--;
                if (partialCharacterCount < averageNumberOfCharacterPerLine * lines + offset) {
                    partialCharacterCount++;
                } else {
                    try {
                        wrappedText = wrappedText.substring(0, partialCharacterCount + 1) + "\n" + wrappedText.substring(partialCharacterCount + 2);
                    } catch (StringIndexOutOfBoundsException e) {
                        Logger.getGlobal().log(Level.FINE, e.getMessage(), e);
                        wrappedText = wrappedText.substring(0, wrappedText.length());
                    }

                    offset = partialCharacterCount - (averageNumberOfCharacterPerLine * lines);
                    lines++;
                    partialCharacterCount++;
                }
            }
        }

        return wrappedText;
    }
}

public class ErrorMessagePopup {
    private String errorMessage;
    private int averageNumberOfCharacterPerLine;

    /**
     * This builder allows you to set the error message to be shown, that will be wrapped automatically in lines with a default amount of characters per line.<br>
     * This value can be changed using the alternative builder of the {@link ErrorMessagePopup} class.
     *
     * @param errorMessage The error message to show
     */
    public ErrorMessagePopup(String errorMessage) {
        this.errorMessage = errorMessage;
        this.averageNumberOfCharacterPerLine = 40; //default value
    }

    /**
     * This builder allows you to set the error message and to specify the average number of characters every line of the final popup should contain.<br>
     * This way the error message is automatically wrapped in lines with the closest possible length to the specified number.
     *
     * @param errorMessage                    The error message to show
     * @param averageNumberOfCharacterPerLine The average number of characters each line will contain
     */
    public ErrorMessagePopup(String errorMessage, int averageNumberOfCharacterPerLine) {
        this.errorMessage = errorMessage;
        this.averageNumberOfCharacterPerLine = averageNumberOfCharacterPerLine;
    }

    public void show() {
        ErrorMessagePopupController errorMessagePopupController = new ErrorMessagePopupController(errorMessage, averageNumberOfCharacterPerLine);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layouts/error_message_popup.fxml"));
        loader.setController(errorMessagePopupController);
        Platform.runLater(() -> {
            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e) {
                Logger.getGlobal().log(Level.WARNING, e.getMessage(), e);
            }
            Stage primaryStage = new Stage();
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.setTitle("Error");
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(false);
            primaryStage.sizeToScene();
            primaryStage.show();
        });


        ((GUIBridge) Client.getPresentationBridge()).setWorkingState(false);


    }
}



