Software Engineering 2016 Final Examination Project
Council of Four

Group: lm_42
Students: Leonardo Chiappalupi, Ivan Bugli, Giuseppe Cannizzaro

Implemented features: All tech
Advanced features: Lobby, Chat, Editable map support + Graphical configurator.

To start the game: 

Method #1:
1) Download "jars.zip" in the "Download" section of BitBucket
2) Extract the archive in a folder
3) Open a new console window, move to the extracted folder, and run the server by entering "java -jar lm_42-server.jar"
4) For every new client, open a new console window, move to the extracted folder, and run the client by entering "java -jar lm_42-client.jar"


Method #2:
1) Run server.Server via Maven "mvn exec:java -Dexec.mainClass="server.Server"
2) Run client.Client via Maven "mvn exec:java -Dexec.mainClass="client.Client"
(Maven run is required, otherwise the dependencies we specified in the pom.xml file are not correctly loaded and the
game does not run)


Note: configurations are stored in .json files, and the game expects to find them in "./configurations",
	where "." is the location of the current console. As required, all 8 base configurations are already stored in there
	and ready to be loaded with no further file copying. New or updated configurations are also saved in
	that path.